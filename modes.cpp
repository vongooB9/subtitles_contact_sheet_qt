#include "modes.h"
#include "common.h"
#include <QRandomGenerator>

modes::modes(QString mode, bool subtitles, ParamsSettings *ps, TextSustitution *ts): Mode(mode_time) {
	PS = ps;
	TS = ts;
	SetMode(mode);
	if (!subtitles && (Mode == mode_time || Mode == mode_line))
		Mode = mode_video_time;

	QString DURATION = TS->Value("format.duration");
	Duration = DURATION.toFloat() * 1000;

	StartTime = StringTime2ms(PS->Value("start"));
	if (StartTime == -1) {
		StartTime = StringTime2ms("30");
		PrintInfo("Incorrect start time, using default, 30");
	}
	if (PS->Value("end").trimmed().startsWith("-")) {
		EndTime = StringTime2ms(PS->Value("end").trimmed().remove(0, 1));
		EndTime = -EndTime;
	} else {
		EndTime = StringTime2ms(PS->Value("end"));
	}

	StartLine = PS->Int("start_line");
	EndLine = PS->Int("end_line");

	FixedInterval = 0;
	if (!subtitles) {
		if (PS->NotEmpty("fixed_interval"))
			FixedInterval = StringTime2ms(PS->Value("fixed_interval"));
		if (FixedInterval < 0) FixedInterval = 0;
		if (FixedInterval > 0) Mode = mode_fixed;
	}

	if (Mode == mode_line) {
		DEBUGVAR(StartLine);
		DEBUGVAR(EndLine);
	} else {
		int64_t NewDuration = Duration;
		if (StartTime > 0) NewDuration = NewDuration - StartTime;
		if (EndTime > 0) NewDuration = EndTime - StartTime;
		if (EndTime < 0) NewDuration = NewDuration + EndTime;
		if (NewDuration < 0) {
			//TODO: Try change to info
			PrintInfo("--start or --end too high for this file, setting them to 0.");
			StartTime = 0;
			EndTime = 0;
			NewDuration = Duration;
		}
		Duration = NewDuration;
		DEBUGVAR(StartTime);
		DEBUGVAR(EndTime);
	}

	if (Mode == mode_fixed) {
		NumberOfFrames = (Duration / FixedInterval);
		if (Duration % FixedInterval != 0) NumberOfFrames++;
		LayoutY = NumberOfFrames / LayoutX;
		if (NumberOfFrames % LayoutX != 0) LayoutY++;
		LAYOUT = QString::number(LayoutX) + "x" + QString::number(LayoutY);
	}
}

void modes::SetMode(QString mode) {
	mode = mode.toLower();
	if (mode == "time") Mode = mode_time;
	if (mode == "line") Mode = mode_line;
	if (mode == "video_time" ) Mode = mode_video_time;
	if (mode == "video_end" ) Mode = mode_video_end;
	if (mode == "random" ) Mode = mode_random;
	if (mode == "fixed" ) Mode = mode_fixed;
}

bool modes::IsVideoEnd() {
	return Mode == mode_video_end;
}

void modes::SetTimestamps(QList<sub_line_t> *timestamps) {
	Timestamps = timestamps;
}

QList<sub_line_t> modes::GenerateScreenshots() {
	switch (Mode) {
		case mode_time:
			return GenerateTime();
		case mode_video_time:
		case mode_random:
			return GenerateVideoTime();
		case mode_video_end:
			return GenerateVideoEnd();
		case mode_line:
			return GenerateLine();
		case mode_fixed:
			return GenerateFixed();
	}
	return Screenshots;
}

bool modes::RepFramesEnabled() {
	if (PS->Int("rep_frames") > 0 && (Mode == mode_video_time || Mode == mode_video_end)) return true;
	return false;
}

QList<sub_line_t> modes::GenerateTime() {
	PrintInfo("Selecting " + QString::number(NumberOfFrames) + " frames between " +
			  QTime::fromMSecsSinceStartOfDay(StartTime).toString("hh:mm:ss") + " and " +
			  QTime::fromMSecsSinceStartOfDay(StartTime + Duration).toString("hh:mm:ss") +
			  ", Time mode:");

	uint64_t MsBetweenFrames = Duration / NumberOfFrames;
	DEBUGVAR(MsBetweenFrames);
	DEBUGVAR(PS->Int("min"));
	int CurrentTimestamp = 0;

	for (uint i = 0; i < NumberOfFrames; i++) {
		uint64_t SubtitleStartTime = StartTime + (i * MsBetweenFrames);
		uint64_t SubtitleNextTime = StartTime + ((i + 1) * MsBetweenFrames);
		if (SubtitleNextTime > (uint64_t)StartTime + (uint64_t)Duration) SubtitleNextTime = StartTime + Duration;

		const sub_line_t *line_not_size_selected = nullptr;

		bool outok = false;

		for (int timestamp = CurrentTimestamp; timestamp < Timestamps->count(); timestamp++) {
			CurrentTimestamp = timestamp;
			const sub_line_t *line = &Timestamps->at(timestamp);

			if (line->start >= SubtitleNextTime) {
				if (line_not_size_selected != nullptr) {
					PrintInfo("\tSelected frame at " +
							  QTime::fromMSecsSinceStartOfDay(line_not_size_selected->start).toString("hh:mm:ss.zzz") +
							  " Less text size than especified");
					Screenshots.append(*line_not_size_selected);
					outok = true;
					break;
				}
				PrintInfo("\tSelected frame at " +
						  QTime::fromMSecsSinceStartOfDay(SubtitleStartTime).toString("hh:mm:ss.zzz") +
						  " No subtitle found");
				Screenshots.append({SubtitleStartTime, 0, 0, 0, false, false});
				outok = true;
				break;
			}

			if (line->start > SubtitleStartTime) {
				if (line->text_len < PS->Int("min")) {
					if (line_not_size_selected == nullptr) {
						line_not_size_selected = line;
					} else {
						if (line->text_len > line_not_size_selected->text_len)
							line_not_size_selected = line;
					}
					continue;
				}
				PrintInfo("\tSelected frame at " +
						  QTime::fromMSecsSinceStartOfDay(line->start).toString("hh:mm:ss.zzz") +
						  " OK");
				Screenshots.append(*line);
				outok = true;
				break;
			}
		}
		if (!outok) {
			PrintInfo("\tSelected frame at " +
					  QTime::fromMSecsSinceStartOfDay(SubtitleStartTime).toString("hh:mm:ss.zzz") +
					  " No subtitle found");
			Screenshots.append({SubtitleStartTime, 0, 0, 0, false, false});
		}
	}
	return Screenshots;
}

QList<sub_line_t> modes::GenerateLine() {
	PrintInfo("Selecting " + QString::number(NumberOfFrames) + " frames from line " +
			  QString::number(StartLine) + "to " + QString::number(EndLine) + ", Line mode:");

	//Removes the lines that do not meet the minimum
	QList<sub_line_t> LineFiltered;
	int MinLen = PS->Int("min");
	for (int i = 0; i < Timestamps->count(); ++i) {
		if (Timestamps->at(i).text_len < MinLen) continue;
		LineFiltered.append(Timestamps->at(i));
	}

	if (EndLine == 0) EndLine = LineFiltered.count();
	if (EndLine < 0) EndLine = LineFiltered.count() - EndLine;

	uint EveryLines = (EndLine - StartLine) / NumberOfFrames;

	DEBUGVAR(StartLine);
	DEBUGVAR(EndLine);
	DEBUGVAR(EveryLines);

	uint Count = 0;
	for (int i = StartLine; i < EndLine; i += EveryLines) {
		Count++;
		if (Count > NumberOfFrames) break;
		PrintInfo("\tSelected frame at " +
				  QTime::fromMSecsSinceStartOfDay(LineFiltered.at(i).start).toString("hh:mm:ss.zzz") +
				  " OK");
		Screenshots.append(LineFiltered.at(i));
	}
	return Screenshots;
}

QList<sub_line_t> modes::GenerateVideoTime() {
	PrintInfo("Selecting " + QString::number(NumberOfFrames) + " frames between " +
			  QTime::fromMSecsSinceStartOfDay(StartTime).toString("hh:mm:ss") + " and " +
			  QTime::fromMSecsSinceStartOfDay(StartTime + Duration).toString("hh:mm:ss") +
			  ", Video Time mode:");

	int MsBetweenFrames = Duration / NumberOfFrames;
	int Cero = 0;
	for (uint i = 0; i < NumberOfFrames; i++) {
		int SnapStartTime = StartTime + (i * MsBetweenFrames);
		if (Mode == mode_random)
			SnapStartTime += QRandomGenerator::global()->bounded(Cero, MsBetweenFrames);

		PrintInfo("\tSelected frame at " +
				  QTime::fromMSecsSinceStartOfDay(SnapStartTime).toString("hh:mm:ss.zzz") +
				  " OK");
		Screenshots.append({(uint64_t)SnapStartTime, 0, 0, 0, false, false});
	}
	return Screenshots;
}

QList<sub_line_t> modes::GenerateVideoEnd() {
	PrintInfo("Selecting " + QString::number(NumberOfFrames) + " frames between " +
			  QTime::fromMSecsSinceStartOfDay(StartTime).toString("hh:mm:ss") + " and " +
			  QTime::fromMSecsSinceStartOfDay(StartTime + Duration).toString("hh:mm:ss") +
			  ", Video End mode:");

	uint64_t MsBetweenFrames = Duration / (NumberOfFrames - 1);
	for (uint i = 0; i < NumberOfFrames - 1; i++) {
		uint64_t SnapStartTime = StartTime + (i * MsBetweenFrames);
		PrintInfo("\tSelected frame at " +
				  QTime::fromMSecsSinceStartOfDay(SnapStartTime).toString("hh:mm:ss.zzz") +
				  " OK");
		Screenshots.append({SnapStartTime, 0, 0, 0, false, false});
	}
	PrintInfo("\tSelected frame at " +
			  QTime::fromMSecsSinceStartOfDay(StartTime + Duration).toString("hh:mm:ss.zzz") +
			  " OK");
	Screenshots.append({((uint64_t)StartTime + (uint64_t)Duration), 0, 0, 0, false, false});
	return Screenshots;
}

QList<sub_line_t> modes::GenerateFixed() {
	PrintInfo("Selecting " + QString::number(NumberOfFrames) + " frames between " +
			  QTime::fromMSecsSinceStartOfDay(StartTime).toString("hh:mm:ss") + " and " +
			  QTime::fromMSecsSinceStartOfDay(StartTime + Duration).toString("hh:mm:ss") +
			  ", at fixed inverval " + QString::number(FixedInterval) + "ms:");
	for (uint i = 0; i < NumberOfFrames; i++) {
		uint64_t SnapStartTime = StartTime + (i * FixedInterval);
		PrintInfo("\tSelected frame at " +
				  QTime::fromMSecsSinceStartOfDay(SnapStartTime).toString("hh:mm:ss.zzz") +
				  " OK");
		Screenshots.append({SnapStartTime, 0, 0, 0, false, false});
	}
	return Screenshots;
}
