SCS_VERSION = $$system(cat \"$${PWD}/../subtitles_contact_sheet.pro\" | grep \"^SCS_VERSION\" | cut -d \"=\" -f2 | tr -d \"[:space:]\")

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

win32 {
    lessThan(QT_MAJOR_VERSION, 6): QT += winextras
}

unix:!android:!macx{
    QT += dbus
}

CONFIG += c++17

!defined(DEFAULT_FONT, var) { DEFAULT_FONT = "DejaVu Sans" }
!defined(DEFAULT_FONT_MONO, var) { DEFAULT_FONT_MONO = "DejaVu Sans Mono" }

defined(PORTABLE, var) {
    win32 {
        PORTABLE_CONFIG = 1
        PORTABLE_SEARCH_DIR = 1
    }
    PORTABLE_FONT_FILES = 1
    !defined(DEFAULT_FONT_FILE, var) { DEFAULT_FONT_FILE = /Fonts/DejaVuSansMono.ttf }
}

DEFINES += SCS_VERSION=\"\\\"$$SCS_VERSION\\\"\"
defined(PORTABLE_CONFIG, var) { DEFINES += PORTABLE_CONFIG=1 }
defined(PORTABLE_FONT_FILES, var) { DEFINES += PORTABLE_FONT_FILES=1 }
defined(PORTABLE_SEARCH_DIR, var) { DEFINES += PORTABLE_SEARCH_DIR=1 }
defined(DEFAULT_FONT, var) { DEFINES += DEFAULT_FONT=\"\\\"$$DEFAULT_FONT\\\"\" }
defined(DEFAULT_FONT_MONO, var) { DEFINES += DEFAULT_FONT_MONO=\"\\\"$$DEFAULT_FONT_MONO\\\"\" }
defined(DEFAULT_FONT_FILE, var) { DEFINES += DEFAULT_FONT_FILE=\"\\\"$$DEFAULT_FONT_FILE\\\"\" }
defined(DEFAULT_FFMPEG, var) { DEFINES += DEFAULT_FFMPEG=\"\\\"$$DEFAULT_FFMPEG\\\"\" }
defined(DEFAULT_FFPROBE, var) { DEFINES += DEFAULT_FFPROBE=\"\\\"$$DEFAULT_FFPROBE\\\"\" }
defined(DEFAULT_GIFSICLE, var) { DEFINES += DEFAULT_GIFSICLE=\"\\\"$$DEFAULT_GIFSICLE\\\"\" }

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

win32 {
    RC_ICONS = SCS.ico
}

macx {
    ICON = SCS.icns
}

SOURCES += \
	batchexternalcommandwidget.cpp \
	common.cpp \
	configffmpegdialog.cpp \
	main.cpp \
	../colors.cpp \
	../paramssettings.cpp \
	../textsustitution.cpp \
	../iso639.cpp \
	batchcommandswidget.cpp \
	batchformwidget.cpp \
	batchowncommandwidget.cpp \
	newtabdialog.cpp \
	optionssearchdialog.cpp \
	paramboolwidget.cpp \
	paramconfigwidget.cpp \
	paramsdialog.cpp \
	paramswidget.cpp \
	paramwidget.cpp \
	configurationdialog.cpp \
	configurationwidget.cpp \
	optioneditor.cpp \
	optionssearch.cpp \
	optionssearchitem.cpp \
	scsmainwindow.cpp \
	taskbarprogress.cpp \
	useroptioneditor.cpp

HEADERS += \
	batchexternalcommandwidget.h \
	common.h \
	../colors.h \
	../paramssettings.h \
	../textsustitution.h \
	../iso639.h \
	batchcommandswidget.h \
	batchformwidget.h \
	batchowncommandwidget.h \
	configffmpegdialog.h \
	newtabdialog.h \
	optionssearchdialog.h \
	paramboolwidget.h \
	paramconfigwidget.h \
	paramsdialog.h \
	paramswidget.h \
	paramwidget.h \
	configurationdialog.h \
	configurationwidget.h \
	optioneditor.h \
	optionssearch.h \
	optionssearchitem.h \
	scsmainwindow.h \
	taskbarprogress.h \
	useroptioneditor.h

RESOURCES += \
	icon.qrc

unix:!android:!macx{
	!defined(USR_DIR, var) { USR_DIR = /usr/local }
	!defined(ICONS_DIR) { ICONS_DIR = /share/icons }
	!defined(DESKTOP_DIR) { DESKTOP_DIR = /share/applications }
	BIN_DIR = $$USR_DIR/bin

	target.path = $$BIN_DIR
	INSTALLS += target

	DESKTOP_FILE = $${TARGET}.desktop
	DESKTOP_CONTENT += "[Desktop Entry]"
	DESKTOP_CONTENT += "Name=Subtitles Contact Sheet GUI"
	DESKTOP_CONTENT += "Comment=Generates video and subtitles previews, small animations, screenshots, thumbnails, etc."
	DESKTOP_CONTENT += "Type=Application"
	DESKTOP_CONTENT += "Exec=$${TARGET}"
	DESKTOP_CONTENT += "Terminal=false"
	DESKTOP_CONTENT += "Categories=AudioVideo"
	DESKTOP_CONTENT += "Icon=SCS"
	write_file($$DESKTOP_FILE, DESKTOP_CONTENT)
	desktopinstall.CONFIG = no_check_exist
	desktopinstall.path = $${USR_DIR}$${DESKTOP_DIR}
	desktopinstall.files = $$DESKTOP_FILE
	INSTALLS += desktopinstall

	iconinstall.CONFIG = no_check_exist
	iconinstall.path = $${USR_DIR}$${ICONS_DIR}
	iconinstall.files += SCS.svg
	INSTALLS += iconinstall
}
