#ifndef CONFIGURATIONDIALOG_H
#define CONFIGURATIONDIALOG_H

#include <QWidget>
#include <QDialog>
#include "../paramssettings.h"
#include "configurationwidget.h"
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>

class ConfigurationDialog : public QDialog {
		Q_OBJECT
	public:
		ConfigurationDialog(QString name, QWidget *parent = nullptr, const Qt::WindowFlags &f = Qt::WindowFlags());

		void SetName(QString name);
	protected:
		QString Name;
		ParamsSettings *PS;
		ConfigurationWidget *CW;

		QVBoxLayout *MainLayout;
		QWidget *ButtonsWidget;
		QHBoxLayout *ButtonsLayout;
		QPushButton *HideDefault;
		QPushButton *CancelButton;
		QPushButton *SaveButton;

		struct CustomElemnt {
			ParamsSettings::context_type Context;
			QString Name;
			QString Value;
		};

		QStringList RemoveList;
		QMap<QString, CustomElemnt> AddList;

	public slots:
		void AddRemoveCustom(QString Param);
		void UpdateCustom(QString Param, ParamsSettings::context_type Context, QString Name, QString Value);

		void Save();
		void Cancel();

};

#endif // CONFIGURATIONDIALOG_H
