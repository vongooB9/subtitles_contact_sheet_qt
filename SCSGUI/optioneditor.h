#ifndef OPTIONEDITOR_H
#define OPTIONEDITOR_H

#include <QWidget>
#include "../paramssettings.h"
#include <QHBoxLayout>
#include <QLineEdit>
#include <QSpinBox>
#include <QCheckBox>
#include <QStringList>
#include <QComboBox>
#include <QPushButton>
#include <QSpacerItem>
#include <QFontComboBox>
#include <QFrame>
#include <QLabel>

class OptionEditor : public QWidget {
		Q_OBJECT
	public:
		explicit OptionEditor(QString ParamName, ParamsSettings::params_t param, ParamsSettings::help_t help,
							  QWidget *parent = nullptr);
		QString ParamName;
		ParamsSettings::params_t Param;
		ParamsSettings::help_t Help;
		QHBoxLayout *Layout;
		QLineEdit *LineEditor;
		QPushButton *SelectButton;
		QSpinBox *SpinEditor;
		QCheckBox *CheckEditor;
		QFontComboBox *FontCombo;
		QFrame *ColorPreview;
		QLineEdit *RawEditor;
		QComboBox *Combo;
		QSpacerItem *Spacer;
		QPushButton *RawTougle;

		QWidget *NormalEditor;

		QWidget *EditLayout;
		QHBoxLayout *LayoutLayout;
		QSpinBox *LayoutC;
		QSpinBox *LayoutR;
		QLabel *LayoutLabel;

		QString GetText();

	private:
		void InsertRawEditors(QWidget *NormalEditor, bool spacer = true);
		QString SelectColor(QString Text, QString CurrentColor);

	signals:
		void Changed(QString Param, QString Value);

	public slots:
		void RawTougleChange(bool Checked);
		void SelectFile();
		void SelectDir();
		void SelectColorClick();
		void ChangeColor();
		void ValueChanged();
		void RawChange();


};

#endif // OPTIONEDITOR_H
