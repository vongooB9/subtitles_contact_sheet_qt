#ifndef BATCHCOMMANDSWIDGET_H
#define BATCHCOMMANDSWIDGET_H

#include <QWidget>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QPushButton>

class BatchCommandsWidget : public QWidget {
		Q_OBJECT
	public:
		explicit BatchCommandsWidget(QStringList *Configs, QWidget *parent = nullptr);

		QVBoxLayout *MainLayout;
		QGroupBox *CommandsGroup;
		QVBoxLayout *CommandsLayout;
		QWidget *ButtonsWidget;
		QHBoxLayout *ButtonsLayout;
		QPushButton *AddButton;
		QPushButton *AddExternalButton;

		void SetCommand(QStringList Commands, bool External = false);
		QString getCommands();
		void AddCommand(QStringList *Commands = nullptr, bool External = false);
		void AddOwnCommand(QStringList *Commands = nullptr);
		void AddExternalCommand(QStringList *Commands = nullptr);

	protected:
		QStringList *AvailableConfigs;
		int Count;
		QList<QWidget *> Items;

	signals:
		void Changed();

	public slots:
		void AddCommandClick();
		void AddExternalClick();
		void DeletedItem(QWidget *Item);
		void Clear();
		void ItemChanged();
		void SetExternalState(bool Enable);

};

#endif // BATCHCOMMANDSWIDGET_H
