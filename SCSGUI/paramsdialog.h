#ifndef PARAMSDIALOG_H
#define PARAMSDIALOG_H

#include <QDialog>
#include "../paramssettings.h"
#include <QStringList>
#include "paramswidget.h"

class ParamsDialog : public QDialog {
	public:
		ParamsDialog(QString &program, QStringList *configlist, ParamsSettings *ps, QWidget *parent = nullptr,
					 const Qt::WindowFlags &f = Qt::WindowFlags());

		void SetParams(QString params);
		QString GetParams();

	private:
		QStringList *AvailableConfigs;
		ParamsSettings *PS;
		ParamsWidget *Params;
		QVBoxLayout *MainLayout;
		QWidget *ButtonsWidget;
		QHBoxLayout *ButtonsLayout;
		QPushButton *CancelButton;
		QPushButton *SaveButton;
	public slots:
		void Save();
		void Cancel();
};

#endif // PARAMSDIALOG_H
