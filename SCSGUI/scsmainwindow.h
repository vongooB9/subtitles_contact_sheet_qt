#ifndef SCSMAINWINDOW_H
#define SCSMAINWINDOW_H

#include <QMainWindow>
#include <QTabWidget>
#include <QPlainTextEdit>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QProgressBar>
#include <QPushButton>
#include "../paramssettings.h"
#include "paramswidget.h"
#include "batchformwidget.h"
#include "QSettings"
#include <QProcess>
#include <QClipboard>
#include "taskbarprogress.h"

class SCSMainWindow : public QMainWindow {
		Q_OBJECT
	public:
		explicit SCSMainWindow(QString OpenFile, QWidget *parent = nullptr);
		void ReadConfigList();
		QString GetCurrentTabCommand();
		void LaunchConfigFFmpeg();
	private:
		QString Program;
		QStringList AvailableConfigs;
		bool ProgramFailed;
		ParamsSettings *PS;

		QWidget *CentralWidget;
		QVBoxLayout *CentralLayout;

		QPlainTextEdit *Log;
		QWidget *ProgressWidget;
		QHBoxLayout *ProgressLayout;
		QProgressBar *Progress;
		QPushButton *ShowLog;
		QPushButton *SaveSettings;
		QPushButton *Cancel;
		QPushButton *Start;
		QPushButton *NewTab;
		QPushButton *CopyCMD;

		QTabWidget *Tabs;
		QHash<QString, int> Tabids;
		QHash<QString, QWidget *> TabWidgets;

		ParamsWidget *Normal;
		ParamsWidget *Subtitles;
		ParamsWidget *Images;
		ParamsWidget *VR;
		ParamsWidget *Gif;
		ParamsWidget *Screenshots;
		ParamsWidget *DVD;
		BatchFormWidget *BatchWidget;

		QSettings *Settings;
		QProcess Process;
		QClipboard *Clipboard;
		TaskbarProgress *Task;
	protected:
		void closeEvent(QCloseEvent *event) override;
	signals:

	public slots:
		void StartClick();
		void CancelClick();

		void ProcessTerminate(int exitCode, QProcess::ExitStatus exitStatus);
		void ProcessReadData();
		void ProcessErrorData();

		void NewTabClicked();
		void TabChanged(int index);
		void TabClose(int index);

		void Save();
		void CopyClick();
};

#endif // SCSMAINWINDOW_H
