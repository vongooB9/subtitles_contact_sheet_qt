#include "configurationwidget.h"
#include <QLabel>
#include <QScrollArea>
#include <QLineEdit>
#include <QSpinBox>
#include <QCheckBox>
#include <QDoubleSpinBox>
#include "optioneditor.h"
#include "useroptioneditor.h"
#include <QApplication>
#include "../textsustitution.h"
#include <QInputDialog>

ConfigurationWidget::ConfigurationWidget(ParamsSettings *ps, QWidget *parent)
	: QTabWidget{parent} {
	PS = ps;
	Load();
}

ConfigurationWidget::ConfigurationWidget(QString ConfigName, QWidget *parent)
	: QTabWidget{parent} {
	PS = new ParamsSettings(QApplication::applicationDirPath(), this);
	PS->SetConfig(ConfigName);
	PS->ReadFromConfigFile();
	Load();
}

void ConfigurationWidget::Load() {
	Options = PS->SearchHelp("", true);

	for (int i = 0; i < Options.count(); ++i) {
		ParamsSettings::help_t option = Options.at(i);
		QWidget *Widget = nullptr;
		QFormLayout *Layout = nullptr;
		if (!Tabs.contains(option.Prefix)) {
			QScrollArea *Scroll = new QScrollArea(this);
			Scroll->setWidgetResizable(true);
			Scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
			Widget = new QWidget(Scroll);
			Layout = new QFormLayout(Widget);
			#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
			Layout->setVerticalSpacing(0);
			#endif
			Widget->setLayout(Layout);
			Scroll->setWidget(Widget);
			int tabid = addTab(Scroll, PS->GetPrefixName(option.Prefix));
			Tabs.insert(option.Prefix, tabid);
			TabsWidgets.insert(option.Prefix, Widget);
		} else {
			Widget = TabsWidgets.value(option.Prefix);
			Layout = static_cast<QFormLayout *>(Widget->layout());
		}
		ParamsSettings::params_t Param = PS->GetVar(option.Param);
		OptionEditor *Editor = new OptionEditor(option.Param, Param, option, Widget);
		#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
		Editor->setContentsMargins(0, 2, 0, 2);
		#endif
		connect(Editor, &OptionEditor::Changed, this, &ConfigurationWidget::OptionChanged);
		Layout->addRow(option.Name, Editor);
	}

	//User Defined Vars
	QScrollArea *Scroll = new QScrollArea(this);
	Scroll->setWidgetResizable(true);
	Scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	UserDefinedWidget = new QWidget(Scroll);
	UserDefinedLayout = new QFormLayout(UserDefinedWidget);
	UserDefinedWidget->setLayout(UserDefinedLayout);
	Scroll->setWidget(UserDefinedWidget);
	int tabid = addTab(Scroll, "User Defined");
	Tabs.insert("UserDefined", tabid);
	TabsWidgets.insert("UserDefined", UserDefinedWidget);

	AddButtonsWidget = new QWidget(this);
	AddButtonsLayout = new QHBoxLayout(AddButtonsWidget);
	AddVar = new QPushButton("Add General", AddButtonsWidget);
	connect(AddVar, &QPushButton::clicked, this, &ConfigurationWidget::AddUserDefinedClick);
	AddVarStream = new QPushButton("Add Stream", AddButtonsWidget);
	connect(AddVarStream, &QPushButton::clicked, this, &ConfigurationWidget::AddUserStreamDefinedClick);
	AddVarVideo = new QPushButton("Add Video", AddButtonsWidget);
	connect(AddVarVideo, &QPushButton::clicked, this, &ConfigurationWidget::AddUserVideoDefinedClick);
	AddVarAudio = new QPushButton("Add Audio", AddButtonsWidget);
	connect(AddVarAudio, &QPushButton::clicked, this, &ConfigurationWidget::AddUserAudioDefinedClick);
	AddVarSub = new QPushButton("Add Subtitle", AddButtonsWidget);
	connect(AddVarSub, &QPushButton::clicked, this, &ConfigurationWidget::AddUserSubDefinedClick);
	AddVarAttach = new QPushButton("Add Attachment", AddButtonsWidget);
	connect(AddVarAttach, &QPushButton::clicked, this, &ConfigurationWidget::AddUserAttachDefinedClick);
	AddButtonsLayout->addWidget(AddVar);
	AddButtonsLayout->addWidget(AddVarStream);
	AddButtonsLayout->addWidget(AddVarVideo);
	AddButtonsLayout->addWidget(AddVarAudio);
	AddButtonsLayout->addWidget(AddVarSub);
	AddButtonsLayout->addWidget(AddVarAttach);
	UserDefinedLayout->addWidget(AddButtonsWidget);

	QStringList ListParams = PS->Params();
	ListParams.sort();
	for (int i = 0; i < ListParams.size(); ++i) {
		QString Option = ListParams.at(i);
		if (!PS->IsUserDefined(Option)) continue;
		ParamsSettings::context_type Context;
		QString VarName;
		if (!PS->GetVarAndContext(Option, VarName, Context)) continue;
		ParamsSettings::params_t Param = PS->GetVar(Option);
		ParamsSettings::help_t Help;
		switch (Context) {
			case ParamsSettings::context_global:
				Help = PS->GetHelp("var");
				break;
			case ParamsSettings::context_stream:
				Help = PS->GetHelp("var_stream");
				break;
			case ParamsSettings::context_video:
				Help = PS->GetHelp("var_stream_video");
				break;
			case ParamsSettings::context_audio:
				Help = PS->GetHelp("var_stream_audio");
				break;
			case ParamsSettings::context_subtitle:
				Help = PS->GetHelp("var_stream_subtitle");
				break;
			case ParamsSettings::context_attachment:
				Help = PS->GetHelp("var_stream_attachment");
				break;
		}
		UserOptionEditor *Editor = new UserOptionEditor(Option, Context, UserDefinedWidget);
		if (Param.Function != "")
			Editor->SetValue(Param.UserName, Param.Function);
		else
			Editor->SetValue(Param.UserName, Param.Value);
		Editor->RawVisivility(false);
		connect(Editor, &UserOptionEditor::Changed, this, &ConfigurationWidget::UserDefinedChanged);
		connect(Editor, &UserOptionEditor::Deleted, this, &ConfigurationWidget::UserDefinedDelete);
		UserDefinedLayout->addRow(Help.Name, Editor);
	}
}

void ConfigurationWidget::OptionChanged(QString Param, QString Value) {
	PS->setValue(Param, Value);
}

void ConfigurationWidget::UserDefinedChanged(QString Param, ParamsSettings::context_type /*Context*/, QString Name,
		QString Value) {
	//	if (Param == "") {
	//		PS->UserDefinedVar(Name + "=" + Value, Context);
	//	} else {
	if (!PS->Exists(Param)) return;
	//	}
	ParamsSettings::params_t Option = PS->GetVar(Param);
	Option.UserName = Name;
	if (TextSustitution::DetectFunction(Value))
		Option.Function = Value;
	else
		Option.Value = Value;
	Option.Changed = true;
	PS->SetVar(Param, Option);
}

void ConfigurationWidget::UserDefinedDelete(QString Param, ParamsSettings::context_type /*Context*/,
		QWidget *Object) {
	emit AddRemoveCustom(Param);
	UserDefinedLayout->removeRow(Object);
}

void ConfigurationWidget::AddUserGeneric(ParamsSettings::context_type Context, QString HelpVar) {
	ParamsSettings::help_t Help = PS->GetHelp(HelpVar);

	bool ok;
	QString Name = QInputDialog::getText(this,
										 "New Name",
										 "Name for " + QString(Help.Name) + ":",
										 QLineEdit::Normal,
										 "",
										 &ok);
	if (!ok || Name.isEmpty()) return;

	QString ParamName = PS->NewUserDefinedOptionName(Context, Name);
	UserOptionEditor *Editor = new UserOptionEditor(ParamName, Context, UserDefinedWidget);
	Editor->SetValue(Name, "");
	Editor->RawVisivility(false);
	connect(Editor, &UserOptionEditor::Changed, this, &ConfigurationWidget::UserDefinedNewChanged);
	connect(Editor, &UserOptionEditor::Deleted, this, &ConfigurationWidget::UserDefinedDelete);
	UserDefinedLayout->addRow(Help.Name, Editor);
}

void ConfigurationWidget::AddUserDefinedClick() {
	AddUserGeneric(ParamsSettings::context_global, "var");
}

void ConfigurationWidget::AddUserStreamDefinedClick() {
	AddUserGeneric(ParamsSettings::context_stream, "var_stream");
}

void ConfigurationWidget::AddUserVideoDefinedClick() {
	AddUserGeneric(ParamsSettings::context_video, "var_stream_video");
}

void ConfigurationWidget::AddUserAudioDefinedClick() {
	AddUserGeneric(ParamsSettings::context_audio, "var_stream_audio");
}

void ConfigurationWidget::AddUserSubDefinedClick() {
	AddUserGeneric(ParamsSettings::context_subtitle, "var_stream_subtitle");
}

void ConfigurationWidget::AddUserAttachDefinedClick() {
	AddUserGeneric(ParamsSettings::context_attachment, "var_stream_attachment");
}

void ConfigurationWidget::HideShowDefaultValueOptions() {
	QStringList Keys = TabsWidgets.keys();
	foreach (QString Key, Keys) {
		if (Key == "UserDefined") continue;
		QFormLayout *FL = (QFormLayout *)TabsWidgets.value(Key)->layout();
		for (int i = 0; i < FL->rowCount(); ++i) {
			QLayoutItem *Item = FL->itemAt(i, QFormLayout::FieldRole);
			QLayoutItem *Label = FL->itemAt(i, QFormLayout::LabelRole);
			OptionEditor *Editor = (OptionEditor *)Item->widget();
			if (Item->widget()->isHidden()) {
				Item->widget()->show();
				Label->widget()->show();

			} else {
				if (Editor->Param.DefaultValue != Editor->Param.Value) continue;
				Item->widget()->hide();
				Label->widget()->hide();
			}
		}
	}
}

void ConfigurationWidget::UserDefinedNewChanged(QString Param, ParamsSettings::context_type Context, QString Name,
		QString Value) {
	emit UpdateCustom(Param, Context, Name, Value);
}
