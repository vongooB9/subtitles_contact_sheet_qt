#include "configffmpegdialog.h"
#include <QApplication>
#include "optioneditor.h"
#include <QFormLayout>
#include <QLabel>

ConfigFFmpegDialog::ConfigFFmpegDialog(QWidget *parent, const Qt::WindowFlags &f) : QDialog(parent, f) {
	PS = new ParamsSettings(QApplication::applicationDirPath(), this);
	PS->SetConfig("config");
	PS->ReadFromConfigFile();

	setWindowTitle("FFmpeg and FFprobe Configuration");

	MainLayout = new QVBoxLayout(this);

	ButtonsWidget = new QWidget(this);
	ButtonsLayout = new QHBoxLayout(ButtonsWidget);
	CancelButton = new QPushButton("Cancel", ButtonsWidget);
	ButtonsLayout->addWidget(CancelButton);
	SaveButton = new QPushButton("Save", ButtonsWidget);
	ButtonsLayout->addWidget(SaveButton);

	connect(CancelButton, &QPushButton::clicked, this, &ConfigFFmpegDialog::Cancel);
	connect(SaveButton, &QPushButton::clicked, this, &ConfigFFmpegDialog::Save);

	QWidget *Widget = new QWidget(this);
	QFormLayout *Layout = new QFormLayout(Widget);
	Widget->setLayout(Layout);

	OptionEditor *ffmpeg = new OptionEditor("ffmpeg", PS->GetVar("ffmpeg"), PS->GetHelp("ffmpeg"), Widget);
	OptionEditor *ffprobe = new OptionEditor("ffprobe", PS->GetVar("ffprobe"), PS->GetHelp("ffprobe"), Widget);
	connect(ffmpeg, &OptionEditor::Changed, this, &ConfigFFmpegDialog::OptionChanged);
	connect(ffprobe, &OptionEditor::Changed, this, &ConfigFFmpegDialog::OptionChanged);
	Layout->addRow(PS->GetHelp("ffmpeg").Name, ffmpeg);
	Layout->addRow(PS->GetHelp("ffprobe").Name, ffprobe);

	QLabel *Text = new
	QLabel("It seems that ffmpeg and/or ffprobe are not configured correctly, please enter the correct path to the binaries.",
		   this);

	MainLayout->addWidget(Text);
	MainLayout->addWidget(Widget);
	MainLayout->addWidget(ButtonsWidget);

	setLayout(MainLayout);
}
void ConfigFFmpegDialog::Save() {
	PS->UpdateConfigFile(true, false);
	accept();
}

void ConfigFFmpegDialog::Cancel() {
	reject();
}

void ConfigFFmpegDialog::OptionChanged(QString Param, QString Value) {
	PS->setValue(Param, Value);
}
