#ifndef BATCHEXTERNALCOMMANDWIDGET_H
#define BATCHEXTERNALCOMMANDWIDGET_H

#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLineEdit>

class BatchExternalCommandWidget : public QWidget {
		Q_OBJECT
	public:
		explicit BatchExternalCommandWidget(QWidget *parent = nullptr);
		QHBoxLayout *Layout;
		QLineEdit *ExternalProgram;
		QPushButton *SelectExternalProgram;
		QLineEdit *ExternalParams;
		QPushButton *DeleteButton;

		QString GetCommand();
		void SetCommand(QStringList *parameters);

	signals:
		void Deleted(QWidget *Item);
		void Changed();

	public slots:
		void RemoveClick();
		void EmitChanged();
		void SelectClick();

};

#endif // BATCHEXTERNALCOMMANDWIDGET_H
