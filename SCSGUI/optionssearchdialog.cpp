#include "optionssearchdialog.h"

OptionsSearchDialog::OptionsSearchDialog(ParamsSettings *ps, QWidget *parent,
		const Qt::WindowFlags &f) : QDialog(parent, f) {
	OS = new OptionsSearch(ps, &ReturnList, this);
	Layout = new QVBoxLayout(this);
	Layout->addWidget(OS);
	setLayout(Layout);

	connect(OS, &OptionsSearch::Acepted, this, &OptionsSearchDialog::AceptedList);
	connect(OS, &OptionsSearch::Canceled, this, &OptionsSearchDialog::CanceledList);
}

QList<ParamsSettings::help_t> *OptionsSearchDialog::GetReturnList() {
	return &ReturnList;
}

void OptionsSearchDialog::AceptedList() {
	accept();
}

void OptionsSearchDialog::CanceledList() {
	reject();
}
