#include "paramwidget.h"
#include <QFileDialog>
#include <QApplication>
#include "../textsustitution.h"

ParamWidget::ParamWidget(ParamsSettings *ps, QStringList *configlist, ParamsSettings::help_t help, QString value,
						 QWidget *parent)
	: QWidget{parent} {
	PS = ps;
	Help = help;
	AvailableConfigs = configlist;
	Value = value;
	Load();
}

ParamWidget::ParamWidget(ParamsSettings *ps, QStringList *configlist, QString param, QString value, QWidget *parent)
	: QWidget{parent} {
	PS = ps;
	Help = PS->GetHelp(param);
	AvailableConfigs = configlist;
	Value = value;
	Load();
}

void ParamWidget::Load() {
	isVar = false;
	isOption = false;
	if (PS->Exists(Help.Param)) {
		Param = PS->GetVar(Help.Param);
		Param.Value = Value;
		isOption = true;
	}
	ValueBlock = false;
	TextBlock = false;

	Layout = new QHBoxLayout(this);
	Layout->setContentsMargins(0, 0, 0, 0);
	setLayout(Layout);

	if (isOption) {
		OE = new OptionEditor(Help.Param, Param, Help, this);
		connect(OE, &OptionEditor::Changed, this, &ParamWidget::RawUpdated);
		Layout->addWidget(OE);
		AddRemove();
		return;
	}

	if (Help.Param == "config")	{
		PC = new ParamConfigWidget(PS, AvailableConfigs, Value, this);
		connect(PC, &ParamConfigWidget::Updated, this, &ParamWidget::ConfigUpdated);
		Layout->addWidget(PC);
		QSpacerItem *Spacer = new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum);
		Layout->addSpacerItem(Spacer);
		AddRemove();
		return;
	}

	if (Help.DataType == ParamsSettings::param_file) {
		RawEdit = new QLineEdit(this);
		RawEdit->setText(Value);
		RawEdit->setToolTip(Help.GetTip());
		connect(RawEdit, &QLineEdit::textChanged, this, &ParamWidget::RawUpdated);
		Layout->addWidget(RawEdit);

		SelectButton = new QPushButton("Select", this);
		Layout->addWidget(SelectButton);
		connect(SelectButton, &QPushButton::clicked, this, &ParamWidget::SelectFile);
		AddRemove();
		return;
	}

	if (Help.DataType == ParamsSettings::param_dir) {
		RawEdit = new QLineEdit(this);
		RawEdit->setText(Value);
		RawEdit->setToolTip(Help.GetTip());
		connect(RawEdit, &QLineEdit::textChanged, this, &ParamWidget::RawUpdated);
		Layout->addWidget(RawEdit);

		SelectButton = new QPushButton("Select", this);
		Layout->addWidget(SelectButton);
		connect(SelectButton, &QPushButton::clicked, this, &ParamWidget::SelectDir);
		AddRemove();
		return;
	}

	if (Help.Param == "var" ||
		Help.Param == "var_stream" ||
		Help.Param == "var_stream_video" ||
		Help.Param == "var_stream_audio" ||
		Help.Param == "var_stream_subtitle" ||
		Help.Param == "var_stream_attachment") {
		isVar = true;
		VarWidget = new QWidget(this);
		VarLayout = new QHBoxLayout(VarWidget);
		VarLayout->setContentsMargins(0, 0, 0, 0);
		VarName = new QLineEdit(VarWidget);
		VarName->setText(Value.section("=", 0, 0));
		VarName->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
		VarEqual = new QComboBox(VarWidget);
		VarEqual->addItem("=");
		VarEqual->addItem("==");
		VarValue = new QLineEdit(VarWidget);
		TextValue = new QPlainTextEdit(VarWidget);
		TextValue->setFrameShape(QFrame::Box);
		TextValue->setFrameShadow(QFrame::Raised);
		TextTougle = new QPushButton(QApplication::style()->standardPixmap(QStyle::SP_ArrowDown), "", this);
		TextTougle->setCheckable(true);

		if (Value.contains("==")) {
			VarEqual->setCurrentText("==");
			VarValue->setText(Value.section("==", 1));
		} else {
			VarEqual->setCurrentText("=");
			VarValue->setText(Value.section("=", 1));
		}

		if (TextSustitution::TextTest(VarValue->text())) {
			TextValue->setPlainText(TextSustitution::TextFunction(VarValue->text()));
			TextTougle->setChecked(true);
			TougleText(true);
		} else {
			TextValue->setPlainText(VarValue->text());
			TextTougle->setChecked(false);
			TougleText(false);
		}

		VarLayout->addWidget(VarName);
		VarLayout->addWidget(VarEqual);
		VarLayout->addWidget(VarValue);
		VarLayout->addWidget(TextValue);
		VarLayout->addWidget(TextTougle);
		VarLayout->setAlignment(VarName, Qt::AlignTop);
		VarLayout->setAlignment(VarEqual, Qt::AlignTop);
		VarLayout->setAlignment(VarValue, Qt::AlignTop);
		VarLayout->setAlignment(TextValue, Qt::AlignTop);
		VarLayout->setAlignment(TextTougle, Qt::AlignTop);

		Layout->addWidget(VarWidget);
		connect(VarName, &QLineEdit::textChanged, this, &ParamWidget::RawUpdated);
		connect(VarEqual, &QComboBox::currentTextChanged, this, &ParamWidget::RawUpdated);
		connect(VarValue, &QLineEdit::textChanged, this, &ParamWidget::ValueUpdated);
		connect(TextValue, &QPlainTextEdit::textChanged, this, &ParamWidget::TextUpdated);
		connect(TextTougle, &QPushButton::clicked, this, &ParamWidget::TougleText);
		AddRemove();
		return;
	}

	RawEdit = new QLineEdit(this);
	RawEdit->setText(Value);
	RawEdit->setToolTip(Help.GetTip());
	connect(RawEdit, &QLineEdit::textChanged, this, &ParamWidget::RawUpdated);
	Layout->addWidget(RawEdit);

	AddRemove();
}

QString ParamWidget::GetCommand() {
	static QRegularExpression EscapeCaracters("[^a-zA-Z0-9,._+:@\\%/\\-]");
	QString Val = "";
	if (isOption) {
		Val = OE->GetText();
	} else if (Help.Param == "config") {
		if (Value == "")
			Val = "config";
		else
			Val = Value;
	} else if (isVar) {
		Val = VarName->text() + VarEqual->currentText() + VarValue->text();
	} else {
		Val = RawEdit->text();
	}
	if (Val.contains(EscapeCaracters)) Val = "\"" + Val + "\"";
	if (Val == "") Val = "\"\"";
	return "--" + Help.Param + " " + Val;
}

void ParamWidget::SetValue(QString Value) {
	if (Help.DataType != ParamsSettings::param_file) return;
	RawEdit->setText(Value);
}

void ParamWidget::AddRemove() {
	Remove = new QPushButton(QApplication::style()->standardPixmap(QStyle::SP_TrashIcon), "", this);
	Remove->setVisible(false);
	connect(Remove, &QPushButton::clicked, this, &ParamWidget::RemoveClick);
	Layout->addWidget(Remove);
	Layout->setAlignment(Remove, Qt::AlignTop);
	Up = new QPushButton(QApplication::style()->standardPixmap(QStyle::SP_ArrowUp), "", this);
	Up->setVisible(false);
	connect(Up, &QPushButton::clicked, this, &ParamWidget::UpClick);
	Layout->addWidget(Up);
	Layout->setAlignment(Up, Qt::AlignTop);
	Down = new QPushButton(QApplication::style()->standardPixmap(QStyle::SP_ArrowDown), "", this);
	Down->setVisible(false);
	connect(Down, &QPushButton::clicked, this, &ParamWidget::DownClick);
	Layout->addWidget(Down);
	Layout->setAlignment(Down, Qt::AlignTop);
}

void ParamWidget::ConfigUpdated(QString value) {
	Value = value;
	emit Updated();
}

void ParamWidget::RawUpdated() {
	emit Updated();
}

void ParamWidget::ValueUpdated() {
	if (TextBlock) return;
	ValueBlock = true;
	if (TextSustitution::TextTest(VarValue->text()))
		TextValue->setPlainText(TextSustitution::TextFunction(VarValue->text()));
	else
		TextValue->setPlainText(VarValue->text());
	emit Updated();
	ValueBlock = false;
}

void ParamWidget::TextUpdated() {
	if (ValueBlock) return;
	TextBlock = true;
	VarValue->setText(TextSustitution::TextFunction(TextValue->toPlainText(), true));
	emit Updated();
	TextBlock = false;
}

void ParamWidget::RemoveClick() {
	emit Removed(this);
}

void ParamWidget::SelectFile() {
	QString File = QFileDialog::getOpenFileName(this,
				   "Select File for " + Help.Name,
				   "",
				   "All Files (*)");
	if (File != "") RawEdit->setText(QDir::toNativeSeparators(File));
}

void ParamWidget::SelectDir() {
	QString Dir = QFileDialog::getExistingDirectory(this,
				  "Select Directory for " + Help.Name,
				  "",
				  QFileDialog::ShowDirsOnly
				  | QFileDialog::DontResolveSymlinks);

	if (Dir != "") RawEdit->setText(QDir::toNativeSeparators(Dir));
}

void ParamWidget::TougleRemoveVisible(bool Visible) {
	Remove->setVisible(Visible);
	Up->setVisible(Visible);
	Down->setVisible(Visible);
}

void ParamWidget::UpClick() {
	emit UpParam(this);
}

void ParamWidget::DownClick() {
	emit DownParam(this);
}

void ParamWidget::TougleText(bool Checked) {
	if (Checked) {
		TextValue->setVisible(true);
		VarValue->setVisible(false);
	} else {
		TextValue->setVisible(false);
		VarValue->setVisible(true);
	}
}
