#include <QApplication>
#include <QFile>
#include "scsmainwindow.h"
#ifdef Q_OS_WIN
	#include <windows.h>
	#include <shellapi.h>
#endif

bool Debug = false;
bool Quiet = false;
bool Cmd = false;
bool GUIProgress = false;

int main(int argc, char *argv[]) {
	#if QT_VERSION >= QT_VERSION_CHECK(6, 4, 0) && defined (Q_OS_WIN)
	QApplication::setStyle("fusion");
	#endif
	QApplication a(argc, argv);

	QString File = "";
	for (int i = 1; i < argc; i++) {
		#ifdef Q_OS_WIN
		QString param = "";
		LPWSTR *szArglist;
		LPCWSTR command = GetCommandLineW();
		int nArgs;
		szArglist = CommandLineToArgvW(command, &nArgs);
		if (szArglist != NULL || i < nArgs) {
			param = QString::fromWCharArray(szArglist[i]);
		}
		LocalFree(szArglist);
		#else
		QString param = QString::fromUtf8(argv[i]);
		#endif
		if (QFile::exists(param)) {
			File = param;
		}
	}

	SCSMainWindow w(File);
	w.show();
	return a.exec();
}
