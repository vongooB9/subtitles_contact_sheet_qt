#include "paramsdialog.h"
#include <QString>

ParamsDialog::ParamsDialog(QString &program, QStringList *configlist, ParamsSettings *ps, QWidget *parent,
						   const Qt::WindowFlags &f) : QDialog(parent, f) {
	AvailableConfigs = configlist;
	PS = ps;
	Params = new ParamsWidget(program, AvailableConfigs, PS, this);
	Params->HideCommand();
	resize(800, 600);

	setWindowTitle("Select parameters for batch command");

	MainLayout = new QVBoxLayout(this);

	ButtonsWidget = new QWidget(this);
	ButtonsLayout = new QHBoxLayout(ButtonsWidget);
	ButtonsLayout->setContentsMargins(0, 0, 0, 0);
	CancelButton = new QPushButton("Cancel", ButtonsWidget);
	ButtonsLayout->addWidget(CancelButton);
	SaveButton = new QPushButton("Save", ButtonsWidget);
	ButtonsLayout->addWidget(SaveButton);

	connect(CancelButton, &QPushButton::clicked, this, &ParamsDialog::Cancel);
	connect(SaveButton, &QPushButton::clicked, this, &ParamsDialog::Save);

	MainLayout->addWidget(Params);
	MainLayout->addWidget(ButtonsWidget);

	setLayout(MainLayout);
}

void ParamsDialog::SetParams(QString params) {
	Params->SetParams(params);
}

QString ParamsDialog::GetParams() {
	return Params->GetParams();
}

void ParamsDialog::Save() {
	accept();
}

void ParamsDialog::Cancel() {
	reject();
}
