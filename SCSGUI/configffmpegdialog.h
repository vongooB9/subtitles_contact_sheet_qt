#ifndef CONFIGFFMPEGDIALOG_H
#define CONFIGFFMPEGDIALOG_H

#include <QWidget>
#include <QDialog>
#include "../paramssettings.h"
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>

class ConfigFFmpegDialog : public QDialog {
	public:
		ConfigFFmpegDialog(QWidget *parent = nullptr, const Qt::WindowFlags &f = Qt::WindowFlags());

	protected:
		ParamsSettings *PS;

		QVBoxLayout *MainLayout;
		QWidget *ButtonsWidget;
		QHBoxLayout *ButtonsLayout;
		QPushButton *CancelButton;
		QPushButton *SaveButton;

	public slots:
		void Save();
		void Cancel();
		void OptionChanged(QString Param, QString Value);

};

#endif // CONFIGFFMPEGDIALOG_H
