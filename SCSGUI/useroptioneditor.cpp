#include "useroptioneditor.h"
#include <QApplication>
#include <QStyle>
#include "../textsustitution.h"

UserOptionEditor::UserOptionEditor(QString ParamName, ParamsSettings::context_type Context, QWidget *parent)
	: QWidget{parent}, ParamName(ParamName), Context(Context) {
	Layout = new QHBoxLayout(this);
	Layout->setContentsMargins(0, 0, 0, 0);

	VarWidget = new QWidget(this);
	VarLayout = new QHBoxLayout(VarWidget);
	VarLayout->setContentsMargins(0, 0, 0, 0);
	VarName = new QLineEdit(VarWidget);
	EqualLabel = new QLabel("=");
	VarName->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	VarName->setReadOnly(true);
	VarValue = new QLineEdit(VarWidget);
	VarText = new QPlainTextEdit(VarWidget);
	TextTougle = new QPushButton(QApplication::style()->standardPixmap(QStyle::SP_ArrowDown), "", this);
	TextTougle->setCheckable(true);
	VarLayout->addWidget(VarName);
	VarLayout->addWidget(EqualLabel);
	VarLayout->addWidget(VarValue);
	VarLayout->addWidget(VarText);
	VarLayout->addWidget(TextTougle);
	VarLayout->setAlignment(VarName, Qt::AlignTop);
	VarLayout->setAlignment(EqualLabel, Qt::AlignTop);
	VarLayout->setAlignment(VarValue, Qt::AlignTop);
	VarLayout->setAlignment(VarText, Qt::AlignTop);
	VarLayout->setAlignment(TextTougle, Qt::AlignTop);
	Layout->addWidget(VarWidget);

	RawEditor = new QLineEdit(this);
	RawEditor->setVisible(false);
	RawEditor->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	Layout->addWidget(RawEditor);
	VarLayout->setAlignment(RawEditor, Qt::AlignTop);
	RawTougle = new QPushButton("Show Raw", this);
	RawTougle->setCheckable(true);
	VarLayout->setAlignment(RawTougle, Qt::AlignTop);

	Delete = new QPushButton(QApplication::style()->standardPixmap(QStyle::SP_TrashIcon), "", this);
	Layout->addWidget(Delete);
	Layout->setAlignment(Delete, Qt::AlignTop);

	connect(VarValue, &QLineEdit::textChanged, this, &UserOptionEditor::ValueChanged);
	connect(VarText, &QPlainTextEdit::textChanged, this, &UserOptionEditor::TextChanged);
	connect(RawTougle, &QPushButton::toggled, this, &UserOptionEditor::RawTougleChange);
	connect(RawEditor, &QLineEdit::textChanged, this, &UserOptionEditor::RawChange);
	connect(Delete, &QPushButton::clicked, this, &UserOptionEditor::DeleteClick);
	connect(TextTougle, &QPushButton::clicked, this, &UserOptionEditor::TougleText);

	RawBlock = false;
	ValueBlock = false;
	TextBlock = false;
}

void UserOptionEditor::SetValue(QString Value) {
	QString name = Value.section("=", 0, 0);
	QString value = Value.section("=", 1, 1);
	SetValue(name, value);
}

void UserOptionEditor::SetValue(QString Name, QString Param) {
	VarName->setText(Name);
	VarValue->setText(Param);
	bool NeedText = TextSustitution::TextTest(Param);
	TextTougle->setChecked(NeedText);
	TougleText(NeedText);
}

void UserOptionEditor::RawVisivility(bool Visible) {
	RawTougle->setVisible(Visible);
}

void UserOptionEditor::RawTougleChange(bool Checked) {
	setUpdatesEnabled(false);
	RawEditor->setVisible(Checked);
	VarWidget->setVisible(!Checked);
	setUpdatesEnabled(true);
}

void UserOptionEditor::ValueChanged() {
	if (RawBlock || TextBlock) return;
	ValueBlock = true;
	RawEditor->setText(VarName->text() + "=" + VarValue->text());
	if (TextSustitution::TextTest(VarValue->text())) {
		VarText->setPlainText(TextSustitution::TextFunction(VarValue->text()));
	} else {
		VarText->setPlainText(VarValue->text());
	}
	emit Changed(ParamName, Context, VarName->text(), VarValue->text());
	ValueBlock = false;
}

void UserOptionEditor::TextChanged() {
	if (ValueBlock || RawBlock) return;
	TextBlock = true;
	VarValue->setText(TextSustitution::TextFunction(VarText->toPlainText(), true));
	RawEditor->setText(VarName->text() + "=" + VarValue->text());
	emit Changed(ParamName, Context, VarName->text(), VarValue->text());
	TextBlock = false;
}

void UserOptionEditor::RawChange() {
	if (ValueBlock || TextBlock) return;
	RawBlock = true;
	VarName->setText(RawEditor->text().section("=", 0, 0));
	VarValue->setText(RawEditor->text().section("=", 1, 1));
	if (TextSustitution::TextTest(VarValue->text())) {
		VarText->setPlainText(TextSustitution::TextFunction(VarValue->text()));
	} else {
		VarText->setPlainText(VarValue->text());
	}
	emit Changed(ParamName, Context, VarName->text(), VarValue->text());
	RawBlock = false;
}

void UserOptionEditor::DeleteClick() {
	emit Deleted(ParamName, Context, this);
}

void UserOptionEditor::TougleText(bool Checked) {
	if (Checked) {
		VarText->setVisible(true);
		VarValue->setVisible(false);
	} else {
		VarText->setVisible(false);
		VarValue->setVisible(true);
	}
}
