#include "optionssearchitem.h"

OptionsSearchItem::OptionsSearchItem(const ParamsSettings::help_t &Help, QWidget *parent,
									 const Qt::WindowFlags &f) : QFrame(parent, f),
	Help(Help) {

	setFrameStyle(QFrame::Panel | QFrame::Raised);
	setLineWidth(2);

	Layout = new QVBoxLayout(this);
	Layout->setContentsMargins(2, 2, 2, 2);
	Name = new QLabel(this);
	//	Name->setContentsMargins(0, 0, 0, 0);
	Name->setText("<b>" + Help.Name + " (" + Help.Param + ")</b>");
	//	Name->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
	Text = new QLabel(Help.Text, this);
	//	Name->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	Text->setWordWrap(true);
	Text->setAlignment(Qt::AlignRight /*| Qt::AlignBottom*/);
	//	Text->setContentsMargins(0, 0, 0, 0);
	Layout->addWidget(Name);
	Layout->addWidget(Text);
}

ParamsSettings::help_t *OptionsSearchItem::GetHelp() {
	return &Help;
}
