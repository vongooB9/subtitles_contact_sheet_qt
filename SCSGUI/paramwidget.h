#ifndef PARAMWIDGET_H
#define PARAMWIDGET_H

#include <QWidget>
#include "../paramssettings.h"
#include <QHBoxLayout>
#include <QLineEdit>
#include "optioneditor.h"
#include "paramconfigwidget.h"
#include <QPushButton>
#include <QPlainTextEdit>

class ParamWidget : public QWidget {
		Q_OBJECT
	public:
		explicit ParamWidget(ParamsSettings *ps, QStringList *configlist, ParamsSettings::help_t help, QString value,
							 QWidget *parent = nullptr);
		explicit ParamWidget(ParamsSettings *ps, QStringList *configlist, QString param, QString value,
							 QWidget *parent = nullptr);
		void Load();
		QString GetCommand();
		ParamsSettings::help_t Help;
		void SetValue(QString Value);
	private:
		ParamsSettings *PS;
		QStringList *AvailableConfigs;
		bool isOption;
		ParamsSettings::params_t Param;
		QString Value;
		QHBoxLayout *Layout;
		QLineEdit *RawEdit;
		OptionEditor *OE;
		ParamConfigWidget *PC;
		QPushButton *Remove;
		QPushButton *Up;
		QPushButton *Down;
		QPushButton *SelectButton;
		QPushButton *TextTougle;

		bool isVar;
		QWidget *VarWidget;
		QHBoxLayout *VarLayout;
		QLineEdit *VarName;
		QComboBox *VarEqual;
		QLineEdit *VarValue;
		QPlainTextEdit *TextValue;
		bool ValueBlock;
		bool TextBlock;

		void AddRemove();

	signals:
		void Updated();
		void Removed(QWidget *RemovedObject);
		void UpParam(QWidget *MoveObject);
		void DownParam(QWidget *MoveObject);

	public slots:
		void ConfigUpdated(QString value);
		void RawUpdated();
		void ValueUpdated();
		void TextUpdated();
		void RemoveClick();
		void SelectFile();
		void SelectDir();
		void TougleRemoveVisible(bool Visible);
		void UpClick();
		void DownClick();
		void TougleText(bool Checked = false);
};

#endif // PARAMWIDGET_H
