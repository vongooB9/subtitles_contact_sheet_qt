#include "configurationdialog.h"
#include "../textsustitution.h"
#include <QApplication>

ConfigurationDialog::ConfigurationDialog(QString name, QWidget *parent, const Qt::WindowFlags &f) : QDialog(parent, f) {

	PS = new ParamsSettings(QApplication::applicationDirPath(), this);
	PS->SetConfig(name);
	PS->ReadFromConfigFile();
	CW = new ConfigurationWidget(PS, this);

	setWindowTitle("Edit \"" + name + "\" configuration file");

	MainLayout = new QVBoxLayout(this);

	ButtonsWidget = new QWidget(this);
	ButtonsLayout = new QHBoxLayout(ButtonsWidget);
	HideDefault = new QPushButton("Hide/Show Defaults", ButtonsWidget);
	ButtonsLayout->addWidget(HideDefault);
	CancelButton = new QPushButton("Cancel", ButtonsWidget);
	ButtonsLayout->addWidget(CancelButton);
	SaveButton = new QPushButton("Save", ButtonsWidget);
	ButtonsLayout->addWidget(SaveButton);

	connect(HideDefault, &QPushButton::clicked, CW, &ConfigurationWidget::HideShowDefaultValueOptions);
	connect(CancelButton, &QPushButton::clicked, this, &ConfigurationDialog::Cancel);
	connect(SaveButton, &QPushButton::clicked, this, &ConfigurationDialog::Save);


	MainLayout->addWidget(CW);
	MainLayout->addWidget(ButtonsWidget);

	setLayout(MainLayout);

	connect(CW, &ConfigurationWidget::AddRemoveCustom, this, &ConfigurationDialog::AddRemoveCustom);
	connect(CW, &ConfigurationWidget::UpdateCustom, this, &ConfigurationDialog::UpdateCustom);
}

void ConfigurationDialog::SetName(QString name) {
	PS->SetConfig(name);
}

void ConfigurationDialog::AddRemoveCustom(QString Param) {
	if (AddList.contains(Param)) {
		AddList.remove(Param);
		return;
	}
	if (RemoveList.contains(Param)) return;
	RemoveList.append(Param);
}

void ConfigurationDialog::UpdateCustom(QString Param, ParamsSettings::context_type Context, QString Name,
									   QString Value) {
	if (AddList.contains(Param)) {
		AddList[Param] = {Context, Name, Value};
	} else {
		AddList.insert(Param, {Context, Name, Value});
	}
}

void ConfigurationDialog::Save() {
	for (int i = 0; i < RemoveList.count(); ++i) {
		PS->RemoveUserDefinedVar(RemoveList.at(i));
	}
	RemoveList.clear();

	while (!AddList.isEmpty()) {
		QString Param = AddList.firstKey();
		CustomElemnt Element = AddList.take(Param);
		ParamsSettings::params_t Option = PS->GetVar(Param);
		Option.UserName = Element.Name;
		if (TextSustitution::DetectFunction(Element.Value))
			Option.Function = Element.Value;
		else
			Option.Value = Element.Value;
		Option.Changed = true;
		PS->SetVar(Param, Option);
	}

	PS->UpdateConfigFile(true, false);
	accept();
}

void ConfigurationDialog::Cancel() {
	reject();
}
