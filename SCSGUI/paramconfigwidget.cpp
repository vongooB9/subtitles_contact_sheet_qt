#include "paramconfigwidget.h"
#include "configurationdialog.h"
#include <QInputDialog>
#include <QMessageBox>

ParamConfigWidget::ParamConfigWidget(ParamsSettings *ps, QStringList *configlist, QString value, QWidget *parent)
	: QWidget{parent} {
	AvailableConfigs = configlist;
	PS = ps;

	Layout = new QHBoxLayout(this);
	Layout->setContentsMargins(0, 0, 0, 0);
	setLayout(Layout);

	Combo = new QComboBox(this);
	Combo->setSizeAdjustPolicy(QComboBox::AdjustToContents);
	Combo->addItems(*AvailableConfigs);
	Combo->setCurrentText(value);

	Edit = new QPushButton("Edit", this);
	Clone = new QPushButton("Clone", this);
	New = new QPushButton("New", this);
	Del = new QPushButton("Delete", this);

	Layout->addWidget(Combo);
	Layout->addWidget(Edit);
	Layout->addWidget(Clone);
	Layout->addWidget(New);
	Layout->addWidget(Del);

	connect(Edit, &QPushButton::clicked, this, &ParamConfigWidget::EditClick);
	connect(Clone, &QPushButton::clicked, this, &ParamConfigWidget::CloneClick);
	connect(New, &QPushButton::clicked, this, &ParamConfigWidget::NewClick);
	connect(Del, &QPushButton::clicked, this, &ParamConfigWidget::DelClick);
	connect(Combo, &QComboBox::currentTextChanged, this, &ParamConfigWidget::ComboChanged);
}

QString ParamConfigWidget::AskForNewName() {
	bool ok;
	QString New = QInputDialog::getText(this, "New Config", "Select new config name", QLineEdit::Normal, "", &ok);
	if (!ok) return "";
	if (New == "") {
		QMessageBox::critical(this, "Error creating config file", "The name cannot be empty");
		return "";
	}
	if (AvailableConfigs->contains(New)) {
		QMessageBox::critical(this, "Error creating config file", "The name is already in use");
		return "";
	}
	return New;
}

void ParamConfigWidget::CreateNew(bool Copy) {
	QString New = AskForNewName();
	if (New == "") return;
	QString Config = New;
	if (Copy) {
		Config = Combo->currentText();
		if (Config == "") Config = "config";
	}
	ConfigurationDialog *CD = new ConfigurationDialog(Config, this);
	if (Copy) CD->SetName(New);
	if (CD->exec() == QDialog::Accepted) {
		AvailableConfigs->append(New);
		AvailableConfigs->sort();
		Combo->clear();
		Combo->addItems(*AvailableConfigs);
		Combo->setCurrentText(New);
	}
}

void ParamConfigWidget::EditClick() {
	QString Config = Combo->currentText();
	if (Config == "") Config = "config";
	ConfigurationDialog *CD = new ConfigurationDialog(Config, this);
	CD->exec();
}

void ParamConfigWidget::CloneClick() {
	CreateNew(true);
}

void ParamConfigWidget::NewClick() {
	CreateNew();
}

void ParamConfigWidget::DelClick() {
	QString remove = Combo->currentText();
	QMessageBox::StandardButton Q = QMessageBox::question(this,
									"Delete configuration " + remove,
									"Are you sure to delete " + remove + "?");
	if (Q != QMessageBox::Yes) return;
	PS->DeleteConfig(remove);
	AvailableConfigs->removeOne(remove);
	Combo->removeItem(Combo->currentIndex());
}

void ParamConfigWidget::ComboChanged(QString Value) {
	emit Updated(Value);
}
