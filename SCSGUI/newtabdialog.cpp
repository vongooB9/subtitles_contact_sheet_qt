#include "newtabdialog.h"

NewTabDialog::NewTabDialog(QWidget *parent, const Qt::WindowFlags &f) : QDialog(parent, f) {
	setWindowTitle("New Tab Options");
	MainLayout = new QVBoxLayout(this);
	FormWidget = new QWidget(this);
	FormLayout = new QFormLayout(FormWidget);
	FormLayout->setContentsMargins(0, 0, 0, 0);

	NormalTab = new QRadioButton(FormWidget);
	BatchTab = new QRadioButton(FormWidget);
	TabName = new QLineEdit(FormWidget);

	NormalTab->setChecked(true);

	FormLayout->addRow("Normal Tab", NormalTab);
	FormLayout->addRow("Batch Tab", BatchTab);
	FormLayout->addRow("Tab Name", TabName);

	ButtonsWidget = new QWidget(this);
	ButtonsLayout = new QHBoxLayout(ButtonsWidget);
	ButtonsLayout->setContentsMargins(0, 0, 0, 0);
	CancelButton = new QPushButton("Cancel", ButtonsWidget);
	NewButton = new QPushButton("Create Tab", ButtonsWidget);
	ButtonsLayout->addWidget(CancelButton);
	ButtonsLayout->addWidget(NewButton);

	MainLayout->addWidget(FormWidget);
	MainLayout->addWidget(ButtonsWidget);
	connect(CancelButton, &QPushButton::clicked, this, &NewTabDialog::Cancel);
	connect(NewButton, &QPushButton::clicked, this, &NewTabDialog::New);
}

void NewTabDialog::New() {
	accept();
}

void NewTabDialog::Cancel() {
	reject();
}
