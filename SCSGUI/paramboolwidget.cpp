#include "paramboolwidget.h"

ParamBoolWidget::ParamBoolWidget(ParamsSettings::help_t help, QWidget *parent)
	: QWidget{parent} {
	Help = help;
	Check = new QCheckBox(this);
	Check->setToolTip(Help.GetTip());
	Check->setChecked(true);
	Value = true;
	Layout = new QHBoxLayout(this);
	Layout->setContentsMargins(0, 0, 0, 0);
	setLayout(Layout);
	Layout->addWidget(Check);
	#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
	connect(Check, &QCheckBox::checkStateChanged, this, &ParamBoolWidget::UpdatedCheck);
	#else
	connect(Check, &QCheckBox::stateChanged, this, &ParamBoolWidget::UpdatedCheck);
	#endif
}

QString ParamBoolWidget::GetCommand() {
	if (Value)
		return "--" + Help.Param;
	else
		return "";
}

#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
void ParamBoolWidget::UpdatedCheck(Qt::CheckState State) {
#else
void ParamBoolWidget::UpdatedCheck(int State) {
#endif
	if (State == Qt::Checked)
		Value = true;
	else
		Value = false;
	emit Updated();
}
