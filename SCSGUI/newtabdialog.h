#ifndef NEWTABDIALOG_H
#define NEWTABDIALOG_H

#include <QDialog>
#include <QFormLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QRadioButton>

class NewTabDialog : public QDialog {
	public:
		NewTabDialog(QWidget *parent = nullptr, const Qt::WindowFlags &f = Qt::WindowFlags());

		QRadioButton *NormalTab;
		QRadioButton *BatchTab;
		QLineEdit *TabName;

	protected:
		QVBoxLayout *MainLayout;
		QWidget *FormWidget;
		QFormLayout *FormLayout;

		QWidget *ButtonsWidget;
		QHBoxLayout *ButtonsLayout;
		QPushButton *CancelButton;
		QPushButton *NewButton;

	public slots:
		void New();
		void Cancel();

};

#endif // NEWTABDIALOG_H
