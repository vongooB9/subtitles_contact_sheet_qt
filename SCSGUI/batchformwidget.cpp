#include "batchformwidget.h"
#include <QFileDialog>
#include <QList>
#include <QStringList>
#include <QMimeData>
#include <QDragEnterEvent>
#include <QProcess>

BatchFormWidget::BatchFormWidget(QString &program, QStringList *configlist, QWidget *parent)
	: QWidget{parent},
	  CommandCounter(0) {
	Program = program;
	AvailableConfigs = configlist;
	AutomaticChange = false;
	this->setAcceptDrops(true);
	CentralLayout = new QVBoxLayout(this);
	CentralLayout->setContentsMargins(0, 0, 0, 0);

	MainSplitter = new QSplitter(Qt::Horizontal, this);
	CentralLayout->addWidget(MainSplitter);

	CommandText = new QLineEdit(this);
	CentralLayout->addWidget(CommandText);

	LeftWidget = new QWidget(this);
	LeftLayout = new QVBoxLayout(LeftWidget);
	MainSplitter->addWidget(LeftWidget);

	RightWidget = new QWidget(this);
	RightLayout = new QVBoxLayout(RightWidget);
	MainSplitter->addWidget(RightWidget);

	InputList = new QListWidget(LeftWidget);
	InputList->setViewMode(QListView::ListMode);
	InputList->setSelectionMode(QAbstractItemView::ExtendedSelection);

	InputButtonsWidget = new QWidget(LeftWidget);
	InputButtonsLayout = new QHBoxLayout(InputButtonsWidget);
	InputButtonsLayout->setContentsMargins(0, 0, 0, 0);
	InputButtonsWidget->setLayout(InputButtonsLayout);

	LeftLayout->addWidget(InputList);
	LeftLayout->addWidget(InputButtonsWidget);

	AddFile = new QPushButton("Add File", InputButtonsWidget);
	connect(AddFile, &QPushButton::clicked, this, &BatchFormWidget::AddFileClick);

	AddDir = new QPushButton("Add Folder", InputButtonsWidget);
	connect(AddDir, &QPushButton::clicked, this, &BatchFormWidget::AddDirClick);

	DelInput = new QPushButton("Remove", InputButtonsWidget);
	connect(DelInput, &QPushButton::clicked, this, &BatchFormWidget::RemoveClick);

	InputButtonsLayout->addWidget(AddFile);
	InputButtonsLayout->addWidget(AddDir);
	InputButtonsLayout->addWidget(DelInput);

	OptionsRow1Widget = new QWidget(RightWidget);
	OptionsRow1Layout = new QHBoxLayout(OptionsRow1Widget);
	OptionsRow1Layout->setContentsMargins(0, 0, 0, 0);
	RightLayout->addWidget(OptionsRow1Widget);

	OptionsGroup = new QGroupBox("Options", RightWidget);
	OptionsLayout = new QFormLayout(OptionsGroup);
	OptionsRow1Layout->addWidget(OptionsGroup);

	NProcessWidget = new QWidget(OptionsGroup);

	NProcess = new QSpinBox(NProcessWidget);
	NProcess->setMaximum(16);
	NProcess->setMinimum(1);
	OptionsLayout->addRow("Parellel process", NProcess);

	Idle = new QCheckBox(OptionsGroup);
	Recursive = new QCheckBox(OptionsGroup);
	IgnoreFail = new QCheckBox(OptionsGroup);
	ShowCMD = new QCheckBox(OptionsGroup);
	Quiet = new QCheckBox(OptionsGroup);
	ShowOutput = new QCheckBox(OptionsGroup);

	OptionsLayout->addRow("Idle Priority", Idle);
	OptionsLayout->addRow("Recursive", Recursive);
	OptionsLayout->addRow("Ignore Fail", IgnoreFail);
	OptionsLayout->addRow("Only show CMD", ShowCMD);
	OptionsLayout->addRow("Quiet", Quiet);
	OptionsLayout->addRow("Show Output", ShowOutput);

	FileTypesGroup = new QGroupBox("File types", RightWidget);
	FileTypesLayout = new QFormLayout(FileTypesGroup);
	OptionsRow1Layout->addWidget(FileTypesGroup);

	VideoFiles = new QCheckBox(FileTypesGroup);
	SubtitlesFiles = new QCheckBox(FileTypesGroup);
	SubtitlesFiles->setObjectName("subs");
	CustomFileExt = new QLineEdit(FileTypesGroup);
	FileTypesLayout->addRow("Video", VideoFiles);
	FileTypesLayout->addRow("Subtitles", SubtitlesFiles);
	FileTypesLayout->addRow("Extensions list", CustomFileExt);

	DaemonGroup = new QGroupBox("Backgound Options", OptionsRow1Widget);
	DaemonLayout = new QFormLayout(DaemonGroup);
	Watch = new QCheckBox(DaemonGroup);
	Time = new QSpinBox(DaemonGroup);
	Time->setMaximum(INT_MAX);
	Time->setMinimum(0);
	Modified = new QSpinBox(DaemonGroup);
	Modified->setMaximum(INT_MAX);
	Modified->setMinimum(0);
	Recheck = new QSpinBox(DaemonGroup);
	Recheck->setMaximum(INT_MAX);
	Recheck->setMinimum(0);
	Delete = new QSpinBox(DaemonGroup);
	Delete->setMaximum(INT_MAX);
	Delete->setMinimum(0);
	DaemonLayout->addRow("Watch Filesystem", Watch);
	DaemonLayout->addRow("Search Time", Time);
	DaemonLayout->addRow("Modification Time Delay", Modified);
	DaemonLayout->addRow("Recheck Time", Recheck);
	DaemonLayout->addRow("Deleted Check Time", Delete);
	OptionsRow1Layout->addWidget(DaemonGroup);

	ExternalGroup = new QGroupBox("External Program", RightWidget);
	ExternalLayout = new QHBoxLayout(ExternalGroup);
	ExternalProgram = new QLineEdit(ExternalGroup);
	SelectExternalProgram = new QPushButton("Select", ExternalGroup);
	connect(SelectExternalProgram, &QPushButton::clicked, this, &BatchFormWidget::SelectExternalClick);
	ExternalLayout->addWidget(ExternalProgram);
	ExternalLayout->addWidget(SelectExternalProgram);
	RightLayout->addWidget(ExternalGroup);

	BatchCommand = new BatchCommandsWidget(AvailableConfigs, RightWidget);
	RightLayout->addWidget(BatchCommand);

	RightSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
	RightLayout->addSpacerItem(RightSpacer);

	connect(InputList, &QListWidget::itemChanged, this, &BatchFormWidget::CommandChanged);
	#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
	connect(VideoFiles, &QCheckBox::checkStateChanged, this, &BatchFormWidget::CommandChanged);
	#else
	connect(VideoFiles, &QCheckBox::stateChanged, this, &BatchFormWidget::CommandChanged);
	#endif
	#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
	connect(SubtitlesFiles, &QCheckBox::checkStateChanged, this, &BatchFormWidget::CommandChanged);
	#else
	connect(SubtitlesFiles, &QCheckBox::stateChanged, this, &BatchFormWidget::CommandChanged);
	#endif
	#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
	connect(Recursive, &QCheckBox::checkStateChanged, this, &BatchFormWidget::CommandChanged);
	#else
	connect(Recursive, &QCheckBox::stateChanged, this, &BatchFormWidget::CommandChanged);
	#endif
	#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
	connect(IgnoreFail, &QCheckBox::checkStateChanged, this, &BatchFormWidget::CommandChanged);
	#else
	connect(IgnoreFail, &QCheckBox::stateChanged, this, &BatchFormWidget::CommandChanged);
	#endif
	#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
	connect(ShowCMD, &QCheckBox::checkStateChanged, this, &BatchFormWidget::CommandChanged);
	#else
	connect(ShowCMD, &QCheckBox::stateChanged, this, &BatchFormWidget::CommandChanged);
	#endif
	#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
	connect(Quiet, &QCheckBox::checkStateChanged, this, &BatchFormWidget::CommandChanged);
	#else
	connect(Quiet, &QCheckBox::stateChanged, this, &BatchFormWidget::CommandChanged);
	#endif
	#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
	connect(ShowOutput, &QCheckBox::checkStateChanged, this, &BatchFormWidget::CommandChanged);
	#else
	connect(ShowOutput, &QCheckBox::stateChanged, this, &BatchFormWidget::CommandChanged);
	#endif
	connect(CustomFileExt, &QLineEdit::textChanged, this, &BatchFormWidget::CommandChanged);
	connect(BatchCommand, &BatchCommandsWidget::Changed, this, &BatchFormWidget::CommandChanged);
	connect(NProcess, &QSpinBox::textChanged, this, &BatchFormWidget::CommandChanged);
	#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
	connect(Watch, &QCheckBox::checkStateChanged, this, &BatchFormWidget::CommandChanged);
	#else
	connect(Watch, &QCheckBox::stateChanged, this, &BatchFormWidget::CommandChanged);
	#endif
	connect(Time, &QSpinBox::textChanged, this, &BatchFormWidget::CommandChanged);
	connect(Modified, &QSpinBox::textChanged, this, &BatchFormWidget::CommandChanged);
	connect(Recheck, &QSpinBox::textChanged, this, &BatchFormWidget::CommandChanged);
	connect(Delete, &QSpinBox::textChanged, this, &BatchFormWidget::CommandChanged);
	connect(ExternalProgram, &QLineEdit::textChanged, this, &BatchFormWidget::CommandChanged);
	connect(ExternalProgram, &QLineEdit::textChanged, this, &BatchFormWidget::ExternalProgramChange);

	connect(CommandText, &QLineEdit::textChanged, this, &BatchFormWidget::CommandManualChanged);
}

void BatchFormWidget::AddFileClick() {
	QStringList Files = QFileDialog::getOpenFileNames(this, "Add File", "",
						"Suported Files (*.mkv *.mp4 *.avi *.mov *.wmv *.srt *.ass *.vtt);;"
						"Video Files (*.mkv *.mp4 *.avi *.mov *.wmv);;"
						"Subtitles Files (*.srt *.ass *.vtt);;"
						"All Files (*)");
	for (int i = 0; i < Files.count(); ++i) {
		InputList->addItem(QDir::toNativeSeparators(Files.at(i)));
	}
	CommandChanged();
}

void BatchFormWidget::AddDirClick() {
	QString Dir = QFileDialog::getExistingDirectory(this,
				  tr("Add Directory"),
				  "",
				  QFileDialog::ShowDirsOnly
				  | QFileDialog::DontResolveSymlinks);
	if (Dir != "") InputList->addItem(QDir::toNativeSeparators(Dir));
	CommandChanged();
}

void BatchFormWidget::RemoveClick() {
	QList<QListWidgetItem *> List = InputList->selectedItems();
	for (int i = 0; i < List.count(); ++i) {
		int row = InputList->row(List.at(i));
		InputList->takeItem(row);
		delete List.at(i);
	}
	CommandChanged();
}

void BatchFormWidget::CommandChanged() {
	AutomaticChange = true;
	if (!Loading) CommandText->setText(GetCommand());
	AutomaticChange = false;
}

void BatchFormWidget::CommandManualChanged() {
	if (!AutomaticChange) LoadCommand(CommandText->text());
}

void BatchFormWidget::SelectExternalClick() {
	QString OpenExtentions = "";
	#ifdef Q_OS_WIN
	OpenExtentions = "Exe files (*.exe);;";
	#endif
	OpenExtentions += "All Files (*)";
	QString File = QFileDialog::getOpenFileName(this,
				   "Select external program",
				   "",
				   OpenExtentions);
	if (File != "") ExternalProgram->setText(QDir::toNativeSeparators(File));

}

void BatchFormWidget::ExternalProgramChange() {
	BatchCommand->SetExternalState((ExternalProgram->text() != ""));
}

void BatchFormWidget::dragEnterEvent(QDragEnterEvent *e) {
	if (e->mimeData()->hasUrls()) {
		e->acceptProposedAction();
	}
}

void BatchFormWidget::dropEvent(QDropEvent *e) {
	foreach (const QUrl &url, e->mimeData()->urls()) {
		QString fileName = url.toLocalFile();
		InputList->addItem(fileName);
	}
	CommandChanged();
}

void BatchFormWidget::LoadCommand(QString Command) {
	Loading = true;
	Command.replace(" \"\"", " \"_@_\"");
	QStringList Params = QProcess::splitCommand(Command);
	if (Params.count() == 0) return;
	Params.removeFirst();
	for (int i = 0; i < Params.count(); ++i) {
		if (Params[i] == "_@_") Params[i] = "";
	}

	InputList->clear();
	CustomFileExt->setText("");
	ExternalProgram->setText("");
	Recursive->setCheckState(Qt::Unchecked);
	VideoFiles->setCheckState(Qt::Unchecked);
	SubtitlesFiles->setCheckState(Qt::Unchecked);
	IgnoreFail->setCheckState(Qt::Unchecked);
	ShowCMD->setCheckState(Qt::Unchecked);
	ShowOutput->setCheckState(Qt::Unchecked);
	Quiet->setCheckState(Qt::Unchecked);
	Watch->setCheckState(Qt::Unchecked);

	while (!Params.isEmpty()) {
		if (Params.count() == 0) return;
		QString Param = Params.takeFirst();

		if (Param == "-r" || Param == "--recursive" ) {
			Recursive->setCheckState(Qt::Checked);
		} else if (Param == "-f" || Param == "--file") {
			QString filetext = CustomFileExt->text();
			if (Params.count() == 0) return;
			QString fileparam = Params.takeFirst();
			if (filetext != "") filetext += ",";
			CustomFileExt->setText(filetext + fileparam);
		} else if (Param == "-b" || Param == "--batch") {
			continue;
		} else if (Param == "--video" || Param == "--videofiles") {
			VideoFiles->setCheckState(Qt::Checked);
		} else if (Param == "--subs" || Param == "--subfiles") {
			SubtitlesFiles->setCheckState(Qt::Checked);
		} else if (Param == "--no_fail") {
			IgnoreFail->setCheckState(Qt::Checked);
		} else if (Param == "-c" || Param == "--command") {
			if (Params.count() == 0) return;
			ExternalProgram->setText(Params.takeFirst());
		} else if (Param == "-p" || Param == "--parallel") {
			if (Params.count() == 0) return;
			NProcess->setValue(Params.takeFirst().toInt());
		} else if (Param == "--idle") {
			Idle->setCheckState(Qt::Checked);
		} else if (Param == "-t" || Param == "--time") {
			if (Params.count() == 0) return;
			Time->setValue(Params.takeFirst().toInt());
		} else if (Param == "-d" || Param == "--delete") {
			if (Params.count() == 0) return;
			Delete->setValue(Params.takeFirst().toInt());
		} else if (Param == "-w" || Param == "--watch") {
			Watch->setCheckState(Qt::Checked);
		} else if (Param == "-m" || Param == "--modified") {
			if (Params.count() == 0) return;
			Modified->setValue(Params.takeFirst().toInt());
		} else if (Param == "--recheck") {
			if (Params.count() == 0) return;
			Recheck->setValue(Params.takeFirst().toInt());
		} else if (Param == "--cmd") {
			ShowCMD->setCheckState(Qt::Checked);
		} else if (Param == "--show_output") {
			ShowOutput->setCheckState(Qt::Checked);
		} else if (Param == "-q" || Param == "--quiet") {
			Quiet->setCheckState(Qt::Checked);
		} else if (Param == "--") {
			BatchCommand->SetCommand(Params, (ExternalProgram->text() != ""));
			break;
		} else {
			InputList->addItem(Param);
		}
	}
	CurrentCommand = Command;
	Loading = false;
}

QString BatchFormWidget::GetCommand() {
	QStringList result;

	for (int i = 0; i < InputList->count(); ++i) {
		QListWidgetItem *Cur = InputList->item(i);
		QString text = Cur->text();
		if (text.contains(" ")) text = "\"" + text + "\"";
		result.append(text);
	}

	result.append("--parallel " + QString::number(NProcess->value()));
	if (Idle->isChecked()) result.append("--idle");
	if (Recursive->isChecked()) result.append("--recursive");
	if (VideoFiles->isChecked()) result.append("--videofiles");
	if (SubtitlesFiles->isChecked()) result.append("--subfiles");
	if (CustomFileExt->text() != "") {
		QStringList Extensions = CustomFileExt->text().split(",");
		for (int i = 0; i < Extensions.count(); ++i) {
			result.append("-f");
			result.append("\"" + Extensions.at(i) + "\"");
		}
	}
	if (IgnoreFail->isChecked()) result.append("--no_fail");
	if (ShowCMD->isChecked()) result.append("--cmd");
	if (ShowOutput->isChecked()) result.append("--show_output");
	if (Quiet->isChecked()) result.append("--quiet");
	if (ExternalProgram->text() != "") {
		result.append("--command");
		if (ExternalProgram->text().contains(" "))
			result.append("\"" + ExternalProgram->text() + "\"");
		else
			result.append(ExternalProgram->text());
	}
	if (Watch->isChecked() || Time->value() > 0) {
		if (Watch->isChecked()) result.append("--watch");
		if (Time->value() > 0) result.append("--time " + QString::number(Time->value()));
		if (Delete->value() > 0) result.append("--delete " + QString::number(Delete->value()));
		if (Modified->value() > 0) result.append("--modified " + QString::number(Modified->value()));
		if (Recheck->value() > 0) result.append("--recheck " + QString::number(Recheck->value()));
	}
	QString Commands = BatchCommand->getCommands();
	return Program + " --batch " + result.join(" ") + Commands;
}

void BatchFormWidget::SetCommand(QString Cmd) {
	CommandText->setText(Cmd);
}
