#ifndef CONFIGURATIONWIDGET_H
#define CONFIGURATIONWIDGET_H

#include <QTabWidget>
#include <QHash>
#include "../paramssettings.h"
#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QFormLayout>

class ConfigurationWidget : public QTabWidget {
		Q_OBJECT
	public:
		explicit ConfigurationWidget(ParamsSettings *ps, QWidget *parent = nullptr);
		explicit ConfigurationWidget(QString ConfigName, QWidget *parent = nullptr);

		ParamsSettings *PS;

		QHash<QString, int> Tabs;
		QHash<QString, QWidget *> TabsWidgets;
		QList<ParamsSettings::help_t> Options;
		QStringList NewUserDefinedList;

	private:
		void Load();

		QFormLayout *UserDefinedLayout;
		QWidget *UserDefinedWidget;

		QWidget *AddButtonsWidget;
		QHBoxLayout *AddButtonsLayout;
		QPushButton *AddVar;
		QPushButton *AddVarStream;
		QPushButton *AddVarVideo;
		QPushButton *AddVarAudio;
		QPushButton *AddVarSub;
		QPushButton *AddVarAttach;

		void AddUserGeneric(ParamsSettings::context_type Context, QString HelpVar);

	signals:
		void AddRemoveCustom(QString Param);
		void UpdateCustom(QString Param, ParamsSettings::context_type Context, QString Name, QString Value);

	private slots:
		void OptionChanged(QString Param, QString Value);
		void UserDefinedChanged(QString Param, ParamsSettings::context_type Context, QString Name, QString Value);
		void UserDefinedDelete(QString Param, ParamsSettings::context_type Context, QWidget *Object);
		void UserDefinedNewChanged(QString Param, ParamsSettings::context_type Context, QString Name, QString Value);
		void AddUserDefinedClick();
		void AddUserStreamDefinedClick();
		void AddUserVideoDefinedClick();
		void AddUserAudioDefinedClick();
		void AddUserSubDefinedClick();
		void AddUserAttachDefinedClick();

	public slots:
		void HideShowDefaultValueOptions();
};

#endif // CONFIGURATIONWIDGET_H
