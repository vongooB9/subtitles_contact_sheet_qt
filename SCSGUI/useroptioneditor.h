#ifndef USEROPTIONEDITOR_H
#define USEROPTIONEDITOR_H

#include <QWidget>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QStringList>
#include <QPlainTextEdit>
#include "../paramssettings.h"

class UserOptionEditor : public QWidget {
		Q_OBJECT
	public:
		explicit UserOptionEditor(QString ParamName, ParamsSettings::context_type Context, QWidget *parent = nullptr);
		QString ParamName;
		ParamsSettings::context_type Context;
		QHBoxLayout *Layout;

		QWidget *VarWidget;
		QHBoxLayout *VarLayout;
		QLineEdit *VarName;
		QLabel *EqualLabel;
		QLineEdit *VarValue;
		QPlainTextEdit *VarText;
		QPushButton *TextTougle;

		QLineEdit *RawEditor;
		QPushButton *RawTougle;
		QPushButton *Delete;

		bool RawBlock;
		bool ValueBlock;
		bool TextBlock;

		void SetValue(QString Value);
		void SetValue(QString Name, QString Param);
		void RawVisivility(bool Visible);

	signals:
		void Changed(QString Param, ParamsSettings::context_type Context, QString Name, QString Value);
		void Deleted(QString Param, ParamsSettings::context_type Context, QWidget *Object);

	public slots:
		void RawTougleChange(bool Checked);
		void ValueChanged();
		void TextChanged();
		void RawChange();
		void DeleteClick();
		void TougleText(bool Checked = false);
};

#endif // USEROPTIONEDITOR_H
