#include "batchcommandswidget.h"
#include "batchowncommandwidget.h"
#include "batchexternalcommandwidget.h"

BatchCommandsWidget::BatchCommandsWidget(QStringList *Configs, QWidget *parent)
	: QWidget{parent}, Count(0) {
	AvailableConfigs = Configs;

	MainLayout = new QVBoxLayout(this);
	MainLayout->setContentsMargins(0, 0, 0, 0);

	CommandsGroup = new QGroupBox("Commands", this);
	CommandsLayout = new QVBoxLayout(CommandsGroup);
	MainLayout->addWidget(CommandsGroup);

	ButtonsWidget = new QWidget(this);
	ButtonsLayout = new QHBoxLayout(ButtonsWidget);
	ButtonsLayout->setContentsMargins(0, 0, 0, 0);
	MainLayout->addWidget(ButtonsWidget);

	AddButton = new QPushButton("Add Command", this);
	ButtonsLayout->addWidget(AddButton);
	AddExternalButton = new QPushButton("Add External Command", this);
	ButtonsLayout->addWidget(AddExternalButton);

	connect(AddButton, &QPushButton::clicked, this, &BatchCommandsWidget::AddCommandClick);
	connect(AddExternalButton, &QPushButton::clicked, this, &BatchCommandsWidget::AddExternalClick);
}

void BatchCommandsWidget::SetCommand(QStringList Commands, bool External) {
	Clear();

	QStringList WidgetCommands;
	for (int i = 0; i < Commands.count(); ++i) {
		QString Command = Commands.at(i);
		if (Command == "--") {
			AddCommand(&WidgetCommands, External);
			WidgetCommands.clear();
		} else {
			WidgetCommands.append(Command);
		}
	}
	AddCommand(&WidgetCommands, External);
}

QString BatchCommandsWidget::getCommands() {
	QString Commands = "";
	for (int i = 0; i < Items.count(); ++i) {
		if (QString(Items.at(i)->metaObject()->className()) == "BatchOwnCommandWidget") {
			BatchOwnCommandWidget *Item = static_cast<BatchOwnCommandWidget *>(Items.at(i));
			Commands += " -- ";
			Commands += Item->GetCommand();
		}
		if (QString(Items.at(i)->metaObject()->className()) == "BatchExternalCommandWidget") {
			BatchExternalCommandWidget *Item = static_cast<BatchExternalCommandWidget *>(Items.at(i));
			Commands += " -- ";
			Commands += Item->GetCommand();
		}
	}
	return Commands;
}

void BatchCommandsWidget::AddCommand(QStringList *Commands, bool External) {
	if (Commands == nullptr) return;
	if (Commands->contains("--command") || External) {
		AddExternalCommand(Commands);
	} else {
		AddOwnCommand(Commands);
	}
}

void BatchCommandsWidget::AddOwnCommand(QStringList *Commands) {
	BatchOwnCommandWidget *Command = new BatchOwnCommandWidget(AvailableConfigs, CommandsGroup);
	Command->setObjectName("Command_" + QString::number(++Count));
	if (Commands != nullptr) Command->SetCommand(Commands);
	CommandsLayout->addWidget(Command);
	Items.append(Command);
	connect(Command, &BatchOwnCommandWidget::Deleted, this, &BatchCommandsWidget::DeletedItem);
	connect(Command, &BatchOwnCommandWidget::Changed, this, &BatchCommandsWidget::ItemChanged);
}

void BatchCommandsWidget::AddExternalCommand(QStringList *Commands) {
	BatchExternalCommandWidget *Command = new BatchExternalCommandWidget(CommandsGroup);
	Command->setObjectName("Command_" + QString::number(++Count));
	if (Commands != nullptr) Command->SetCommand(Commands);
	CommandsLayout->addWidget(Command);
	Items.append(Command);
	connect(Command, &BatchExternalCommandWidget::Deleted, this, &BatchCommandsWidget::DeletedItem);
	connect(Command, &BatchExternalCommandWidget::Changed, this, &BatchCommandsWidget::ItemChanged);
}

void BatchCommandsWidget::AddCommandClick() {
	AddOwnCommand();
	emit Changed();
}

void BatchCommandsWidget::AddExternalClick() {
	AddExternalCommand();
	emit Changed();
}

void BatchCommandsWidget::DeletedItem(QWidget *Item) {
	Items.removeOne(Item);
	CommandsLayout->removeWidget(Item);
	delete Item;
	emit Changed();
}

void BatchCommandsWidget::Clear() {
	while (Items.count() > 0) {
		QWidget *item = Items.takeLast();
		CommandsLayout->removeWidget(item);
		delete item;
	}
	Items.clear();
	Count = 0;
	emit Changed();
}

void BatchCommandsWidget::ItemChanged() {
	emit Changed();
}

void BatchCommandsWidget::SetExternalState(bool Enable) {
	AddButton->setEnabled(!Enable);
}
