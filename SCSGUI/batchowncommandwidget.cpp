#include "batchowncommandwidget.h"
#include <QApplication>
#include "paramsdialog.h"
#include "../paramssettings.h"
#include "configurationdialog.h"
#include <QInputDialog>
#include <QMessageBox>

BatchOwnCommandWidget::BatchOwnCommandWidget(QStringList *Configs, QWidget *parent)
	: QWidget{parent} {
	AvailableConfigs = Configs;
	Layout = new QHBoxLayout(this);
	Config = new QComboBox(this);
	Edit = new QPushButton("Edit", this);
	New = new QPushButton("New", this);
	CustomParameters = new QLineEdit(this);
	Config->setSizeAdjustPolicy(QComboBox::AdjustToContents);
	Config->addItems(*AvailableConfigs);
	EditParamsButton = new QPushButton("Edit", this);
	DeleteButton = new QPushButton(QApplication::style()->standardPixmap(QStyle::SP_TrashIcon), "", this);

	Layout->addWidget(Config);
	Layout->addWidget(Edit);
	Layout->addWidget(New);
	Layout->addWidget(CustomParameters);
	Layout->addWidget(EditParamsButton);
	Layout->addWidget(DeleteButton);

	connect(DeleteButton, &QPushButton::clicked, this, &BatchOwnCommandWidget::RemoveClick);
	connect(EditParamsButton, &QPushButton::clicked, this, &BatchOwnCommandWidget::EditClick);
	connect(Config, &QComboBox::currentTextChanged, this, &BatchOwnCommandWidget::EmitChanged);
	connect(CustomParameters, &QLineEdit::textChanged, this, &BatchOwnCommandWidget::EmitChanged);

	connect(Edit, &QPushButton::clicked, this, &BatchOwnCommandWidget::ConfigEditClick);
	connect(New, &QPushButton::clicked, this, &BatchOwnCommandWidget::ConfigNewClick);
}

QString BatchOwnCommandWidget::GetCommand() {
	QString Commands = "";
	if (Config->currentText() != "") {
		if (Commands != "") Commands += " ";
		Commands += "--config " + Config->currentText();
	}
	if (CustomParameters->text() != "") {
		if (Commands != "") Commands += " ";
		Commands += CustomParameters->text();
	}
	return Commands;
}

void BatchOwnCommandWidget::SetCommand(QStringList *parameters) {
	QString text = "";
	while (!parameters->empty()) {
		QString parameter = parameters->takeFirst();
		if (parameter == "--config") {
			if (parameters->empty()) break;
			QString config = parameters->takeFirst();
			Config->setCurrentText(config);
		} else {
			if (text != "") text += " ";
			text += parameter;
		}
	}
	CustomParameters->setText(text);
}

void BatchOwnCommandWidget::ConfigEditClick() {
	QString CurConfig = Config->currentText();
	if (CurConfig == "") CurConfig = "config";
	ConfigurationDialog *CD = new ConfigurationDialog(CurConfig, this);
	CD->exec();
}

void BatchOwnCommandWidget::ConfigNewClick() {
	bool ok;
	QString New = QInputDialog::getText(this, "New Config", "Select new config name", QLineEdit::Normal, "", &ok);
	if (!ok) return;
	if (New == "") {
		QMessageBox::critical(this, "Error creating config file", "The name cannot be empty");
		return;
	}
	if (AvailableConfigs->contains(New)) {
		QMessageBox::critical(this, "Error creating config file", "The name is already in use");
		return;
	}
	ConfigurationDialog *CD = new ConfigurationDialog(New, this);
	if (CD->exec() == QDialog::Accepted) {
		AvailableConfigs->append(New);
		AvailableConfigs->sort();
		Config->clear();
		Config->addItems(*AvailableConfigs);
		Config->setCurrentText(New);
	}
}

void BatchOwnCommandWidget::RemoveClick() {
	emit Deleted(this);
}

void BatchOwnCommandWidget::EmitChanged() {
	emit Changed();
}

void BatchOwnCommandWidget::EditClick() {
	QString Program;
	ParamsSettings *PS = new ParamsSettings(QApplication::applicationDirPath(), this);
	ParamsDialog *Dia = new ParamsDialog(Program, AvailableConfigs, PS, this);
	Dia->SetParams(CustomParameters->text());
	if (Dia->exec() == QDialog::Accepted) {
		CustomParameters->setText(Dia->GetParams());
	}
}
