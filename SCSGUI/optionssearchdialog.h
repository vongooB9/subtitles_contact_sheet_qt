#ifndef OPTIONSSEARCHDIALOG_H
#define OPTIONSSEARCHDIALOG_H

#include <QDialog>
#include "optionssearch.h"
#include "../paramssettings.h"
#include <QVBoxLayout>

class OptionsSearchDialog : public QDialog {
		Q_OBJECT
	public:
		OptionsSearchDialog(ParamsSettings *ps, QWidget *parent = nullptr, const Qt::WindowFlags &f = Qt::WindowFlags());

		QList<ParamsSettings::help_t> *GetReturnList();

	private:
		ParamsSettings *PS;
		OptionsSearch *OS;
		QVBoxLayout *Layout;
		QList<ParamsSettings::help_t> ReturnList;

	public slots:
		void AceptedList();
		void CanceledList();

};

#endif // OPTIONSSEARCHDIALOG_H
