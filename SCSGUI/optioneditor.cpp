#include "optioneditor.h"
#include <QFileDialog>
#include <QColor>
#include <QColorDialog>
#include "../colors.h"
#include <QSizePolicy>

OptionEditor::OptionEditor(QString ParamName, ParamsSettings::params_t param, ParamsSettings::help_t help,
						   QWidget *parent)
	: QWidget{parent}, ParamName(ParamName) {
	Param = param;
	Help = help;
	Layout = new QHBoxLayout(this);
	Layout->setContentsMargins(0, 0, 0, 0);

	switch (Param.Type) {
		case ParamsSettings::param_font:
			FontCombo = new QFontComboBox(this);
			FontCombo->setCurrentText(Param.Value);
			FontCombo->setToolTip(Help.GetTip());
			Layout->addWidget(FontCombo);
			InsertRawEditors(FontCombo, true);
			if (Param.Function != "") RawTougle->setChecked(true);
			connect(FontCombo, &QFontComboBox::currentTextChanged, this, &OptionEditor::ValueChanged);
			break;
		case ParamsSettings::param_color:
			LineEditor = new QLineEdit(this);
			LineEditor->setText(Param.Value);
			LineEditor->setToolTip(Help.GetTip());;
			Layout->addWidget(LineEditor);
			ColorPreview = new QFrame(this);
			ColorPreview->setFrameStyle(QFrame::Panel | QFrame::Raised);
			ColorPreview->setMinimumSize(40, 20);
			if (Param.Value != "")
				ColorPreview->setStyleSheet("background-color: #" + StringColor2RGB(Param.Value).left(6));
			Layout->addWidget(ColorPreview);
			SelectButton = new QPushButton("Select", this);
			Layout->addWidget(SelectButton);
			connect(SelectButton, &QPushButton::clicked, this, &OptionEditor::SelectColorClick);
			connect(LineEditor, &QLineEdit::textChanged, this, &OptionEditor::ChangeColor);
			connect(LineEditor, &QLineEdit::textChanged, this, &OptionEditor::ValueChanged);
			break;
		case ParamsSettings::param_file:
			LineEditor = new QLineEdit(this);
			LineEditor->setText(Param.Value);
			LineEditor->setToolTip(Help.GetTip());;
			Layout->addWidget(LineEditor);
			SelectButton = new QPushButton("Select", this);
			Layout->addWidget(SelectButton);
			connect(SelectButton, &QPushButton::clicked, this, &OptionEditor::SelectFile);
			connect(LineEditor, &QLineEdit::textChanged, this, &OptionEditor::ValueChanged);
			break;
		case ParamsSettings::param_dir:
			LineEditor = new QLineEdit(this);
			LineEditor->setText(Param.Value);
			LineEditor->setToolTip(Help.GetTip());;
			Layout->addWidget(LineEditor);
			SelectButton = new QPushButton("Select", this);
			Layout->addWidget(SelectButton);
			connect(SelectButton, &QPushButton::clicked, this, &OptionEditor::SelectDir);
			connect(LineEditor, &QLineEdit::textChanged, this, &OptionEditor::ValueChanged);
			break;
		case ParamsSettings::param_string:
			if (Param.ValidValues.contains("|")) {
				Combo = new QComboBox(this);
				Combo->setSizeAdjustPolicy(QComboBox::AdjustToContents);
				QStringList Values = Param.ValidValues.split("|");
				while (!Values.isEmpty()) {
					QString Value = Values.takeFirst();
					if (Value == "raw") {
						Combo->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
						Combo->setEditable(true);
						continue;
					}
					Combo->addItem(Value);
				}
				Combo->setCurrentText(Param.Value);
				Combo->setToolTip(Help.GetTip());;
				Layout->addWidget(Combo);
				if (!Combo->isEditable()) {
					InsertRawEditors(Combo, true);
					if (Param.Function != "") RawTougle->setChecked(true);
				}
				connect(Combo, &QComboBox::currentTextChanged, this, &OptionEditor::ValueChanged);
			} else {
				LineEditor = new QLineEdit(this);
				LineEditor->setText(Param.Value);
				LineEditor->setToolTip(Help.GetTip());;
				Layout->addWidget(LineEditor);
				connect(LineEditor, &QLineEdit::textChanged, this, &OptionEditor::ValueChanged);
			}
			break;
		case ParamsSettings::param_layout:
			EditLayout = new QWidget(this);
			LayoutLayout = new QHBoxLayout(EditLayout);
			LayoutLayout->setContentsMargins(0, 0, 0, 0);
			EditLayout->setLayout(LayoutLayout);
			LayoutC = new QSpinBox(EditLayout);
			LayoutC->setMinimum(1);
			LayoutC->setMaximum(256);
			LayoutR = new QSpinBox(EditLayout);
			LayoutR->setMaximum(256);
			LayoutLabel = new QLabel("x", this);
			LayoutC->setValue(Param.Value.split("x").first().toUInt());
			LayoutR->setValue(Param.Value.split("x").last().toUInt());
			LayoutC->setToolTip(Help.GetTip());;
			LayoutR->setToolTip(Help.GetTip());;
			LayoutLayout->addWidget(LayoutC);
			LayoutLayout->addWidget(LayoutLabel);
			LayoutLayout->addWidget(LayoutR);
			Layout->addWidget(EditLayout);
			InsertRawEditors(EditLayout, true);
			if (Param.Function != "") RawTougle->setChecked(true);
			connect(LayoutC, &QSpinBox::textChanged, this, &OptionEditor::ValueChanged);
			connect(LayoutR, &QSpinBox::textChanged, this, &OptionEditor::ValueChanged);
			break;
		case ParamsSettings::param_int:
			if (Param.ValidValues.contains("|")) {
				Combo = new QComboBox(this);
				Combo->setSizeAdjustPolicy(QComboBox::AdjustToContents);
				QStringList Values = Param.ValidValues.split("|");
				QString CurText = "";
				while (!Values.isEmpty()) {
					QString Value = Values.takeFirst();
					QString Val = Value.section("=", 0, 0);
					QString Text = Value.section("=", 1, 1);
					Combo->addItem(Text, Val);
					if (Param.Value == Val) CurText = Text;
				}
				Combo->setCurrentText(CurText);
				Combo->setToolTip(Help.GetTip());;
				Layout->addWidget(Combo);
				connect(Combo, &QComboBox::currentTextChanged, this, &OptionEditor::ValueChanged);
				InsertRawEditors(Combo, true);
			} else {
				SpinEditor = new QSpinBox(this);
				SpinEditor->setMinimum(INT_MIN);
				SpinEditor->setMaximum(INT_MAX);
				if (Param.ValidValues != "") {
					QStringList Values = Param.ValidValues.split(",");
					while (!Values.isEmpty()) {
						QString Value = Values.takeFirst();
						QString Option = Value.section("=", 0, 0);
						QString Val = Value.section("=", 1, 1);
						if (Option == "min") {
							SpinEditor->setMinimum(Val.toInt());
						}
						if (Option == "max") {
							SpinEditor->setMaximum(Val.toInt());
						}
					}
				}

				SpinEditor->setValue(Param.Value.toInt());
				SpinEditor->setToolTip(Help.GetTip());;
				Layout->addWidget(SpinEditor);
				connect(SpinEditor, &QSpinBox::textChanged, this, &OptionEditor::ValueChanged);
				InsertRawEditors(SpinEditor, true);
			}
			if (Param.Function != "") RawTougle->setChecked(true);
			break;
		case ParamsSettings::param_bool:
			CheckEditor = new QCheckBox(this);
			if (Param.Value == "1")
				CheckEditor->setCheckState(Qt::Checked);
			else
				CheckEditor->setCheckState(Qt::Unchecked);
			CheckEditor->setToolTip(Help.GetTip());;
			Layout->addWidget(CheckEditor);
			InsertRawEditors(CheckEditor, true);
			if (Param.Function != "") RawTougle->setChecked(true);
			#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
			connect(CheckEditor, &QCheckBox::checkStateChanged, this, &OptionEditor::ValueChanged);
			#else
			connect(CheckEditor, &QCheckBox::stateChanged, this, &OptionEditor::ValueChanged);
			#endif
			break;
		case ParamsSettings::param_shortcut:
		case ParamsSettings::param_none:
			break;
	}
}

QString OptionEditor::GetText() {
	return Param.Value;
}

void OptionEditor::InsertRawEditors(QWidget *normaleditor, bool spacer) {
	NormalEditor = normaleditor;
	RawEditor = new QLineEdit(this);
	RawEditor->setVisible(false);
	RawEditor->setText(Param.Value);
	RawEditor->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	Layout->addWidget(RawEditor);
	if (spacer) {
		Spacer = new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum);
		Layout->addSpacerItem(Spacer);
	} else {
		Spacer = nullptr;
	}
	RawTougle = new QPushButton("Show Raw", this);
	RawTougle->setCheckable(true);
	connect(RawTougle, &QPushButton::toggled, this, &OptionEditor::RawTougleChange);
	Layout->addWidget(RawTougle);
	connect(RawEditor, &QLineEdit::textChanged, this, &OptionEditor::RawChange);
}

void OptionEditor::RawTougleChange(bool Checked) {
	setUpdatesEnabled(false);
	if (Checked) {
		if (Param.UserDefined) {
			if (Param.Function != "")
				RawEditor->setText(Param.UserName + "=" + Param.Function);
			else
				RawEditor->setText(Param.UserName + "=" + Param.Value);
		} else {
			RawEditor->setText(Param.Value);
		}
	} else {
		switch (Param.Type) {
			case ParamsSettings::param_font:
				FontCombo->setCurrentText(Param.Value);
				break;
			case ParamsSettings::param_color:
			case ParamsSettings::param_file:
			case ParamsSettings::param_dir:
				LineEditor->setText(Param.Value);
				break;
			case ParamsSettings::param_string:
				if (Param.ValidValues.contains("|"))
					Combo->setCurrentText(Param.Value);
				else
					LineEditor->setText(Param.Value);
				break;
			case ParamsSettings::param_layout:
				LayoutC->setValue(Param.Value.split("x").first().toUInt());
				LayoutR->setValue(Param.Value.split("x").last().toUInt());
				break;
			case ParamsSettings::param_int:
				if (Param.ValidValues.contains("|")) {
					QStringList Values = Param.ValidValues.split("|");
					QString Text = "";
					while (!Values.isEmpty()) {
						QString Value = Values.takeFirst();
						QString Val = Value.section("=", 0, 0);
						Text = Value.section("=", 1, 1);
						if (Param.Value == Val) break;
					}
					Combo->setCurrentText(Text);
				} else {
					SpinEditor->setValue(Param.Value.toInt());
				}
				break;
			case ParamsSettings::param_bool:
				if (RawEditor->text() == "0" || RawEditor->text() == "")
					CheckEditor->setCheckState(Qt::Unchecked);
				else
					CheckEditor->setCheckState(Qt::Checked);
				break;
			case ParamsSettings::param_shortcut:
			case ParamsSettings::param_none:
				break;
		}
	}
	if (Spacer != nullptr) {
		if (Checked)
			Layout->removeItem(Spacer);
		else
			Layout->insertSpacerItem(1, Spacer);
	}
	RawEditor->setVisible(Checked);
	NormalEditor->setVisible(!Checked);
	setUpdatesEnabled(true);
}

void OptionEditor::SelectFile() {
	QString OpenExtentions = "";
	QStringList ValidExtensions = Param.ValidValues.split(",");
	for (int i = 0; i < ValidExtensions.count(); ++i) {
		if (ValidExtensions.at(i) == "") continue;
		#ifndef Q_OS_WIN
		if (ValidExtensions.at(i) == "exe") continue;
		#endif
		if (OpenExtentions != "") OpenExtentions += " ";
		OpenExtentions += "*." + ValidExtensions.at(i);
	}
	if (OpenExtentions != "") {
		OpenExtentions = "Suported files (" + OpenExtentions + ");;";
	}
	OpenExtentions += "All Files (*)";

	QString File = QFileDialog::getOpenFileName(this,
				   "Select File for " + Help.Name,
				   "",
				   OpenExtentions);
	if (File != "") LineEditor->setText(QDir::toNativeSeparators(File));
}

void OptionEditor::SelectDir() {
	QString Dir = QFileDialog::getExistingDirectory(this,
				  "Select Directory for " + Help.Name,
				  "",
				  QFileDialog::ShowDirsOnly
				  | QFileDialog::DontResolveSymlinks);
	if (Dir != "") LineEditor->setText(QDir::toNativeSeparators(Dir));
}

void OptionEditor::SelectColorClick() {
	QString Color = SelectColor("Select Color", LineEditor->text());
	if (Color != LineEditor->text()) LineEditor->setText(Color);
}

void OptionEditor::ChangeColor() {
	ColorPreview->setStyleSheet("background-color: #" + Color2RGB(String2Color(LineEditor->text())));
}

void OptionEditor::ValueChanged() {
	switch (Param.Type) {
		case ParamsSettings::param_font:
			Param.Value = FontCombo->currentText();
			break;
		case ParamsSettings::param_color:
		case ParamsSettings::param_file:
		case ParamsSettings::param_dir:
			Param.Value = LineEditor->text();
			break;
		case ParamsSettings::param_string:
			if (Param.ValidValues.contains("|")) {
				Param.Value = Combo->currentText();
			} else {
				Param.Value = LineEditor->text();
			}
			break;
		case ParamsSettings::param_layout:
			Param.Value = LayoutC->text() + "x" + LayoutR->text();
			break;
		case ParamsSettings::param_int:
			if (Param.ValidValues.contains("|")) {
				Param.Value = Combo->currentData().toString();
			} else {
				Param.Value = QString::number(SpinEditor->value());
			}
			break;
		case ParamsSettings::param_bool:
			if (CheckEditor->isChecked())
				Param.Value = "1";
			else
				Param.Value = "0";
			break;
		case ParamsSettings::param_shortcut:
		case ParamsSettings::param_none:
			break;
	}
	emit Changed(Help.Param, Param.Value);
}

void OptionEditor::RawChange() {
	Param.Value = RawEditor->text();
	emit Changed(Help.Param, Param.Value);
}

QString OptionEditor::SelectColor(QString Text, QString CurrentColor) {
	color_t IntColor = String2Color(CurrentColor);
	QColor Current;
	Current.setRed(IntColor.R);
	Current.setGreen(IntColor.G);
	Current.setBlue(IntColor.B);
	Current.setAlpha(IntColor.A);

	QColor Color = QColorDialog::getColor(Current, this, Text, QColorDialog::ShowAlphaChannel);
	if (!Color.isValid()) return CurrentColor;
	int r, g, b, a;
	Color.getRgb(&r, &g, &b, &a);
	QString ColorStr = ColorChannel2Hex(r) + ColorChannel2Hex(g) + ColorChannel2Hex(b);
	if (a != 255) ColorStr += ColorChannel2Hex(a);
	return ColorStr;
}
