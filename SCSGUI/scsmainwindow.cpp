#include "scsmainwindow.h"
#include <QProcess>
#include <QApplication>
#include <QDir>
#include <QFileInfo>
#include <QRegularExpression>
#include <QIcon>
#include <QFileDialog>
#include "configffmpegdialog.h"
#include <QInputDialog>
#include <QMessageBox>
#include <QStandardPaths>
#include "newtabdialog.h"

SCSMainWindow::SCSMainWindow(QString OpenFile, QWidget *parent)
	: QMainWindow{parent} {

	setWindowTitle("Subtitles Contact Sheet GUI " + QString(SCS_VERSION));
	QIcon icon;
	icon.addFile(QString::fromUtf8(":/SCS.ico"), QSize(), QIcon::Normal, QIcon::Off);
	setWindowIcon(icon);

	#ifndef PORTABLE_CONFIG
	//	Settings = new QSettings("subtitles_contact_sheet_GUI", "config", this);
	QString GUIConfigFile = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + QDir::separator() +
							"subtitles_contact_sheet_GUI" + QDir::separator() + "config.ini";
	Settings = new QSettings(GUIConfigFile, QSettings::IniFormat, this);
	#else
	QString GUIConfigFile = QApplication::applicationDirPath() + QDir::separator() + "GUI" + QDir::separator() +
							"config.ini";
	Settings = new QSettings(GUIConfigFile, QSettings::IniFormat, this);
	#endif

	Program = Settings->value("Program", "").toString();

	if (Program == "") {
		#ifdef Q_OS_WIN
		Program = QDir::toNativeSeparators(QApplication::applicationDirPath() + QDir::separator() +
										   "subtitles_contact_sheet.exe");
		if (!QFileInfo::exists(Program)) Program = QDir::toNativeSeparators(QApplication::applicationDirPath() +
					QDir::separator() + "scs.exe");
		#else
		Program = QDir::toNativeSeparators(QApplication::applicationDirPath() + QDir::separator() +
										   "subtitles_contact_sheet");
		if (!QFileInfo::exists(Program)) Program = QDir::toNativeSeparators(QApplication::applicationDirPath() +
					QDir::separator() + "scs");
		if (!QFileInfo::exists(Program)) Program = "subtitles_contact_sheet";
		#endif
	}

	ReadConfigList();

	if (ProgramFailed) {
		#ifdef Q_OS_WIN
		QString FileType = "Exe Files (*.exe)";
		#else
		QString FileType = "All Files (*)";
		#endif

		Program = QFileDialog::getOpenFileName(this,
											   "Select subtitles_contact_sheet executable",
											   "",
											   FileType);
		Settings->setValue("Program", Program);
		ReadConfigList();
	}
	static QRegularExpression EscapeCaracters("[^a-zA-Z0-9,._+:@\\%/\\-]");
	if (Program.contains(EscapeCaracters)) Program = "\"" + Program + "\"";

	PS = new ParamsSettings(QApplication::applicationDirPath(), this);
	PS->SetConfig("config");
	PS->ReadFromConfigFile();

	if (!PS->NotEmpty("ffmpeg") || !PS->NotEmpty("ffprobe"))
		LaunchConfigFFmpeg();

	CentralWidget = new QWidget(this);
	CentralLayout = new QVBoxLayout(CentralWidget);
	CentralWidget->setLayout(CentralLayout);
	setCentralWidget(CentralWidget);

	Clipboard = QGuiApplication::clipboard();

	Tabs = new QTabWidget(CentralWidget);

	Normal = new ParamsWidget(Program, &AvailableConfigs, PS, Tabs);
	Normal->setObjectName("NormalTabCommand");
	QString NormalTabCommand = Settings->value("NormalTabCommand",
							   Program + " --video \"\" --config config --layout 3x7 --size 640 --process 2 "
							   "--start 30 --end -15 --only_keyframes 0").toString();
	Normal->SetCommand(NormalTabCommand);
	Tabs->addTab(Normal, "Normal");

	Subtitles = new ParamsWidget(Program, &AvailableConfigs, PS, Tabs);
	Subtitles->setObjectName("SubtitlesTabCommand");
	QString SubtitlesTabCommand = Settings->value("SubtitlesTabCommand",
								  Program + " --video \"\" --sub \"\" --config config --mode time --min 4 --start 30 "
								  "--end 15 --lang en --sub_font \"DejaVu Sans\" --sub_size 16 --sub_color white").toString();
	Subtitles->SetCommand(SubtitlesTabCommand);
	Tabs->addTab(Subtitles, "Subtitles");

	Images = new ParamsWidget(Program, &AvailableConfigs, PS, Tabs);
	Images->setObjectName("ImagesTabCommand");
	QString ImagesTabCommand = Settings->value("ImagesTabCommand",
							   Program + " --images \"\" --config config --layout 3x7 --images_recursive 0 --images_resolution_stats 0 --images_text 1").toString();
	Images->SetCommand(ImagesTabCommand);
	Tabs->addTab(Images, "Images");

	VR = new ParamsWidget(Program, &AvailableConfigs, PS, Tabs);
	VR->setObjectName("VRTabCommand");
	QString VRTabCommand = Settings->value("VRTabCommand",
										   Program + " --video \"\" --config config --layout 3x7 --size 960 --vr 1 "
										   "--vr_only_crop 1 --vr_in hequirect --vr_in_stereo sbs --vr_ih_fov 180 --vr_iv_fov 180 "
										   "--vr_d_fov 100 --vr_right_eye 0").toString();
	VR->SetCommand(VRTabCommand);
	Tabs->addTab(VR, "VR");

	Gif = new ParamsWidget(Program, &AvailableConfigs, PS, Tabs);
	Gif->setObjectName("GifTabCommand");
	QString GifTabCommand = Settings->value("GifTabCommand",
											Program + " --video \"\" --config config --gif_clip 10").toString();
	Gif->SetCommand(GifTabCommand);
	Tabs->addTab(Gif, "Gif");

	Screenshots = new ParamsWidget(Program, &AvailableConfigs, PS, Tabs);
	Screenshots->setObjectName("ScreenshotsTabCommand");
	QString ScreenshotsTabCommand = Settings->value("ScreenshotsTabCommand",
									Program + " --video \"\" --config config --screenshots 10").toString();
	Screenshots->SetCommand(ScreenshotsTabCommand);
	Tabs->addTab(Screenshots, "Screenshost");

	BatchWidget = new BatchFormWidget(Program, &AvailableConfigs, Tabs);
	BatchWidget->setObjectName("BatchTabCommand");
	QString BatchCommand = Settings->value("BatchTabCommand", "").toString();
	if (BatchCommand != "") BatchWidget->SetCommand(BatchCommand);
	Tabs->addTab(BatchWidget, "Batch");

	DVD = new ParamsWidget(Program, &AvailableConfigs, PS, Tabs);
	DVD->setObjectName("NormalTabCommand");
	QString DVDTabCommand = Settings->value("DVDTabCommand",
											Program + " --dvd \"\" --dvd_title 0 --config config --layout 3x7 --size 640 --process 2 "
											"--start 30 --end -15 --only_keyframes 0 --aspect_ratio display").toString();
	DVD->SetCommand(DVDTabCommand);
	Tabs->addTab(DVD, "DVD");

	Tabs->setTabsClosable(true);
	NewTab = new QPushButton(QApplication::style()->standardPixmap(QStyle::SP_DirOpenIcon), "", Tabs);
	connect(NewTab, &QPushButton::clicked, this, &SCSMainWindow::NewTabClicked);
	Tabs->setCornerWidget(NewTab);

	for (int i = 0; i < Tabs->count(); ++i) {
		QWidget *button = Tabs->tabBar()->tabButton(i, QTabBar::RightSide);
		if (button == nullptr) button = Tabs->tabBar()->tabButton(i, QTabBar::LeftSide);
		if (button != nullptr) button->setDisabled(true);
	}

	Settings->beginGroup("CustomTabs");
	QStringList AllKeys = Settings->allKeys();
	for (int i = 0; i < AllKeys.count(); ++i) {
		QString nameraw = AllKeys.at(i);
		QString name = nameraw.section("_", 1);
		if (name.startsWith("Custom_")) {
			ParamsWidget *widget = new ParamsWidget(Program, &AvailableConfigs, PS, Tabs);
			widget->setObjectName(name);
			widget->SetCommand(Settings->value(nameraw, "").toString());
			Tabs->addTab(widget, name.section("_", 1));
		}
		if (name.startsWith("CustomBatch_")) {
			BatchFormWidget *widget = new BatchFormWidget(Program, &AvailableConfigs, Tabs);
			widget->setObjectName(name);
			widget->SetCommand(Settings->value(nameraw, "").toString());
			Tabs->addTab(widget, name.section("_", 1));
		}
	}
	Settings->endGroup();

	Log = new QPlainTextEdit(CentralWidget);
	Log->setVisible(false);
	ProgressWidget = new QWidget(CentralWidget);
	ProgressLayout = new QHBoxLayout(ProgressWidget);
	ProgressWidget->setLayout(ProgressLayout);
	CentralLayout->addWidget(Tabs);
	CentralLayout->addWidget(Log);
	CentralLayout->addWidget(ProgressWidget);

	Progress = new QProgressBar(ProgressWidget);
	ShowLog = new QPushButton("Show Log", ProgressWidget);
	ShowLog->setCheckable(true);
	connect(ShowLog, &QPushButton::toggled, Log, &QPlainTextEdit::setVisible);
	SaveSettings = new QPushButton("Save", ProgressWidget);
	connect(SaveSettings, &QPushButton::clicked, this, &SCSMainWindow::Save);
	Cancel = new QPushButton("Cancel", ProgressWidget);
	Cancel->setDisabled(true);
	connect(Cancel, &QPushButton::clicked, this, &SCSMainWindow::CancelClick);
	Start = new QPushButton("Start", ProgressWidget);
	connect(Start, &QPushButton::clicked, this, &SCSMainWindow::StartClick);
	CopyCMD  = new QPushButton("Copy Command", ProgressWidget);
	connect(CopyCMD, &QPushButton::clicked, this, &SCSMainWindow::CopyClick);
	ProgressLayout->addWidget(Progress);
	ProgressLayout->addWidget(ShowLog);
	ProgressLayout->addWidget(CopyCMD);
	ProgressLayout->addWidget(SaveSettings);
	ProgressLayout->addWidget(Cancel);
	ProgressLayout->addWidget(Start);

	connect(&Process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this,
			&SCSMainWindow::ProcessTerminate);
	connect(&Process, &QProcess::readyReadStandardOutput, this, &SCSMainWindow::ProcessReadData);
	connect(&Process, &QProcess::readyReadStandardError, this, &SCSMainWindow::ProcessErrorData);

	Settings->beginGroup("MainWindow");
	if (Settings->contains("CurrentTab"))
		Tabs->setCurrentIndex(Settings->value("CurrentTab").toInt());
	if (Settings->contains("size"))
		resize(Settings->value("size", QSize(400, 400)).toSize());
	if (Settings->contains("pos"))
		move(Settings->value("pos", QPoint(200, 200)).toPoint());
	if (Settings->value("maximized", "0").toBool())
		showMaximized();
	if (Settings->value("fullscreen", "0").toBool())
		showFullScreen();
	Settings->endGroup();

	if (OpenFile != "") {
		Normal->SetInputFile(OpenFile);
		Subtitles->SetInputFile(OpenFile);
		VR->SetInputFile(OpenFile);
		Gif->SetInputFile(OpenFile);
		Screenshots->SetInputFile(OpenFile);
	}

	Task = new TaskbarProgress(this);
	Task->EndProgress();

	connect(Tabs, &QTabWidget::currentChanged, this, &SCSMainWindow::TabChanged);
	connect(Tabs, &QTabWidget::tabCloseRequested, this, &SCSMainWindow::TabClose);
}

void SCSMainWindow::ReadConfigList() {
	ProgramFailed = false;
	QStringList params;
	params << "--list_config_gui";
	QProcess process;
	process.start(Program, params);
	if (!process.waitForFinished(-1)) {
		ProgramFailed = true;
		return;
	}
	if (process.exitCode() != 0 ) {
		ProgramFailed = true;
		return;
	}

	QString Data = process.readAll().trimmed();
	AvailableConfigs = Data.split("\n");
	AvailableConfigs.prepend("");
}

QString SCSMainWindow::GetCurrentTabCommand() {
	QWidget *Widget = Tabs->currentWidget();
	if (QString(Widget->metaObject()->className()) == "ParamsWidget") {
		ParamsWidget *CurParamsWidget = static_cast<ParamsWidget *>(Widget);
		return CurParamsWidget->GetCommand();
	} else if (QString(Widget->metaObject()->className()) == "BatchFormWidget") {
		BatchFormWidget *Batch = static_cast<BatchFormWidget *>(Widget);
		return Batch->GetCommand();
	}
	return "";
}

void SCSMainWindow::LaunchConfigFFmpeg() {
	ConfigFFmpegDialog *Dialog = new ConfigFFmpegDialog(this);
	Dialog->exec();
}

void SCSMainWindow::StartClick() {
	QString Command = GetCurrentTabCommand();
	if (Command == "") return;
	Command.replace(" \"\"", " \"_@_\"");
	QStringList Parameters = QProcess::splitCommand(Command);
	Parameters.takeFirst();
	for (int i = 0; i < Parameters.count(); ++i) {
		if (Parameters[i] == "_@_") Parameters[i] = "";
	}
	Parameters.prepend("--gui_progress");
	Process.start(Program, Parameters);
	Process.waitForStarted(-1);
	Start->setDisabled(true);
	Cancel->setDisabled(false);
}

void SCSMainWindow::CancelClick() {
	if (Process.state() != QProcess::NotRunning) Process.close();
	Progress->setValue(0);
	Start->setDisabled(false);
	Cancel->setDisabled(true);
}

void SCSMainWindow::ProcessTerminate(int exitCode, QProcess::ExitStatus exitStatus) {
	Progress->setValue(Progress->maximum());
	Task->EndProgress();
	Start->setDisabled(false);
	Cancel->setDisabled(true);
	if (exitStatus == QProcess::NormalExit && exitCode == 9) { //FFmpeg error
		LaunchConfigFFmpeg();
	}
}

void SCSMainWindow::ProcessReadData() {
	QString Data = Process.readAllStandardOutput();
	static QRegularExpression ProgressRegExp("^([0-9]+)/([0-9]+)$");
	QRegularExpressionMatch Match;
	#ifdef Q_OS_WIN
	QStringList Lines = Data.split("\r\n");
	#else
	QStringList Lines = Data.split("\n");
	#endif
	for (int i = 0; i < Lines.count(); ++i) {
		Match = ProgressRegExp.match(Lines.at(i));
		if (Match.hasMatch()) {
			int Current = Match.captured(1).toInt();
			int Total = Match.captured(2).toInt();
			Progress->setMaximum(Total);
			Progress->setValue(Current);
			Task->BeginProgress(Total, false);
			Task->Value(Current);
		} else {
			if (Lines.at(i) != "") Log->appendPlainText(Lines.at(i));
		}
	}
}

void SCSMainWindow::ProcessErrorData() {
	QString Data = Process.readAllStandardError();
	#ifdef Q_OS_WIN
	QStringList Lines = Data.split("\r\n");
	#else
	QStringList Lines = Data.split("\n");
	#endif
	for (int i = 0; i < Lines.count(); ++i) {
		if (Lines.at(i) != "") Log->appendHtml("<p style=color:red;>" + Lines.at(i) + "</p>");
	}
	ShowLog->setChecked(true);
}

void SCSMainWindow::NewTabClicked() {
	NewTabDialog *Dialog = new NewTabDialog(this);
	int ok = Dialog->exec();
	if (ok == QDialog::Rejected) return;
	QString New = Dialog->TabName->text();
	if (New == "") {
		QMessageBox::critical(this, "Error creating config file", "The name cannot be empty");
		return;
	}
	if (Dialog->NormalTab->isChecked()) {
		ParamsWidget *NewParams = new ParamsWidget(Program, &AvailableConfigs, PS, Tabs);
		NewParams->setObjectName("Custom_" + New);
		NewParams->SetCommand(Program + " --video \"\"");
		Tabs->addTab(NewParams, New);
		Tabs->setCurrentIndex(Tabs->count() - 1);

	} else if (Dialog->BatchTab->isChecked()) {
		BatchFormWidget *NewBatch = new BatchFormWidget(Program, &AvailableConfigs, Tabs);
		NewBatch->setObjectName("CustomBatch_" + New);
		Tabs->addTab(NewBatch, New);
		Tabs->setCurrentIndex(Tabs->count() - 1);
	}

	Dialog->deleteLater();
}

void SCSMainWindow::TabChanged(int index) {
	for (int i = 0; i < Tabs->count(); ++i) {
		QWidget *button = Tabs->tabBar()->tabButton(i, QTabBar::RightSide);
		if (button == nullptr) button = Tabs->tabBar()->tabButton(i, QTabBar::LeftSide);
		if (button != nullptr) button->setDisabled(!(i == index && (Tabs->widget(i)->objectName().startsWith("Custom_") ||
					Tabs->widget(i)->objectName().startsWith("CustomBatch_"))));
	}
}

void SCSMainWindow::TabClose(int index) {
	Settings->remove(Tabs->widget(index)->objectName());
	Tabs->removeTab(index);
}

void SCSMainWindow::Save() {
	Settings->setValue("NormalTabCommand", Normal->GetCommand());
	Settings->setValue("SubtitlesTabCommand", Subtitles->GetCommand());
	Settings->setValue("ImagesTabCommand", Images->GetCommand());
	Settings->setValue("VRTabCommand", VR->GetCommand());
	Settings->setValue("GifTabCommand", Gif->GetCommand());
	Settings->setValue("ScreenshotsTabCommand", Screenshots->GetCommand());
	Settings->setValue("DVDTabCommand", DVD->GetCommand());
	Settings->setValue("BatchTabCommand", BatchWidget->GetCommand());

	Settings->beginGroup("CustomTabs");

	QStringList AllKeys = Settings->allKeys();
	foreach (QString var, AllKeys)
		Settings->remove(var);

	for (int i = 0; i < Tabs->count(); ++i) {
		QWidget *widget = Tabs->widget(i);
		if ((!widget->objectName().startsWith("Custom_")) &&
			(!widget->objectName().startsWith("CustomBatch_"))) continue;
		if (QString(widget->metaObject()->className()) == "ParamsWidget") {
			ParamsWidget *pw = static_cast<ParamsWidget *>(widget);
			Settings->setValue(QString::number(i).rightJustified(3, '0') + "_" + widget->objectName(), pw->GetCommand());
		}
		if (QString(widget->metaObject()->className()) == "BatchFormWidget") {
			BatchFormWidget *pw = static_cast<BatchFormWidget *>(widget);
			Settings->setValue(QString::number(i).rightJustified(3, '0') + "_" + widget->objectName(), pw->GetCommand());
		}
	}
	Settings->endGroup();

	Settings->beginGroup("MainWindow");
	Settings->setValue("CurrentTab", Tabs->currentIndex());
	if (!isFullScreen() && !isMaximized()) {
		Settings->setValue("size", size());
		Settings->setValue("pos", pos());
	}
	Settings->setValue("maximized", isMaximized());
	Settings->setValue("fullscreen", isFullScreen());
	Settings->endGroup();
	Settings->sync();
}

void SCSMainWindow::CopyClick() {
	Clipboard->setText(GetCurrentTabCommand());
}

void SCSMainWindow::closeEvent(QCloseEvent */*event*/) {
	Save();
}
