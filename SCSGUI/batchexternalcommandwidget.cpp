#include "batchexternalcommandwidget.h"
#include <QProcess>
#include <QFileDialog>
#include <QApplication>
#include <QStyle>

BatchExternalCommandWidget::BatchExternalCommandWidget(QWidget *parent)
	: QWidget{parent} {
	Layout = new QHBoxLayout(this);
	ExternalProgram = new QLineEdit(this);
	SelectExternalProgram = new QPushButton("Select", this);
	ExternalParams = new QLineEdit(this);
	DeleteButton = new QPushButton(QApplication::style()->standardPixmap(QStyle::SP_TrashIcon), "", this);

	Layout->addWidget(ExternalProgram);
	Layout->addWidget(SelectExternalProgram);
	Layout->addWidget(ExternalParams);
	Layout->addWidget(DeleteButton);

	connect(SelectExternalProgram, &QPushButton::clicked, this, &BatchExternalCommandWidget::SelectClick);
	connect(DeleteButton, &QPushButton::clicked, this, &BatchExternalCommandWidget::RemoveClick);
	connect(ExternalParams, &QLineEdit::textChanged, this, &BatchExternalCommandWidget::EmitChanged);
	connect(ExternalProgram, &QLineEdit::textChanged, this, &BatchExternalCommandWidget::EmitChanged);
}

QString BatchExternalCommandWidget::GetCommand() {
	QString Result = "";
	QString External = ExternalProgram->text();
	if (External != "") {
		if (External.contains(" ")) External = "\"" + External + "\"";
		Result = "--command " + External + " ";
	}
	return Result + ExternalParams->text();
}

void BatchExternalCommandWidget::SetCommand(QStringList *parameters) {
	QString text = "";
	while (!parameters->empty()) {
		QString parameter = parameters->takeFirst();
		if (parameter == "--command") {
			if (parameters->empty()) break;
			ExternalProgram->setText(parameters->takeFirst());
		} else {
			if (text != "") text += " ";
			if (parameter.contains(" ")) parameter = "\"" + parameter + "\"";
			if (parameter == "") parameter = "\"\"";
			text += parameter;
		}
	}
	ExternalParams->setText(text);
}

void BatchExternalCommandWidget::RemoveClick() {
	emit Deleted(this);
}

void BatchExternalCommandWidget::EmitChanged() {
	emit Changed();
}

void BatchExternalCommandWidget::SelectClick() {
	QString OpenExtentions = "";
	#ifdef Q_OS_WIN
	OpenExtentions = "Exe files (*.exe);;";
	#endif
	OpenExtentions += "All Files (*)";
	QString File = QFileDialog::getOpenFileName(this,
				   "Select external program",
				   "",
				   OpenExtentions);
	if (File != "") ExternalProgram->setText(QDir::toNativeSeparators(File));
}
