#include "ffmpeginstances.h"
#include "common.h"
#include <thread>
#include "processpriority.h"

FFmpegInstances::FFmpegInstances(int CreateInstances) {
	if (CreateInstances == 0)
		CreateInstances = std::thread::hardware_concurrency();

	for (int i = 0; i < CreateInstances; i++) {
		Instances.append({new QProcess(), {{}, "", {}, ""}, false, ""});
	}
}

void FFmpegInstances::Free() {
	for (int i = 0; i < Instances.count(); i++) {
		delete Instances.at(i).Process;
	}
}

void FFmpegInstances::AddJob(QStringList &Params, QString &Massage) {
	ProcessQueue.enqueue({Params, Massage, QStringList(), ""});
}

void FFmpegInstances::AddJob(QStringList &Params, QString &Massage, QStringList &DependentParams,
							 QString &DependentMessage) {
	ProcessQueue.enqueue({Params, Massage, DependentParams, DependentMessage});
}

bool FFmpegInstances::Run() {
	if (Instances.count() > 1) {
		bool Running = true;

		while (Running) {
			Running = false;

			for (int i = 0; i < Instances.count(); i++) {
				instance_t *Instance = &Instances[i];
				bool launch = false;

				if (Instance->Process->state() == QProcess::Running ) {
					Running = true;
					Instance->Process->waitForFinished(20);
				}

				if (Instance->Process->state() == QProcess::NotRunning) {

					if (Instance->InProgress) {
						if (Instance->Process->exitStatus() == QProcess::CrashExit) {
							QString error = Instance->Process->readAllStandardError();
							PrintError(Instance->Command);
							PrintError(error);
							return false;
						}
						if (Instance->Queue.DependentParams.isEmpty()) AddProgress();
						Instance->Queue.Params.clear();
						Instance->Queue.Message = "";
						Instance->Command = "";
						Instance->InProgress = false;
					}

					if (!Instance->Queue.DependentParams.isEmpty()) {
						Instance->Queue.Params = Instance->Queue.DependentParams;
						Instance->Queue.Message = Instance->Queue.DependentMessage;
						Instance->Queue.DependentParams.clear();
						Instance->Queue.DependentMessage = "";
						launch = true;
					} else if (!ProcessQueue.isEmpty()) {
						Instance->Queue = ProcessQueue.dequeue();
						launch = true;
					}

					if (launch) {
						Instance->InProgress = true;
						Instance->Command = FFMPEG;
						for (int i = 0; i < Instance->Queue.Params.count(); ++i) {
							if (Instance->Queue.Params.at(i).contains(EscapeCaracters)) {
								Instance->Command.append(" \"" + Instance->Queue.Params.at(i) + "\"");
							} else {
								Instance->Command.append(" " + Instance->Queue.Params.at(i));
							}
						}
						// Instance->Command = FFMPEG + " " + Instance->Queue.Params.join(' ');
						Running = true;

						PrintInfo(Instance->Queue.Message + " instance " + QString::number(i + 1));

						Instance->Process->start(FFMPEG, Instance->Queue.Params);
						if (Cmd) PrintInfo(Instance->Command);
						if (!Instance->Process->waitForStarted(-1)) {
							PrintError("Executing command: " + Instance->Command);
							PrintError(Instance->Process->errorString());
							return false;
						} else {
							if (Idle)
								ProcessIdlePriority(Instance->Process->processId());
						}
					}
				}
			}
			if (!ProcessQueue.isEmpty()) Running = true;
		}
	} else {
		while (ProcessQueue.count() > 0) {
			processqueue_t Process = ProcessQueue.dequeue();
			PrintInfo(Process.Message);
			if (!ffmpeg(Process.Params)) {
				return false;
			}
			if (!Process.DependentParams.isEmpty()) {
				PrintInfo(Process.DependentMessage);
				if (!ffmpeg(Process.DependentParams)) {
					return false;
				}
			}
			AddProgress();
		}
	}
	return true;
}
