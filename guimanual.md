%
% Written by vongooB9 <vongooB9@protonmail.com>
% 

# Subtitles Contact Sheet GUI User Manual

This manual will explain how to use the *Subtitles Contact Sheet GUI* interface but without detailing the options and parameters of the [command line program](manpage.md).

# Basic Configuration

When opening the program for the first time, if it does not automatically find ffmpeg and ffprobe, a window will open to configure them because they are mandatory and subtitles_contact_sheet does not work without them.

![GUI ffmpeg config](Images/GUIManualBasicConfig.png)

Click the select button on both ffmpeg and ffprobe to find the executable file and then click save. Doing this will save it in the default configuration file (represented empty in the GUI) so that subtitles_contact_sheet will always use it.

This configuration dialog may also appear if for any reason ffmpeg is not found or cannot be run.

# General Organization

**SCSGUI** works as a command line generator for *subtitles_contact_sheet*, dynamically creating the formulary using the parameters defined in the command line. It works both ways, you can use the interface to compose the parameters or generate the form from the parameters.

The user interface has [tabs](#tabs) at the top, each of these is a different command line customized for different purposes. There are some tabs that are permanent, but additional tabs can be added. The tabs can be of two types, [normal](#normal) and [batch](#batch) tabs. The normal tabs can process a single input file, while the batch tabs are used to search for files and run the program on each of them. It has a completely different user interface. At the bottom, regardless of the type of tab, is the current command line, below it, the progress bar, and the [control buttons](#command-and-controls).

## Tabs

![GUI Tabs](Images/GUIManualTabs.png)

The first tabs are always *Normal*, *Subtitles*, *VR*, *Gif*, *Screenshots* and *Batch*. They represent a typical use case, you can change their content but not delete them, they are permanent. On the side there is a button to create a [new tab](#new-tab-dialog), clicking it opens a dialog box to configure the type and name. You can add as many tabs as you want. The tabs that we create can be deleted, when they are selected, pressing the button with the **X** next to the name.

### Normal Tab Type

![GUI Normal Tab](Images/GUIManualNormalTab.png)

The structure of the normal tabs is a form with all the parameters in order and buttons at the bottom. 

**Add Param** Open the [parameter search](#params-search) to add it at the end of the form.

**Show Buttons** Show/hide the delete and order buttons of each parameter.

![GUI Delete and Order Buttoms](Images/GUIManualDelNovButtoms.png)

The form is fully dynamic and we can add and remove the parameters we want. When a tab is created it has only the **Video File** parameter.

![GUI Deafult Normal Tab](Images/GUIManualDefaultTab.png)

### Batch Tab Type

![GUI Batch Tab](Images/GUIManualBatchTabs.png)

This is the most complicated tab, allowing to execute several actions on several files at once. In the left side is the list of input files and folders, below are the buttons for adding and removing entries from the list. When individual files are added they are processed as is, when a folder is added, all files of the configured type (selected in the file types section) that are inside are processed. If the Recursive checkbox is checked, subfolders are also scanned.

#### Options section

In this section are the general options of the batch mode.

- **Paralel process**: Number of parallel processes to be launched.
- **Recursive**: Search inside the subfolders for files to process.
- **Ignore Fail**: Continues to process files even if they fail, if unchecked, when a single file fails the program stops.
- **Only Show CMD**: Instead of processing the files, just show the command with which they would be processed.
- **Quiet**: No messages are displayed.
- **Show Output**: Displays command messages, it is incompatible with using multiple processes in parallel.

#### File Types section

This section defines the types of files that will be searched within the folders.

- **Video**: Searches all known video files (mkv, mp4, avi, mov, wmv...)
- **Subtitles**: Searches all suported subtitles files (srt, ass, vtt)
- **Extensions List**: List of extensions separated by commas, permits to add more file types to search.

#### Background Options section

These options make the program continue to run after finishing processing all the files, waiting for new ones.

- **Watch Filesystem**: Uses file system events to detect when there are changes inside the folders.
- **Search Time**: Searches for new files every this number of seconds. *0* means disabled.
- **Modification Time Delay**: Only select files with modification time older than this. This option avoids processing files that are currently being written. Default for windows: *10*, on linux *1*. In the GUI *0* means default.
- **Recheck Time**: Recheck incomplete files after this time. *0* to disable. Default for windows: *10*, on linux *0*. In the GUI *0* means default.
- **Delete Check Time**: Time in seconds when it performs a check of the deleted files to remove them from its internal list. Default *3600*. In the GUI *0* means default.

#### External Program section

Program that will launch batch mode instead of subtitbles_contact_sheet. Using it disables the **Add Command** button, it is recommended to use the **Add External Command** button and especify the program there.

#### Commands section

This section defines the commands to be executed for each file found. You can add as many as you want by pressing the **Add Command** and **Add External Command** buttons.

This part of the program consists of the list of commands to be executed and the buttons to add more commands.

- **Command**: This section lists the commands, one on each line.
- **Add Command**: Adds a line to the list of commands.
- **Add External Command**: Add a line to the list of commands but to launch external programs.

If none is defined the batch mode will run subtitles_contact_sheet without any parameters other than the file found, this will perform the option that is configured in the default configuration file (Empty name in the GUI).

#### Command

When a normal line is added to the commands section it appears as follows:

![GUI Batch Command](Images/GUIManualBatchCommand.png)

- **ComboBox with configuration name**: Select the configuration to be applied in this command.
- **Edit**: Pressing it will open the window for editing the previously selected configuration.
- **New**: Pressing this button creates a new configuration.
- **Command Parameters**: Text with additional parameters to pass to the command.
- **Edit**: Pressing it opens a window for editing additional parameters in the same way as the parameters are edited in the normal tabs.
- **Remove**: Removes this line.
 
#### External Command

![GUI Batch External Command](Images/GUIManualBatchExtCommand.png)

- **Program**: Path to the binary file.
- **Select**: Pressing it opens the dialog for selecting the executable file.
- **Parameters**: Text with additional parameters to pass to the command.
- **Remove**: Removes this line.

### New Tab Dialog

![GUI New Tab Dialog](Images/GUIManualNewTab.png)

When the new tab button is clicked this dialog will appear to select the type of tab to create and type the name it will have.

## Command And Controls

![GUI Command and Controls](Images/GUIManualComandAndControls.png)

At the bottom, independently of the tab type, is the current command line, below it, the progress bar, and the following buttons:

**Command Line**: Displays the current command, it can be modified manually and the form will be updated accordingly.

**Progress Bar**: Displays the progress while a process is running.

![GUI Progress](Images/GUIManualProgress.png)

**Show Log**: Show/hide the log. It appears between the command line and the buttons.

**Copy Command**: Copies the current command line to the clipboard.

**Save**: Save the command lines of all tabs in the configuration file.

**Cancel**: Cancels a running process.

**Start**: Execute the current command line.

# Params Search

Clicking the **Add Param** button opens this window, which displays a list of available options to add and search through them.

![GUI Param Search Dialog](Images/GUIManualParamSearchDialog.png)

Double click on the desired option or click to select it and then click the **Add** button to add it.

# Parameters / Options

There are several different types of options and parameters, each one has a different editor, these are the main types.

## Files

![GUI Select File](Images/GUIManualSelectFile.png)

When an option or parameter requires a file, it displays the path to the current file and a select button which, when pressed, navigates through the folders to locate the desired file.

## Configuration

This editor is used to select, create, delete and copy configuration files. 

![GUI Config Selector](Images/GUIManualConfigSelector.png)

- **Configuration name**: ComboBox to select the configuration, when empty the default configuration file is selected.
- **Edit**: Opens the [editor](#configuration-editor) of the selected configuration.
- **Clone**: Copies the selected configuration to a new one, first ask for a new configuration name and then opens the [editor](#configuration-editor).
- **New**: Create a new configuration, first ask for a new configuration name and then opens the [editor](#configuration-editor).
- **Delete**: Deletes the selected configuration.

## Number

![GUI Number Selector](Images/GUIManualNumberSelect.png)

When the option or parameter requires a number, it displays a number editor and a **Show Raw** button. Pressing this button, instead of showing the number editor, displays the text editor with the equivalent value. This raw text editor is used to include functions that automatically calculate the value.

## Font

![GUI Font Selector](Images/GUIManualFontSelect.png)

When the option or parameter requires a font, it displays the system installed fonts selector and a **Show Raw** button. Pressing this button, instead of showing the fonts selector, displays the text editor with the equivalent value. This raw text editor is used to include functions that automatically calculate the value.

## Color

![GUI Color Selector](Images/GUIManualColorSelect.png)

When the option or parameter requires a color, it displays the text representation of the color, a box with the preview of the color and a **Select** button. Clicking this button opens a window with sample colours, a visual colour selector and various numerical values for selecting and manipulating colours.

![GUI Color Editor](Images/GUIManualColorEditor.png)

## Boolean

![GUI Check Selector](Images/GUIManualCheckSelect.png)

When the option or parameter requires a boolean, it displays a check box and a **Show Raw** button. Pressing this button, instead of showing the check box, displays the text editor with the equivalent value. This raw text editor is used to include functions that automatically calculate the value.

# Configuration Editor

![GUI Configuration Editor](Images/GUIManualConfigurationEditor.png)

The configuration options are divided into tabs depending on which functionality of the programme they correspond to.

- **General**: Here are all the general options and those that do not fit in any of the other tabs.
- **Subtitles**: All options concerning subtitles.
- **Text Block**: Here are the options that define the appearance and content of the text containing the basic information and technical characteristics.
- **Frame Time**: Options about the text showing the time of each captured frame.
- **Virtual Reality**: Options related to virtual reality.
- **Rename**: Options for when the programme renames, copies or links files.
- **Images**: Here are the options that control the behaviour of the program when using a folder with images as input.
- **User Defined**: User-defined variables for use in text substitution and functions.

At the bottom are the following buttons:

- **Hide/Show Default**: Pressing it shows or hides the options that have the default value, allowing you to easily see the options that are actually changed in the current configuration.
- **Cancel**: Discard all changes made to the configuration and close the window.
- **Save**: Save the configuration changes and close the window.

# BUGS
You can report bugs or problems on the [gitlab issues page](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/issues).

# COPYRIGHT
Copyright © vongooB9.

License GPLv3: GNU GPL version 3 [https://gnu.org/licenses/gpl.html](https://gnu.org/licenses/gpl.html). This is free software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law.


---
header-includes: |
  <style>
  body {max-width: 90%;}
  figure {text-align: center;}
  a, a:visited {color: #1a1aaa;}
  @media (prefers-color-scheme: dark) {
    html {background: black; color: white;}
    a, a:visited {color: #AAF;}
  }
  </style>
---

