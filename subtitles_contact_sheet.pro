SCS_VERSION = 1.7

QT -= gui
QT += core

CONFIG += c++17
CONFIG += console
CONFIG -= app_bundle
CONFIG -= warm_on

!defined(DEFAULT_FONT, var) { DEFAULT_FONT = "DejaVu Sans" }
!defined(DEFAULT_FONT_MONO, var) { DEFAULT_FONT_MONO = "DejaVu Sans Mono" }

defined(PORTABLE, var) {
    win32 {
        PORTABLE_CONFIG = 1
        PORTABLE_SEARCH_DIR = 1
    }
    PORTABLE_FONT_FILES = 1
    !defined(DEFAULT_FONT_FILE, var) { DEFAULT_FONT_FILE = /Fonts/DejaVuSansMono.ttf }
}

DEFINES += SCS_VERSION=\"\\\"$$SCS_VERSION\\\"\"
defined(PORTABLE_CONFIG, var) { DEFINES += PORTABLE_CONFIG=1 }
defined(PORTABLE_FONT_FILES, var) { DEFINES += PORTABLE_FONT_FILES=1 }
defined(PORTABLE_SEARCH_DIR, var) { DEFINES += PORTABLE_SEARCH_DIR=1 }
defined(DEFAULT_FONT, var) { DEFINES += DEFAULT_FONT=\"\\\"$$DEFAULT_FONT\\\"\" }
defined(DEFAULT_FONT_MONO, var) { DEFINES += DEFAULT_FONT_MONO=\"\\\"$$DEFAULT_FONT_MONO\\\"\" }
defined(DEFAULT_FONT_FILE, var) { DEFINES += DEFAULT_FONT_FILE=\"\\\"$$DEFAULT_FONT_FILE\\\"\" }
defined(DEFAULT_FFMPEG, var) { DEFINES += DEFAULT_FFMPEG=\"\\\"$$DEFAULT_FFMPEG\\\"\" }
defined(DEFAULT_FFPROBE, var) { DEFINES += DEFAULT_FFPROBE=\"\\\"$$DEFAULT_FFPROBE\\\"\" }
defined(DEFAULT_GIFSICLE, var) { DEFINES += DEFAULT_GIFSICLE=\"\\\"$$DEFAULT_GIFSICLE\\\"\" }

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
		batch.cpp \
		colors.cpp \
		common.cpp \
		configurationfunctions.cpp \
		contactsheettextblock.cpp \
		ffmpeginstances.cpp \
		filters.cpp \
		images.cpp \
		iso639.cpp \
		layeredfilter.cpp \
		main.cpp \
		modes.cpp \
		paramssettings.cpp \
		subtitles.cpp \
		textblock.cpp \
                textsustitution.cpp \
                processpriority.cpp

HEADERS += \
	batch.h \
	colors.h \
	common.h \
	configurationfunctions.h \
	contactsheettextblock.h \
	ffmpeginstances.h \
	filters.h \
	images.h \
	iso639.h \
	layeredfilter.h \
	modes.h \
	paramssettings.h \
	subtitles.h \
	textblock.h \
        textsustitution.h \
        processpriority.h

unix:!android{
	!defined(USR_DIR, var) { USR_DIR = /usr/local }
	!defined(MAN_DIR) { MAN_DIR = /man/man1 }
	BIN_DIR = $$USR_DIR/bin
	target.path = $$BIN_DIR
	INSTALLS += target

	mantarget.target = makeman
	DOC_DATE = $$system(LANG=en date \"+%B %Y\")
	mantarget.commands = pandoc manpage.md -s -t man -M \"title=subtitles_contact_sheet(1) subtitles_contact_sheet $${SCS_VERSION}\" -M \"date=$${DOC_DATE}\" | sed -r \"s/\\-\\-/\\\\\\-\\\\\\-/g\" | sed -r \"s/([^\\\\])\\-/\\1\\\\\\-/g\" | gzip > $${TARGET}.1.gz
	QMAKE_EXTRA_TARGETS += mantarget
	POST_TARGETDEPS += makeman

	maninstall.CONFIG = no_check_exist
	maninstall.path = $${USR_DIR}$${MAN_DIR}
	maninstall.files += $${TARGET}.1.gz
	INSTALLS += maninstall
}
