#include "filters.h"
#include "colors.h"
#include "configurationfunctions.h"
#include <QDir>

Filters::Filters(ParamsSettings *ps, TextSustitution *ts) {
	PS = ps;
	TS = ts;
	GridBorder = PS->Int("grid_border");
	GridUp = (PS->Int("grid_border_up") == -1) ? GridBorder : PS->Int("grid_border_up");
	GridDown = (PS->Int("grid_border_down") == -1) ? GridBorder : PS->Int("grid_border_down");
	GridLeft = (PS->Int("grid_border_left") == -1) ? GridBorder : PS->Int("grid_border_left");
	GridRight = (PS->Int("grid_border_right") == -1) ? GridBorder : PS->Int("grid_border_right");
	GridVertial = (PS->Int("grid_border_vertical") == -1) ? GridBorder : PS->Int("grid_border_vertical");
	GridHorizontal = (PS->Int("grid_border_horizontal") == -1) ? GridBorder : PS->Int("grid_border_horizontal");
}

Filters::~Filters() {}

QString Filters::RawFPS(QString fps) {
	return "fps=fps=" + fps;
}

QString Filters::FPS() {
	return RawFPS(PS->Value("video_fps"));
}

QString Filters::RawThumbnail(QString n) {
	return "thumbnail=n=" + n;
}

QString Filters::Thunbnail() {
	return RawThumbnail(PS->Value("rep_frames"));
}

QString Filters::RawCrop(QString w, QString h, QString x, QString y) {
	return "crop=w=" + w + ":h=" + h + ":x=" + x + ":y=" + y;
}

QString Filters::EyeCrop(QString VR_IN_STEREO) {
	if (VR_IN_STEREO == "sbs") {
		if (PS->Bool("vr_right_eye")) {
			return RawCrop("iw/2", "ih", "iw/2", "0");
		} else {
			return RawCrop("iw/2", "ih", "0", "0");
		}
	}
	if (VR_IN_STEREO == "tb") {
		if (PS->Bool("vr_right_eye")) {
			return RawCrop("iw", "ih/2", "0", "ih/2");
		} else {
			return RawCrop("iw", "ih/2", "0", "0");
		}
	}
	return "";
}

QString Filters::VR(QString VR_IN_STEREO, QString VR_W, QString VR_H, int VR_IH_FOV, int VR_IV_FOV) {
	QStringList VR_FILTER = {
		"v360=" + PS->Value("vr_in"),
		PS->Value("vr_out"),
		"in_stereo=" + VR_IN_STEREO,
		"out_stereo=" + PS->Value("vr_out_stereo"),
		"yaw=" + PS->Value("vr_yaw"),
		"pitch=" + PS->Value("vr_pitch"),
		"roll=" + PS->Value("vr_roll"),
		"w=" + VR_W,
		"h=" + VR_H,
		"interp=" + PS->Value("vr_interp")
	};

	if (PS->Int("vr_ih_fov") != 0) VR_FILTER << "ih_fov=" + QString::number(VR_IH_FOV);
	if (PS->Int("vr_iv_fov") != 0) VR_FILTER << "iv_fov=" + QString::number(VR_IV_FOV);

	if (PS->Int("vr_d_fov") != 0) VR_FILTER << "d_fov=" + PS->Value("vr_d_fov");
	if (PS->Int("vr_h_fov") != 0) VR_FILTER << "h_fov=" + PS->Value("vr_h_fov");
	if (PS->Int("vr_v_fov") != 0) VR_FILTER << "v_fov=" + PS->Value("vr_v_fov");

	if (PS->Bool("vr_h_flip")) VR_FILTER << "h_flip=1";
	if (PS->NotEmpty("vr_aditional")) VR_FILTER << PS->Value("vr_aditional");
	return VR_FILTER.join(':');
}

QString Filters::RawScale(QString W, QString H, QString Algo, QString Flags, QString ForceAR, QString Divisible) {
	QString Filter = "scale=" + W + ":" + H +	":flags=" + Algo + Flags;
	if (ForceAR != "") Filter += ":force_original_aspect_ratio=" + ForceAR;
	if (Divisible != "")Filter += ":force_divisible_by=" + Divisible;
	return Filter;
}

QString Filters::RawZScale(QString W, QString H, QString Algo) {
	return "zscale=w=" + W + ":h=" + H + ":filter=" + Algo;
}

QString Filters::Scale(int W, int H, QString ForceAR) {
	QString ScaleFilter = "";
	if (PS->Bool("scale_zscale")) {
		ScaleFilter = RawZScale(QString::number(W), QString::number(H), PS->Value("scale_algorithm"));
	} else {
		ScaleFilter = RawScale(QString::number(W), QString::number(H), PS->Value("scale_algorithm"), PS->Value("scale_flags"),
							   ForceAR);
	}
	if (PS->NotEmpty("scale_custom_options"))
		ScaleFilter += ":" + PS->Value("scale_custom_options");
	return ScaleFilter;
}

QString Filters::RawSubtitles(QString F, QString FontsDir, QString ForceStyle) {
	QString Sub = "subtitles=f='" + EscapeFFFilters(F) + "'";
	if (FontsDir != "") {
		Sub += ":fontsdir='" + EscapeFFFilters(QDir::toNativeSeparators(FontsDir)) + "'";
	}
	if (ForceStyle != "") {
		Sub += ":force_style='" + ForceStyle + "'";
	}
	return Sub;
}

QString Filters::Subtitles(QString File, bool ForceStyle) {
	QStringList SUBTITLE_FORCE_STYLE;
	if (ForceStyle) {
		if (PS->NotEmpty("sub_font")) SUBTITLE_FORCE_STYLE.append("Fontname=" + PS->Value("sub_font"));
		if (PS->NotEmpty("sub_color")) SUBTITLE_FORCE_STYLE.append("PrimaryColour=&H" +
					StringColor2BGR(PS->Value("sub_color")));
		if (PS->NotEmpty("sub_size")) SUBTITLE_FORCE_STYLE.append("Fontsize=" + PS->Value("sub_size"));
		if (PS->NotEmpty("sub_border")) SUBTITLE_FORCE_STYLE.append("BorderStyle=" + PS->Value("sub_border"));
		if (PS->NotEmpty("sub_bc")) SUBTITLE_FORCE_STYLE.append("BackColour=&H" + StringColor2BGR(PS->Value("sub_bc")));
		if (PS->NotEmpty("sub_oc")) SUBTITLE_FORCE_STYLE.append("OutlineColour=&H" + StringColor2BGR(PS->Value("sub_oc")));
	}

	QString SUBTITLES_FILTER = RawSubtitles(File, PS->Value("sub_fonts_dir"), SUBTITLE_FORCE_STYLE.join(','));

	if (PS->NotEmpty("sub_custom"))
		SUBTITLES_FILTER += ":" + PS->Value("sub_custom");

	return SUBTITLES_FILTER;
}

Filters::ft_positions Filters::String2FrameTimePosition(QString pos) {
	pos = pos.toLower();
	if (pos == "up_left") return ft_up_left;
	if (pos == "up_right") return ft_up_right;
	if (pos == "down_left") return ft_down_left;
	if (pos == "down_right") return ft_down_right;
	if (pos == "up_center") return ft_up_center;
	if (pos == "down_center") return ft_down_center;
	if (pos == "middle_left") return ft_middle_left;
	if (pos == "middle_right") return ft_middle_right;
	return ft_up_right;
}

QString Filters::RawDrawtext(QString Text,
							 bool EscapeText,
							 QString Font,
							 bool FontIsFile,
							 int &AddedW,
							 int &AddedH,
							 QString Position,
							 QString Vertical,
							 bool Outside,
							 QString fontsize,
							 QString fontcolor,
							 QString boxborderw,
							 QString boxcolor,
							 QString borderw,
							 QString bordercolor,
							 QString shadowcolor,
							 QString shadowx,
							 QString shadowy,
							 int Margin,
							 QString Custom) {

	AddedW = 0;
	AddedH = 0;

	QStringList FILTERS;
	ft_positions FrameTimePosition = String2FrameTimePosition(Position);
	if (Vertical != "disabled") {
		if (Vertical == "upward") {
			FILTERS.append("transpose=clock");
			switch (FrameTimePosition) {
				case ft_up_left:
					FrameTimePosition = ft_up_right;
					break;
				case ft_up_right:
					FrameTimePosition = ft_down_right;
					break;
				case ft_down_left:
					FrameTimePosition = ft_up_left;
					break;
				case ft_down_right:
					FrameTimePosition = ft_down_left;
					break;
				case ft_up_center:
					FrameTimePosition = ft_middle_left;
					break;
				case ft_down_center:
					FrameTimePosition = ft_middle_right;
					break;
				case ft_middle_left:
					FrameTimePosition = ft_up_center;
					break;
				case ft_middle_right:
					FrameTimePosition = ft_down_center;
					break;
			}

		} else if (Vertical == "downward") {
			FILTERS.append("transpose=cclock");
			switch (FrameTimePosition) {
				case ft_up_left:
					FrameTimePosition = ft_down_left;
					break;
				case ft_up_right:
					FrameTimePosition = ft_up_left;
					break;
				case ft_down_left:
					FrameTimePosition = ft_down_right;
					break;
				case ft_down_right:
					FrameTimePosition = ft_up_right;
					break;
				case ft_up_center:
					FrameTimePosition = ft_middle_right;
					break;
				case ft_down_center:
					FrameTimePosition = ft_middle_left;
					break;
				case ft_middle_left:
					FrameTimePosition = ft_down_center;
					break;
				case ft_middle_right:
					FrameTimePosition = ft_up_center;
					break;
			}
		}
	}

	QStringList TIME_FILTER;

	if (Outside) {
		int TextHeight = fontsize.toInt() + (boxborderw.toInt() + Margin) * 2;
		switch (FrameTimePosition) {
			case ft_up_left:
			case ft_up_right:
			case ft_up_center:
				FILTERS.append(RawPad("iw", "ih+" + QString::number(TextHeight), "0", QString::number(TextHeight),
									  StringColor2RGB(boxcolor)));
				if (Vertical == "disabled") {
					AddedH = TextHeight;
				} else {
					AddedW = TextHeight;
				}
				break;
			case ft_down_left:
			case ft_down_right:
			case ft_down_center:
				FILTERS.append(RawPad("iw", "ih+" + QString::number(TextHeight), "0", "0", StringColor2RGB(boxcolor)));
				if (Vertical == "disabled") {
					AddedH = TextHeight;
				} else {
					AddedW = TextHeight;
				}
				break;
			case ft_middle_left:
			case ft_middle_right:
				break;
		}
	}

	if (EscapeText) {
		TIME_FILTER.append("text='" + EscapeFFFilters(Text) + "'");
	} else {
		TIME_FILTER.append("text='" + Text + "'");
	}

	if (Font != "") {
		if (FontIsFile) {
			TIME_FILTER.append("fontfile='" + EscapeFFFilters(Font) + "'");
		} else {
			TIME_FILTER.append("font='" + EscapeFFFilters(Font) + "'");
		}
	}

	TIME_FILTER.append("fontsize=" + fontsize);
	TIME_FILTER.append("fontcolor=0x" + StringColor2RGB(fontcolor));
	TIME_FILTER.append("box=1");
	TIME_FILTER.append("boxborderw=" + boxborderw);
	TIME_FILTER.append("boxcolor=0x" + StringColor2RGB(boxcolor));
	TIME_FILTER.append("borderw=" + borderw);
	TIME_FILTER.append("bordercolor=" + StringColor2RGB(bordercolor));
	TIME_FILTER.append("shadowcolor=" + StringColor2RGB(shadowcolor));
	TIME_FILTER.append("shadowx=" + shadowx);
	TIME_FILTER.append("shadowy=" + shadowy);

	QString Ditance = QString::number(boxborderw.toInt() + Margin);

	switch (FrameTimePosition) {
		case ft_up_left:
		case ft_down_left:
		case ft_middle_left:
			TIME_FILTER.append("x=" + Ditance);
			break;
		case ft_up_right:
		case ft_down_right:
		case ft_middle_right:
			TIME_FILTER.append("x=w-tw-" + Ditance);
			break;
		case ft_up_center:
		case ft_down_center:
			TIME_FILTER.append("x=(w-tw)/2");
			break;
	}

	switch (FrameTimePosition) {
		case ft_up_left:
		case ft_up_right:
		case ft_up_center:
			if (Outside)
				TIME_FILTER.append("y=((" + fontsize + "-th)/2)+" + Ditance);
			else
				TIME_FILTER.append("y=" + Ditance);
			break;
		case ft_down_left:
		case ft_down_right:
		case ft_down_center:
			if (Outside)
				TIME_FILTER.append("y=h-th-((" + fontsize + "-th)/2)-" + Ditance);
			else
				TIME_FILTER.append("y=h-th-" + Ditance);
			break;
		case ft_middle_left:
		case ft_middle_right:
			TIME_FILTER.append("y=(h-th-((" + fontsize + "-th)/2))/2");
			break;
	}

	if (Custom != "") TIME_FILTER.append(Custom);

	FILTERS.append("drawtext=" + TIME_FILTER.join(':'));

	if (Vertical != "disabled") {
		if (Vertical == "upward") {
			FILTERS.append("transpose=cclock");
		} else if (Vertical == "downward") {
			FILTERS.append("transpose=clock");
		}
	}
	return FILTERS.join(",");
}

QString Filters::Drawtext(int &AddedW, int &AddedH) {
	QString FT_FORMAT = PS->Value("ft_format");
	QString Text;
	if (FT_FORMAT == "" || FT_FORMAT == "HH:MM:SS.mmm") {
		Text = "%{pts\\:hms}";
	} else if (FT_FORMAT == "seconds") {
		Text = "%{eif\\:t\\:u\\:1}";
	} else if (FT_FORMAT == "miliseconds") {
		Text = "%{eif\\:t*1000\\:u\\:1}";
	} else if (FT_FORMAT == "HH:MM:SS") {
		Text = "%{pts\\:gmtime\\:0\\:%H\\\\\\:%M\\\\\\:%S}";
	} else if (FT_FORMAT == "MM:SS") {
		Text = "%{pts\\:gmtime\\:0\\:%M\\\\\\:%S}";
	} else if (FT_FORMAT == "short") {
		if (TS->Value("format.duration").toFloat() > 3600) {
			Text = "%{pts\\:gmtime\\:0\\:%H\\\\\\:%M\\\\\\:%S}";
		} else {
			Text = "%{pts\\:gmtime\\:0\\:%M\\\\\\:%S}";
		}
	} else {
		Text = "%{pts\\:gmtime\\:0\\:" + EscapeFFFilters(FT_FORMAT.replace(":", "\\:")) + "}";
	}

	bool Outside = !PS->Bool("ft_overlay");
	if (PS->Value("ft_mask_layer_pos") == "inside") Outside = false;

	QString Font;
	bool FontIsFile = AdaptFont(PS, "ft_font", Font);

	return RawDrawtext(Text,
					   false,
					   Font,
					   FontIsFile,
					   AddedW,
					   AddedH,
					   PS->Value("ft_pos"),
					   PS->Value("ft_vertical"),
					   Outside,
					   PS->Value("ft_size"),
					   PS->Value("ft_color"),
					   PS->Value("ft_border"),
					   PS->Value("ft_bg"),
					   PS->Value("ft_font_border"),
					   PS->Value("ft_font_border_color"),
					   PS->Value("ft_shadow_color"),
					   PS->Value("ft_shadow_x"),
					   PS->Value("ft_shadow_y"),
					   PS->Int("ft_margin"),
					   PS->Value("ft_custom")
					  );
}

QString Filters::ScreenshostLogoOverlayOptions() {
	QString OVERLAY = "";
	QString pos = PS->Value("logo_pos");
	QString margin = PS->Value("logo_margin");
	if (pos == "up_left") {
		OVERLAY = "x=" + margin + ":y=" + margin;
	} else if (pos == "up_right") {
		OVERLAY = "x=main_w-overlay_w-" + margin + ":y=" + margin;
	} else if (pos == "down_right") {
		OVERLAY = "x=main_w-overlay_w-" + margin + ":y=main_h-overlay_h-" + margin;
	} else {
		OVERLAY = "x=" + margin + ":y=main_h-overlay_h-" + margin;
	}
	return OVERLAY;
}

// QString Filters::RawTile(QString Layout, int Border, QString Color) {
// 	QString GRID_FILTER = "format=rgba,tile=layout=" + Layout;
// 	if (Border > 0) {
// 		GRID_FILTER += ":padding=" + QString::number(Border) +
// 					   ":margin=" + QString::number(Border);
// 		if (Color != "") GRID_FILTER += ":color=" + StringColor2RGB(Color);
// 	}
// 	return GRID_FILTER;
// }

QString Filters::RawPad(QString W, QString H, QString X, QString Y, QString Color) {
	QString Filter = "pad=w=" + W + ":h=" + H + ":x=" + X + ":y=" + Y;
	if (Color != "") Filter += ":color=0x" + Color;
	return Filter;
}

QString Filters::RawResizeBlurBG(QString W, QString H, QString BgW, QString BgH, QString Algo, QString Flags,
								 QString SizeX, QString Exposure,
								 QString Saturation, QString Aditional, QString AditionalBG, QString AditionalImg) {
	QString CF = "split[bg][img];"
				 "[bg]" + RawScale(BgW, BgH, Algo, Flags, "increase", "2") + ","
				 "crop=w=" + BgW + ":h=" + BgH;

	if (SizeX != "" && SizeX != "0") CF += "," + RawBlur(SizeX);
	if (Exposure != "" && Exposure != "0") CF += "," + RawExposure(Exposure);
	if (Saturation != "" && Saturation != "1") CF += "," + RawSaturation(Saturation);
	if (AditionalBG != "") CF += "," + AditionalBG;

	CF += "[bgscale];[img]" + RawScale(W, H, Algo, Flags, "decrease", "2");
	if (AditionalImg != "") CF += "," + AditionalImg;
	CF += "[imgscale];[bgscale][imgscale]overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/2";
	if (Aditional != "") CF += "," + Aditional;
	return CF;
}

QString Filters::ResizeBlurBG(int W, int H, int BGAddW, int BGAddH, QString Aditional) {
	QString Exp = QString::number((double)PS->Int("images_fill_exposure") / (double)100);
	QString S = QString::number((double)PS->Int("images_fill_saturation") / (double)100);
	QString AditionalImg = "";
	if (!PS->Bool("images_text_padding")) {
		AditionalImg = Aditional;
		Aditional = "";
	} else {
		BGAddW = 0;
		BGAddH = 0;
	}
	if (PS->Bool("images_text_overlay")) {
		BGAddW = 0;
		BGAddH = 0;
	}
	return RawResizeBlurBG(QString::number(W),
						   QString::number(H),
						   QString::number(W + BGAddW),
						   QString::number(H + BGAddH),
						   PS->Value("scale_algorithm"),
						   PS->Value("scale_flags"),
						   PS->Value("images_fill_blur"),
						   Exp,
						   S,
						   Aditional,
						   PS->Value("images_fill_custom"),
						   AditionalImg);
}

QString Filters::RawMask(int W, int H, QString Mask, bool Scale, QString AditionalFilters) {
	QString Filter = "";
	if (QFile::exists(Mask)) {
		Filter = "[normal];movie=filename='" + EscapeFFFilters(Mask) + "'";
		if (Scale) Filter += "," + RawScale(QString::number(W), QString::number(H), "lanczos", "+accurate_rnd+full_chroma_int");
		Filter +=  AditionalFilters + "[mask];[normal][mask]alphamerge";
	} else {
		Filter = "[normal];" + String2GradientFilter(W, H, Mask) + AditionalFilters + "[mask];[normal][mask]alphamerge";
	}
	return Filter;
}

void Filters::RawSVGRoundedCorners(int W, int H, int R, QString File) {
	QString RoundedCornersSVG = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>"
								"<svg width=\"%W%px\" height=\"%H%px\" "
								"viewBox=\"0 0 %W% %H%\" version=\"1.1\" id=\"svg5\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:svg=\"http://www.w3.org/2000/svg\">"
								"<defs id=\"defs7\"/>"
								"<g id=\"layer1\">"
								"<rect style=\"fill:#000000\" id=\"bg\" width=\"%W%px\" height=\"%H%px\" x=\"0\" y=\"0\"/>"
								"</g>"
								"<rect style=\"fill:#ffffff;\" id=\"round\" width=\"%W%px\" height=\"%H%px\" x=\"0\" y=\"0\" rx=\"%R%px\" ry=\"%R%px\"/>"
								"</svg>";
	RoundedCornersSVG.replace("%W%", QString::number(W));
	RoundedCornersSVG.replace("%H%", QString::number(H));
	RoundedCornersSVG.replace("%R%", QString::number(R));
	WriteFile(File, RoundedCornersSVG);
}

QString Filters::Mask(int W, int H, QString AditionalFilters) {
	QString Mask;
	bool Scale;
	if (PS->Int("rounded_corners") > 0) {
		Mask = TMP + QDir::separator() + "rounded_mask.svg";
		RawSVGRoundedCorners(W, H, PS->Int("rounded_corners"), Mask);
		Scale = false;
	} else {
		Mask = PS->Value("mask");
		Scale = true;
	}
	return RawMask(W, H, Mask, Scale, AditionalFilters);
}

void Filters::LayeredMask(LayeredFilter &F, int W, int H, QString AditionalFilters, QString AlphamergeOptions) {
	QString Mask;
	bool Scale;
	if (PS->Int("rounded_corners") > 0) {
		Mask = TMP + QDir::separator() + "rounded_mask.svg";
		RawSVGRoundedCorners(W, H, PS->Int("rounded_corners"), Mask);
		Scale = false;
	} else {
		Mask = PS->Value("mask");
		Scale = true;
	}

	QString SF = "";
	if (QFile::exists(Mask)) {
		if (Scale)
			SF = RawScale(QString::number(W), QString::number(H), "lanczos",
						  "+accurate_rnd+full_chroma_int") + "," + AditionalFilters;
		if (AditionalFilters != "") {
			if (SF != "") SF += ",";
			SF += AditionalFilters;
		}
		F.AddInputFile(Mask, "mask", SF, F.merge_alphamerge, AlphamergeOptions);
	} else {
		SF = String2GradientFilter(W, H, Mask);
		if (AditionalFilters != "") {
			if (SF != "") SF += ",";
			SF += AditionalFilters;
		}
		F.AddInputFilter(SF, "mask", "", F.merge_alphamerge, AlphamergeOptions);
	}
}

QString Filters::String2GradientFilter(int width, int height, QString gradient) {
	if (!gradient.contains(":"))
		return GenerateSolidColorFilter(width, height, gradient);

	QString type = gradient.section(":", 0, 0);
	QString direcction = gradient.section(":", 1, 1);
	QString colors = gradient.section(":", 2, 2);
	return GenerateGradientFilter(width, height, type, colors, direcction);
}

QString Filters::GenerateGradientFilter(int width, int height, QString type, QString colors, QString direction) {
	QString Result = "gradients=size=" + QString::number(width) + "x" + QString::number(height);
	Result += ":type=" + type;
	QStringList ColorsSplit = colors.split(",");
	int NColors = 0;
	for (int i = 0; i < ColorsSplit.count(); ++i) {
		if (i > 7) break;
		Result += ":c" + QString::number(i) + "=" + StringColor2RGB(ColorsSplit.at(i));
		NColors++;
	}
	if (NColors > 2 && NColors < 8) Result += ":nb_colors=" + QString::number(NColors);

	int FromX = 0;
	int FromY = 0;
	int ToX = 100;
	int ToY = 0;

	if (direction.contains("-")) {
		QString From = direction.section('-', 0, 0).toLower();
		QString To = direction.section('-', 1, 1).toLower();
		FromX = From.section("x", 0, 0).toInt();
		FromY = From.section("x", 1, 1).toInt();
		ToX = To.section("x", 0, 0).toInt();
		ToY = To.section("x", 1, 1).toInt();
	} else {
		if (direction == "horizontal" || direction == "h") {
			FromX = 0;
			FromY = 0;
			ToX = 100;
			ToY = 0;
		}
		if (direction == "vertical" || direction == "v") {
			FromX = 0;
			FromY = 0;
			ToX = 0;
			ToY = 100;
		}
		if (direction == "diagonalup" || direction == "du") {
			FromX = 100;
			FromY = 100;
			ToX = 0;
			ToY = 0;
		}
		if (direction == "diagonaldown" || direction == "dd") {
			FromX = 0;
			FromY = 0;
			ToX = 100;
			ToY = 100;
		}
	}

	FromX = FromX * width / 100;
	FromY = FromY * height / 100;
	ToX = ToX * width / 100;
	ToY = ToY * height / 100;

	if (FromX >= width) FromX = width - 1;
	if (FromY >= height) FromY = height - 1;
	if (ToX >= width) ToX = width - 1;
	if (ToY >= height) ToY = height - 1;

	if (FromX < 0) FromX = 0;
	if (FromY < 0) FromY = 0;
	if (ToX < 0) ToX = 0;
	if (ToY < 0) ToY = 0;

	Result += ":x0=" + QString::number(FromX) +
			  ":y0=" + QString::number(FromY) +
			  ":x1=" + QString::number(ToX) +
			  ":y1=" + QString::number(ToY);

	//	Result += ",format=rgba";
	return Result;
}

QString Filters::GenerateSolidColorFilter(int width, int height, QString color) {
	return "color=c=0x" + StringColor2RGB(color) +
		   ":size=" + QString::number(width) + "x" + QString::number(height) +
		   ",format=rgba";
}

bool Filters::GenerateGradient(QString Out, int width, int height, QString gradient) {
	QStringList gradient_params = {"-hide_banner",
								   "-loglevel",
								   "error",
								   "-y",
								  };
	gradient_params << "-f" << "lavfi" << "-i";
	gradient_params << String2GradientFilter(width, height, gradient);
	gradient_params << "-vframes" << "1" << Out;
	return ffmpeg(gradient_params);
}

void Filters::RawXstackSizeCalc(int &W, int &H, uint Num, int LayoutX, int Size, int Height,
								int BorderLeft, int BorderRight, int BorderUp, int BorderDown, int BorderVertical, int BorderHorizontal) {
	W = ((LayoutX * Size) + ((LayoutX - 1) * BorderVertical)) + BorderLeft + BorderRight;
	int LayoutY = Num / LayoutX;
	if (Num % LayoutX > 0) LayoutY++;
	H = ((LayoutY * Height) + ((LayoutY - 1) * BorderHorizontal)) + BorderUp + BorderDown;
}

QString Filters::RawXstack(QStringList &OutInput, uint Num, QString Tmp, QString TmpExt, int LayoutX, int Size,
						   int Height, int BorderLeft, int BorderRight, int BorderUp, int BorderDown, int BorderVertical, int BorderHorizontal,
						   QString Color) {
	QStringList XstackLayout;
	uint UpdatedNum = 0;
	for (uint i = 0; i < Num; i++) {
		QString In = Tmp + QDir::separator() + "screenshot" + QString::number(i + 1) + "." + TmpExt;
		if (!QFileInfo::exists(In)) continue;
		OutInput << "-i" << In;
		int row = i / LayoutX;
		int column = i % LayoutX;
		XstackLayout << QString::number((column * (Size + BorderVertical)) + BorderLeft) + "_" +
					 QString::number((row * (Height + BorderHorizontal)) + BorderUp);
		UpdatedNum++;
	}

	QString Filter = "xstack=inputs=" + QString::number(UpdatedNum) + ":fill=0x" + StringColor2RGB(Color) +
					 ":layout=" + XstackLayout.join('|') +
					 ",pad=color=0x" + StringColor2RGB(Color) + ":w=iw+" + QString::number(BorderRight) +
					 ":h=ih+" + QString::number(BorderDown);

	if (!IsTransparent(Color)) {
		int w, h;
		RawXstackSizeCalc(w, h, Num, LayoutX, Size, Height, BorderLeft, BorderRight, BorderUp, BorderDown,
						  BorderVertical,
						  BorderHorizontal);
		Filter += "[normal];"
				  "color=c=0x" + StringColor2RGB(Color) + ":size=" + QString::number(w) + "x" + QString::number(h) + "[bg];"
				  "[bg][normal]overlay=format=rgb:shortest=1";
	}
	return Filter;
}

void Filters::RawXstack(LayeredFilter &F, uint Num, QString Tmp, QString TmpExt, int LayoutX,
						int Size, int Height, int BorderLeft, int BorderRight, int BorderUp, int BorderDown, int BorderVertical,
						int BorderHorizontal, QString Color) {
	QStringList FileLists;
	QString Filter = RawXstack(FileLists, Num, Tmp, TmpExt, LayoutX, Size, Height, BorderLeft, BorderRight, BorderUp,
							   BorderDown, BorderVertical, BorderHorizontal, SetTransparentRGB(Color));

	foreach (QString File, FileLists) {
		if (File == "-i") continue;
		F.AddInputFile(File, "xt", "", F.merge_none);
	}

	F.AddIntermediate(F.GetInputNames("xt"), "out", Filter);

	// if (!IsTransparent(Color)) {
	// 	int w, h;
	// 	RawXstackSizeCalc(w, h, Num, LayoutX, Size, Height, Border);
	// 	F.AddIntermediate("", "color", "color=c=0x" + StringColor2RGB(Color) + ":size=" + QString::number(
	// 								  w) + "x" + QString::number(h), true);
	// }
}

void Filters::XstackSizeCalc(int &W, int &H, int Height) {
	RawXstackSizeCalc(W, H, NumberOfFrames, LayoutX, Size, Height, GridLeft, GridRight, GridUp,
					  GridDown, GridVertial, GridHorizontal);
}

QString Filters::Xstack(QStringList &OutInput, int Height, QString Color) {
	return RawXstack(OutInput, NumberOfFrames, TMP, TMP_FRAME_FORMAT, LayoutX, Size, Height, GridLeft, GridRight, GridUp,
					 GridDown, GridVertial, GridHorizontal, Color);
}

void Filters::Xstack(LayeredFilter &F, int Height, QString Color) {
	return RawXstack(F, NumberOfFrames, TMP, TMP_FRAME_FORMAT, LayoutX, Size, Height, GridLeft, GridRight, GridUp,
					 GridDown, GridVertial, GridHorizontal, Color);
}

QString Filters::RawBlur(QString Sigma) {
	return "gblur=sigma=" + Sigma;
}

QString Filters::RawExposure(QString Exposure) {
	return "exposure=exposure=" + Exposure;
}

QString Filters::RawSaturation(QString Saturation) {
	return "eq=saturation=" + Saturation;
}

QString Filters::RawOpacity(QString Opacity) {
	return "format=rgba,colorchannelmixer=aa=" + Opacity;
}

QString Filters::RawColorKey(QString Color, int Similarity, int Blend) {
	return "colorkey=color=" + StringColor2RGB(Color) +
		   ":similarity=" + QString::number((double)Similarity / (double)100, 'f', 2) +
		   ":blend=" + QString::number((double)Blend / (double)100, 'f', 2);
}
