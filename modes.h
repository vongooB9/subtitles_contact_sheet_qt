#ifndef MODES_H
#define MODES_H
#include <QString>
#include "paramssettings.h"
#include "textsustitution.h"
#include "subtitles.h"

class modes {
	public:
		modes(QString mode, bool subtitles, ParamsSettings *ps, TextSustitution *ts);

		enum modes_t {mode_time, mode_line, mode_video_time, mode_video_end, mode_random, mode_fixed};
		void SetMode(QString mode);
		bool IsVideoEnd();
		void SetTimestamps(QList<sub_line_t> *timestamps);
		QList<sub_line_t> GenerateScreenshots();
		bool RepFramesEnabled();
	private:
		modes_t Mode;
		ParamsSettings *PS;
		TextSustitution *TS;
		int StartTime;
		int EndTime;
		int StartLine;
		int EndLine;
		int64_t Duration;
		int FixedInterval;
		QList<sub_line_t> GenerateTime();
		QList<sub_line_t> GenerateLine();
		QList<sub_line_t> GenerateVideoTime();
		QList<sub_line_t> GenerateVideoEnd();
		QList<sub_line_t> GenerateFixed();
		QList<sub_line_t> *Timestamps;
		QList<sub_line_t> Screenshots;
};

#endif // MODES_H
