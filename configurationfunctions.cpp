#include "configurationfunctions.h"

#include <QStandardPaths>

void ProcessFunctions(ParamsSettings *PS, TextSustitution *TS) {
	QStringList ListParams = PS->Params();
	ListParams.sort();
	for (int i = 0; i < ListParams.size(); ++i) {
		QString Param = ListParams.at(i);
		QString Value = PS->Value(Param);
		QString FUNCTION = PS->Function(Param);
		if (FUNCTION != "")
			PrintDebug("---> " + Param + " = " + FUNCTION);
		else
			PrintDebug("---> " + Param + " = " + Value);

		if (PS->IsUserDefined(Param)) {
			ParamsSettings::context_type Context;
			QString VarName;
			if (!PS->GetVarAndContext(Param, VarName, Context)) continue;

			if (Context == ParamsSettings::context_global) {
				if (FUNCTION != "")
					Value = TS->DetectAndEjecuteFunction(FUNCTION, "");
				PrintDebug("\t" + VarName + " = " + Value);
				TS->Insert(VarName, Value, false);
				continue;
			}

			for (int i = 0; i < TS->Value("streams.count").toInt(); i++) {
				QString CONTEXT = "streams." + QString::number(i) + ".";
				QString STREAM_TYPE = TS->Value(CONTEXT + "codec_type");
				QString LocalVarName = VarName;
				switch (Context) {
					case ParamsSettings::context_stream:
						break;
					case ParamsSettings::context_video:
						if (STREAM_TYPE != "video") continue;
						break;
					case ParamsSettings::context_audio:
						if (STREAM_TYPE != "audio") continue;
						break;
					case ParamsSettings::context_subtitle:
						if (STREAM_TYPE != "subtitle") continue;
						break;
					case ParamsSettings::context_attachment:
						if (STREAM_TYPE != "attachment") continue;
						break;
					default:
						continue;
				}

				if (LocalVarName.startsWith("context.")) LocalVarName.replace("context.", CONTEXT);

				if (!TS->Contains(LocalVarName)) TS->Insert(LocalVarName, "", false);
				if (FUNCTION != "")
					Value = TS->DetectAndEjecuteFunction(FUNCTION, CONTEXT);
				PrintDebug("\t" + LocalVarName + " = " + Value);
				TS->Update(LocalVarName, Value, false);
			}
		} else {
			if (FUNCTION == "") continue;
			Value = TS->DetectAndEjecuteFunction(FUNCTION, "");
			PS->setValue(Param, Value, false);
		}
	}
}

QString AutoDetectVRFormat = "";
QString AutoDetectVRMode = "";
QString AutoDetectVRFov = "";

void VRAutodetection(ParamsSettings *PS, TextSustitution *TS, QString Filename, double AR, int W, int H) {
	bool AutoDetectVREnable = true;
	AutoDetectVREnable = !PS->Bool("vr_disable_autodetection");

	//VR Filename Detection
	QString FileNameUpper = Filename.toUpper();
	if (AutoDetectVREnable && AutoDetectVRFormat == "") {
		if (FileNameUpper.contains("MKX200")) {
			PrintInfo("\tVR Filename Detection: contains MKX200");
			AutoDetectVRFormat = "fisheye";
			AutoDetectVRMode = "sbs";
			AutoDetectVRFov = "200";
		} else if (FileNameUpper.contains("MKX22") || FileNameUpper.contains("VRCA220")) {
			PrintInfo("\tVR Filename Detection: contains MKX22 or VRCA220");
			AutoDetectVRFormat = "fisheye";
			AutoDetectVRMode = "sbs";
			AutoDetectVRFov = "220";
		} else if (FileNameUpper.contains("FISHEYE190") || FileNameUpper.contains("RF52")) {
			PrintInfo("\tVR Filename Detection: contains FISHEYE190 or RF52");
			AutoDetectVRFormat = "fisheye";
			AutoDetectVRMode = "sbs";
			AutoDetectVRFov = "190";
		} else if (FileNameUpper.contains("F180") || FileNameUpper.contains("180F")) {
			PrintInfo("\tVR Filename Detection: contains F180 or 180F");
			AutoDetectVRFormat = "fisheye";
			AutoDetectVRMode = "sbs";
			AutoDetectVRFov = "180";
		} else if (FileNameUpper.contains("360_SBS") || FileNameUpper.contains("SBS_360")) {
			PrintInfo("\tVR Filename Detection: contains 360 and SBS");
			AutoDetectVRFormat = "equirect";
			AutoDetectVRMode = "sbs";
			AutoDetectVRFov = "360";
		} else if (FileNameUpper.contains("TB_360") || FileNameUpper.contains("360_TB") || FileNameUpper.contains("360_OI") ||
				   FileNameUpper.contains("OI_360")) {
			PrintInfo("\tVR Filename Detection: contains 360 and TB or OI");
			AutoDetectVRFormat = "equirect";
			AutoDetectVRMode = "tb";
			AutoDetectVRFov = "360";
		} else if (FileNameUpper.contains("MONO_360") || FileNameUpper.contains("360_MONO") ||
				   FileNameUpper.contains("360MONO") || FileNameUpper.contains("MONO360")) {
			PrintInfo("\tVR Filename Detection: contains 360 and MONO");
			AutoDetectVRFormat = "equirect";
			AutoDetectVRMode = "2d";
			AutoDetectVRFov = "360";
		} else if (FileNameUpper.contains("TB_180") || FileNameUpper.contains("180_TB") || FileNameUpper.contains("180TB") ||
				   FileNameUpper.contains("TB180") || FileNameUpper.contains("180_3DV") || FileNameUpper.contains("3DV_180")) {
			PrintInfo("\tVR Filename Detection: contains 180 and TB or 3DV");
			AutoDetectVRFormat = "equirect";
			AutoDetectVRMode = "tb";
			AutoDetectVRFov = "180";
		} else if (FileNameUpper.contains("LR_180") || FileNameUpper.contains("180_LR") || FileNameUpper.contains("180LR") ||
				   FileNameUpper.contains("LR180") || FileNameUpper.contains("180_3DH") || FileNameUpper.contains("3DH_180")) {
			PrintInfo("\tVR Filename Detection: contains 180 and LR or 3DH");
			AutoDetectVRFormat = "equirect";
			AutoDetectVRMode = "sbs";
			AutoDetectVRFov = "180";
		} else if (FileNameUpper.contains("180X180")) {
			PrintInfo("\tVR Filename Detection: contains 180");
			AutoDetectVRFormat = "equirect";
			if (FileNameUpper.contains("3DV") || FileNameUpper.contains("TB")) {
				PrintInfo("\tVR Filename Detection: contains TB or 3DV");
				AutoDetectVRMode = "tb";
			} else {
				AutoDetectVRMode = "sbs";
			}
			AutoDetectVRFov = "180";
		} else if (FileNameUpper.contains("3DV")) {
			PrintInfo("\tVR Filename Detection: contains 3DV");
			AutoDetectVRFormat = "equirect";
			AutoDetectVRMode = "tb";
			AutoDetectVRFov = "360";
		} else if (FileNameUpper.contains("3DH")) {
			PrintInfo("\tVR Filename Detection: contains 3DH");
			AutoDetectVRFormat = "equirect";
			AutoDetectVRMode = "sbs";
			AutoDetectVRFov = "360";
		}
	}

	//VR Metadata Detection
	if (AutoDetectVREnable && TS->Value("streams.video.first.side_data_list.count", "0").toInt() > 0 &&
		AutoDetectVRFormat == "") {
		for (int i = 0; i < TS->Value("streams.video.first.side_data_list.count", "0").toInt(); ++i) {
			QString DataType = TS->Value("streams.video.first.side_data_list." + QString::number(i) + ".side_data_type", "");
			if ( DataType == "Stereo 3D") {
				QString Type3D = TS->Value("streams.video.first.side_data_list." + QString::number(i) + ".type", "");
				if ( Type3D == "side by side") {
					AutoDetectVRMode = "sbs";
				} else if ( Type3D == "top and bottom") {
					AutoDetectVRMode = "tb";
				} else if ( Type3D == "2D") {
					AutoDetectVRMode = "2d";
				}
				if (AutoDetectVRMode != "") PrintInfo("\tVR Metadata Detection: Stereo " + TS->Value("vr.mode"));
			}
			if ( DataType == "Spherical Mapping") {
				QString Projection = TS->Value("streams.video.first.side_data_list." + QString::number(i) + ".projection", "");
				if ( Projection == "equirectangular") {
					AutoDetectVRFormat = "equirect";
					if (AutoDetectVRMode != "") AutoDetectVRMode = "sbs";
					AutoDetectVRFov = "180";
					PrintInfo("\tVR Metadata Detection: Equirrectangural, assumed 180 degree");
				}
			}
		}
	}

	//VR Aspect Ratio Detection
	if (AutoDetectVREnable && PS->Bool("vr_detect_ar") && AutoDetectVRFormat == "" && H > 0 && W > 0) {
		if (AR == 2) {
			PrintInfo("\tVR Detection: Aspect Ratio = 2");
			AutoDetectVRFormat = "equirect";
			AutoDetectVRMode = "sbs";
			AutoDetectVRFov = "180";
		} else if (AR == 1) {
			PrintInfo("\tVR Detection: Aspect Ratio = 1");
			AutoDetectVRFormat = "equirect";
			AutoDetectVRMode = "tb";
			AutoDetectVRFov = "360";
		} else if ((double)H / (double)W == 2) {
			PrintInfo("\tVR Detection: Aspect Ratio = 0.5");
			AutoDetectVRFormat = "equirect";
			AutoDetectVRMode = "tb";
			AutoDetectVRFov = "180";
		}
	}

	if (AutoDetectVRFormat != "") {
		TS->Insert("vr.file", "1");
		TS->Insert("vr.format", AutoDetectVRFormat);
		TS->Insert("vr.mode", AutoDetectVRMode);
		TS->Insert("vr.fov", AutoDetectVRFov);
	} else {
		TS->Insert("vr.file", "0");
	}
}

QStringList FFMPEG_COMPILE;
bool ffmpeg_has_libfontconfig = false;
bool DetectFontconfig() {
	QString FFMPEG_COMP_TMP = ReadProgram(FFMPEG, {"-hide_banner", "-loglevel", "error", "-buildconf"});
	if (FFMPEG_COMP_TMP == "") {
		PrintError("Error with ffmpeg check if the configuration is correct");
		return false;
	}

	FFMPEG_COMPILE = FFMPEG_COMP_TMP.split("\n");

	for (int i = 0; i < FFMPEG_COMPILE.count(); i++) {
		FFMPEG_COMPILE.replace(i, FFMPEG_COMPILE.at(i).trimmed());
	}

	if (FFMPEG_COMPILE.contains("--enable-libfontconfig") ||
		FFMPEG_COMPILE.contains("--enable-fontconfig")) ffmpeg_has_libfontconfig = true;

	return true;
}

bool AdaptFont(ParamsSettings *PS, QString FontOption, QString &Font) {
	Font = PS->Value(FontOption);
	bool FontIsFile = QFile::exists(Font);

	if (!ffmpeg_has_libfontconfig && !FontIsFile) {
		QString fcmatch = QStandardPaths::findExecutable("fc-match");
		if (fcmatch.isEmpty()) {
			FontIsFile = QFile::exists(PS->AppPath + QDir::separator() + Font);
			if (FontIsFile) {
				Font = PS->AppPath + QDir::separator() + Font;
			}
		} else {
			Font = ReadProgram(fcmatch, {"-f", "'%{file}'", Font});
			FontIsFile = true;
		}
		if (FontIsFile || Font == "") {
			PrintError("Error reading font file, configure " + FontOption + " with a valid ttf file, using default Font");
			Font = "";
			return false;
		}
	}
	if (FontIsFile) Font = QDir::toNativeSeparators(Font);
	return FontIsFile;
}

QString GetIndividalNumber(TextSustitution *TS, int I, const sub_line_t *ActualScreenshot, QString Value) {
	QString Result = "";
	bool suffix_number_isn = false;
	int suffix_number = Value.toInt(&suffix_number_isn);
	if (suffix_number_isn) {
		if (suffix_number == 1)
			Result = QString::number(I);
		else
			Result = QString::number(I).rightJustified(suffix_number, '0');
	} else if (Value == "auto") {
		Result = QString::number(I).rightJustified(QString::number(NumberOfFrames).size(), '0');
	} else if (Value == "short") {
		if (TS->Value("format.duration").toFloat() > 3600)
			Result = QTime::fromMSecsSinceStartOfDay(ActualScreenshot->start).toString("hh:mm:ss");
		else
			Result = QTime::fromMSecsSinceStartOfDay(ActualScreenshot->start).toString("mm:ss");
	} else if (Value == "time") {
		Result = QTime::fromMSecsSinceStartOfDay(ActualScreenshot->start).toString("hh:mm:ss.zzz");
	} else if (Value != "") {
		Result = QTime::fromMSecsSinceStartOfDay(ActualScreenshot->start).toString(Value);
	}
	return Result;
}
