#ifndef FILTERS_H
#define FILTERS_H

#include "paramssettings.h"
#include "textsustitution.h"
#include "layeredfilter.h"

class Filters {
	public:
		Filters(ParamsSettings *ps, TextSustitution *ts);
		~Filters();
		static QString RawFPS(QString fps);
		QString FPS();

		static QString RawThumbnail(QString n);
		QString Thunbnail();

		static QString RawCrop(QString w, QString h, QString x, QString y);
		QString EyeCrop(QString VR_IN_STEREO);

		QString VR(QString VR_IN_STEREO, QString VR_W, QString VR_H,
				   int VR_IH_FOV, int VR_IV_FOV);

		static QString RawScale(QString W, QString H,
								QString Algo = "lanczos", QString Flags = "",
								QString ForceAR = "", QString Divisible = "");
		static QString RawZScale(QString W, QString H,
								 QString Algo = "lanczos");
		QString Scale(int W, int H, QString ForceAR = "");

		static QString RawSubtitles(QString F, QString FontsDir,
									QString ForceStyle);
		QString Subtitles(QString File, bool ForceStyle);

		enum ft_positions {
			ft_up_left,
			ft_up_right,
			ft_down_left,
			ft_down_right,
			ft_up_center,
			ft_down_center,
			ft_middle_left,
			ft_middle_right
		};
		static ft_positions String2FrameTimePosition(QString pos);

		static QString RawDrawtext(
					QString Text, bool EscapeText, QString Font, bool FontIsFile,
					int &AddedW, int &AddedH, QString Position = "up_right",
					QString Vertical = "disabled", bool Outside = false,
					QString fontsize = "12", QString fontcolor = "white",
					QString boxborderw = "2", QString boxcolor = "black",
					QString borderw = "0", QString bordercolor = "black",
					QString shadowcolor = "black", QString shadowx = "0",
					QString shadowy = "0", int Margin = 0, QString Custom = "");

		QString Drawtext(int &AddedW, int &AddedH);

		QString ScreenshostLogoOverlayOptions();

		// static QString RawTile(QString Layout, int Border, QString Color);

		static QString RawPad(QString W, QString H, QString X, QString Y,
							  QString Color = "black");

		static QString
		RawResizeBlurBG(QString W, QString H, QString BgW, QString BgH,
						QString Algo = "lanczos", QString Flags = "",
						QString SizeX = "20", QString Exposure = "",
						QString Saturation = "", QString Aditional = "",
						QString AditionalBG = "", QString AditionalImg = "");
		QString ResizeBlurBG(int W, int H, int BGAddW = 0, int BGAddH = 0,
							 QString Aditional = "");

		static QString RawMask(int W, int H, QString Mask, bool Scale = true,
							   QString AditionalFilters = "");
		static void RawSVGRoundedCorners(int W, int H, int R, QString File);
		QString Mask(int W, int H, QString AditionalFilters = "");
		void LayeredMask(LayeredFilter &F, int W, int H,
						 QString AditionalFilters = "",
						 QString AlphamergeOptions = "");

		static QString String2GradientFilter(int width, int height,
											 QString gradient);
		static QString GenerateGradientFilter(int width, int height,
											  QString type, QString colors,
											  QString direction);
		static QString GenerateSolidColorFilter(int width, int height,
												QString color);
		static bool GenerateGradient(QString Out, int width, int height,
									 QString gradient);

		static void RawXstackSizeCalc(int &W, int &H, uint Num, int LayoutX, int Size, int Height,
									  int BorderLeft, int BorderRight, int BorderUp, int BorderDown, int BorderVertical, int BorderHorizontal);
		static QString RawXstack(QStringList &OutInput, uint Num, QString Tmp, QString TmpExt, int LayoutX, int Size,
								 int Height, int BorderLeft, int BorderRight, int BorderUp, int BorderDown, int BorderVertical, int BorderHorizontal,
								 QString Color);
		static void RawXstack(LayeredFilter &F, uint Num, QString Tmp, QString TmpExt, int LayoutX, int Size, int Height,
							  int BorderLeft, int BorderRight, int BorderUp, int BorderDown, int BorderVertical, int BorderHorizontal,
							  QString Color);

		void XstackSizeCalc(int &W, int &H, int Height);
		QString Xstack(QStringList &OutInput, int Height, QString Color);
		void Xstack(LayeredFilter &F, int Height, QString Color);

		static QString RawBlur(QString Sigma);
		static QString RawExposure(QString Exposure);
		static QString RawSaturation(QString Saturation);
		static QString RawOpacity(QString Opacity);

		static QString RawColorKey(QString Color, int Similarity, int Blend);

	protected:
		ParamsSettings *PS;
		TextSustitution *TS;
		int GridBorder;
		int GridUp;
		int GridDown;
		int GridLeft;
		int GridRight;
		int GridVertial;
		int GridHorizontal;
};

#endif // FILTERS_H
