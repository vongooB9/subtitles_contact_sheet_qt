#ifndef FFMPEGINSTANCES_H
#define FFMPEGINSTANCES_H

#include <QProcess>
#include <QQueue>
#include <QString>
#include <QStringList>

class FFmpegInstances {
	public:
		struct processqueue_t {
			QStringList Params;
			QString Message;
			QStringList DependentParams;
			QString DependentMessage;
		};

		struct instance_t {
			QProcess *Process;
			processqueue_t Queue;
			bool InProgress;
			QString Command;
		};
		FFmpegInstances(int CreateInstances);
		void Free();

		void AddJob(QStringList &Params, QString &Massage);
		void AddJob(QStringList &Params, QString &Massage, QStringList &DependentParams, QString &DependentMessage);

		bool Run();
	protected:
		QList<instance_t> Instances;
		QQueue<processqueue_t> ProcessQueue;



};

#endif // FFMPEGINSTANCES_H
