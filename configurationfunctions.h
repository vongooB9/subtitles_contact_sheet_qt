#ifndef CONFIGURATIONFUNCTIONS_H
#define CONFIGURATIONFUNCTIONS_H

#include "paramssettings.h"
#include "textsustitution.h"
#include "subtitles.h"

void ProcessFunctions(ParamsSettings *PS, TextSustitution *TS);

extern QString AutoDetectVRFormat;
extern QString AutoDetectVRMode;
extern QString AutoDetectVRFov;
void VRAutodetection(ParamsSettings *PS, TextSustitution *TS, QString Filename, double AR, int W, int H);

extern QStringList FFMPEG_COMPILE;
extern bool ffmpeg_has_libfontconfig;
bool DetectFontconfig();

bool AdaptFont(ParamsSettings *PS, QString FontOption, QString &Font);

QString GetIndividalNumber(TextSustitution *TS, int I, const sub_line_t *ActualScreenshot, QString Value);

#endif // CONFIGURATIONFUNCTIONS_H
