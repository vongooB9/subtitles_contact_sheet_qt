# More information on the [Subtitles Contact Sheet QT Site](https://vongoob9.gitlab.io/subtitles_contact_sheet_qt).

# Subtitles Contact Sheet QT

This program generates video, subtitles and images contact sheets, previews, small animations (gif, webp,...), screenshots and thumbnails. it is programmed in C++ using the QT libraries, It is specially designed to display subtitles, but isn't limited to that. It works with subtitles in **srt**, **ass**, **vtt** format and with all video files supported by **FFmpeg**. It has many options for customization of appearance, performance and control over the output format. It also has batch processing options and can save multiple configurations to make it much easier to use. It also has a GUI that allows you to edit the configuration and compose the command line, it is fully dynamic to adapt to all needs.

## Features

- Multiplatform (linux, windows and macos).
- Supports all FFmpeg video formats.
- Supports srt, ass and vtt subtitles.
- Supports multiple configuration files.
- It can create contact sheets and animations of folders with images.
- Parallel processing, can perform multiple screenshots simultaneously.
- Can select keyframes for speedup the screenshots.
- Can skip black frames.
- Can select frames containing subtitles.
- It has a text block that displays information about the video and/or subtitles, fully configurable in content and appearance.
- You can create gif, webp animations and x264, x265 videos, compositing the final result in several ways, individual clips, concatenated or in a grid.
- It has batch processing options. You can process all files recursively from one or several folders and also process the same files with several different options.
- It supports VR video, detects its format, displays the information on the contact sheet and can process the image to show only half of the 3D and process the image to correct the distortion.
- It allows to select the output folder, absolute or relative to the input file.
- It can rename, create soft and hard links, shortcuts and organize input files by folders using the video metadata.
- It can generate text files (html, bbcode...) using templates and the video metadata.

## Usage

A complete list of options with explanations of how to use them can be found on the [man page](manpage.md), and the [GUI manual](guimanual.md).

### Images Examples

- [Video examples](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/wikis/Video-examples)
- [Subtitles examples](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/wikis/Subtitles-examples)
- [Image folder examples](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/wikis/Image-folder-examples)
- [VR examples](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/wikis/VR-examples) OUTDATED
- [Customization examples](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/wikis/Customization-examples) OUTDATED

## Download and install

The binaries can be downloaded from the [Deployments page](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/releases).

### Windows

In windows it works as a portable application, just unzip the zip file and put inside the files ffmpeg.exe and ffprobe.exe. Can be downloaded by following the instructions on the [FFmpeg page](https://ffmpeg.org/download.html#build-windows).

### Linux

For distributions that support it, you can install the **.deb**, For the others the easiest way is to use the AppImage packages. Or alternatively you can compile it using the manual below.

### macOS

For macOS I can only distribute the GUI in dmg format with everything needed inside. To install the command line application for now you have to compile it and use "make install".

## Compiling

### Requirements

Needs qmake and QT base development files, In debian they are the packages *qtbase5-dev* and *qt5-qmake* or *qmake6* and *qt6-base-dev* for the QT6 version.

### Linux

[GNU/Linux Compilations instructions](Manuals/compile_linux.md)

### Windows

[Window Compilations instructions](Manuals/compile_windows.md)
