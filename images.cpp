#include "images.h"
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "common.h"
#include "configurationfunctions.h"
#include <QCollator>
#include <QRandomGenerator>
#include <algorithm>

Images::Images(QString imgdir, ParamsSettings *ps, TextSustitution *ts, FFmpegInstances *inst) {
	PS = ps;
	TS = ts;
	Inst = inst;
	ImgDir = imgdir;
	FilterList = PS->Value("images_search_filter").split("|");
	LAYOUT = PS->Value("layout");
	NumberOfFrames = LayoutSplit(LAYOUT, LayoutX, LayoutY);

	StatsEnabled = PS->Bool("images_resolution_stats");
	if (PS->Bool("concat")) StatsEnabled = false;

	if (PS->Value("tb") == "hidden") StatsEnabled = false;

	Recursive = PS->Bool("images_recursive");

	Stats = {0, 0, 0, UINT_MAX, {}, {}};

	QDir DirObj(ImgDir);
	DirName = DirObj.dirName();
	QDir DirPathObj(ImgDir + "/..");
	DirPath = DirPathObj.absolutePath();

	TS->Insert("dir.name", DirName);
	TS->Insert("dir.path", DirPath);

	ShowTB = (PS->Value("tb") != "hidden");
	NeedBG = (PS->NotEmpty("bg_image") || PS->Value("bg_gradient") != "none");
	NeedWatermark = PS->NotEmpty("watermark");

	CTB = new ContactSheetTextBlock(PS, TS);
	F = new Filters(PS, TS);
}

void Images::GenerateImageList() {
	PrintInfo("Searching for image files:");
	ImgList = GenerateImageListReculsive(ImgDir, Recursive);
	QCollator Collator;
	Collator.setNumericMode(true);
	std::sort(ImgList.begin(), ImgList.end(), Collator);
	PrintInfo("\tOk\n");
}

QStringList Images::GenerateImageListReculsive(QString Dir, bool Recursive) {
	QDir DirObj(Dir);
	QStringList CurrentList = DirObj.entryList(FilterList, QDir::Files);
	for (int i = 0; i < CurrentList.count(); ++i) {
		CurrentList[i] = QDir::toNativeSeparators(Dir + QDir::separator() + CurrentList.at(i));
	}
	if (Recursive) {
		QStringList DirList  = DirObj.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
		for (int i = 0; i < DirList.count(); ++i) {
			CurrentList.append(GenerateImageListReculsive(Dir + QDir::separator() + DirList.at(i), true));
		}
	}
	return CurrentList;
}

void Images::AddFilter(QString Filter) {
	Filter.append(Filter);
}

void Images::SetFilters(QStringList filters) {
	FilterList = filters;
}

bool Images::SelectImages() {
	PrintInfo("Selecting " + QString::number(NumberOfFrames) + " image files:");

	if (ImgList.count() <= (int)NumberOfFrames) {
		if (ImgList.count() < (int)NumberOfFrames) {
			NumberOfFrames = ImgList.count();
			LayoutY = NumberOfFrames / LayoutX;
			if (NumberOfFrames % LayoutX != 0) LayoutY++;
			LAYOUT = QString::number(LayoutX) + "x" + QString::number(LayoutY);
			PrintInfo("Images in directory lees than layout, ajusting layout rows to " + LAYOUT);
		}

		for (int i = 0; i < ImgList.count(); ++i)
			SelectedOrder.append(i);

	} else if (PS->Value("images_select") == "range" || PS->Value("images_select") == "random_range") {
		uint EveryImages = ImgList.count() / NumberOfFrames;
		uint Compare = 0;
		if (PS->Value("images_select") == "random_range")
			Compare = QRandomGenerator::global()->bounded((int)1, (int)EveryImages);
		for (int i = 0; i < ImgList.count(); ++i) {
			if (i % EveryImages == Compare) SelectedOrder.append(i);
			if (SelectedOrder.count() >= (int)NumberOfFrames) break;
		}

	} else if (PS->Value("images_select") == "random_ordered" || PS->Value("images_select") == "random") {
		for (int i = 0; i < ImgList.count(); ++i)
			SelectedOrder.append(i);

		while ((uint)SelectedOrder.count() > NumberOfFrames) {
			int Discart = QRandomGenerator::global()->bounded((int)0, (int)SelectedOrder.count() - 1);
			SelectedOrder.removeAt(Discart);
		}
	}

	if (PS->Value("images_select") == "random") {
		std::random_device rd;
		std::mt19937 g(rd());
		std::shuffle(SelectedOrder.begin(), SelectedOrder.end(), g);
	}

	for (int i = 0; i < ImgList.count(); ++i) {
		//TODO: Try to do this in parallel
		QString Name;
		uint Width = 0;
		uint Height = 0;
		double AR = 0;
		TextSustitution *ITS = nullptr;
		if (StatsEnabled) {
			ITS = ReadImageMetadata(ImgList.at(i), Name, Width, Height, AR);
			uint FileSize = ITS->Value("file.size").toUInt();
			QString Ext = ITS->Value("file.extension");
			QString Res = QString::number(Width) + "x" + QString::number(Height);
			Stats.Count++;
			Stats.Size += FileSize;
			if (FileSize > Stats.MaxFileSize) Stats.MaxFileSize = FileSize;
			if (FileSize < Stats.MinFileSize) Stats.MinFileSize = FileSize;
			if (Stats.FormatsList.contains(Ext)) {
				Stats.FormatsList[Ext].Count++;
				Stats.FormatsList[Ext].Size += FileSize;
			} else {
				Stats.FormatsList.insert(Ext, {1, FileSize});
			}
			if (Stats.ResolutionsList.contains(Res)) {
				Stats.ResolutionsList[Res].Count++;
				Stats.ResolutionsList[Res].Size += FileSize;
			} else {
				Stats.ResolutionsList.insert(Res, {1, FileSize});
			}
		} else {
			Stats.Count++;
			QFileInfo FI(ImgList.at(i));
			uint FileSize = FI.size();
			Stats.Size += FileSize;
			if (FileSize > Stats.MaxFileSize) Stats.MaxFileSize = FileSize;
			if (FileSize < Stats.MinFileSize) Stats.MinFileSize = FileSize;
			QString Ext = FI.suffix();
			if (Stats.FormatsList.contains(Ext)) {
				Stats.FormatsList[Ext].Count++;
				Stats.FormatsList[Ext].Size += FileSize;
			} else {
				Stats.FormatsList.insert(Ext, {1, FileSize});
			}
		}

		if (!SelectedOrder.contains(i)) continue;

		if (SelectedImages.count() < (int)NumberOfFrames) {
			PrintInfo("\tSelected image " + ImgList.at(i));
			if (!StatsEnabled) ITS = ReadImageMetadata(ImgList.at(i), Name, Width, Height, AR);
			selected_images_t Selected = {ImgList.at(i), Name, Width, Height, AR, ITS};
			SelectedImages.insert(i, Selected);
			AddProgress();
		}
	}

	TS->Insert("stats.count", QString::number(Stats.Count));
	TS->Insert("stats.size", QString::number(Stats.Size));
	TS->Insert("stats.max_file_size", QString::number(Stats.MaxFileSize));
	TS->Insert("stats.min_file_size", QString::number(Stats.MinFileSize));
	TS->Insert("stats.avg_file_size", QString::number(Stats.Size / Stats.Count));

	if (PS->Bool("images_format_stats")) {
		TS->Insert("stats.formats.count", QString::number(Stats.FormatsList.count()));

		QMultiMap<int, int> FormatsOrdered;
		QStringList Keys = Stats.FormatsList.keys();
		for (int i = 0; i < Keys.count(); ++i) {
			FormatsOrdered.insert(Stats.FormatsList.value(Keys.at(i)).Count, i);
		}

		int Counter = 0;
		while (!FormatsOrdered.isEmpty()) {
			int Index = FormatsOrdered.last();
			QString Key = Keys.at(Index);
			count_t C = Stats.FormatsList.value(Key);
			TS->Insert("stats.formats." + QString::number(Counter) + ".name", Key);
			TS->Insert("stats.formats." + QString::number(Counter) + ".count", QString::number(C.Count));
			TS->Insert("stats.formats." + QString::number(Counter) + ".size", QString::number(C.Size));
			FormatsOrdered.remove(FormatsOrdered.lastKey(), Index);
			Counter++;
		}
	}

	if (PS->Bool("images_resolution_stats")) {
		TS->Insert("stats.resolutions.count", QString::number(Stats.ResolutionsList.count()));

		QMultiMap<int, int> ResOrdered;
		QStringList Keys = Stats.ResolutionsList.keys();
		for (int i = 0; i < Keys.count(); ++i) {
			ResOrdered.insert(Stats.ResolutionsList.value(Keys.at(i)).Count, i);
		}

		int Counter = 0;
		while (!ResOrdered.isEmpty()) {
			int Index = ResOrdered.last();
			QString Key = Keys.at(Index);
			count_t C = Stats.ResolutionsList.value(Key);
			TS->Insert("stats.resolutions." + QString::number(Counter) + ".name", Key);
			TS->Insert("stats.resolutions." + QString::number(Counter) + ".count", QString::number(C.Count));
			TS->Insert("stats.resolutions." + QString::number(Counter) + ".size", QString::number(C.Size));
			ResOrdered.remove(ResOrdered.lastKey(), Index);
			Counter++;
		}
	}

	PrintInfo("\tOk\n");
	return true;
}

QStringList Images::FfmpegResizeImage(selected_images_t Image, QString Output) {
	QStringList Result;
	Result << "-hide_banner" << "-loglevel" << "error" << "-y"
		   << "-i" << Image.File;

	QString TextFilter = "";
	int AddedW = 0;
	int AddedH = 0;

	class Filters F(PS, TS);

	if (PS->Bool("images_text")) {
		QString Text = PS->Value("images_custom_text");
		if (Image.ITS != nullptr) Text = Image.ITS->ProcessCMD("", Text);
		if (PS->Int("images_max_text") > 0)
			Text = TextSustitution::CutCenterText(Text, PS->Int("images_max_text"));

		QString Font;
		bool FontIsFile = AdaptFont(PS, "images_font", Font);

		if (Text != "") {
			TextFilter = F.RawDrawtext(
									 Text,
									 true,
									 Font,
									 FontIsFile,
									 AddedW,
									 AddedH,
									 PS->Value("images_pos"),
									 PS->Value("images_vertical"),
									 !PS->Bool("images_text_overlay"),
									 PS->Value("images_size"),
									 PS->Value("images_color"),
									 PS->Value("images_border"),
									 PS->Value("images_bg"),
									 PS->Value("images_font_border"),
									 PS->Value("images_font_border_color"),
									 PS->Value("images_shadow_color"),
									 PS->Value("images_shadow_x"),
									 PS->Value("images_shadow_y"),
									 PS->Int("images_margin")
						 );
		}
	}

	QString Color = PS->Value("images_fill_color");
	QString Filter = "format=rgba";

	if (Color == "image_fill") {
		Filter += "," + F.ResizeBlurBG(OutputW, OutputH, AddedW, AddedH, TextFilter);
	} else {
		if (Color == "grid_border") Color = CTB->GridBorderColor;

		Filter += ", " + F.RawScale(QString::number(OutputW),
									QString::number(OutputH),
									PS->Value("scale_algorithm"),
									PS->Value("scale_flags"),
									"decrease");

		if (!PS->Bool("images_text_padding")) {
			if (TextFilter != "") Filter += "," + TextFilter;
			Filter += "," + F.RawPad(QString::number(OutputW + AddedW),
									 QString::number(OutputH + AddedH),
									 "(ow-iw)/2",
									 "(oh-ih)/2",
									 Color);
		} else {
			Filter += "," + F.RawPad(QString::number(OutputW),
									 QString::number(OutputH),
									 "(ow-iw)/2",
									 "(oh-ih)/2",
									 Color);
			if (TextFilter != "") Filter += "," + TextFilter;
		}
	}

	bool out_map = false;
	if (PS->NotEmpty("mask")) {
		Filter = "[0:v:0]" + Filter;
		Filter += F.Mask(OutputW + AddedW, OutputH + AddedH);
		Filter += "[out]";
		out_map = true;
	}

	if (out_map)
		Result << "-filter_complex" << Filter << "-map" << "[out]";
	else
		Result << "-vf" << Filter;

	Result << "-pix_fmt" << "rgba" << "-frames:v" << "1" << Output;

	return Result;
}

void Images::CalculateOutputResolution() {
	PrintInfo("Calculating thumbnains resolution:");
	int Size = PS->Int("size");
	int MinSize = PS->Int("min_size");
	if (Size == 0) {
		PrintError("\tImages input needs a defined size, using 640px by default");
		Size = 640;
	}
	int ARVerticalCount = 0;
	int ARHorizontalCount = 0;
	QMap<double, int> ARCount;
	foreach (selected_images_t val, SelectedImages) {
		double AR = val.AspectRatio;
		if (ARCount.contains(AR)) {
			ARCount[AR]++;
		} else {
			ARCount.insert(AR, 1);
		}
		if (AR >= 1) {
			ARHorizontalCount++;
		} else {
			ARVerticalCount++;
		}
	}

	bool ARVertical = (ARVerticalCount > ARHorizontalCount);

	int VertHoriz = ARVertical ? ARHorizontalCount : ARVerticalCount;
	VertHoriz = (double)VertHoriz / (double)SelectedImages.count() * 100;

	PrintInfo("\t" + QString::number(ARVerticalCount) + " vertical images, " + QString::number(
						  ARHorizontalCount) + " horizontal images, " + QString::number(VertHoriz) + "%");

	double ARMoreUsed = 0;

	if (PS->Int("images_ar_selection") > 0 && VertHoriz >= PS->Int("images_ar_selection")) {
		PrintInfo("\tUsing output aspect ratio 1 (" + QString::number(VertHoriz) + "% >= " +
				  PS->Value("images_ar_selection") + "%)");
		ARMoreUsed = 1;
	} else {
		int MaxCount = 0;
		QMap<double, int>::const_iterator i;
		for (i = ARCount.constBegin(); i != ARCount.constEnd(); ++i) {
			if (ARVertical && i.key() > 1) continue;
			if (!ARVertical && i.key() <= 1) continue;
			if (i.value() > MaxCount) {
				MaxCount = i.value();
				ARMoreUsed = i.key();
			}
		}
		PrintInfo("\tUsing output aspect ratio " + QString::number(ARMoreUsed) + " (more used)");
	}

	if (ARMoreUsed >= 1) {
		OutputW = Size;
		OutputH = (Size / 2) / ARMoreUsed * 2;
		if (OutputH < MinSize) {
			OutputH = MinSize;
			OutputW = (OutputH / 2) * ARMoreUsed * 2;
		}
	} else {
		OutputW = (Size / 2) * ARMoreUsed * 2;
		OutputH = Size;
		if (OutputW < MinSize) {
			OutputW = MinSize;
			OutputH = (OutputW / 2) / ARMoreUsed * 2;
		}
	}
	PrintInfo("\tUsing thumbnail resolution " + QString::number(OutputW) + "x" + QString::number(OutputH));
	PrintInfo("\tOk\n");
}

bool Images::GenarateThumbnails() {
	PrintInfo("Generating Image thumbnails:");
	for (int i = 0; i < SelectedOrder.count(); ++i) {
		int Index = SelectedOrder.at(i);
		Images::selected_images_t current = SelectedImages.value(Index);
		QString out = TMP + QDir::separator() + "screenshot" + QString::number(i + 1) + ".png";
		QStringList Params = FfmpegResizeImage(current, out);
		QString Message = "\tProcessing " + current.Name;
		Inst->AddJob(Params, Message);
	}
	bool ok = Inst->Run();
	PrintInfo("\tOk\n");
	return ok;
}

bool Images::GenerateGrid() {
	PrintInfo("Generating grid:");
	uint64_t height;
	GetImageSize(TMP + QDir::separator() + "screenshot1." + TMP_FRAME_FORMAT, Size, height);
	QStringList params;
	params << "-hide_banner" << "-loglevel" << "error" << "-y" << "-r" << "1";
	QString xtackfilter = F->Xstack(params, height, CTB->GridBorderColor);
	params << "-filter_complex" << xtackfilter << "-frames:v" << "1";

	if (ShowTB || NeedBG || NeedWatermark)
		params << TMP_IMAGES + "." + TMP_FRAME_FORMAT;
	else
		params << OutputFormat << OutputFile;

	if (!ffmpeg(params)) return false;
	PrintInfo("\tOk\n");

	return true;
}

bool Images::GenerateAnimation() {
	PrintInfo("Generating animation:");
	QStringList params;
	params << "-hide_banner" << "-loglevel" << "error" << "-y" << "-r" << PS->Value("video_fps")
		   << "-i" << TMP + QDir::separator() + "screenshot%d.png" << OutputFormat << OutputFile;

	if (!ffmpeg(params)) return false;
	PrintInfo("\tOk\n");

	return true;
}

bool Images::GenetateStatsBlock() {
	PrintInfo("Generating text block:");
	if (CTB->IsHidden()) return true;
	CTB->SetTitle(PS->Value("images_title"));
	if (!CTB->GenerateImagesText()) return false;
	PrintInfo("\tOk\n");
	if (!CTB->JoinAll(OutputFile, OutputFormat, NextStepInput)) return false;
	return true;
}

bool Images::ApplyBG() {
	if (!NeedBG) { return true; }
	CTB->GenerateBG();
	if (NeedWatermark) {
		if (!CTB->ApplyBG(NextStepInput, TMP_WITHOUT_WATERMARK, TMP_FRAME_FORMAT_OPTIONS,
						  NextStepInput)) return false;
	} else {
		if (!CTB->ApplyBG(NextStepInput, OutputFile, OutputFormat, NextStepInput)) return false;
	}
	return true;
}

bool Images::ApplyWatermark() {
	if (!NeedWatermark) { return true; }
	if (!CTB->ApplyWatermark(NextStepInput, OutputFile, OutputFormat)) return false;
	return true;
}

void Images::SetOutput(QString File) {
	QString FormatExt;
	QStringList AudioFormat;
	bool IndependentCommand;
	OutputFormat = PS->GetOutputFormat(FormatExt, AudioFormat, IndependentCommand, OutputQuality);

	if (File == "") {
		QString OutDir = QDir::toNativeSeparators(PS->Value("out_dir"));
		if (OutDir == "") {
			OutDir = DirPath;
			if (PS->NotEmpty("out_relative_dir"))
				OutDir += QDir::separator() + PS->Value("out_relative_dir");
		}
		OutputFile = OutDir + QDir::separator() + PS->Value("prefix") + DirName + PS->Value("suffix") + "." + FormatExt;
	} else {
		OutputFile = File;
	}
	OutputFile = QDir::toNativeSeparators(OutputFile);
}

TextSustitution *Images::ReadImageMetadata(QString Image, QString &Name, uint &Width, uint &Height,
		double &AspectRatio) {
	TextSustitution *ITS = new TextSustitution();

	QString JsonResult = "";
	if (!ffprobe(Image, JsonResult)) return nullptr;

	QJsonParseError JsonError;
	QJsonDocument Metadata = QJsonDocument::fromJson(JsonResult.toUtf8(), &JsonError);
	if (!Metadata.isObject()) return nullptr;

	QJsonObject JsonRoot = Metadata.object();
	QJsonArray Streams = JsonRoot.value("streams").toArray();
	QJsonObject S0 = Streams.at(0).toObject();
	//	QJsonObject Format = JsonRoot.value("format").toObject();

	Width = (uint)S0.value("width").toInt();
	Height = (uint)S0.value("height").toInt();
	AspectRatio = S0.value("width").toDouble() / S0.value("height").toDouble();

	Name = ITS->LoadFileVars(Image);
	ITS->Insert("image.width", QString::number(Width));
	ITS->Insert("image.height", QString::number(Height));
	ITS->Insert("image.codec_name", S0.value("codec_name").toString());
	ITS->Insert("image.codec_long_name", S0.value("codec_long_name").toString());
	ITS->Insert("image.ar_resolution", QString::number(AspectRatio));

	return ITS;
}
