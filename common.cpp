#include "common.h"
#include <stdio.h>
#include <stdlib.h>
#include <QProcess>
#include <filesystem>
#include <QRegularExpression>
#include <QTime>
#include <QTextStream>
#include "processpriority.h"
#include <QDir>
#include <QDirIterator>
#ifdef Q_OS_WIN
	#include <windows.h>
	#include <winbase.h>
	#include <shellapi.h>
#endif


bool Debug = false;
bool Quiet = false;
bool Cmd = false;
bool GUIProgress = false;
bool Idle = false;
int Steps = 0;
int CurrentStep;
QString LAYOUT;
uint LayoutX;
uint LayoutY;
uint NumberOfFrames;
QString SizeString;
uint64_t Size;

QString FFMPEG;
QString FFPROBE;
QString TMP;
QString TMP_TEXT;
QString TMP_TEXT_IMG;
QString TMP_IMAGES;
QString TMP_BACKGROUND;
QString TMP_WATERMARK;
QString TMP_TRANSPARENT_SHEET;
QString TMP_WITHOUT_WATERMARK;
QString TMP_SUBSTREAM;
QString TMP_FRAME_FORMAT;
QStringList TMP_FRAME_FORMAT_OPTIONS;
QStringList TMP_VIDEO_FORMAT_OPTIONS;
QStringList TMP_AUDIO_FORMAT_OPTIONS;

const QStringList VideoExtensions = {"mkv", "avi", "mp4", "mov", "m4v", "wmv", "webm", "gif", "flv",
									 "ogv", "ogg", "mts", "m2ts", "ts", "qt", "m4v", "mpg", "mpeg", "f4v"
									};

const QStringList SubtitleExtensions = {"srt", "vtt", "ass"};

const QStringList ImageExtensions = {"jpeg", "jpg", "gif", "png", "apng", "jp2", "jpx", "jxl", "webp", "heif", "avif", "tiff", "bmp"};

const QRegularExpression EscapeCaracters("[^a-zA-Z0-9,._+:@\\%/\\-]");

void PrintInfo(QString line) {
	if (Quiet) return;
	fprintf(stdout, "%s\n", line.toUtf8().data());
}

void PrintDebug(QString line) {
	if (!Quiet && Debug) fprintf(stdout, "%s\n", line.toUtf8().data());
}

void PrintError(QString line) {
	fprintf(stderr, "Error: %s\n", line.toUtf8().data());
}

void PrintProgress(int step, int total) {
	if (GUIProgress)
		fprintf(stdout, "%i/%i\n", step, total);
	fflush(stdout);
}

void AddProgress() {
	PrintProgress(++CurrentStep, Steps);
}

void DebugVarFunt(QString opt, QString var) {
	PrintDebug(opt + "=" + var);
}

void DebugVarFunt(QString opt, uint64_t var) {
	DebugVarFunt(opt + "(int)", QString::number(var));
}

QString EscapeFFFilters(QString in) {
	return QDir::toNativeSeparators(in).replace("\\", "\\\\").replace(":", "\\:").replace("%", "\\%").replace(",",
			"\\,").replace("'", "\\'").replace("[", "\\[").replace("]", "\\]");
}

bool ffmpeg(QStringList params) {
	QProcess process;
	process.start(FFMPEG, params);
	if (Cmd) PrintInfo(FFMPEG + " " + params.join(' '));

	if (process.waitForStarted(-1)) {
		if (Idle)
			ProcessIdlePriority(process.processId());
	}

	process.waitForFinished(-1);

	if (process.exitCode() != 0 ) {
		QString error = process.readAllStandardError();
		PrintError(FFMPEG + " " + params.join(' '));
		PrintError(error);
		return false;
	}

	return true;
}

bool ffprobe(QString file, int stream, QString var, QString &out, bool dvd, int title, bool silent) {
	QProcess process;
	QStringList params;
	if (dvd)
		params << "-f" << "dvdvideo" << "-title" << QString::number(title);
	params << "-i" << file;
	if (stream >= 0) params << "-select_streams" << QString::number(stream);
	if (var != "")
		params << "-show_entries" << var << "-v" << "quiet" << "-of" << "csv=p=0";
	else
		params << "-print_format" << "json" << "-show_format" << "-show_streams" << "-show_chapters";
	process.start(FFPROBE, params);
	process.waitForFinished(-1);
	if (Cmd) PrintInfo(FFPROBE + " " + params.join(' '));

	if (process.exitCode() != 0 ) {
		if (!silent) {
			QString error = process.readAllStandardError();
			PrintError(FFPROBE + " " + params.join(' '));
			PrintError(error);
		}
		return false;
	}
	out = process.readAll().trimmed();
	return true;
}

bool ffprobe(QString file, QString var, QString &out, bool dvd, int title) {
	return ffprobe(file, -1, var, out, dvd, title);
}

bool ffprobe(QString file, QString &out, bool dvd, int title) {
	return ffprobe(file, -1, "", out, dvd, title);
}

QString ReadProgram(QString program, QStringList params, QString Dir) {
	QProcess process;
	if (Dir != "") process.setWorkingDirectory(Dir);
	process.start(program, params);
	process.waitForFinished(-1);
	if (Cmd) PrintInfo(program + " " + params.join(' '));

	if (process.exitCode() != 0 ) {
		QString error = process.readAllStandardError();
		PrintInfo(error);
		return "";
	}

	return process.readAll().trimmed();
}

bool CreateSymlink(QString File, QString Link) {
	#ifdef Q_OS_WIN
	LPCSTR o = File.toUtf8();
	LPCSTR d = Link.toUtf8();
	return CreateSymbolicLinkA(d, o, 0);
	#else
	std::error_code ec;
	std::filesystem::create_symlink(File.toUtf8().data(), Link.toUtf8().data(), ec);
	if (ec)
		return false;
	else
		return true;
	#endif
}

bool CreateHardlink(QString File, QString Link) {
	std::error_code ec;
	std::filesystem::create_hard_link(File.toUtf8().data(), Link.toUtf8().data(), ec);
	if (ec)
		return false;
	else
		return true;
}

#ifdef Q_OS_WIN
bool CreateShortcutU(QString File, QString Shortcut, bool Overwrite) {
	QFile F(File);
	QFile DestFile(Shortcut + ".lnk");

	if (!Overwrite && DestFile.exists()) {
		PrintError("Error, the destination file \"" + Shortcut + ".lnk\" anready exists");
		return false;
	}
	if (Overwrite && DestFile.exists()) DestFile.remove();

	return F.link(Shortcut + ".lnk");
}
#else
bool CreateShortcutU(QString /*File*/, QString /*Shortcut*/, bool /*Overwrite*/) {
	PrintError("Error, Shortcuts links are not supported in non-windows syetems, use symlink instead");
	return false;
}
#endif

haligned_t String2HAling(QString pos) {
	pos = pos.toLower();
	if (pos == "left") return aligned_left;
	if (pos == "right") return aligned_right;
	if (pos == "center") return aligned_center;
	return pos.toInt();
}

valigned_t String2VAling(QString pos) {
	pos = pos.toLower();
	if (pos == "up") return aligned_up;
	if (pos == "middle") return aligned_middle;
	if (pos == "down") return aligned_down;
	return pos.toInt();
}

int StringTime2ms(QString time) {
	static QRegularExpression MS("^([0-9]+)ms$");
	static QRegularExpression hhmmssms("^([0-9]{1,2}):([0-9][0-9]):([0-9][0-9])\\.([0-9][0-9][0-9])$");
	static QRegularExpression hhmmss("^([0-9]{1,2}):([0-9][0-9]):([0-9][0-9])$");
	static QRegularExpression mmss("^([0-9]{1,2}):([0-9][0-9])$");
	static QRegularExpression mmssms("^([0-9]{1,2}):([0-9][0-9])\\.([0-9][0-9][0-9])$");
	static QRegularExpression S("^[0-9.]+$");

	QRegularExpressionMatch Match = MS.match(time);
	if (Match.hasMatch()) {
		return Match.captured(1).toInt();
	}

	Match = S.match(time);
	if (Match.hasMatch()) {
		return time.toFloat() * 1000;
	}

	Match = mmss.match(time);
	if (Match.hasMatch()) {
		return QTime::fromString(time, "m:ss").msecsSinceStartOfDay();
	}

	Match = mmssms.match(time);
	if (Match.hasMatch()) {
		return QTime::fromString(time, "m:ss.zzz").msecsSinceStartOfDay();
	}

	Match = hhmmss.match(time);
	if (Match.hasMatch()) {
		return QTime::fromString(time, "H:mm:ss").msecsSinceStartOfDay();
	}

	Match = hhmmssms.match(time);
	if (Match.hasMatch()) {
		return QTime::fromString(time, "H:mm:ss.zzz").msecsSinceStartOfDay();
	}

	PrintError("Incorrect time: " + time);
	return -1;
}

bool GetImageSize(QString image, uint64_t &width, uint64_t &height) {
	QString Output;
	if (!ffprobe(image, "stream=width,height", Output)) return false;
	width = Output.section(',', 0, 0).toInt();
	height = Output.section(',', 1, 1).toInt();
	return true;
}

void WriteFile(QString Filename, QString Content) {
	QFile file(Filename);
	file.open(QIODevice::WriteOnly | QIODevice::Text);
	QTextStream out(&file);
	out << Content;
	file.close();
}

void AppendFile(QString Filename, QString Content) {
	QFile file(Filename);
	file.open(QIODevice::Append);
	QTextStream out(&file);
	out << Content;
	file.close();
}

QString ReadFile(QString Filename) {
	QFile file(Filename);
	if (!file.open(QFile::ReadOnly | QFile::Text)) return "";
	QTextStream in(&file);
	return in.readAll();
}

QStringList ExtensionListToFilters(const QStringList &List) {
	QStringList Result;
	for (int i = 0; i < List.count(); ++i) {
		Result << "*." + List.at(i);
	}
	return Result;
}

int LayoutSplit(QString layout, uint &X, uint &Y) {
	QString LAYOUT = layout.toLower();
	QString LAYOUT_X = LAYOUT.split("x").first();
	QString LAYOUT_Y = LAYOUT.split("x").last();
	X = LAYOUT_X.toUInt();
	Y = LAYOUT_Y.toUInt();
	if (X == 0) { X = 1; }
	if (Y == 0) { Y = 1; }
	return X * Y;
}

uint64_t DirSize(QString Dir) {
	uint64_t total = 0;
	QDir D(Dir);
	QDirIterator it(D);
	while (it.hasNext()) {
		it.next();
		total += it.fileInfo().size();
	}
	return total;
}

QStringList ListAllDirs(QString Dir) {
	QStringList Result;
	Result.append(Dir);
	QDir DirObj(Dir);
	QStringList Dirs = DirObj.entryList(QDir::AllDirs | QDir::NoDotAndDotDot, QDir::Name);
	foreach (QString CurDir, Dirs) {
		Result.append(ListAllDirs(Dir + QDir::separator() + CurDir));
	}
	return Result;
}

#if defined (Q_OS_WIN)
QString ReadParameter(int n) {
	LPWSTR *szArglist;
	LPCWSTR command = GetCommandLineW();
	int nArgs;
	szArglist = CommandLineToArgvW(command, &nArgs);
	if (szArglist == NULL || n >= nArgs) {
		PrintError("Reading wchar parameters");
		return "";
	}
	QString output = QString::fromWCharArray(szArglist[n]);
	LocalFree(szArglist);
	return output;
}
#endif
