#ifndef TEXTSUSTITUTION_H
#define TEXTSUSTITUTION_H

#include <QJsonObject>
#include <QMap>
#include <QLocale>
#include "textblock.h"

struct pix_fmt_data {
	uint8_t bits;
	QString Chroma;
	QString Mode;
};

class TextSustitution {
	public:
		explicit TextSustitution();
		QString ScapeCaracters(QString in);
		void SetReplaceDir(QString dir);
		void SetMediaInfo(QString mediainfo);
		void LoadVars(QJsonObject Object, QString Name = "");
		void CopyVars(QString OriginContext, QString DestinationContext);
		QString PrefixConverter(double Value, bool Binary, uint8_t Decimals, bool Separated, bool Formated, QStringList Units,
								QList<uint8_t> DecimalsList = {});
		static bool DetectFunction(QString Text);
		QString DetectAndEjecuteFunction(QString Text, QString Context = "", bool *Executed = nullptr);
		QString ProcessFunctions(QString Function, QStringList Params, QString Context, bool TextFormat = true, int level = 0);
		QString ProcessText(QString Context, QString Text, text_sub_style_t *Style);
		QString ProcessCMD(QString Context, QString CMD);
		QString ProcessTemplate(QString Template);
		QStringList SplitFunctionParameters(QString Params);

		void Insert(QString Var, QString Value, bool scape = true);
		QString Value(QString Var, QString Default = "");
		bool Contains(QString Var);
		void Remove(QString Var);
		void Update(QString Var, QString Value, bool scape = true);
		void PrintAll();
		double HumanFrameRate(QString Var);
		QString LoadFileVars(QString File);
		static QString CutCenterText(QString text, int number);
		static QString Text(QString text, bool invert = false);
		static QString TextFunction(QString text, bool invert = false);
		static bool TextTest(QString text);

		QString LoadVarsFromFile(QString File, QString Context, bool Trim);
	private:
		QString ReplaceDir;
		QString MediaInfo;

		QLocale *Local;

		QMap<QString, QString> Vars;
		QMap<QString, QStringList> Replace;

		void EmptyMatch(QString Context, QString &Result, bool TextFormat = true);
		void SimpleMatch(QString Context, QString &Result, bool TextFormat = true);
		QString ValueCheckContext(QString Var, QString Context = "", bool Empty = false);
		QString Operation(QString Operation, QString Value, QStringList Params, QString Context);

		static QRegularExpression SearchTextEmpty;
		static QRegularExpression SearchSimpleText;
		static QRegularExpression SearchFormatText;
		static QRegularExpression FunctionSearch;
		static QRegularExpression ParameterSearch;
		static QRegularExpression VarSearch;
		static QRegularExpression MultineVarSearch;
		static const QMap<QString, pix_fmt_data> PixelFormatDatabase;

};

#endif // TEXTSUSTITUTION_H
