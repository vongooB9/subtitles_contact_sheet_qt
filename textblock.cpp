#include "textblock.h"
#include "colors.h"
#include "common.h"
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QRegularExpression>
#include <QProcess>
#include "filters.h"
#include "layeredfilter.h"

aligment_t String2Alingment(QString pos) {
	pos = pos.toLower();
	if (pos == "up_left") return align_up_left;
	if (pos == "up_right") return align_up_right;
	if (pos == "down_left") return align_down_left;
	if (pos == "down_right") return align_down_right;
	if (pos == "up_center") return align_up_center;
	if (pos == "down_center") return align_down_center;
	if (pos == "middle_left") return align_middle_left;
	if (pos == "middle_right") return align_middle_right;
	if (pos == "middle_center") return align_middle_center;
	return align_up_left;
}

text_sub_style_t::text_sub_style_t(QString name, text_sub_style_t *copy, ParamsSettings *PS, QString style) {
	Name = name;
	if (copy != nullptr) {
		Fontname = copy->Fontname;
		Fontsize = copy->Fontsize;
		PrimaryColour = copy->PrimaryColour;
		SecondaryColour = copy->SecondaryColour;
		OutlineColour = copy->OutlineColour;
		BackColour = copy->BackColour;
		Bold = copy->Bold;
		Italic = copy->Italic;
		Underline = copy->Underline;
		StrikeOut = copy->StrikeOut;
		BorderStyle = copy->BorderStyle;
		Outline = copy->Outline;
		Shadow = copy->Shadow;
		Alignment = copy->Alignment;
		MarginL = copy->MarginL;
		MarginR = copy->MarginR;
		MarginV = copy->MarginV;
		BorderBlur = copy->BorderBlur;
	} else {
		Fontname = "Sans";
		Fontsize = 12;
		PrimaryColour = "white";
		SecondaryColour = "yellow";
		OutlineColour = "black";
		BackColour = "black";
		Bold = false;
		Italic = false;
		Underline = false;
		StrikeOut = false;
		BorderStyle = border_outline_shadow;
		Outline = 0;
		Shadow = 0;
		Alignment = align_up_left;
		MarginL = 10;
		MarginR = 10;
		MarginV = 10;
		BorderBlur = 0;
	}
	if (PS != nullptr) {
		if (style == "font") {
			Fontname = PS->Value("tb_font");
			Fontsize = PS->Int("tb_font_size");
		} else {
			if (PS->NotEmpty("tb_" + style + "_font")) Fontname = PS->Value("tb_" + style + "_font");
			if (PS->Int("tb_" + style + "_font_size") > 0) Fontsize = PS->Int("tb_" + style + "_font_size");
		}
		PrimaryColour = PS->Value("tb_" + style + "_color");
		OutlineColour = PS->Value("tb_" + style + "_border_color");
		BackColour = PS->Value("tb_" + style + "_shadow_color");
		Outline = PS->Int("tb_" + style + "_border");
		BorderBlur = PS->Int("tb_" + style + "_border_blur");
		Shadow = PS->Int("tb_" + style + "_shadow");
		Italic = PS->Bool("tb_" + style + "_italic");
		Bold = PS->Bool("tb_" + style + "_bold");
		SetAlignment(PS->Value("tb_" + style + "_pos"));
	}
}

void text_sub_style_t::SetAlignment(QString align) {
	Alignment = String2Alingment(align);
}

TextBlock::TextBlock(QObject *parent)
	: QObject{parent} {
	BGInputImage = "";
	BGImageOpacity = 100;
	TmpSub = QDir::toNativeSeparators(TMP + QDir::separator() + "head_subtitle.ass");
	TmpBackground = QDir::toNativeSeparators(TMP + QDir::separator() + "tb_background.png");
	BGGradient = false;
	Logo = "";
	LogoWidth = 0;
	LogoAR = 0;
	LogoLeft = false;
	LogoOverlay = true;
}

void TextBlock::AddStyle(text_sub_style_t &Style) {
	Styles.append(Style);
}

int TextBlock::AddLine(QString text, QString StyleName, int MarginL, int MarginR, int MarginV) {
	if (text == "") return -1;

	text_sub_line_t Line;
	Line.Text = text;
	Line.Style = nullptr;
	Line.MarginL = MarginL;
	Line.MarginR = MarginR;
	Line.MarginV = MarginV;
	Line.Lines = text.count("\\N") + 1;

	if (StyleName != "") {
		for (int i = 0; i < Styles.count(); ++i) {
			if (StyleName == Styles.at(i).Name) Line.Style = &Styles[i];
		}
	} else {
		if (Styles.empty()) {
			Styles.append(text_sub_style_t("Default"));
		}
		Line.Style = &Styles.first();
	}

	if (Line.Style == nullptr) {
		PrintError("Invalid Style: " + StyleName);
		return -1;
	}

	if (Line.Style->BorderBlur > 0)
		Line.Text = "{\\blur" + QString::number(Line.Style->BorderBlur) + "}" + Line.Text;

	int pos = Lines.size();
	Lines.insert(pos, Line);
	return pos;
}

void TextBlock::AppendLine(QString text, int line) {
	if (text == "") return;
	if (line >= Lines.size()) return;
	text_sub_line_t Line = Lines[line];
	Line.Text += "\\N" + text;
	Line.Lines = Line.Text.count("\\N") + 1;
	Lines[line] = Line;
}

void TextBlock::SetFontDir(QString Dir) {
	FontDir = QDir::toNativeSeparators(Dir);
}

void TextBlock::SetBGImage(QString File, haligned_t ha, valigned_t va, bool scale, bool tile, int blur, int exposure,
						   int saturation, int opacity, QString mask) {
	BGImageFile = QDir::toNativeSeparators(File);
	if (BGImageFile != "" && !QFileInfo::exists(BGImageFile)) {
		PrintError("tb_bg_image file is not accessible: " + BGImageFile);
		BGImageFile = "";
	}
	BGImageHAlign = ha;
	BGImageVAlign = va;
	BGImageScale = scale;
	BGImageTile = tile;
	BGImageBlur = blur;
	BGImageExposure = exposure;
	BGImageSaturation = saturation;
	BGImageOpacity = opacity;
	BGInputMask = mask;
}

void TextBlock::SetBGColor(QString Color) {
	BGColor = Color;
}

void TextBlock::SetGradient(QString grad) {
	BGGradient = (grad != "none");
	BGGrad = grad;
}

void TextBlock::SetLogo(QString logo, bool left, bool overlay, QString transcolor, int transsimilarity,
						int transblend) {
	Logo = QDir::toNativeSeparators(logo);
	LogoLeft = left;
	LogoOverlay = overlay;
	LogoTransColor = transcolor;
	LogoTransSimilarity = transsimilarity;
	LogoTransBlend = transblend;

	QString TB_LOGO_ASPECT_RATIO;
	ffprobe(Logo, "stream=display_aspect_ratio", TB_LOGO_ASPECT_RATIO);
	QStringList AS_SPLIT = TB_LOGO_ASPECT_RATIO.split(":");
	LogoAR = AS_SPLIT.first().toDouble() / AS_SPLIT.last().toDouble();
	if (LogoAR == 0 || qIsNaN(LogoAR)) {
		uint64_t w, h;
		GetImageSize(Logo, w, h);
		LogoAR = (double)w / (double)h;
	}
}

void TextBlock::SetBorder(int border) {
	Border = border;
}

void TextBlock::SetBGInputImage(QString image, uint64_t pos) {
	BGInputImage = image;
	BGInputImagePos = pos;
}

void TextBlock::GenerateSubTmpFile(int Width, int Height, wrap_style_t Wrap) {

	QString SubtitleContent = "[Script Info]\n\
Title: Text header file\n\
ScriptType: v4.00+\n\
WrapStyle: " + QString::number(Wrap) + "\n\
ScaledBorderAndShadow: yes\n\
YCbCr Matrix: TV.601\n\
PlayResX: " + QString::number(Width) + "\n\
PlayResY: " + QString::number(Height) + "\n\
\n\
[V4+ Styles]\n\
Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, StrikeOut, ScaleX, ScaleY, Spacing, Angle, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, Encoding\n";


	for (int i = 0; i < Styles.count(); ++i) {
		text_sub_style_t Style = Styles.at(i);
		int L = Style.MarginL;
		int R = Style.MarginR;
		if (Logo != "" && !LogoOverlay) {
			if (LogoLeft)
				L = LogoWidth + (Border * 2);
			else
				R = LogoWidth + (Border * 2);
		}

		SubtitleContent += "Style: " + Style.Name + ", " +							//Name
						   Style.Fontname + ", " +									//Fontname
						   QString::number(Style.Fontsize) + ", " +					//Fontsize
						   "&H" + StringColor2BGR(Style.PrimaryColour) + ", " +		//PrimaryColour
						   "&H" + StringColor2BGR(Style.SecondaryColour) + ", " +	//SecondaryColour
						   "&H" + StringColor2BGR(Style.OutlineColour) + ", " +		//OutlineColour
						   "&H" + StringColor2BGR(Style.BackColour) + ", " +		//BackColour
						   QString::number(Style.Bold) + ", " +						//Bold
						   QString::number(Style.Italic) + ", " +					//Italic
						   QString::number(Style.Underline) + ", " +				//Underline
						   QString::number(Style.StrikeOut) + ", " +				//StrikeOut
						   "100, " +												//ScaleX
						   "100, " +												//ScaleY
						   "0, " +													//Spacing
						   "0, " +													//Angle
						   QString::number(Style.BorderStyle) + ", " +				//BorderStyle
						   QString::number(Style.Outline) + ", " +					//Outline
						   QString::number(Style.Shadow) + ", " +					//Shadow
						   QString::number(Style.Alignment) + ", " +				//Alignment
						   QString::number(L) + ", " +								//MarginL
						   QString::number(R) + ", " +								//MarginR
						   QString::number(Style.MarginV) + ", " +					//MarginV
						   "1\n";													//Encoding
	}

	SubtitleContent += "\n\
[Events]\n\
Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text\n";

	for (int i = 0; i < Lines.count(); ++i) {
		text_sub_line_t Line = Lines.at(i);
		SubtitleContent += "Dialogue: 0, "								//Layer
						   "0: 00: 00.00, "								//Start
						   "0: 00: 05.00, " +							//End
						   Line.Style->Name + "," +						//Style
						   ", " +										//Name
						   QString::number(Line.MarginL) + ", " +		//MarginL
						   QString::number(Line.MarginR) + ", " +		//MarginR
						   QString::number(Line.MarginV) + "," +		//MarginV
						   "," +										//Effect
						   Line.Text + "\n";							//Text
	}

	QFile SubTmpFile(TmpSub);
	if (SubTmpFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
		QTextStream stream(&SubTmpFile);
		stream << SubtitleContent;
		stream.flush();
	}
	SubTmpFile.close();
}

QStringList TextBlock::GenerateParams(int Width, int Height, QString Output) {

	QString FONTDIR = "";
	if (FontDir != "") {
		FONTDIR = ":fontsdir='" + EscapeFFFilters(FontDir) + "'";
	}

	QStringList Params;
	Params << "-hide_banner" << "-loglevel" << "error" << "-y";

	QString bgcolororgradient = BGColor;
	if (BGGradient)
		bgcolororgradient = BGGrad;


	if (BGImageFile != "") {
		if (GenerateBackgroundImage(BGImageFile,
									TmpBackground,
									Width,
									Height,
									BGImageHAlign,
									BGImageVAlign,
									BGImageScale,
									BGImageTile,
									BGImageBlur,
									BGImageExposure,
									BGImageSaturation,
									BGImageOpacity,
									BGColor,
									BGInputImage,
									bgcolororgradient,
									BGInputMask,
									BGInputImagePos)) {
			Params << "-i" << TmpBackground;
		} else {
			PrintError("Error in text block background image");
			BGImageFile = "";
		}
	} else {
		Params << "-f" << "lavfi" << "-i";
		if (BGGradient) {
			Params << Filters::String2GradientFilter(Width, Height, BGGrad);
		} else {
			Params << Filters::GenerateSolidColorFilter(Width, Height, BGColor);
		}
	}

	QStringList Filters;

	if (Logo != "") {
		Params << "-i" << Logo;

		if (BGInputImage != "" && BGImageFile == "") {
			Params << "-i" << BGInputImage;
		}

		QString TransparentColorFilter = "";
		if (LogoTransColor != "") TransparentColorFilter = Filters::RawColorKey(LogoTransColor, LogoTransSimilarity,
					LogoTransBlend) + ",";

		Filters << "[1:v]" + TransparentColorFilter + "scale=" + QString::number(LogoWidth) + ":" +
				QString::number(Height - (Border * 2)) + ":flags=lanczos+accurate_rnd+full_chroma_int+full_chroma_inp[logo]";

		if (LogoLeft) {
			Filters << "[0:v][logo]overlay=x=" + QString::number(Border) + ":y=" + QString::number(Border) + ":format=rgb[over]";
		} else {
			Filters << "[0:v][logo]overlay=x=main_w-overlay_w-" + QString::number(Border) +
					":y=" + QString::number(Border) + ":format=rgb[over]";
		}
		if (BGInputImage != "" && BGImageFile == "") {
			Filters << "[2:v]crop=w=" + QString::number(Width) + ":h=" + QString::number(Height) +
					":x=0:y=" + QString::number(BGInputImagePos) + "[bg]" <<
					"[bg][over]overlay=x=0:y=0:format=rgb[over]";
		}

		Filters << "[over]subtitles=alpha=1:f='" + EscapeFFFilters(TmpSub) + "'" + FONTDIR;
	} else {
		Filters << "subtitles=alpha=1:f='" + EscapeFFFilters(TmpSub) + "'" + FONTDIR;
	}

	Params << "-filter_complex" << Filters.join("; ") << "-vframes" << "1" << Output;

	return Params;
}

bool TextBlock::GenerateBackgroundImage(QString In, QString Out, int w, int h, haligned_t ha, valigned_t va,
										bool scale, bool tile, int blur, int exposure, int saturation, int opacity, QString fillcolor, QString bgimage,
										QString bg, QString mask, int pos, QString PreFilters, QString PostFilters) {
	if (!QFile(In).exists()) return false;

	struct layers_t {
		QString Name;
		QString Filters;
		QString File;
	};

	QList<layers_t> Layers;

	uint64_t BgWidth, BgHeight;
	GetImageSize(In, BgWidth, BgHeight);

	QString cropx = "";

	switch (ha) {
		case aligned_left:
			cropx = "x=0";
			break;
		case aligned_right:
			cropx = "x=in_w-out_w";
			break;
		case aligned_center:
			cropx = "x=(in_w-out_w)/2";
			break;
		default:
			cropx = "x=(in_w-out_w)*(" + QString::number(ha) + "/100)";
			break;
	}

	QString cropy = "";

	switch (va) {
		case aligned_up:
			cropy = "y=0";
			break;
		case aligned_down:
			cropy = "y=in_h-out_h";
			break;
		case aligned_middle:
			cropy = "y=(in_h-out_h)/2";
			break;
		default:
			cropy = "y=(in_h-out_h)*(" + QString::number(va) + "/100)";
			break;
	}

	QStringList bg_filters;
	if (PreFilters != "") bg_filters << PreFilters;

	if (scale) {
		bg_filters << "scale=flags=lanczos+accurate_rnd+full_chroma_int+full_chroma_inp:force_original_aspect_ratio=increase:w="
				   + QString::number(w) +
				   ":h=" + QString::number(h);
	} else if (tile) {
		uint64_t TileCols = 0;
		uint64_t TileRows = 0;

		if (BgWidth < (uint64_t)w)
			TileCols = w / BgWidth + 1;
		else
			TileCols = 1;

		if (BgHeight < (uint64_t)h)
			TileRows = h / BgHeight + 1;
		else
			TileRows = 1;
		DEBUGVAR(TileCols);
		DEBUGVAR(TileRows);

		if (TileCols > 1 || TileRows > 1) {
			bg_filters << "loop=loop=" + QString::number(TileRows * TileCols) + ":size=1";
			bg_filters << "tile=layout=" + QString::number(TileCols) + "x" + QString::number(TileRows);
		}
	} else {
		uint64_t neww = w;
		if (neww < BgWidth) neww = BgWidth;
		uint64_t newh = h;
		if (newh < BgHeight) newh = BgHeight;

		QString padx = "";
		switch (ha) {
			case aligned_left:
				padx = "x=0";
				break;
			case aligned_right:
				padx = "x=out_w-in_w";
				break;
			case aligned_center:
				padx = "x=(out_w-in_w)/2";
				break;
			default:
				padx = "x=(out_w-in_w)*(" + QString::number(ha) + "/100)";
				break;
		}

		QString pady = "";
		switch (va) {
			case aligned_up:
				pady = "y=0";
				break;
			case aligned_down:
				pady = "y=out_h-in_h";
				break;
			case aligned_middle:
				pady = "y=(out_h-in_h)/2";
				break;
			default:
				pady = "y=(out_h-in_h)*(" + QString::number(va) + "/100)";
				break;
		}
		bg_filters << "pad=w=" + QString::number(neww) + ":h=" + QString::number(newh) + ":" + padx + ":" + pady +
				   ":color=" + StringColor2RGB(fillcolor);
	}

	bg_filters << "crop=w=" + QString::number(w) + ":h=" + QString::number(h) + ":" + cropx + ":" + cropy;

	//TODO: Change this to use filters.h
	if (saturation != 100)
		bg_filters << "eq=saturation=" + QString::number((double)saturation / 100);

	if (exposure != 0)
		bg_filters << "exposure=" + QString::number((double)exposure / 100);

	if (blur != 0)
		bg_filters << "gblur=sigma=" + QString::number((double)blur / 100);

	if (opacity != 100)
		bg_filters << "format=rgba" << "colorchannelmixer=aa=" + QString::number((double)opacity / 100);

	if (PostFilters != "") bg_filters << PostFilters;

	LayeredFilter F;

	if (bgimage != "") F.AddInput(bgimage, "bgimage", "crop=w=" + QString::number(w) + ":h=" + QString::number(h) +
									  ":x=0:y=" + QString::number(pos));

	if (bg != "") F.AddInputFilter(Filters::String2GradientFilter(w, h, bg), "bg", "");

	if (mask != "")
		F.AddInputFile(In, "normal", bg_filters.join(",") + Filters::RawMask(w, h, mask));
	else
		F.AddInputFile(In, "normal", bg_filters.join(","));

	QStringList Params;
	Params << "-hide_banner" << "-loglevel" << "error" << "-y";
	Params << F.GetInputParams();
	Params << F.GetComplexFilter() << F.GetOutputName() << "-frames:v" << "1" << Out;

	return ffmpeg(Params);
}

int TextBlock::Generate(int Width, int Height, QString Output) {
	LogoWidth = qRound(((Height - (Border * 2)) / 2) * LogoAR) * 2;
	GenerateSubTmpFile(Width, Height);
	QStringList Params = GenerateParams(Width, Height, Output);
	return ffmpeg(Params);
}

int TextBlock::TotalTextHeight() {
	int Total = 0;
	for (int i = 0; i < Lines.count(); ++i) {
		Total += Lines.at(i).Lines * Lines.at(i).Style->Fontsize;
	}
	return Total;
}
