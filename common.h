#ifndef COMMON_H
#define COMMON_H

#include <QString>
#include <QDir>
#include <QStringList>

extern bool Debug;
extern bool Quiet;
extern bool Cmd;
extern bool GUIProgress;
extern bool Idle;

extern QString FFMPEG;
extern QString FFPROBE;
extern QString TMP;
extern QString TMP_TEXT;
extern QString TMP_TEXT_IMG;
extern QString TMP_IMAGES;
extern QString TMP_BACKGROUND;
extern QString TMP_WATERMARK;
extern QString TMP_TRANSPARENT_SHEET;
extern QString TMP_WITHOUT_WATERMARK;
extern QString TMP_SUBSTREAM;
extern QString TMP_FRAME_FORMAT;
extern QStringList TMP_FRAME_FORMAT_OPTIONS;
extern QStringList TMP_VIDEO_FORMAT_OPTIONS;
extern QStringList TMP_AUDIO_FORMAT_OPTIONS;

extern QString LAYOUT;
extern uint LayoutX;
extern uint LayoutY;
extern uint NumberOfFrames;
extern QString SizeString;
extern uint64_t Size;

typedef uint8_t haligned_t;
typedef uint8_t valigned_t;

enum haligval : uint8_t { aligned_left = 0, aligned_center = 50, aligned_right = 100 };
enum valigval : uint8_t { aligned_up = 0, aligned_middle = 50, aligned_down = 100 };

haligned_t String2HAling(QString pos);
valigned_t String2VAling(QString pos);

void PrintInfo(QString line);
void PrintDebug(QString line);
void PrintError(QString line);

extern int Steps;
extern int CurrentStep;
void PrintProgress(int step, int total);

#define DEBUGVAR(var) DebugVarFunt(#var, var)
void DebugVarFunt(QString opt, QString var);
void DebugVarFunt(QString opt, uint64_t var);

QString EscapeFFFilters(QString in);

bool ffmpeg(QStringList params);
bool ffprobe(QString file, int stream, QString var, QString &out, bool dvd = false, int title = 0, bool silent = false);
bool ffprobe(QString file, QString var, QString &out, bool dvd = false, int title = 0);
bool ffprobe(QString file, QString &out, bool dvd = false, int title = 0);
QString ReadProgram(QString program, QStringList params, QString Dir = "");

bool CreateSymlink(QString File, QString Link);
bool CreateHardlink(QString File, QString Link);
bool CreateShortcutU(QString File, QString Shortcut, bool Overwrite);

int StringTime2ms(QString time);

bool GetImageSize(QString image, uint64_t &width, uint64_t &height);

void WriteFile(QString Filename, QString Content);
void AppendFile(QString Filename, QString Content);
QString ReadFile(QString Filename);

extern const QStringList VideoExtensions;
extern const QStringList SubtitleExtensions;
extern const QStringList ImageExtensions;

void AddProgress();

QStringList ExtensionListToFilters(const QStringList &List);

int LayoutSplit(QString layout, uint &X, uint &Y);

extern const QRegularExpression EscapeCaracters;

uint64_t DirSize(QString Dir);

QStringList ListAllDirs(QString Dir);

#if defined (Q_OS_WIN)
	#define READPARAMETER(n) ReadParameter(n)
	QString ReadParameter(int n);
#else
	#define READPARAMETER(n) QString::fromUtf8(argv[n])
#endif

#endif // COMMON_H
