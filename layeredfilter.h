#ifndef LAYEREDFILTER_H
#define LAYEREDFILTER_H
#include <QString>
#include <QStringList>
#include <QList>

class LayeredFilter {
	public:
		LayeredFilter();
		enum inputtype_t {input_file, input_filter, input_especial_file, input_raw, input_none};
		enum mergetype_t {merge_overlay, merge_alphamerge, merge_none};

		void AddInput(QString File, QString Name, QString Filters, mergetype_t Merge = merge_overlay,
					  QString MergeOptions = "");
		void AddInputFile(QString File, QString Name, QString Filters, mergetype_t Merge = merge_overlay,
						  QString MergeOptions = "");
		void AddInputFilter(QString File, QString Name, QString Filters, mergetype_t Merge = merge_overlay,
							QString MergeOptions = "");
		void AddInputSpecialFile(QString File, QString Name, QString Filters, mergetype_t Merge = merge_overlay,
								 QString MergeOptions = "");

		void AddIntermediate(QString Input, QString Output, QString Filters, mergetype_t Merge = merge_overlay,
							 QString MergeOptions = "");

		QStringList GetInputParams();
		QStringList GetComplexFilter();
		QStringList GetOutputName();
		QStringList GetAll();
		QString GetInputNames(QString Name);

	protected:
		struct layers_t {
			inputtype_t InputType;
			QString Input;
			QString InputName;
			QString OutputName;
			QString Name;
			QString Filters;
			int InputNumber;
			mergetype_t MergeType;
			QString MergeOptions;
		};

		QList<layers_t> Layers;

		QString OutputName;

};

#endif // LAYEREDFILTER_H
