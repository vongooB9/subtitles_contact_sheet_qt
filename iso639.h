#ifndef ISO639_H
#define ISO639_H
#include <QString>

QString Iso639ToLangName(QString iso639);
QString Iso639ToShortName(QString iso639);
QString TwoLetter2ThreeLetter(QString Two);
bool EqualLang(QString Lang1, QString Lang2);

#endif // ISO639_H
