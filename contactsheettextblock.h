#ifndef CONTACTSHEETTEXTBLOCK_H
#define CONTACTSHEETTEXTBLOCK_H
#include <QString>
#include "common.h"
#include "paramssettings.h"
#include "textsustitution.h"
#include "textblock.h"

class ContactSheetTextBlock {
	public:
		ContactSheetTextBlock(ParamsSettings *ps, TextSustitution *ts);
		~ContactSheetTextBlock();
		enum text_block_positions { text_top, text_bottom, text_hidden };
		text_block_positions TextBlockPosition = text_top;
		static text_block_positions String2TextBlockPotion(QString pos);
		valigned_t BackgroundVAling = aligned_middle;
		haligned_t BackgroundHAling = aligned_center;
		valigned_t TextBlockBackgroundVAling = aligned_middle;
		haligned_t TextBlockBackgroundHAling = aligned_center;
		uint64_t TextBlockWidth;
		uint64_t TextBlockHeight;
		int64_t CoverTopCalculatedHeight;
		QString BGColor;
		QString GridBorderColor;

		bool IsHidden();
		void SetTitle(QString title);
		void SetBGColor(QString Color);
		void SetGridBorderColor(QString Color);
		bool GenerateVideoText(bool ShowSubText, bool VR);
		bool GenerateImagesText();
		bool JoinAll(QString OutputFile, QStringList FormatOptions, QString &Output);
		bool ApplyBG(QString In, QString Out, QStringList FormatOptions, QString &Output);
		bool ApplyWatermark(QString In, QString Out, QStringList FormatOptions);
		void CalculateDimensions();
		bool GenerateBG(uint64_t W = 0, uint64_t H = 0);
		bool GenerateWatermark(uint64_t W, uint64_t H);
	private:
		ParamsSettings *PS;
		TextSustitution *TS;
		TextBlock TB;
		text_sub_style_t *DefaultStyle;
		text_sub_style_t *TitleStyle;
		text_sub_style_t *CommentsStyle;

		QString Title;
		int Border;

		void ApplyTitle();
		int ApplyComments();
		void ApplyLogo();
};

#endif // CONTACTSHEETTEXTBLOCK_H
