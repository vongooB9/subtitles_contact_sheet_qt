#ifndef PARAMSSETTINGS_H
#define PARAMSSETTINGS_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QHash>
#include <QSettings>

class ParamsSettings : public QObject {
		Q_OBJECT
	public:
		enum param_type {param_int, param_bool, param_string/*, param_double*/, param_file, param_dir, param_color, param_font, param_layout, param_shortcut, param_none};
		struct params_t {
			QString Value;
			QString DefaultValue;
			bool Changed;
			bool Required;
			param_type Type;
			bool UserDefined;
			int Number;
			QString UserName;
			QString Function;
			QString ValidValues;
		};
		struct help_t {
			QString Prefix;
			QString Param;
			QString ShortParam;
			QString Type;
			QString Text;
			QString Name;
			param_type DataType;
			QString GetTip();
		};

		QString AppPath;

		explicit ParamsSettings(QString applicationDirPath, QObject *parent = nullptr);
		QString Value(QString Param);
		bool Bool(QString Param);
		int Int(QString Param);
		//		double Double(QString Param);
		bool NotEmpty(QString Param);
		bool Exists(QString Param);
		void setValue(QString Param, QString Value, bool DetectFunction = true);
		void setValueInt(QString Param, int Value);
		void setValueDouble(QString Param, double Value);
		void setValueBool(QString Param, bool Value);
		QStringList Params();
		param_type Type(QString Param);
		QString Function(QString Param);
		bool IsUserDefined(QString Param);

		void SetConfig(QString Name);
		void SetConfigFile(QString File);
		bool ConfigExists(QString Name);
		QStringList ConfigList();
		void ReadFromConfigFile();
		void LoadConfig(QString Config);
		void UpdateConfigFile(bool Sync = true, bool All = false);
		void UpdateConfigFile(QString Param, bool All = false);
		void PrintParams(bool Changed, bool Default);
		bool ProcessParameter(int &Current, int Count, char *Params[]);
		bool ProcessParameter(QString &Param, bool HasNext, QString NextParam, bool &UsedNext);

		enum context_type {context_global, context_stream, context_video, context_audio, context_subtitle, context_attachment};
		void UserDefinedVar(QString Param, context_type Context = context_global);
		void RemoveUserDefinedVar(QString SaveName);
		QString Context2Str(context_type Context);
		context_type Str2Context(QString Context);
		int GetUserDefinedVarCount();
		QString NewUserDefinedOptionName(context_type context, QString Name, int NewID = -1);
		bool GetVarAndContext(QString Name, QString &Var, context_type &Context);
		void printhelp(QString AppName, QString prefix = "");

		QList<help_t> SearchHelp(QString Search, bool OptionsOnly = false);
		params_t GetVar(QString Param);
		void SetVar(QString Param, params_t &value);
		help_t GetHelp(QString Param);
		QString GetPrefixName(QString Prefix);
		void DeleteConfig(QString Config);

		QStringList GetOutputFormat(QString &OUTPUT_FORMAT, QStringList &Audio, bool &INDEPENDENT_COMMAND,
									QString &OUTPUT_QUALITY);
		void ReadBGAndGridBorderColor(QString &BG, QString &GridBorder);

		//		void ProcessFunctions(TextSustitution *TS);

	private:
		QString ConfigPath;
		QString ConfigExt;

		QHash<QString, params_t> VarList;
		QHash<QString, QString> ShortParams;
		QHash<QString, QString> HelpPrefixText;
		QHash<QString, QString> HelpPrefixName;
		QList<help_t> HelpList;
		QMultiHash<QString, QString> UserDefinedTranslator;
		QSettings *Settings;

		void AddStr(QString Param, QString DefaultVar, QString ValidValues, bool Required = false, QString Prefix = "",
					QString Type = "", QString Text = "", QString Name = "");
		void AddFile(QString Param, QString DefaultVar, QString ValidExtensions, bool Required = false, QString Prefix = "",
					 QString Text = "", QString Name = "");
		void AddDir(QString Param, QString DefaultVar, bool Required = false, QString Prefix = "",
					QString Text = "", QString Name = "");
		void AddColor(QString Param, QString DefaultVar, bool Required = false, QString Prefix = "",
					  QString Text = "", QString Name = "");
		void AddFont(QString Param, QString DefaultVar, bool Required = false, QString Prefix = "",
					 QString Text = "", QString Name = "");
		void AddLayout(QString Param, QString DefaultVar, bool Required = false, QString Prefix = "",
					   QString Text = "", QString Name = "");
		void AddInt(QString Param, int DefaultVar, QString ValidValues, bool Required = false, QString Prefix = "",
					QString Type = "", QString Text = "", QString Name = "");
		//		void AddDouble(QString Param, double DefaultVar, QString ValidValues, bool Required = false, QString Prefix = "",
		//					   QString Type = "", QString Text = "", QString Name = "");
		void AddBool(QString Param, bool DefaultVar, bool Required = false, QString Prefix = "", QString Text = "",
					 QString Name = "");
		void AddUserDefined(QString Param, QString Name, int Num);
		void AddHelp(QString Prefix, QString Param, QString Type, QString Text, QString ShortParam = "", QString Name = "",
					 param_type DataType = param_string);
		void AddShortParam(QString Short, QString Param);

		int UserDefinedVarOrderCount;

	signals:

};

#endif // PARAMSSETTINGS_H
