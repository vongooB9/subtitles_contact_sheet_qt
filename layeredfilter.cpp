#include "layeredfilter.h"
#include <QFileInfo>
#include <QMap>
#include <QProcess>

LayeredFilter::LayeredFilter() {}

void LayeredFilter::AddInputFile(QString File, QString Name, QString Filters, mergetype_t Merge, QString MergeOptions) {
	QFileInfo file(File);
	if (file.isFile())
		Layers.append({input_file, File,  "", "", Name, Filters, -1, Merge, MergeOptions});
}

void LayeredFilter::AddInputFilter(QString File, QString Name, QString Filters, mergetype_t Merge,
								   QString MergeOptions) {
	Layers.append({input_filter, File,  "", "", Name, Filters, -1, Merge, MergeOptions});
}

void LayeredFilter::AddInputSpecialFile(QString File, QString Name, QString Filters, mergetype_t Merge,
										QString MergeOptions) {
	Layers.append({input_especial_file, File,  "", "", Name, Filters, -1, Merge, MergeOptions});
}

void LayeredFilter::AddInput(QString File, QString Name, QString Filters, mergetype_t Merge, QString MergeOptions) {
	Layers.append({input_raw, File,  "", "", Name, Filters, -1, Merge, MergeOptions});
}

void LayeredFilter::AddIntermediate(QString Input, QString Output, QString Filters, mergetype_t Merge,
									QString MergeOptions) {
	Layers.append({input_none, "", Input, Output, "", Filters, -1, Merge, MergeOptions});
}

QStringList LayeredFilter::GetInputParams() {
	QStringList Params;
	int InputCount = -1;
	for (int i = 0; i < Layers.count(); ++i) {
		layers_t Layer = Layers.at(i);
		if (Layer.Input == "") continue;
		InputCount++;
		Layers[i].InputNumber = InputCount;
		Layers[i].OutputName = "out" + QString::number(InputCount);
		switch (Layer.InputType) {
			case input_file:
			case input_especial_file:
				Params << "-i" << Layer.Input;
				break;
			case input_filter:
				Params << "-f" << "lavfi" << "-i" << Layer.Input;
				break;
			case input_raw:
				Params << QProcess::splitCommand(Layer.Input);
				break;
			case input_none:
				break;
		}
	}
	return Params;
}

QStringList LayeredFilter::GetComplexFilter() {
	QStringList ComplexFilters;
	struct outputs_t {
		QString Output;
		QString MergeOptions;
	};
	QList<outputs_t> OverlayOutputs;
	QList<outputs_t> AlphamergeOutputs;
	QString ComplexFilter = "";
	for (int i = 0; i < Layers.count(); ++i) {
		layers_t Layer = Layers.at(i);
		if (Layer.Filters == "") {
			if (Layer.InputNumber >= 0 && Layer.MergeType != merge_none) {
				OverlayOutputs.append({QString::number(Layer.InputNumber), Layer.MergeOptions});
				if (Layer.MergeType == merge_alphamerge)
					AlphamergeOutputs.append({QString::number(Layer.InputNumber), Layer.MergeOptions});

			}
			continue;
		}
		if (ComplexFilter != "") ComplexFilter += ";";
		if (Layer.InputName != "") {
			if (Layer.InputName.contains("["))
				ComplexFilter += Layer.InputName;
			else
				ComplexFilter += "[" + Layer.InputName + "]";
			ComplexFilter += Layer.Filters;
		} else {
			if (Layer.InputNumber >= 0)
				ComplexFilter += "[" + QString::number(Layer.InputNumber) + "]" + Layer.Filters;
		}
		ComplexFilter += "[" + Layer.OutputName + "]";
		if (Layer.MergeType != merge_none) {
			OverlayOutputs.append({Layer.OutputName, Layer.MergeOptions});
			if (Layer.MergeType == merge_alphamerge)
				AlphamergeOutputs.append({Layer.OutputName, Layer.MergeOptions});
		}
	}

	int Count = 0;
	while (!AlphamergeOutputs.isEmpty()) {
		outputs_t Down = AlphamergeOutputs.takeFirst();
		if (AlphamergeOutputs.isEmpty()) {
			break;
		}
		outputs_t Up = AlphamergeOutputs.takeFirst();
		Count++;
		QString Out = "int" + QString::number(Count);
		if (ComplexFilter != "") ComplexFilter += ";";
		ComplexFilter += "[" + Down.Output + "][" + Up.Output + "]alphamerge" + Up.MergeOptions + "[" + Out + "]";

		QList<outputs_t> newoutputs;
		foreach (outputs_t O, OverlayOutputs) {
			if (O.Output == Down.Output) continue;
			if (O.Output == Up.Output) O = {Out, ""};
			newoutputs.append(O);
		}
		OverlayOutputs = newoutputs;
	}

	while (!OverlayOutputs.isEmpty()) {
		outputs_t Down = OverlayOutputs.takeFirst();
		if (OverlayOutputs.isEmpty()) {
			if (ComplexFilter != "") OutputName = Down.Output;
			break;
		}
		outputs_t Up = OverlayOutputs.takeFirst();

		Count++;
		QString Out = "int" + QString::number(Count);
		OverlayOutputs.prepend({Out, ""});
		if (ComplexFilter != "") ComplexFilter += ";";
		QString OverlayOptions = "x=0:y=0:";
		if (Up.MergeOptions != "")  OverlayOptions = Up.MergeOptions + ":";
		ComplexFilter += "[" + Down.Output + "][" + Up.Output + "]overlay=" + OverlayOptions + "format=rgb[" + Out + "]";
		OutputName = Out;
	}
	if (ComplexFilter == "") return QStringList();
	ComplexFilters << "-filter_complex" << ComplexFilter;
	return ComplexFilters;
}

QStringList LayeredFilter::GetOutputName() {
	QStringList Output;
	if (OutputName != "") Output << "-map" << "[" + OutputName + "]";
	return Output;
}

QStringList LayeredFilter::GetAll() {
	QStringList Output;
	Output << GetInputParams() << GetComplexFilter() << GetOutputName();
	return Output;
}

QString LayeredFilter::GetInputNames(QString Name) {
	QString Output = "";
	int InputCount = -1;
	for (int i = 0; i < Layers.count(); ++i) {
		layers_t Layer = Layers.at(i);
		if (Layer.Input == "") continue;
		InputCount++;
		if (Layer.Name != Name) continue;
		Output += "[" + QString::number(InputCount) + "]";
	}
	return Output;
}
