#include "paramssettings.h"
#include "textsustitution.h"
#include "common.h"
#include "colors.h"
#include <QDir>
#include <QRegularExpression>
#include <QStandardPaths>
#include <QFileInfo>
#include <QFileInfoList>
#ifdef Q_OS_WIN
	#include <windows.h>
#else
	#include <sys/ioctl.h>
	#include <stdio.h>
#endif

struct format_definition_t {
	QString Ext;
	QString Options;
	QString DefaultQuality;
	bool IndependentCommand;
	QString AudioOptions;
};

QHash<QString, format_definition_t> OUTPUT_AVAILABLE_FORMATS = {
	{"jpg", {"jpg", "-q:v {quality}", "3", false, ""}},
	{"jpeg", {"jpeg", "-q:v {quality}", "3", false, ""}},
	{"jxl", {"jxl", "-q:v {quality}", "90", false, ""}},
	{"png", {"png", "", "", false, ""}},
	{"apng", {"png", "-f apng -plays 0 -c:v apng -pred mixed", "", false, ""}},
	{"gif", {"gif", "-filter_complex [0:v]split[a][b];[a]palettegen=stats_mode=single[p];[b][p]paletteuse=new=1", "", true, ""}},
	{"giflow", {"gif", "-filter_complex [0:v]split[a][b];[a]palettegen[p];[b][p]paletteuse", "", true, ""}},
	{"giflossy", {"gif", "-filter_complex [0:v]split[a][b];[a]palettegen=stats_mode=single[p];[b][p]paletteuse=new=1", "120", true, ""}},
	{"webp", {"webp", "-c:v libwebp -q:v {quality} -compression_level 6", "80", false, ""}},
	{"losslesswebp", {"webp", "-c:v libwebp -lossless 1 -compression_level 6", "", false, ""}},
	{"webploop", {"webp", "-c:v libwebp -q:v {quality} -compression_level 6 -loop 0", "60", false, ""}},
	{"webmvp8", {"webm", "-c:v libvpx -crf {quality} -b:v 10M", "24", false, "-c:a libopus -b:a 96K"}},
	{"webmvp9", {"webm", "-c:v libvpx-vp9 -crf {quality} -b:v 0", "24", false, "-c:a libopus -b:a 96K"}},
	{"webmav1", {"webm", "-c:v libsvtav1 -crf {quality}", "24", false, "-c:a libopus -b:a 96K"}},
	{"webmaomav1", {"webm", "-c:v libaom-av1 -crf {quality}", "24", false, "-c:a libopus -b:a 96K"}},
	{"avif", {"avif", "-c:v libsvtav1 -crf {quality}", "24", false, ""}},
	{"avifaom", {"avif", "-c:v libaom-av1 -crf {quality}", "20", false, ""}},
	{"xvid", {"mkv", "-c:v libxvid -qscale:v {quality}", "3", false, "-c:a libmp3lame -b:a 128K"}},
	{"x264", {"mkv", "-c:v libx264 -crf {quality} -preset medium", "24", false, "-c:a aac -b:a 96k"}},
	{"x265", {"mkv", "-c:v libx265 -crf {quality} -x265-params log-level=warning -preset medium", "24", false, "-c:a aac -b:a 96k"}},
	{"av1", {"mkv", "-c:v libsvtav1 -crf {quality}", "24", false, "-c:a aac -b:a 96k"}}
};

QRegularExpression RegexpUserDefinedVar("^VAR_(\\d+)_(SV_|SA_|SS_|SF_|S_|)(.+)|VAR_((.+))$");

ParamsSettings::ParamsSettings(QString applicationDirPath, QObject *parent)
	: QObject{parent}, UserDefinedVarOrderCount(0) {

	AppPath = applicationDirPath;

	#ifdef PORTABLE_CONFIG
	ConfigPath = applicationDirPath;
	ConfigExt = "ini";
	#else
	ConfigPath = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + QDir::separator() +
				 "subtitles_contact_sheet";
	#ifdef Q_OS_LINUX
	ConfigExt = "conf";
	#else
	ConfigExt = "ini";
	#endif
	#endif

	#if !defined(DEFAULT_FFMPEG) || !defined(DEFAULT_FFPROBE) || !defined(DEFAULT_GIFSICLE)
	#ifdef PORTABLE_SEARCH_DIR
	QStringList ExeSearchPaths = ListAllDirs(applicationDirPath);
	#endif
	#endif

	#ifdef DEFAULT_FFMPEG
	QString DefaultFfmpeg = DEFAULT_FFMPEG;
	#else
	QString	DefaultFfmpeg;
	#ifdef PORTABLE_SEARCH_DIR
	defaultFfmpeg = QDir::toNativeSeparators(QStandardPaths::findExecutable("ffmpeg", ExeSearchPaths));
	if (DefaultFfmpeg.isEmpty())
	#endif
		DefaultFfmpeg = QDir::toNativeSeparators(QStandardPaths::findExecutable("ffmpeg"));
	#endif

	#ifdef DEFAULT_FFPROBE
	QString DefaultFfprobe = DEFAULT_FFPROBE;
	#else
	QString DefaultFfprobe;
	#ifdef PORTABLE_SEARCH_DIR
	DefaultFfprobe = QDir::toNativeSeparators(QStandardPaths::findExecutable("ffprobe", ExeSearchPaths));
	if (DefaultFfprobe.isEmpty())
	#endif
		DefaultFfprobe = QDir::toNativeSeparators(QStandardPaths::findExecutable("ffprobe"));
	#endif

	#ifdef DEFAULT_GIFSICLE
	QString DefaultGifsicle = DEFAULT_GIFSICLE;
	#else
	QString DefaultGifsicle;
	#ifdef PORTABLE_SEARCH_DIR
	DefaultGifsicle = QDir::toNativeSeparators(QStandardPaths::findExecutable("gifsicle", ExeSearchPaths));
	if (DefaultGifsicle.isEmpty())
	#endif
		DefaultGifsicle = QDir::toNativeSeparators(QStandardPaths::findExecutable("gifsicle"));
	#endif

	QString Fonts = "";
	QString FontMono = DEFAULT_FONT_MONO;
	#ifdef PORTABLE_FONT_FILES
	if (QDir(AppPath + QDir::separator() + "Fonts").exists())
		Fonts = QDir::toNativeSeparators(AppPath + QDir::separator() + "Fonts");
	FontMono = QDir::toNativeSeparators(AppPath + DEFAULT_FONT_FILE);
	#endif

	HelpPrefixText.insert("", "\nUsage: %1$s [subtitle_or_video_file] [options]\n"
						  "\n"
						  "This program generate a contact sheet for the subtitle or video file in the same directory of it, "
						  "with the same name attaching '.contact_sheet.<format>' at the end. "
						  "Can also run in batch mode see \"--help-batch\" for instructions.\n"
						  "\n"
						  "Simple examples: \n"
						  "\n"
						  "%1$s input_sub_file.en.srt\n"
						  "Only works if the video file has the name input_sub_file, "
						  "the extension is the suported list (see --list_extensions) and they are in the same folder\n"
						  "\n"
						  "%1$s input_sub_file.mkv\n"
						  "Generates the contact sheet of the video\n"
						  "\n"
						  "All options that have a color as input use the RRGGBB[AA] format. "
						  "You can also use the color names from ffmpeg. https://ffmpeg.org/ffmpeg-utils.html#Color\n"
						  "\n"
						  "Boolean parameters support forcing a value by adding 0 or 1. "
						  "The parameter without specifying the value changes the defaults, "
						  "if the default value is 1 it sets it to 0 and vice versa.\n"
						  "Example \"--ft_hide\" is the same as \"--ft_hide 1\" and can be disabled with \"--ft_hide 0\"\n\n"
						  "General Options:\n\n");

	AddHelp("", "config", "<name|file>", "Name or file of the config file used.", "c", "Configutarion");
	AddStr("add_config", "", "", false, "", "<name|file>", "Additional configuration name or file.",
		   "Additional Configuration");
	AddStr("vertical_config", "", "", false, "", "<name|file>", "Load this configuration file when detect Vertical video",
		   "Vertical Config");
	AddHelp("", "list_config", "", "Shows a list of the names of the available configuration files.", "", "", param_bool);
	AddStr("config_desc", "", "", false, "", "<text>", "Small text describing what this configuration file do.",
		   "Configuration description");
	AddHelp("", "save", "", "Save the actual parameters in configuration file.", "", "", param_bool);
	AddHelp("", "save_all", "",
			"Same as --save but creates the complete config file, all the unset options with the default value.", "", "",
			param_bool);
	AddHelp("", "batch", "",
			"Enables batch mode, allows to process multiple input files or multiple options for one file, has its own special parameters and systax, by running \"--help-batch\".",
			"b", "", param_bool);

	AddHelp("", "sub", "<file>", "Subtitle file input.", "", "Subtitle File", param_file);
	AddHelp("", "sub_stream", "<n>", "Subtitle stream input.", "", "Subtitle Stream", param_int);
	AddHelp("", "video", "<file>", "Video file input.", "", "Video File", param_file);
	AddHelp("", "images", "<dir>", "Images directory input.", "", "Images Directory", param_dir);
	AddHelp("", "dvd", "<dir>", "DVD VIDEO_TS directory.", "", "DVD VIDEO_TS Directory", param_dir);
	AddHelp("", "dvd_title", "<n>", "Select this DVD Title, *0* means automatically select the longest title.", "",
			"DVD Title", param_int);

	AddHelp("", "out", "<file>", "Output file. This option has priority over the other output options.", "", "Output File",
			param_file);
	AddDir("out_dir", "", false, "out", "Output directory.", "Output directory");
	AddStr("out_relative_dir", "", "", false, "out", "<name>", "Output directory relative to input file.",
		   "Relative Output Directory");
	AddStr("out_name", "%in.filename%", "raw|%in.filename%|%in.basename%", false, "out", "<name>",
		   "Modify output file name using text substitution.",
		   "Output Name");
	AddStr("suffix", ".contact_sheet", "", false, "out", "<text>",
		   "Text appended to the output file name before the format extension. Default .contact_sheet.", "Suffix");
	AddStr("suffix_number", "1", "raw|auto|time|short|1|2|3|4|5", false, "out", "<number|time_format>",
		   "Format for numbering individual files, can be a number of digits, \"auto\", \"time\", \"short\" or a time format",
		   "Suffix Individal Number Format");
	AddStr("prefix", "", "", false, "out", "<text>",
		   "Text preceding the output file name. Default empty", "Prefix");
	AddStr("prefix_number", "", "raw|auto|time|short|1|2|3|4|5", false, "out", "<number|time_format>",
		   "Format for numbering individual files, can be a number of digits, \"auto\", \"time\", \"short\" or a time format",
		   "Prefix Individal Number Format");
	AddBool("no_overwrite", false, false, "out",
			"Does not overwrite the output file, if it already exists it simply does nothing.", "No Overwrite");
	AddInt("process", 1, "min=0,max=32", false, "", "<number>", "Nunber of parallel processes. Default 1.",
		   "Parallel processes");
	AddBool("idle", false, false, "", "Runs ffmpeg processes with idle priority", "Idle prority");
	AddLayout("layout", "3x7", true, "as", "Number of captured frames in the format COLUMSxROWS.", "Grid layout");
	AddInt("size", 640, "min=0", false, "as", "<px>",
		   "Size in pixels of the largest dimension of the video, the width in horizontal videos, the height in vertical videos. 0 means input size.",
		   "Screenshots Size");
	AddInt("min_size", 300, "min=0", false, "as", "<px>",
		   "The smallest dimension of a video will never be smaller than this, 0 to disable.", "Screenshots Minimum Size");
	AddStr("aspect_ratio", "display", "raw|display|resolution", false, "as", "<ar>",
		   "Allows control the aspect ratio of the output. in format W:H, example 16:9. display is the ar configured in file, "
		   "resolution is the ar of input resolution. Default display", "Aspect Ratio");
	AddStr("format", "jpg",
		   "jpg|jxl|png|apng|webp|webploop|gif|giflow|giflossy|xvid|x264|x265|av1|webmvp8|webmvp9|webmav1|avif|custom",
		   true, "out", "<name>", "Output format.", "Output Format");
	AddInt("output_quality", -1, "min=-1,max=1000", false, "out", "<number>",
		   "For formats that support quality settings, each format has different supported values. Default -1 (use format default value).",
		   "Output Quality");
	AddBool("enable_audio", false, false, "out",
			"Enable audio output for suported formats", "Enable Audio");
	AddStr("custom_format_ext", "", "", true, "ad", "<ext>", "Custom format extension.", "Custom Format Extension");
	AddStr("custom_format_video", "", "", true, "ad", "<string>", "Custom format video ffmpeg options.",
		   "Custom Format Video");
	AddStr("custom_format_audio", "", "", true, "ad", "<string>", "Custom format video ffmpeg options.",
		   "Custom Format Audio");
	AddStr("custom_format_default_quality", "", "", true, "ad", "<string>", "Custom format default quality.",
		   "Custom Format Default Quality");
	AddStr("pixel_format", "", "raw|yuv420p|yuv422p|yuv444p|yuva420p|rgb24|rgba|rgb48be|rgba64be|bgra", false, "out",
		   "<pix_fmt>",
		   "Pixel format of the output, some formats can use several pixel formats, this option allows you to specify a specific one. "
		   "Normally it is used when a format supports transparency but we don't want to use it.",
		   "Output Pixel Format");

	AddStr("scale_algorithm", "lanczos", "raw|bilinear|bicubic|lanczos", true, "ad",
		   "<alg>", "Algorithm used to resize to the desired image size.", "Scaling Algorithm");
	AddStr("scale_flags", "+accurate_rnd+full_chroma_int",
		   "raw|+accurate_rnd+full_chroma_int+full_chroma_inp|+accurate_rnd+full_chroma_int|+accurate_rnd|+full_chroma_int|+full_chroma_inp",
		   false, "ad",
		   "<flags>", "Additional flags for the scale filter. https://ffmpeg.org/ffmpeg-scaler.html#sws_005fflags",
		   "Scaling Flags");
	AddStr("scale_custom_options", "", "", false, "ad",
		   "<opt>", "Additional options passed to the corresponding filter.", "Scaling Custom Options");
	AddBool("scale_zscale", false, false, "ad", "Uses the zscale filter instead of scale.",
			"Scaling use zscale");

	AddStr("custom_capture_filters", "", "", false, "ad",
		   "<filters>", "Additional filters for captured screenshots and clips.", "Custom Capture Filters");

	AddHelp("out", "jpg", "", "Output format jpg (default).", "", "", param_bool);
	AddHelp("out", "jpeg", "", "Output format jpg (default).", "", "", param_bool);
	AddHelp("out", "jxp", "", "Output format jpeg xl.", "", "", param_bool);
	AddHelp("out", "png", "", "Output format png.", "", "", param_bool);
	AddHelp("out", "apng", "", "Output format animated png (Only video).", "", "", param_bool);
	AddHelp("out", "webp", "", "Output format webp.", "", "", param_bool);
	AddHelp("out", "losslesswebp", "", "Output format lossless webp.", "", "", param_bool);
	AddHelp("out", "webploop", "", "Output format webp loop video.", "", "", param_bool);
	AddHelp("out", "gif", "", "Output format gif. Each frame has its own color palette.", "", "", param_bool);
	AddHelp("out", "giflow", "", "Output format gif. Same color palette for the whole file, has less quality.", "", "",
			param_bool);
	AddHelp("out", "giflossy", "", "Output format gif. Same as gif but use gifsicle to reduce the output file size.", "",
			"", param_bool);
	AddHelp("out", "xvid", "", "Output format mkv with xvid codec and mp3 audio.", "", "", param_bool);
	AddHelp("out", "x264", "", "Output format mkv with x264 codec and AAC audio.", "", "", param_bool);
	AddHelp("out", "x265", "", "Output format mkv with x265 codec and AAC audio.", "", "", param_bool);
	AddHelp("out", "av1", "", "Output format mkv with AV1 codec and AAC audio.", "", "", param_bool);
	AddHelp("out", "webmvp8", "", "Output format webm with vp8 codec and Opus audio.", "", "", param_bool);
	AddHelp("out", "webmvp9", "", "Output format webm with vp9 codec and Opus audio.", "", "", param_bool);
	AddHelp("out", "webmav1", "", "Output format webm with av1 codec and Opus audio.", "", "", param_bool);
	AddHelp("out", "avif", "", "Output format avif with AV1 codec.", "", "", param_bool);

	AddStr("start", "30", "", false, "", "<time>",
		   "Exclude the first part, in time mode is in seconds or HH:MM:SS, in line mode is in lines.", "Start Time");
	AddStr("end", "-30", "", false, "", "<time>",
		   "In time mode is the time in second on HH:MM:SS to end, If it is negative, it is subtracted from the duration.",
		   "End Time");
	AddStr("mode", "time", "time|line|video_time|video_end|random|fixed", true, "", "<mode>",
		   "Mode of operation, searches for frames to capture using different methods. "
		   "It can be: time, line, video_time, video_end, random, fixed. time and line are for subtitles, "
		   "video_time, and video_end are for video only. More details in man page."
		   "random uses video_time intervals but select a random frame inside."
		   "fixed is selected automaticaly defining fixed_interval.", "Mode");
	AddStr("fixed_interval", "", "", false, "", "<time>",
		   "Fixed interval between screenshots, when defined the layout rows are calculated automatically."
		   "", "Fixed Interval");

	AddBool("only_keyframes", false, false, "", "Try to campure only keyframes, sometimes this is faster.",
			"Only Keyframes");
	AddInt("rep_frames", 0, "min=1", false, "", "<number>",
		   "selects the most representative frame considering the specified number of frames.", "Most Representative Frames");

	AddHelp("", "mf", "[nunber,]time",
			"Allows manual selection of screenshots. Replaces the automatically selected time with the indicated in time. "
			"You can add as many --mf as you want, if no number is indicated the order of --mf in the parameters is used, "
			"the first parameter would be 1 and successively.", "", "Manual Frame", param_string);
	AddStr("mfs", "", "", false, "", "<time_list>",
		   "Comma-separated time list with all selected frames, the same as the previous command but in one go, "
		   "the current command is always displayed in the output messages.", "Multiple Manual Frames");
	AddStr("mfs_separator", ",", "raw|,|-| |\n", false, "", "<separator>",
		   "Character or string used as separator in --mfs.", "Multiple Manual Frames Separator");
	AddBool("show_mfs", false, false, "", "Displays the mfs parameter that contains the current list of frames.",
			"Show mfs parameter");

	AddBool("no_grid", false, false, "out",
			"Don't generate the screenshot grid, generates an individual file for each screenshot.", "Disable Grid");
	AddInt("grid_border", 2, "min=0", false, "as", "<px>", "Set all the borders of the grid in pixels.", "Grid Border");
	AddInt("grid_border_up", -1, "min=-1", false, "as", "<px>",
		   "Set up border of the grid in pixels. -1 means use grid_border value.", "Grid Up Border");
	AddInt("grid_border_down", -1, "min=-1", false, "as", "<px>",
		   "Set down border of the grid in pixels. -1 means use grid_border value.",
		   "Grid Down Border");
	AddInt("grid_border_left", -1, "min=-1", false, "as", "<px>",
		   "Set left border of the grid in pixels. -1 means use grid_border value.",
		   "Grid Left Border");
	AddInt("grid_border_right", -1, "min=-1", false, "as", "<px>",
		   "Set right border of the grid in pixels. -1 means use grid_border value.",
		   "Grid Right Border");
	AddInt("grid_border_vertical", -1, "min=-1", false, "as", "<px>",
		   "Set vertical internal borders of the grid in pixels. -1 means use grid_border value.",
		   "Grid Vertical Borders");
	AddInt("grid_border_horizontal", -1, "min=-1", false, "as", "<px>",
		   "Set horizontal internal borders of the grid in pixels. -1 means use grid_border value.",
		   "Grid Horizontal Borders");
	AddColor("grid_border_color", "", false, "as",
			 "Set the background for the borders of the grid in RRGGBBAA format. By default uses the same value as --tb_bg_color.",
			 "Grid Border Color");
	AddStr("bg_image", "", "", false, "as", "<file>", "Set the background image for the entire contsct sheet. "
		   "Implies that grid and text block backgrounds become transparent.", "Background Image");
	AddStr("bg_image_halign", "center", "raw|right|center|left", true, "as", "<haling>",
		   "Horizontal alignment of background image, can be right, center, left or a percentage.",
		   "Horizontal Align for Bacground Image");
	AddStr("bg_image_valign", "middle", "raw|up|middle|down", true, "as", "<valing>",
		   "Vertical alignment of background image, can be up, middle, down or a percentage.",
		   "Vertical Align for Bacground Image");
	AddBool("bg_image_scale", false, false, "as", "Scale the background image before crop it.",
			"Autoscale Background Image");
	AddBool("bg_image_tile", true, false, "as", "Repeat the background image as many times as necessary to fill the space.",
			"Tile Background Image");
	AddInt("bg_image_blur", 0, "min=0,max=100", false, "as", "<number>",
		   "Applies a blur filter to the background image. the higher the number the more blurred. Default 0 (disabled).",
		   "Blur Background Image");
	AddInt("bg_image_exposure", 0, "min=-300,max=300", false, "as", "<number>",
		   "Applies an exposure filter to the background image. "
		   "allows a number between -300 and 300, negative numbers decrease the exposure making the image darker, "
		   "positive numbers make the image lighter. Default 0 (disabled).", "Exposure for Background Image");
	AddInt("bg_image_saturation", 100, "min=0,max=300", false, "as", "<number>",
		   "Applies a saturation filter to the background image. "
		   "Accepts a number between 0 and 300, 0 converts the image to black and white. Default 100 (disabled).",
		   "Saturation for Background Image");
	AddStr("bg_gradient", "none", "", false, "as", "<gradient>",
		   "Generates a gradient as a background image with the indicated parameters. Example \"linear:h:white,black\".",
		   "Gradient as Background");

	AddStr("logo", "", "", false, "as", "<file>", "Add this logo to each screenshot.", "Frames Logo");
	AddStr("logo_pos", "down_left", "up_left|up_right|down_left|down_right", true, "as", "<position>",
		   "Position of the logo, can be up_left, up_right, down_left, down_right. Default down_left.", "Frames Logo Position");
	AddInt("logo_margin", 10, "min=0", false, "as", "<px>", "Pixel separation of logo from borders. Default 10.",
		   "Frames Logo Margin");
	AddInt("logo_scale", 0, "min=0,max=100", false, "as", "<%>",
		   "Size of the logo in percentage with respect to the size of the screenshot. "
		   "0 means no resizing, 100 same size as the screenshot. Default 0.",
		   "Logo Scale");
	AddInt("logo_blur", 0, "min=0,max=100", false, "as", "<number>",
		   "Applies a blur filter to the logo. the higher the number the more blurred. Default 0 (disabled).",
		   "Blur Logo");
	AddInt("logo_exposure", 0, "min=-300,max=300", false, "as", "<number>",
		   "Applies an exposure filter to the logo. "
		   "allows a number between -300 and 300, negative numbers decrease the exposure making the image darker, "
		   "positive numbers make the image lighter. Default 0 (disabled).", "Exposure for Logo");
	AddInt("logo_saturation", 100, "min=0,max=300", false, "as", "<number>",
		   "Applies a saturation filter to the logo. "
		   "Accepts a number between 0 and 300, 0 converts the image to black and white. Default 100 (disabled).",
		   "Saturation for Logo");
	AddInt("logo_opacity", 100, "min=0,max=100", true, "as", "<number>",
		   "Percentage of transparency of the logo. 100 is opaque, 0 totally transparent. Default 100",
		   "Opacity for Logo");
	AddColor("logo_transparent_color", "", false, "as", "Makes this color transparent.",
			 "Logo Transp. Color");
	AddInt("logo_transparent_similarity", 1, "min=1,max=100", true, "as", "<number>",
		   "Percentage of similarity with logo_transparent_color to consider a color transparent.",
		   "Logo Transp. Color Similarity");
	AddInt("logo_transparent_blend", 0, "min=0,max=100", true, "as", "<number>",
		   "Percentage of transparency for pixels that fall outside the similarity.",
		   "Logo Transp. Color Blend");


	AddStr("mask", "", "", false, "as", "<color|gradient|file>",
		   "Mask applied to each screenshot to define the transparency of each part of the image. "
		   "It can be a solid color (the whole image has the same opacity), a gradient. Or an image file. "
		   "In this option, white means totally opaque and black means totally transparent.",
		   "Mask for Screenshots");
	AddInt("rounded_corners", 0, "min=0", false, "as", "<px>", "Set the radius of rounded corners in pixels.",
		   "Rounded corners");

	AddInt("video_preview", 0, "min=0", false, "out", "<milliseconds>",
		   "Instead of capturing images, it captures videos of the specified duration.", "Video Preview");
	AddInt("video_fps", 0, "min=0", false, "out", "<fps>",
		   "When capturing videos or generating videos from images, these are the frames per second of the result.", "Video FPS");
	AddBool("concat", false, false, "out",
			"Instead of putting the screenshots in a grid, puts them one after the other, requires an output format that supports video.",
			"Concat");

	AddHelp("out", "full", "", "Screenshots at input resolution, same as --size \"\".", "", "Full Resolution", param_bool);
	AddHelp("out", "screenshots", "<number>",
			"Full size unaltered screentshots, same as --no_grid --size \"\" --format png --ft_hide --sufix \".screenshot_\"", "",
			"Unaltered Screenshots", param_shortcut);

	AddHelp("out", "gif_images", "<images>",
			"Short gif video with images. Same as --format gif - l 1x<images> --ft_hide --concat --video_fps 1 --suffix \".images\"",
			"", "Gif Images", param_shortcut);
	AddHelp("out", "webp_images", "<images>",
			"Short webp video with images. Same as --format webploop - l 1x<images> --ft_hide --concat --video_fps 1 --suffix \".images\"",
			"", "Webp Images", param_shortcut);
	AddHelp("out", "gif_clip", "<clips>",
			"Gif video with short one - second videos. Same as --format gif - l 1x<clips> --ft_hide --concat --video_preview 1000 --video_fps 12 --size 320 --suffix \".clip\"",
			"", "Gif Clips", param_shortcut);
	AddHelp("out", "webp_clip", "<clips>",
			"Gif video with short one - second videos. Same as --format webploop - l 1x<clips> --ft_hide --concat --video_preview 1000 --video_fps 12 --suffix \".clip\"",
			"", "Webp Clips", param_shortcut);
	AddHelp("out", "gif_singles", "<clips>",
			"Same as --gif_clip but does not join the fragments together, it generates as many independent gifs as indicated. "
			"Same as --format gif - l 1x<clips> --ft_hide --video_preview 1000 --video_fps 12 --size 320 --suffix \".single\"",
			"", "Gif Independent clips", param_shortcut);

	AddFile("ffmpeg", DefaultFfmpeg, "exe", true, "", "ffmpeg executable file.", "Ffmpeg");
	AddFile("ffprobe", DefaultFfprobe, "exe", true, "", "ffprobe executable file.", "Ffprobe");
	AddFile("gifsicle", DefaultGifsicle, "exe", false, "", "gifsicle executable file.", "Gifsicle");
	AddFile("mediainfo", "", "exe", false, "", "mediainfo cli executable file.", "mediainfo");
	AddBool("enable_mediainfo", false, false, "", "Imports to variables the information provided by mediainfo.",
			"Enable mediainfo");
	AddDir("tmp", "", false, "ad", "Temporal directory.", "Tmp Directory");

	AddStr("exec_before", "", "", false, "ad", "<command>",
		   "Execute this command before start. Supports text substitution to customize the command.", "Command to Execute Before");
	AddStr("exec_after", "", "", false, "ad", "<command>",
		   "Execute this command at the end. Supports text substitution to customize the command. This command is executed even if no_overwrite is enabled.",
		   "Command to Execute After");
	AddStr("exec", "", "", false, "ad", "<command>",
		   "Execute this command instead of the main action of the program. Supports text substitution to customize the command.",
		   "Command to Execute");

	AddHelp("ad", "var", "<var=value>",
			"Allows to create variables to be used within text substitution, these variables are also saved in the configuration file "
			"prefixed with \"VAR_\" and a number to execute them in order.", "", "User Defined Variable", param_string);
	AddHelp("ad", "var_stream", "<var=value>",
			"Same as var but it is executed in each stream, the value of the variable can start with \"context.\" "
			"to define it within that context, if it is not a normal variable.", "", "User Defined Variable Stream context",
			param_string);
	AddHelp("ad", "var_stream_video", "<var=value>", "Same as \"--var_stream\" but are executed only on video streams.", "",
			"User Defined Variable Video context", param_string);
	AddHelp("ad", "var_stream_audio", "<var=value>", "Same as \"--var_stream\" but are executed only on audio streams.", "",
			"User Defined Variable Audio context", param_string);
	AddHelp("ad", "var_stream_subtitle", "<var=value>",
			"Same as \"--var_stream\" but are executed only on subtitles streams.", "", "User Defined Variable Subtitles context",
			param_string);
	AddHelp("ad", "var_stream_attachment", "<var=value>",
			"Same as \"--var_stream\" but are executed only on attachment streams.", "",
			"User Defined Variable Attachment context", param_string);
	AddHelp("ad", "var_file", "<file|context=file>",
			"Loads the variables contained in this file, using the line format name=value.", "", "User Defined Variable File",
			param_file);

	AddFile("template", "", "", false, "out",
			"Generate a text using this file as a template using text substitution.", "Template File");
	AddStr("template_ext", "txt", "", false, "out", "<extension>",
		   "Extension added to the video name to generate the file with the substituted values.", "Template Extension");

	AddBool("quiet", false, false, "ad", "Disable messages, for batch processing.", "Quiet");
	AddBool("debug", false, false, "ad", "Enable debug mode.", "Debug");
	AddBool("cmd", false, false, "ad", "Show ffmpeg and ffprobe commands.", "Show CMD");

	AddHelp("", "gui_progress", "", "Displays messages that the GUI interprets to show the progress bar.");
	AddHelp("", "version", "", "Displays the current version of this program.", "v");
	AddHelp("", "help", "", "Displays help on commandline options.", "h");
	AddHelp("", "help-sub", "", "Displays subtitles options.");
	AddHelp("", "help-text", "", "Displays text block options.");
	AddHelp("", "help-time", "", "Displays frame times options.");
	AddHelp("", "help-vr", "", "Displays virtual reality options.");
	AddHelp("", "help-rename", "", "Displays rename options.");
	AddHelp("", "help-images", "", "Displays image options.");
	AddHelp("", "help-batch", "", "Displays batch mode options.");
	AddHelp("", "help-wm", "", "Displays Watermark options.");
	AddHelp("", "help-all", "", "Displays all the options.");

	AddHelp("", "list_vars", "", "Displays all the variables available to use in text substitution.");
	AddHelp("", "list_extensions", "", "Displays all the file extensions used in automatic detection.");

	HelpPrefixText.insert("sub", "\nSubtitles Options:\n");

	AddHelp("sub", "mode", "<time|line>", "Mode of operation, searches for frames to capture using different methods.");
	AddHelp("sub", "time", "", "Set time mode of operation.", "", "", param_bool);
	AddHelp("sub", "line", "", "Set line mode of operation.", "", "", param_bool);

	AddHelp("sub", "srt", "", "Force srt subtitle file format.", "", "Force srt", param_bool);
	AddHelp("sub", "ass", "", "Force ass subtitle file format.", "", "Force ass", param_bool);
	AddHelp("sub", "vtt", "", "Force vtt subtitle file format.", "", "Force vtt", param_bool);
	AddHelp("sub", "lang", "<code>", "Force subtitle language in ISO 639 language code.", "", "Force Subtitle Language",
			param_string);

	AddBool("sub_show_embedded", true, false, "sub", "When it is 1, shows the embedded subtitles, if any.",
			"Show Embedded Subtitles");
	AddInt("sub_emb_select", 0,
		   "0=First Forced|1=First Lang Forced|2=First Non-Forced|3=First Lang Non-Forced|4=First Pref Forced|5=First Lang Pref Forced",
		   false, "sub", "<type>",
		   "Change the selection method of the embedded subtitles: "
		   "0 - First forced subtitle, "
		   "1 - First forced subtitle of the selected language, "
		   "2 - First non-forced subtitle, "
		   "3 - First non-forced subtitle of the selected language, "
		   "4 - First subtitle prefers forced, "
		   "5 - First subtitle of the selected language prefers forced", "Embedded Sub. Selection");
	AddStr("sub_emb_lang", "en", "", false, "sub", "<code>",
		   "Language to localize and select subtitles depending on the previous option.", "Embedded Sub. Prefered Language");

	AddInt("min", 4, "min=0", false, "sub", "<number>",
		   "Minimum subtitle size in characters. Works differently depending on the mode.", "Min Subtitle Lenght");
	AddInt("start_line", 1, "min=1", false, "sub", "<number>",
		   "Subtitle line in which to start taking screenshots, only used in line mode", "Start Line");
	AddInt("end_line", -1, "", false, "sub", "<number>",
		   "Subtitle line in which to finish taking screenshots, if it is negative it is deducted from the total number of lines, "
		   "it is only used in line mode", "End Line");
	AddInt("sub_delay", -1, "", false, "sub", "<ms>", "Time in milliseconds added to the start of each subtitle line, "
		   "to adjust for discrepancies between subtitle start time and frame time. "
		   "-1 means add the time corresponding to one frame.", "Subtitle Delay");

	AddFont("sub_font", DEFAULT_FONT, false, "sub", "Set the subtitles font name.", "Subtitle Font");
	AddDir("sub_fonts_dir", Fonts, false, "sub", "Set the aditional directory to search for fonts.",
		   "Subtitle Font Directory");
	AddColor("sub_color", "FFFFFF", false, "sub",
			 "Set the subtitles font color in RRGGBBAA format. Default: FFFFFF.", "Subtitle Color");
	AddInt("sub_size", 16, "min=4", false, "sub", "<px>", "Set the subtitles font size. Default: 16.",
		   "Subtitle Font Size");
	AddInt("sub_border", 1, "0=None|1=Outline|3=Line Box|4=Box", false, "sub", "<type>",
		   "Set the subtitles border:  0 - none, "
		   "1 - Outline, Default --sub_oc for outline color, "
		   "3 - Each line in one box, --sub_oc for background color, "
		   "4 - All lines in a box, --sub_bc for background color", "Subtitle Border");
	AddColor("sub_bc", "00000000", false, "sub", "Set the background color on border=4 in RRGGBBAA format. "
			 "Example: '-sub_oc 00000055' for tranparent black background. Default: 00000000.", "Subtitle Background Color");
	AddColor("sub_oc", "00000000", false, "sub",
			 "Set the outline and background color on border=3 in RRGGBBAA format. Default: 00000000.", "Subtitle Outline Color");
	AddBool("sub_force_ass", false, false, "sub", "Ignores the style inside the ass and force the configured style",
			"Force Style on ass");
	AddStr("sub_custom", "", "", false, "sub", "<string>", "Custom options for subtitles ffmpeg filter.",
		   "Custom Subtitle ffmpeg Options");

	AddHelp("sub", "green", "", "Simple style, green subtitles text. Same as --sub_color 00FF00.", "", "Green Subtitles",
			param_bool);
	AddHelp("sub", "yellow", "", "Simple style, yellow subtitles text. Same as --sub_color FFEA00.", "",
			"Yellow Subtitles", param_bool);
	AddHelp("sub", "small", "", "Simple style, small subtitle text. Same as -sub_size 12.", "", "Small Subtitles",
			param_bool);
	AddHelp("sub", "big", "", "Simple style, big subtitle text. Same as --sub_size 24.", "", "Big Subtitles", param_bool);
	AddHelp("sub", "verybig", "", "Simple style, very big subtitle text. Same as --sub_size 32.", "", "Bigger Subtitles",
			param_bool);
	AddHelp("sub", "box", "", "Simple style, subtitle text inside a black box. Same as --sub_border 4.", "",
			"Box Background Subtitles", param_bool);
	AddHelp("sub", "tbox", "", "Simple style, subtitle text inside a black transparent box. "
			"Same as --sub_border 4 --sub_bc 00000050.", "", "Transparent Box Subtitles", param_bool);

	HelpPrefixText.insert("tb", "\nText block Options:\nThis options controls the aspect of the text block.\n\n");

	AddStr("tb", "top", "top|bottom|hidden", true, "tb", "<pos>", "Location of text block, top, bottom or hidden.",
		   "Text Block Position");
	AddStr("tb_title", "%title%", "", false, "tb", "<text>",
		   "Set the title, default use the video or subtitle file name, can use text substitution.", "Text Block Title");
	AddInt("tb_border", 10, "min=0", false, "tb", "<px>", "Border size in pixels around the text and the logo.",
		   "Text Block Border");
	AddInt("tb_add_height", 20, "min=0", false, "tb", "<px>",
		   "Adds this number of pixels to the height automatically calculated based on the text.", "Text Block Add Height");
	AddStr("tb_comments",
		   "Made with %B1%Subtitles Contact Sheet%B0% "
		   "(https://gitlab.com/vongooB9/subtitles_contact_sheet_qt)", "", false, "tb",
		   "<text>",
		   "Set the comment text. It is a text that appears at the end of the text "
		   "block and in which you can put anything. Can use text substitution.", "Text Block Comments");

	AddHelp("tb", "tb_comment_append", "<text>", "Adds the specified text after the current comment text.", "",
			"Text Block Comments Append", param_string);
	AddHelp("tb", "tb_comment_prepend", "<text>", "Adds the specified text before the current comment text.", "",
			"Text Block Comments Prepend", param_string);

	AddFont("tb_font", DEFAULT_FONT, false, "tb", "Set the font for text header.", "Text Block Font");
	AddDir("tb_fonts_dir", Fonts, false, "tb", "Set the additional directory to search for fonts",
		   "Text Block Fonts Directory");
	AddInt("tb_font_size", 24, "min=4", false, "tb", "<px>", "Set the font height for text header in pixels. Default 24.",
		   "Text Block Font Size");
	AddColor("tb_font_color", "909090", false, "tb",
			 "Set the color of the font for text header in RRGGBBAA format or color name. Default 909090.", "Text Block Font Color");
	AddInt("tb_font_border", 0, "min=0", false, "tb", "<px>", "Set the width in pixels of the border around the font.",
		   "Text Block Font Border");
	AddColor("tb_font_border_color", "000000", false, "tb",
			 "Set the color of the border around the font. Default 000000.", "Text Block Font Border Color");
	AddInt("tb_font_border_blur", 0, "min=0", false, "tb", "<amount>",
		   "When it is greater than 0 it blurs the border of text. Default 0.", "Text Block Font Border Blur");
	AddInt("tb_font_shadow", 0, "", false, "tb", "<px>",
		   "Shadow offset in pixels with respect to the text, Default 0 (disabled).", "Text Block Font Shadow");
	AddColor("tb_font_shadow_color", "000000", false, "tb", "Color of shadow, Default 000000.",
			 "Text Block Font Shadow Color");
	AddBool("tb_font_italic", true, false, "tb", "Italicizes the text. Default 1.", "Text Block Font Italic");
	AddBool("tb_font_bold", false, false, "tb", "Sets the text to bold. Default 0.", "Text Block Font Bold");
	AddStr("tb_font_pos", "middle_left",
		   "up_left|up_right|down_left|down_right|up_center|down_center|middle_left|middle_right|middle_center", true, "tb",
		   "<position>",
		   "Alignment of the text within the block, it can be: up_left, up_right, down_left, "
		   "down_right, up_center, down_center, middle_left, middle_right, middle_center. Default up_left.",
		   "Text Block Font Position");
	AddInt("tb_font_left_margin", 0, "min=0", false, "tb", "<px>", "Margin to the left in pixels of the text. Default 0.",
		   "Text Block Font Left Margin");
	AddInt("tb_font_right_margin", 0, "min=0", false, "tb", "<px>", "Margin to the right in pixels of the text. Default 0.",
		   "Text Block Font Right Margin");
	AddInt("tb_font_vertical_margin", 0, "min=0", false, "tb", "<px>",
		   "Margin in pixels applied to the text, when the text is aligned up it is the "
		   "top margin, when aligned down it is the bottom margin. Default 0.", "Text Block Font Vertical Margin");

	AddFont("tb_title_font", "", false, "tb", "Set the font for the title. The default value is empty. "
			"When empty use the value of --tb_font.", "Title Font");
	AddInt("tb_title_font_size", 30, "min=0", false, "tb", "<px>",
		   "Set the font height for title in px. The default value is 30. "
		   "When 0 use the value of --tb_font_size.", "Title Font Size");
	AddColor("tb_title_color", "FFFFFF", false, "tb",
			 "Set the color of the font for the title. Default FFFFFF.", "Title Font Color");
	AddInt("tb_title_border", 0, "min=0", false, "tb", "<px>",
		   "Set the width in pixels of the border around the title. Default 0.", "Title Font Border");
	AddColor("tb_title_border_color", "000000", false, "tb",
			 "Set the color of the border around the title. Default 000000.", "Title Font Border Color");
	AddInt("tb_title_border_blur", 0, "min=0", false, "tb", "<amount>",
		   "When it is greater than 0 it blurs the border of title. Default 0.", "Title Font Border Blur");
	AddInt("tb_title_shadow", 0, "min=0", false, "tb", "<px>",
		   "Shadow offset in pixels for the title with respect to the text. "
		   "Default 0 (disabled).", "Title Font Shadow");
	AddColor("tb_title_shadow_color", "000000", false, "tb", "Color of shadow for the title, Default 000000.",
			 "Title Font Shadow Color");
	AddBool("tb_title_italic", false, false, "tb", "Italicizes the title. Default 0.", "Title Font Italic");
	AddBool("tb_title_bold", true, false, "tb", "Sets the title to bold. Default 1.", "Title Font Bold");
	AddStr("tb_title_pos", "up_left",
		   "up_left|up_right|down_left|down_right|up_center|down_center|middle_left|middle_right|middle_center", true, "tb",
		   "<position>",
		   "Alignment of the title within the block, it can be: up_left, up_right, "
		   "down_left, down_right, up_center, down_center, middle_left, middle_right, middle_center. Default up_left.",
		   "Title Position");
	AddInt("tb_title_left_margin", 0, "min=0", false, "tb", "<px>",
		   "Margin to the left in pixels of the title text. Default 0.", "Title Left Margin");
	AddInt("tb_title_right_margin", 0, "min=0", false, "tb", "<px>",
		   "Margin to the right in pixels of the title text. Default 0.", "Title Right Margin");
	AddInt("tb_title_vertical_margin", 0, "min=0", false, "tb", "<px>",
		   "Margin in pixels applied to the title, when the title is aligned "
		   "up it is the top margin, when aligned down it is the bottom margin. Default 0.", "Title Vertical Margin");

	AddFont("tb_comments_font", "", false, "tb", "Set the font for the comments. The default value is empty. "
			"When empty use the value of --tb_font.", "Comments Font");
	AddInt("tb_comments_font_size", 18, "min=0", false, "tb", "<px>",
		   "Set the font height for comments in px. The default value is 18. "
		   "When 0 use the value of --tb_font_size.", "Comments Font Size");
	AddColor("tb_comments_color", "505050", false, "tb",
			 "Set the color of the font for the comments. Default 505050.", "Comments Font Color");
	AddInt("tb_comments_border", 0, "min=0", false, "tb", "<px>",
		   "Set the size in px of the border around the comments. Default 0.", "Comments Font Border");
	AddColor("tb_comments_border_color", "000000", false, "tb",
			 "Set the color of the border around the comments. Default 000000.", "Comments Font Border Color");
	AddInt("tb_comments_border_blur", 0, "min=0", false, "tb", "<amount>",
		   "When it is greater than 0 it blurs the border of comments. Default 0.", "Comments Font Border Blur");
	AddInt("tb_comments_shadow", 0, "min=0", false, "tb", "<px>",
		   "Shadow offset in pixels for the comments with respect to the text. "
		   "Default 0 (disabled).", "Comments Font Shadow");
	AddColor("tb_comments_shadow_color", "000000", false, "tb",
			 "Color of shadow for the comments, Default 000000.", "Comments Font Shadow Color");
	AddBool("tb_comments_italic", true, false, "tb", "Italicizes the comments. Default 1.", "Comments Font Italic");
	AddBool("tb_comments_bold", false, false, "tb", "Sets the comments to bold. Default 0.", "Comments Font Bold");
	AddStr("tb_comments_pos", "down_left",
		   "up_left|up_right|down_left|down_right|up_center|down_center|middle_left|middle_right|middle_center", true, "tb",
		   "<position>", "Alignment of the comments within the block, it can be: "
		   "up_left, up_right, down_left, down_right, up_center, down_center, middle_left, middle_right, middle_center. Default up_left.",
		   "Comments Position");
	AddInt("tb_comments_left_margin", 0, "min=0", false, "tb", "<px>",
		   "Margin to the left in pixels of the comments text. Default 0.", "Comments Left Margin");
	AddInt("tb_comments_right_margin", 0, "min=0", false, "tb", "<px>",
		   "Margin to the right in pixels of the comments text. Default 0.", "Comments Right Margin");
	AddInt("tb_comments_vertical_margin", 0, "min=0", false, "tb", "<px>",
		   "Margin in pixels applied to the comments, when the comments is aligned "
		   "up it is the top margin, when aligned down it is the bottom margin. Default 0.", "Comments Vertical Margin");

	AddColor("tb_bg_color", "000000", false, "tb",
			 "Set the background color for text header in RRGGBBAA format or color name.", "Text Block Background Color");
	AddHelp("tb", "tb_transparent", "", "Set the background transparent. "
			"Same as --tb_bg_color 00000000 --grid_border_color 00000000 --format png", "", "Transparent Background", param_bool);
	AddStr("tb_bg_gradient", "none", "", false, "tb", "<gradient>",
		   "Generates a gradient as a background image for the text block with the indicated parameters.",
		   "Gradient as Text Block Background");

	AddHelp("tb", "tb_left", "", "Aligns text to the left. "
			"Same as --tb_title_pos up_left --tb_font_pos middle_left --tb_comments_pos down_left --tb_logo_pos right", "",
			"Text Block aligned to left", param_bool);
	AddHelp("tb", "tb_right", "", "Aligns text to the right. "
			"Same as --tb_title_pos up_right --tb_font_pos middle_right --tb_comments_pos down_right --tb_logo_pos left", "",
			"Text Block aligned to right", param_bool);

	AddFile("tb_bg_image", "", "png,jpg,jpeg,bmp,webp", false, "tb", "Set the background image for the text header. "
			"if the image is smaller than the space to be occupied, it is repeated, if it is larger, it is cropped.",
			"Text Block Background Image");
	AddStr("tb_bg_image_halign", "center", "raw|right|center|left", true, "tb", "<haling>",
		   "Horizontal alignment of background image, can be right, center, left or a percentage. Default center.",
		   "Horizontal Align for Text Block Bacground Image");
	AddStr("tb_bg_image_valign", "middle", "raw|up|middle|down", true, "tb", "<valing>",
		   "Vertical alignment of background image, can be up, middle, down or a percentage. Default middle.",
		   "Vertical Align for Text Block Bacground Image");
	AddBool("tb_bg_image_scale", false, false, "tb", "Scale the background image before crop it. Default 0.",
			"Autoscale Text Block Background Image");
	AddBool("tb_bg_image_tile", true, false, "tb",
			"When the background image is smaller than the result it repeats it in tiles to fill the necessary space. Default 1.",
			"Tile Text Block Background Image");
	AddInt("tb_bg_image_blur", 0, "min=0", true, "tb", "<number>",
		   "Applies a blur filter to the background image. the higher the number the more blurred. Default 0 (disabled).",
		   "Blur Text block Background Image");
	AddInt("tb_bg_image_exposure", 0, "min=-300,max=300", true, "tb", "<number>",
		   "Applies an exposure filter to the background image. allows a number between -300 and 300, "
		   "negative numbers decrease the exposure making the image darker, positive numbers make the image lighter. "
		   "Default 0 (disabled).", "Exposure for Text block Background Image");
	AddInt("tb_bg_image_saturation", 100, "min=0,max=300", true, "tb", "<number>",
		   "Applies a saturation filter to the background image. Accepts a number between 0 and 300, "
		   "0 converts the image to black and white. Default 100 (disabled).", "Saturation for Text block Background Image");
	AddInt("tb_bg_image_opacity", 100, "min=0,max=100", true, "tb", "<number>",
		   "Percentage of transparency of the background image with respect to "
		   "the other backgrounds (solid color, gradient...). 100 is opaque, 0 totally transparent. Default 100",
		   "Opacity for Text block Background Image");
	AddStr("tb_bg_image_mask", "", "", false, "tb", "<color|gradient|file>",
		   "Mask applied to the background image to define the transparency of each part of the image. "
		   "It can be a solid color (the whole image has the same opacity), a gradient. Or an image file. "
		   "In this option, white means totally opaque and black means totally transparent.",
		   "Mask for Text block Background Image");

	AddFile("tb_logo", "", "png,jpg,jpeg,bmp,webp", false, "tb",
			"Put this logo in the header aligned to the right and rescaled to the header height.", "Text Block Logo");
	AddInt("tb_logo_height", 0, "min=0", false, "tb", "<px>",
		   "Force the logo to be this height in pixels, enlarge the entire text block if necessary.",
		   "Force Text Block Logo Height");
	AddStr("tb_logo_pos", "right", "left|right", false, "tb", "<left|right>",
		   "Position of the logo in the text block, it can be right or left. Default right.", "Text Block Logo Position");
	AddBool("tb_logo_text_overlay", false, false, "tb", "Allows the text to overlay the logo. Default 0.",
			"Text Block Logo Permit Overlay");
	AddColor("tb_logo_transparent_color", "", false, "tb", "Makes this color transparent.",
			 "Text Block Logo Transp. Color");
	AddInt("tb_logo_transparent_similarity", 1, "min=1,max=100", true, "tb", "<number>",
		   "Percentage of similarity with logo_transparent_color to consider a color transparent.",
		   "Text Block Logo Transp. Color Similarity");
	AddInt("tb_logo_transparent_blend", 0, "min=0,max=100", true, "tb", "<number>",
		   "Percentage of transparency for pixels that fall outside the similarity.",
		   "Text Block Logo Transp. Color Blend");

	AddFile("tb_cover_top_image", "", "png,jpg,jpeg,bmp,webp", false, "tb",
			"Displays this cover on top of the text block, automatically adjusts it to the width respecting the aspect ratio.",
			"Text Block Cover Top Image");

	AddBool("tb_video_info", false, false, "tb",
			"Force video info in header, when show subtitles info, video info is hidden. This option force to show it.",
			"Show Video Info");
	AddBool("tb_chapters_list", false, false, "tb",
			"Enables the display of the list of chapters in the video file, disabled only shows its number.", "Show Chapters List");
	AddBool("tb_hide_video_info", false, false, "tb", "Don't show video information in text header.", "Hide Video Info");
	AddBool("tb_hide_sub_info", false, false, "tb", "Don't show subtitle information in text header.",
			"Hide Subtitles Info");
	AddBool("tb_hide_vr_warning", false, false, "tb", "Don't show the warning when enable VR options.", "Hide VR Warning");
	AddBool("tb_hide_vr_text", false, false, "tb", "Don't show the VR format text.", "Hide VR Text");

	AddStr("tb_custom_subtitle",
		   "Format: %B1%%subtitle.format%%B0% Size: %B1%%human_size(subtitle.size)%%B0%$ Language: %B1%$%subtitle.language%$%B0%$"
		   "%N%Lines: %B1%%subtitle.lines%%B0% Length, Total: %B1%%subtitle.total_length%%B0% Average: %B1%%subtitle.avg_lenght%%B0%"
		   "%N%Duration, Video: %B1%%time(format.duration)%%B0% Subtitles: %B1%%time(subtitle.total_duration)%%B0%"
		   " Average Line: %B1%%subtitle.avg_duration_ms%ms%B0%", "", false, "tb", "<text>",
		   "Personalize the text displayed when the input is a subtitle.", "Custom Subtitles Text");

	AddStr("tb_custom_header",
		   "Duration: %B1%%time(format.duration)%%B0% "
		   "Size: %B1%%human_size(format.size)% (%numsep(format.size)% Bytes)%B0%"
		   "$ Bitrate: %B1%$%human_bitrate(format.bit_rate)%$%B0%$"
		   " Container: %B1%%format.format_long_name%%B0%", "", false, "tb", "<text>",
		   "Personalize the text when the input is a video, only the basic part.", "Custom Video Header Text");

	AddStr("tb_custom_vr",
		   "$VR Format: %B1%%replace(vr.format,equirect=Equirectangular|fisheye=Fisheye)%%B0% "
		   "FOV: %B1%$%vr.fov%$°%B0% "
		   "%replace(vr.mode,sbs=3D: |tb=3D: |2d=)%%B1%%replace(vr.mode,sbs=Side by side|tb=Top bottom|2d=2D)%%B0%$",
		   "", false, "tb", "<text>", "Personalize VR format description.", "Custom VR Header Text");

	AddStr("tb_custom_video_stream",
		   "$$%if(streams.multiple_same_type,=,1,Stream,)%$ %index% $"
		   "Video: %B1%%width%x%height%$ $%human_pixformat(pix_fmt)%$ bits$%if(HDR,=,1, HDR)% "
		   "$$%if(ar_display,!=,ar_resolution,display_aspect_ratio)%$ $%human_framerate(r_frame_rate)% fps"
		   "$ $%if(r_frame_rate,!=,avg_frame_rate,VFR,)%$$$ $%human_bitrate(bit_rate)%$$ %codec_long_name%%B0%",
		   "", false, "tb", "<text>", "Personalize video stream text lines.", "Custom Video Stream Text");

	AddStr("tb_custom_audio_stream",
		   "$$%if(streams.multiple_same_type,=,1,Stream,)%$ %index% $"
		   "Audio: %B1%$$%language_shortname(tags.language)%$$$ $%capitalize(channel_layout)%$$"
		   "$ $%human_sample_rate(sample_rate)%$$$ $%human_bitrate(bit_rate)%$$ %codec_long_name%%B0%", "", false, "tb", "<text>",
		   "Personalize audio stream text lines.", "Custom Audio Stream Text");

	AddStr("tb_custom_subtitle_stream",
		   "$$%if(streams.multiple_same_type,=,1,Stream,)%$ %index% $"
		   "Subtitle: %B1%$$%capitalize(tags.title)%$ - $$$%language_shortname(tags.language)%$ $%codec_long_name%%B0%"
		   "$ $%if(subtitle_visible,=,1,(Currently Visible),)%$$", "", false,
		   "tb", "<text>", "Personalize subtitles stream text lines.", "Custom Suntitle Stream Text");

	AddStr("tb_custom_attachment_stream",
		   "$$%if(streams.multiple_same_type,=,1,Stream,)%$ %index% $"
		   "Attachment: %B1%%tags.filename%$ $%human_size(extradata_size)%$$$ ($%numsep(extradata_size)%$ Bytes)$"
		   " %tags.mimetype%%B0%", "", false, "tb", "<text>", "Personalize attachments text lines.",
		   "Custom Attachment Stream Text");

	AddStr("tb_custom_chapters", "$Chapters: %B1%$%chapters.count%$%B0%$", "", false, "tb", "<text>",
		   "Personalize the chapters count text.", "Custom Chapters Text");

	AddStr("tb_custom_chapters_list", "Chapter %B0%%time(start_time)%: %B1%%tags.title%%B0%", "", false, "tb", "<text>",
		   "Personalize the list of chapters text lines.", "Custom Chapters List Text");

	AddStr("tb_custom_images_header",
		   "Images: %B1%%stats.count% Files%B0% Total Size: %B1%%human_size(stats.size)%%B0% Avg. File Size: %B1%%human_size(stats.avg_file_size)%%B0%",
		   "", false, "tb", "<text>",
		   "Personalize the text when the input is a image folder, only header.", "Custom Images Header Text");
	AddStr("tb_custom_images_resolutions",
		   "%count% Files and %human_size(size)% at %B1%%name%%B0%",
		   "", false, "tb", "<text>",
		   "Personalize the text of images resolution list.", "Custom Images Resolution Text");
	AddStr("tb_custom_images_formats",
		   "%B1%%upper(name)%%B0%: %count% Files and %human_size(size)%",
		   "", false, "tb", "<text>",
		   "Personalize the text of images format list.", "Custom Images Format Text");

	HelpPrefixText.insert("out", "Output Options:\n\n");

	HelpPrefixText.insert("as", "Aspect Options:\n\n");

	HelpPrefixText.insert("ad", "Advanced Options:\n\n");

	HelpPrefixText.insert("ft", "\nFrame Time Options:\n"
						  "This options controls the position and aspect of the time inside each frame.\n\n");

	AddBool("ft_hide", false, false, "ft", "Hide the time of frames.", "Hide Frame Time");
	AddBool("ft_overlay", true, false, "ft",
			"Prints the time of frames on top of the image.", "Frame Time Overlay");
	AddFont("ft_font", FontMono, false, "ft", "Font for the time of frames.",
			"Frame Time Font");
	AddColor("ft_color", "white", false, "ft",
			 "Color of text for the time of frames, RRGGBBAA format or color name. Default white.", "Frame Time Font Color");
	AddInt("ft_font_border", 0, "min=0", false, "ft", "<px>",
		   "Set the width in pixels of the border around font. Default: 0.", "Frame Time Font Border");
	AddColor("ft_font_border_color", "black", false, "ft",
			 "Set the color of the border around the font. Default: black.", "Frame Time Font Border Color");
	AddColor("ft_shadow_color", "black", false, "ft",
			 "Set the color of the shadow of the font. Default: black.", "Frame Time Shadow Color");
	AddInt("ft_shadow_x", 0, "min=0", false, "ft", "<px>",
		   "Set the offset on the horizontal axis of the shadow with respect to the font. Default: 0.",
		   "Frame Time Shadow Horizontal Displacement");
	AddInt("ft_shadow_y", 0, "min=0", false, "ft", "<px>",
		   "Set the offset on the vertical axis of the shadow with respect to the font. Default: 0.",
		   "Frame Time Shadow Vertical Displacement");
	AddColor("ft_bg", "00000090", false, "ft", "Background color for the time of frames. Default 00000090.",
			 "Frame Time Background Color");
	AddInt("ft_size", 12, "min=4", false, "ft", "<px>", "Text size for the time of frames. Default 12.",
		   "Frame Time Font Size");
	AddInt("ft_border", 2, "min=0", false, "ft", "<px>", "Border in pixels for the time of frames. Default 2.",
		   "Frame Time Border");
	AddInt("ft_margin", 0, "min=0", false, "ft", "<px>", "Pixel spacing of the edges of the screen. Default 0.",
		   "Frame Time Margin");
	AddStr("ft_pos", "up_right", "up_left|up_right|down_left|down_right|up_center|down_center|middle_left|middle_right",
		   true, "ft", "<pos>", "Time of frames position up_left, up_right, down_left, down_right, "
		   "up_center, down_center, middle_left, middle_right.", "Frame Time Position");
	AddStr("ft_vertical", "disabled", "disabled|upward|downward", true, "ft", "<direction>",
		   "Print the time of frames in vertical position, it can be: disabled, upward, downward. Default disabled.",
		   "Frame Time Vertical Direcction");
	AddStr("ft_mask_layer_pos", "before", "before|after|inside",
		   true, "ft", "<pos>", "Controls where to print the frame time with respect to applying the image mask."
		   "Only has effect when the --mask is configured. Default before."
		   "before: Before applying the mask."
		   "after: After applying the mask."
		   "inside: Prints it on the mask.", "Frame Time Mask Layer Position");
	AddStr("ft_format", "", "raw|short|seconds|miliseconds|HH:MM:SS.mmm|HH:MM:SS|MM:SS", false, "ft", "<format>",
		   "Timestamp format, uses the same format characters as the C++ strftime function "
		   "(https://cplusplus.com/reference/ctime/strftime/) Special formats: "" is the HH:MM:SS.mmm format "
		   "\"short\" is HH:MM:SS or MM:SS format, depending of the length. "
		   "It can also be a string in the strftime format like %T or %H:%M:%S.", "Frame Time Format");
	AddStr("ft_custom", "", "", false, "ft", "<text>",
		   "Custom filter options for the time of frames. (https://ffmpeg.org/ffmpeg-filters.html#drawtext-1)",
		   "Custom Frame Time Parameters");

	HelpPrefixText.insert("vr", "\nVR Options:\n"
						  "This options allow you to correct the image of a VR video so that its content can be better appreciated.\n\n");

	AddBool("vr", false, false, "vr", "Enables the VR correction filter. (https://ffmpeg.org/ffmpeg-filters.html#v360)",
			"Enable VR");
	AddBool("vr_auto_enable", false, false, "vr",
			"Automatically activates the VR options when it detects a video with the correct name.", "Autoenable VR");
	AddBool("vr_disable_autodetection", false, false, "vr",
			"Disables VR video detection completely.", "Disable VR Autodetection");
	AddBool("vr_detect_ar", false, false, "vr",
			"Detects the vr videos based on aspect ratio.", "VR Detection AR");
	AddStr("vr_config", "", "", false, "vr", "<name|file>", "Load this configuration file when detect a VR input file",
		   "VR Config");
	AddBool("vr_only_crop", true, false, "vr",
			"If set to 1 it cuts only one of the eyes of the video, and deactivates the distortion correction.",
			"VR Only Discart One Eye");
	AddStr("vr_in", "hequirect", "hequirect|equirect|fisheye", false, "vr", "<format>", "Input format. Default hequirect",
		   "VR Input Format");
	AddStr("vr_out", "flat", "flat|hequirect|equirect|fisheye", false, "vr", "<format>", "Output format. Default flat.",
		   "VR Output Format");
	AddStr("vr_in_stereo", "sbs", "sbs|tb|2d", false, "vr", "<stereo>", "Input stereo mode, can be: sbs(default), tb, 2d.",
		   "VR Input Stereo Mode");
	AddStr("vr_out_stereo", "2d", "sbs|tb|2d", false, "vr", "<stereo>",
		   "Output stereo mode, Same options than input. Default 2d.", "VR Output Stereo Mode");
	AddInt("vr_ih_fov", 180, "min=0,max=360", false, "vr", "<degrees>",
		   "Set the input horizontal field of view. Default 180.", "VR Input Horizontal FOV");
	AddInt("vr_iv_fov", 180, "min=0,max=360", false, "vr", "<degrees>",
		   "Set the input vertical field of view. Default 180.", "VR Input Vertical FOV");
	AddInt("vr_d_fov", 105, "min=20,max=180", false, "vr", "<degrees>",
		   "Set the output diagonal field of view. Default 105.", "VR Output Diagonal FOV");
	AddInt("vr_h_fov", 0, "min=0,max=360", false, "vr", "<degrees>", "Set the output horizontal field of view. Default 0.",
		   "VR Output Horizontal FOV");
	AddInt("vr_v_fov", 0, "min=0,max=360", false, "vr", "<degrees>", "Set the output vertical field of view. Default 0.",
		   "VR Output Vertical FOV");
	AddInt("vr_yaw", 0, "min=-180,max=180", false, "vr", "<degrees>",
		   "Moves the camera to the right(positive value) and left(negative value).", "VR Camera Yaw");
	AddInt("vr_pitch", 0, "min=-180,max=180", false, "vr", "<degrees>",
		   "moves the camera upward(positive value) and downward(negative value).", "VR Camera Pitch");
	AddInt("vr_roll", 0, "min=-180,max=180", false, "vr", "<degrees>",
		   "rotates the camera counterclockwise(positive value) and clockwise(negative value)", "VR Camera Roll");
	AddStr("vr_w", "", "min=0", false, "vr", "<px>", "Set the output resolution width. Default the same as size.",
		   "VR Output Horizontal Resolution");
	AddStr("vr_h", "", "min=0", false, "vr", "<px>", "Set the output resolution height. Default the same as size.",
		   "VR Output Vertical Resolution");
	AddStr("vr_interp", "lanczos", "nearest|linear|lagrange9|cubic|lanczos|spline16|gaussian|mitchell", false, "vr",
		   "<int_method>", "Interpolation method. Default lanczos.", "VR Interpolation");
	AddBool("vr_h_flip", false, false, "vr", "Flip the output video horizontally", "VR Flip Output");
	AddBool("vr_right_eye", false, false, "vr", "Use the right eye. By default use the left.", "VR Show Right Eye");
	AddStr("vr_aditional", "", "", false, "vr", "<options>", "Pass aditional options to v360 ffmpeg filter.",
		   "VR Custom Aditional Options");

	AddHelp("vr", "vr.format", "<format>",
			"Sets the variable to configure the format. It can be equirect or fisheye.",
			"", "Force VR Format", param_string);
	AddHelp("vr", "vr.mode", "<mode>",
			"Sets the variable to configure the 3d mode. It can be sbs, tb or 2d.",
			"", "Force VR Mode", param_string);
	AddHelp("vr", "vr.fov", "<FOV>",
			"Sets the variable to configure the FOV. Normally 180, 190 or 200.",
			"", "Force VR FOV", param_string);
	AddHelp("vr", "fisheye200", "",
			"For fisheye 200FOV video files. Same as --vr.format fisheye --vr.mode sbs --vr.fov 200",
			"", "VR Fisheye 200 Format", param_none);
	AddHelp("vr", "fisheye190", "",
			"For fisheye 190FOV video files. Same as --vr.format fisheye --vr.mode sbs --vr.fov 190",
			"", "VR Fisheye 190 Format", param_none);
	AddHelp("vr", "vr180", "",
			"For 180FOV video files. Same as --vr.format equirect --vr.mode sbs --vr.fov 180.",
			"", "VR 180 Format", param_none);
	AddHelp("vr", "vr360", "",
			"For 360FOV video files. Same as --vr.format equirect --vr.mode tb --vr.fov 360.",
			"", "VR 360 Format", param_none);

	HelpPrefixText.insert("rename", "\nRename Options:\n");

	AddBool("rename", false, false, "rename", "Enables video renaming.", "Enable Renaming");
	AddStr("rename_type", "print", "print|move|copy|symlink|hardlink|shortcut|link|fastcopy", false, "rename", "<type>",
		   "Operation on the file system that will be executed, Can be print, move, copy, symlink, hardlink, shortcut, link, fastcopy.",
		   "Rename Type");
	AddStr("rename_dest", "", "", false, "rename", "<text>", "Path to the output file for renaming. Use text substitution.",
		   "Rename Destination");
	AddBool("rename_overwrite", false, false, "rename",
			"When activated it overwrites the output if it already exists. By default it fails.", "Remane Overwrite");
	AddBool("rename_mkdir", false, false, "rename",
			"When activated it creates all the intermediate folders to reach the output file. By default it fails if a folder that does not exist is used.",
			"Rename Create Directories");

	HelpPrefixText.insert("images", "\nImages Options:\n"
						  "Options for when the program uses a folder of images as input.\n\n");

	AddStr("images_search_filter", "*.jpeg|*.jpg|*.gif|*.png|*.apng|*.jp2|*.jpx|*.jxl|*.webp|*.heif|*.avif|*.tiff|*.bmp",
		   "", true, "images", "<extensions>", "List of filters separated by | to search for image files.",
		   "Images Search Filter");
	AddBool("images_recursive", false, false, "images",
			"When activated, the program scans all subdirectories.", "Images Recursive");
	AddStr("images_select", "range", "range|random_range|random_ordered|random",
		   true, "images", "<select>",
		   "Method for selecting the images to show, can be range, random_range, random_ordered, random. Default range.",
		   "Images Select");
	AddBool("images_format_stats", true, false, "images",
			"When activated, the program scans all images for format statistics.", "Images Format Statistics");
	AddBool("images_resolution_stats", false, false, "images",
			"When activated, the program scans all images for resolution statistics.", "Images Resolution Statistics");
	AddStr("images_title", "%dir.name%", "", false, "images", "<text>",
		   "Set the title, default use the forder name, can use text substitution.", "Images Text Block Title");

	AddBool("images_text", true, false, "images",
			"Displays the individual text information of the selected images.", "Images Show Text");
	AddStr("images_custom_text", "%file.filename%", "", false, "images", "<text>",
		   "Set the text of the selected images, can use text substitution.", "Images Custom Text");
	AddInt("images_max_text", 80, "min=0", false, "images", "<number>",
		   "Maximun caracters of selected images text. Default: 0.", "Images Max Text");
	AddBool("images_text_overlay", false, false, "images",
			"Prints the text on top of the image.", "Images Text Overlay");
	AddBool("images_text_padding", false, false, "images",
			"Prints text aligned with respect to the additional border added to the image.", "Images Text Padding");

	AddInt("images_ar_selection", 25, "min=0,max=100", false, "images", "<%>",
		   "Controls when images should be adjusted to an aspect ratio of 1:1. "
		   "0: always uses the most commonly used aspect ratio, 100: always uses 1:1."
		   "25: apply 1:1 when there are 25% or more images with different layout (vertical/horizontal). Default: 25.",
		   "Images Aspect Ratio Selection");

	AddFont("images_font", FontMono, false, "images", "Font for the text of selected images.",
			"Images Text Font");
	AddColor("images_color", "white", false, "images",
			 "Color of text of selected images, RRGGBBAA format or color name. Default white.", "Images Font Color Text");
	AddInt("images_font_border", 0, "min=0", false, "images", "<px>",
		   "Set the width in pixels of the border around font. Default: 0.", "Images Font Border");
	AddColor("images_font_border_color", "black", false, "images",
			 "Set the color of the border around the font. Default: black.", "Images Font Border Color");
	AddColor("images_shadow_color", "black", false, "images",
			 "Set the color of the shadow of the font. Default: black.", "Images Font Shadow Color");
	AddInt("images_shadow_x", 0, "min=0", false, "images", "<px>",
		   "Set the offset on the horizontal axis of the shadow with respect to the font. Default: 0.",
		   "Images Font Shadow Horizontal Displacement");
	AddInt("images_shadow_y", 0, "min=0", false, "images", "<px>",
		   "Set the offset on the vertical axis of the shadow with respect to the font. Default: 0.",
		   "Images Font Shadow Vertical Displacement");
	AddColor("images_bg", "00000090", false, "images", "Background color of selected images. Default 00000090.",
			 "Images Text Background Color");
	AddInt("images_size", 12, "min=4", false, "images", "<px>", "Text size of selected images. Default 12.",
		   "Images Font Size");
	AddInt("images_border", 2, "min=0", false, "images", "<px>", "Border in pixels of selected images. Default 2.",
		   "Images Text Border");
	AddInt("images_margin", 0, "min=0", false, "images", "<px>", "Pixel spacing of the edges of the screen. Default 0.",
		   "Images Text Margin");
	AddStr("images_pos", "down_center",
		   "up_left|up_right|down_left|down_right|up_center|down_center|middle_left|middle_right",
		   true, "images", "<pos>", "Text of selected images position up_left, up_right, down_left, down_right, "
		   "up_center, down_center, middle_left, middle_right.", "Images Text Position");
	AddStr("images_vertical", "disabled", "disabled|upward|downward", true, "images", "<direction>",
		   "Print the text of selected images in vertical position, it can be: disabled, upward, downward. Default disabled.",
		   "Images Vertical Direcction");
	AddColor("images_fill_color", "grid_border", false, "images",
			 "Color to fill the enlarged images. "
			 "Expecial values: grid_border to use the same color as the grid (default) and image_fill to use a blured and altered version of the image.",
			 "Images Fill Color");
	AddInt("images_fill_blur", 20, "min=0,max=100", false, "images", "<number>",
		   "Applies a blur filter to the fill area. the higher the number the more blurred. Only when images_fill_color = image_fill. Default 20.",
		   "Images Fill Blur");
	AddInt("images_fill_exposure", 0, "min=-300,max=300", false, "images", "<number>",
		   "Applies an exposure filter to the fill area. Only when images_fill_color = image_fill."
		   "allows a number between -300 and 300, negative numbers decrease the exposure making the image darker, "
		   "positive numbers make the image lighter. Default 0 (disabled).",
		   "Images Fill Exposure");
	AddInt("images_fill_saturation", 100, "min=0,max=300", false, "images", "<number>",
		   "Applies a saturation filter to the fill area. Only when images_fill_color = image_fill."
		   "Accepts a number between 0 and 300, 0 converts the image to black and white. Default 100 (disabled).",
		   "Images Fill Saturation");
	AddStr("images_fill_custom", "", "", true, "images", "<filter>",
		   "Custom ffmpeg filter to the fill area.",
		   "Images Fill Custom");

	AddStr("watermark", "", "", false, "wm", "<file>", "Set watermark image for the entire contact sheet.",
		   "Watermark Image");
	AddStr("watermark_halign", "center", "raw|right|center|left", true, "wm", "<haling>",
		   "Horizontal alignment of watermark image, can be right, center, left or a percentage.",
		   "Horizontal Align for Watermark Image");
	AddStr("watermark_valign", "middle", "raw|up|middle|down", true, "wm", "<valing>",
		   "Vertical alignment of watermark image, can be up, middle, down or a percentage.",
		   "Vertical Align for Watermark Image");
	AddBool("watermark_scale", false, false, "wm", "Scale the watermark image before crop it.",
			"Autoscale Watermark Image");
	AddBool("watermark_tile", true, false, "wm", "Repeat the watermark image as many times as necessary to fill the space.",
			"Tile Watermark Image");
	AddInt("watermark_blur", 0, "min=0", true, "wm", "<number>",
		   "Applies a blur filter to the watermark image. the higher the number the more blurred. Default 0 (disabled).",
		   "Blur Watermark Image");
	AddInt("watermark_exposure", 0, "min=-300,max=300", true, "wm", "<number>",
		   "Applies an exposure filter to the watermark image. allows a number between -300 and 300, "
		   "negative numbers decrease the exposure making the image darker, positive numbers make the image lighter. "
		   "Default 0 (disabled).", "Exposure for Watermark Image");
	AddInt("watermark_saturation", 100, "min=0,max=300", true, "wm", "<number>",
		   "Applies a saturation filter to the watermark image. Accepts a number between 0 and 300, "
		   "0 converts the image to black and white. Default 100 (disabled).", "Saturation for Watermark Image");
	AddInt("watermark_opacity", 100, "min=0,max=100", true, "wm", "<number>",
		   "Percentage of transparency of the watermark image. "
		   "100 is opaque, 0 totally transparent. Default 100",
		   "Opacity for Watermark Image");
	AddColor("watermark_transparent_color", "", false, "wm", "Makes this color transparent in the watermark.",
			 "Watermark Transp. Color");
	AddInt("watermark_transparent_similarity", 1, "min=1,max=100", true, "wm", "<number>",
		   "Percentage of similarity with watermark_transparent_color to consider a color transparent.",
		   "Watermark Transp. Color Similarity");
	AddInt("watermark_transparent_blend", 0, "min=0,max=100", true, "wm", "<number>",
		   "Percentage of transparency for pixels that fall outside the similarity.",
		   "Watermark Transp. Color Blend");

	AddShortParam("l", "layout");
	AddShortParam("s", "size");
	AddShortParam("p", "process");

	HelpPrefixName.insert("", "General");
	HelpPrefixName.insert("out", "Output");
	HelpPrefixName.insert("as", "Aspect");
	HelpPrefixName.insert("ad", "Advanced");
	HelpPrefixName.insert("sub", "Subtitles");
	HelpPrefixName.insert("vr", "Virtual Reality");
	HelpPrefixName.insert("tb", "Text Block");
	HelpPrefixName.insert("ft", "Frame Time");
	HelpPrefixName.insert("rename", "Rename");
	HelpPrefixName.insert("images", "Images");
	HelpPrefixName.insert("wm", "Watermark");
	Settings = nullptr;
}

void ParamsSettings::AddStr(QString Param, QString DefaultVar, QString ValidValues, bool Required, QString Prefix,
							QString Type, QString Text, QString Name) {
	VarList.insert(Param, {DefaultVar, DefaultVar, false, Required, param_string, false, 0, "", "", ValidValues});
	AddHelp(Prefix, Param, Type, Text, "", Name, param_string);
}

void ParamsSettings::AddFile(QString Param, QString DefaultVar, QString ValidExtensions, bool Required, QString Prefix,
							 QString Text,
							 QString Name) {
	VarList.insert(Param, {DefaultVar, DefaultVar, false, Required, param_file, false, 0, "", "", ValidExtensions});
	AddHelp(Prefix, Param, "<file>", Text, "", Name, param_file);
}

void ParamsSettings::AddDir(QString Param, QString DefaultVar, bool Required, QString Prefix, QString Text,
							QString Name) {
	VarList.insert(Param, {DefaultVar, DefaultVar, false, Required, param_dir, false, 0, "", "", ""});
	AddHelp(Prefix, Param, "<dir>", Text, "", Name, param_dir);
}

void ParamsSettings::AddColor(QString Param, QString DefaultVar, bool Required, QString Prefix, QString Text,
							  QString Name) {
	VarList.insert(Param, {DefaultVar, DefaultVar, false, Required, param_color, false, 0, "", "", ""});
	AddHelp(Prefix, Param, "<color>", Text, "", Name, param_color);
}

void ParamsSettings::AddFont(QString Param, QString DefaultVar, bool Required, QString Prefix, QString Text,
							 QString Name) {
	VarList.insert(Param, {DefaultVar, DefaultVar, false, Required, param_font, false, 0, "", "", ""});
	AddHelp(Prefix, Param, "<font>", Text, "", Name, param_font);
}

void ParamsSettings::AddLayout(QString Param, QString DefaultVar, bool Required, QString Prefix, QString Text,
							   QString Name) {
	VarList.insert(Param, {DefaultVar, DefaultVar, false, Required, param_layout, false, 0, "", "", ""});
	AddHelp(Prefix, Param, "<CxR>", Text, "", Name, param_layout);
}

void ParamsSettings::AddInt(QString Param, int DefaultVar, QString ValidValues, bool Required, QString Prefix,
							QString Type, QString Text,
							QString Name) {
	QString Val = QString::number(DefaultVar);
	VarList.insert(Param, {Val, Val, false, Required, param_int, false, 0, "", "", ValidValues});
	AddHelp(Prefix, Param, Type, Text, "", Name, param_int);
}

//void ParamsSettings::AddDouble(QString Param, double DefaultVar, QString ValidValues, bool Required, QString Prefix,
//							   QString Type,
//							   QString Text, QString Name) {
//	QString Val = QString::number(DefaultVar, 'f');
//	VarList.insert(Param, {Val, Val, false, Required, param_double, false, 0, "", "", ValidValues});
//	AddHelp(Prefix, Param, Type, Text, "", Name, param_double);
//}

void ParamsSettings::AddBool(QString Param, bool DefaultVar, bool Required, QString Prefix, QString Text,
							 QString Name) {
	QString Val;
	if (DefaultVar) Val = "1";
	else Val = "0";
	VarList.insert(Param, {Val, Val, false, Required, param_bool, false, 0, "", "", ""});
	AddHelp(Prefix, Param, "[0|1]", Text, "", Name, param_bool);
}

void ParamsSettings::AddUserDefined(QString Param, QString Name, int Num) {
	VarList.insert(Param, {"", "", false, false, param_string, true, Num, Name, "", ""});
	UserDefinedTranslator.insert(Name, Param);
}

void ParamsSettings::AddHelp(QString Prefix, QString Param, QString Type, QString Text,
							 QString ShortParam, QString Name, param_type DataType) {
	if (Text == "") return;
	HelpList.append({Prefix, Param, ShortParam, Type, Text, Name, DataType});
}

void ParamsSettings::AddShortParam(QString Short, QString Param) {
	ShortParams.insert("-" + Short, "--" + Param);
	for (int i = 0; i < HelpList.count(); ++i) {
		help_t Help = HelpList[i];
		if (Help.Param != Param) continue;
		Help.ShortParam = Short;
		HelpList.replace(i, Help);
		break;
	}
}

QString ParamsSettings::Value(QString Param) {
	if (!VarList.contains(Param)) return "";
	params_t Var = VarList.value(Param);
	if (Var.Required && Var.Value == "")
		return Var.DefaultValue;
	else
		return Var.Value;
}

bool ParamsSettings::Bool(QString Param) {
	if (VarList.contains(Param)) {
		params_t Var = VarList.value(Param);
		if (Var.Type == param_bool)
			return Var.Value != "0";
	}
	return false;
}

int ParamsSettings::Int(QString Param) {
	if (VarList.contains(Param)) {
		params_t Var = VarList.value(Param);
		if (Var.Type == param_int)
			return Var.Value.toInt();
	}
	return 0;
}

//double ParamsSettings::Double(QString Param) {
//	if (VarList.contains(Param)) {
//		params_t Var = VarList.value(Param);
//		if (Var.Type == param_double)
//			return Var.Value.toDouble();
//	}
//	return 0;
//}

bool ParamsSettings::NotEmpty(QString Param) {
	if (VarList.contains(Param))
		return VarList.value(Param).Value != "";
	return false;
}

bool ParamsSettings::Exists(QString Param) {
	return VarList.contains(Param);
}

void ParamsSettings::setValue(QString Param, QString Value, bool DetectFunction) {
	if (!VarList.contains(Param)) return;
	params_t Var = VarList.value(Param);
	Var.Changed = true;
	if (DetectFunction && TextSustitution::DetectFunction(Value)) {
		Var.Function = Value;
	} else {
		Var.Value = Value;
		if (DetectFunction) Var.Function = "";
	}
	VarList[Param] = Var;
	PrintDebug("\t--" + Param + " = " + Value);
}

void ParamsSettings::setValueInt(QString Param, int Value) {
	setValue(Param, QString::number(Value));
}

void ParamsSettings::setValueDouble(QString Param, double Value) {
	setValue(Param, QString::number(Value));
}

void ParamsSettings::setValueBool(QString Param, bool Value) {
	setValue(Param, QString::number(Value));
}

QStringList ParamsSettings::Params() {
	return VarList.keys();
}

ParamsSettings::param_type ParamsSettings::Type(QString Param) {
	return VarList.value(Param).Type;
}

QString ParamsSettings::Function(QString Param) {
	if (!VarList.contains(Param)) return "";
	params_t Var = VarList.value(Param);
	return Var.Function;
}

bool ParamsSettings::IsUserDefined(QString Param) {
	if (!VarList.contains(Param)) return false;
	params_t Var = VarList.value(Param);
	return Var.UserDefined;
}

void ParamsSettings::SetConfig(QString Name) {
	if (Settings != nullptr) Settings->deleteLater();
	QString File = ConfigPath + QDir::separator() + Name + "." + ConfigExt;
	Settings = new QSettings(File, QSettings::IniFormat, this);
	PrintDebug("Using config: " + File);
}

void ParamsSettings::SetConfigFile(QString File) {
	if (Settings != nullptr) Settings->deleteLater();
	Settings = new QSettings(File, QSettings::IniFormat, this);
	PrintDebug("Using config: " + File);
}

bool ParamsSettings::ConfigExists(QString Name) {
	QDir ConfigDir(ConfigPath);
	if (ConfigDir.exists()) {
		QFileInfoList List = ConfigDir.entryInfoList({"*." + ConfigExt}, QDir::Files | QDir::NoDot | QDir::NoDotDot,
							 QDir::Time | QDir::Reversed);
		for (int i = 0; i < List.count(); ++i) {
			QFileInfo file = List.value(i);
			if (Name == file.baseName()) return true;
		}
	}
	return false;
}

QStringList ParamsSettings::ConfigList() {
	QStringList Result;
	QDir ConfigDir(ConfigPath);
	if (ConfigDir.exists()) {
		QFileInfoList List = ConfigDir.entryInfoList({"*." + ConfigExt}, QDir::Files | QDir::NoDot | QDir::NoDotDot,
							 QDir::Time | QDir::Reversed);
		for (int i = 0; i < List.count(); ++i) {
			QFileInfo fileinfo = List.value(i);
			if (fileinfo.baseName() == "config") continue;
			QSettings *File = new QSettings(fileinfo.absoluteFilePath(), QSettings::IniFormat, this);
			Result.append(fileinfo.baseName() + ": " + File->value("config_desc", "").toString());
			File->deleteLater();
		}
	}
	Result.sort();
	return Result;
}

void ParamsSettings::ReadFromConfigFile() {
	if (Settings == nullptr) return;
	QStringList SettingsKeys = Settings->allKeys();
	for (int i = 0; i < SettingsKeys.size(); ++i) {
		QString Key = SettingsKeys.at(i);
		QString NewKey = Key;
		if (Key.startsWith("VAR_")) {
			QRegularExpressionMatch match = RegexpUserDefinedVar.match(Key);
			int num;
			QString Varname = "";
			if (match.hasMatch()) {
				if (match.captured(4) != "") {
					num = GetUserDefinedVarCount();
					NewKey = NewUserDefinedOptionName(context_global, match.captured(4), num);
					Varname = match.captured(4);
				} else {
					num = match.captured(1).toInt();
					if (match.captured(1).length() < 3)
						NewKey.replace(match.captured(1), match.captured(1).rightJustified(3, '0'));
					if (UserDefinedVarOrderCount < num) UserDefinedVarOrderCount = num;
					Varname = match.captured(3);
				}
				AddUserDefined(NewKey, Varname, num);
			}
		}
		if (!VarList.contains(NewKey)) continue;
		params_t Var = VarList.value(NewKey);
		if (TextSustitution::DetectFunction(Settings->value(Key).toString())) {
			Var.Function = Settings->value(Key).toString();
			Var.Value = Var.DefaultValue;
		} else {
			switch (Var.Type) {
				case param_string:
				case param_file:
				case param_color:
				case param_font:
				case param_dir:
				case param_layout:
					Var.Value = Settings->value(Key).toString();
					break;
				case param_int:
					Var.Value = QString::number(Settings->value(Key).toInt());
					break;
				case param_bool:
					if (Settings->value(Key).toBool())
						Var.Value = "1";
					else
						Var.Value = "0";
					break;
				//				case param_double:
				//					Var.Value = QString::number(Settings->value(Key).toDouble(), 'f');
				//					break;
				case param_shortcut:
				case param_none:
					break;
			}
		}

		if (NewKey != Key) {
			Settings->remove(Key);
			Settings->setValue(NewKey, Var.Value);
		}
		Var.Changed = false;
		VarList[NewKey] = Var;
		if (Var.Function != "")
			PrintDebug("\t" + NewKey + " = " + Var.Function);
		else
			PrintDebug("\t" + NewKey + " = " + Var.Value);
	}

	//Convert old comments, remove in future versions
	int comments_size = Settings->beginReadArray("TB_COMMENTS");
	if (comments_size > 0) {
		QString Comments = "";
		for (int i = 0; i < comments_size; i++) {
			Settings->setArrayIndex(i);
			QString Title = Settings->value("title", "").toString();
			QString Text = Settings->value("text", "").toString();
			QString Comment = "";
			if (Title != "") Comment += "%#%" + Title + ":%B1% ";
			if (Text != "") Comment += Text + "%B0%";
			if (Comment != "") {
				if (Comments != "") Comments += "%N%";
				Comments += Comment;
			}
		}
		Settings->setValue("tb_comments", Comments);
		Settings->remove("TB_COMMENTS");
		Settings->remove("tb_comment");
		Settings->remove("tb_comment2");
		Settings->remove("tb_comment_title");
		Settings->remove("tb_comment2_title");
		Settings->sync();
	}
	Settings->endArray();
}

void ParamsSettings::LoadConfig(QString Config) {
	if ((Config.endsWith(".conf") || Config.endsWith(".ini")) && QFile::exists(Config)) {
		SetConfigFile(Config);
	} else {
		SetConfig(Config);
	}
	ReadFromConfigFile();
}

void ParamsSettings::UpdateConfigFile(bool Sync, bool All) {
	QStringList Keys = VarList.keys();
	for (int i = 0; i < Keys.size(); ++i) {
		UpdateConfigFile(Keys.at(i), All);
	}
	if (Sync) Settings->sync();
}

void ParamsSettings::UpdateConfigFile(QString Param, bool All) {
	if (!VarList.contains(Param)) return;
	if (Settings->contains(Param) && VarList.value(Param).Changed == false) return;
	if (VarList.value(Param).Function != "") {
		Settings->setValue(Param, VarList.value(Param).Function);
	} else {
		if (!All && VarList.value(Param).Value == VarList.value(Param).DefaultValue) {
			Settings->remove(Param);
			return;
		}
		Settings->setValue(Param, VarList.value(Param).Value);
	}
}

void ParamsSettings::PrintParams(bool Changed, bool Default) {
	QHashIterator<QString, params_t> i(VarList);
	while (i.hasNext()) {
		i.next();
		if (Changed && !i.value().Changed) continue;
		if (!Default && i.value().Value == i.value().DefaultValue) continue;
		PrintInfo(i.key() + ": " + i.value().Value);
	}
}

bool ParamsSettings::ProcessParameter(int &Current, int Count, char *Params[]) {
	QString Param(Params[Current]);
	QString NextParam = "";
	bool hasNext = false;
	if (Current + 1 < Count) {
		NextParam = Params[Current + 1];
		hasNext = true;
	}
	bool usedNext = false;
	bool Result = ProcessParameter(Param, hasNext, NextParam, usedNext);
	if (usedNext) Current++;
	return Result;
}

bool ParamsSettings::ProcessParameter(QString &Param, bool HasNext, QString NextParam, bool &UsedNext) {
	UsedNext = false;
	if (ShortParams.contains(Param)) Param = ShortParams.value(Param);

	if (!Param.startsWith("--")) {
		//		PrintError("Parameter Error: " + Param);
		return false;
	}
	Param.remove(0, 2);
	if (!VarList.contains(Param)) {
		//		PrintError("Parameter Not Found: " + Param);
		return false;
	}
	params_t Var = VarList.value(Param);

	bool function = false;

	if (Var.Type == param_bool) {
		bool HasParam = false;
		QString NextValue = NextParam;
		if (HasNext) {
			if (TextSustitution::DetectFunction(NextParam)) {
				Var.Function = NextParam;
				NextValue = Var.Value;
				function = true;
			}
			if (NextValue == "1" || NextValue == "0") HasParam = true;
		}
		if (HasParam) {
			Var.Value = NextValue;
			UsedNext = true;
		} else {
			if (Var.DefaultValue == "0")
				Var.Value = "1";
			else
				Var.Value = "0";
		}
	} else {
		UsedNext = true;
		QString Value = NextParam;
		if (!HasNext) {
			PrintError("Parameter Incorect value: " + Param);
			return false;
		}
		if (TextSustitution::DetectFunction(Value)) {
			Var.Function = Value;
			Value = Var.DefaultValue;
			function = true;
		}
		if (Var.Required && Value == "") {
			PrintError("Parameter: " + Param + " can't be empty");
			return false;
		}

		if (Var.Type == param_int) {
			if (Value.startsWith("+")) {
				int Increment = Value.remove(0, 1).toInt();
				Value = QString::number(Var.Value.toInt() + Increment);
			}
		}

		Var.Value = Value;
	}
	Var.Changed = true;
	VarList[Param] = Var;
	if (function)
		PrintDebug("\t --" + Param + " = " + Var.Function);
	else
		PrintDebug("\t --" + Param + " = " + Var.Value);
	return true;
}

void ParamsSettings::UserDefinedVar(QString Param, context_type Context) {
	bool overwrite = false;
	QString VarName = Param.section("=", 0, 0);
	QString SaveName;
	if (VarName != "") {
		if (Param.contains("==")) overwrite = true;
		QString Value = Param.section("=", 1, -1, QString::SectionSkipEmpty);
		QStringList ConfigNames = UserDefinedTranslator.values(VarName);
		if (overwrite && !ConfigNames.isEmpty()) {
			ConfigNames.sort();
			SaveName = ConfigNames.first();
			PrintDebug("\t\t" + SaveName + ": Change Variable " + VarName + " = " + Value);
		} else {
			int NewID = GetUserDefinedVarCount();
			SaveName = NewUserDefinedOptionName(Context, VarName, NewID);
			AddUserDefined(SaveName, VarName, NewID);
			PrintDebug("\t\t" + SaveName + ": Add Variable " + VarName + " = " + Value);
		}
		params_t Var = VarList.value(SaveName);
		if (TextSustitution::DetectFunction(Value)) {
			Var.Function = Value;
		} else {
			Var.Value = Value;
			Var.Changed = true;
		}
		VarList[SaveName] = Var;
	}
}

void ParamsSettings::RemoveUserDefinedVar(QString SaveName) {
	if (!VarList.contains(SaveName)) return;
	params_t Var = VarList.value(SaveName);
	if (!Var.UserDefined) return;
	VarList.remove(SaveName);
	Settings->remove(SaveName);
}

int ParamsSettings::GetUserDefinedVarCount() {
	return ++UserDefinedVarOrderCount;
}

QString ParamsSettings::NewUserDefinedOptionName(context_type context, QString Name, int NewID) {
	if (NewID == -1) NewID = GetUserDefinedVarCount();
	return "VAR_" + QString::number(NewID).rightJustified(3, '0') + "_" + Context2Str(context) + Name;
}

bool ParamsSettings::GetVarAndContext(QString Name, QString &Var, context_type &Context) {
	QRegularExpressionMatch match = RegexpUserDefinedVar.match(Name);
	if (match.hasMatch()) {
		//		int num = match.captured(1).toInt();
		Var = match.captured(3);
		Context = Str2Context(match.captured(2));
		return true;
	}
	return false;
}

void ParamsSettings::printhelp(QString AppName, QString prefix) {
	int ConsoleWidth = 120;

	#ifdef Q_OS_WIN
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	int ret;
	ret = GetConsoleScreenBufferInfo(GetStdHandle( STD_OUTPUT_HANDLE ), &csbi);
	if (ret) ConsoleWidth = csbi.dwSize.X;
	#else
	struct winsize w;
	ioctl(0, TIOCGWINSZ, &w);
	ConsoleWidth = w.ws_col;
	#endif

	QString text;
	if (HelpPrefixText.contains(prefix))
		text = HelpPrefixText.value(prefix);

	fprintf(stdout, text.toUtf8().constData(), AppName.toUtf8().constData());

	int maxlengt = 0;
	int newlinelength = 0;
	QList<QString> List;
	QList<QString> TextList;
	for (int i = 0; i < HelpList.count(); ++i) {
		help_t helpline = HelpList.at(i);
		if (helpline.Prefix != prefix) continue;
		QString lineformat = "--" + helpline.Param;
		if (helpline.Type != "") lineformat += " " + helpline.Type;
		if (helpline.ShortParam != "") lineformat = "-" + helpline.ShortParam + ", " + lineformat;
		if (lineformat.length() > maxlengt) maxlengt = lineformat.length();
		List.append(lineformat);
		TextList.append(helpline.Text);
	}

	newlinelength = maxlengt + 2;

	for (int i = 0; i < TextList.count(); ++i) {
		QString Text = TextList.at(i);
		QStringList Words = Text.split(" ");
		int maxlinelength = ConsoleWidth - newlinelength;
		int linelength = 0;
		QString newtext = "";
		for (int i2 = 0; i2 < Words.count(); ++i2) {
			if (linelength + Words.at(i2).length() + 1 >= maxlinelength) {
				linelength = 0;
				newtext += "\n" + QString("").leftJustified(newlinelength);
			}
			if (newtext != "") {
				newtext += " ";
				linelength++;
			}
			newtext += Words.at(i2);
			linelength += Words.at(i2).length();
		}
		TextList[i] = newtext;
	}

	QString LineFormat = "  %-*s %s\n";

	for (int i = 0; i < List.count(); ++i) {
		fprintf(stdout, LineFormat.toUtf8().constData(), maxlengt, List.at(i).toUtf8().constData(),
				TextList.at(i).toUtf8().constData());
	}
	fprintf(stdout, "\n");
}

QList<ParamsSettings::help_t> ParamsSettings::SearchHelp(QString Search, bool OptionsOnly) {
	//	if (Search == "") return HelpList;
	QList<help_t> ResultParam;
	QList<help_t> ResultText;
	for (int i = 0; i < HelpList.count(); ++i) {
		help_t help = HelpList.at(i);
		if (help.Name == "") continue;
		if (OptionsOnly) {
			if (!VarList.contains(help.Param)) continue;
		}
		if (Search == "")
			ResultParam.append(help);
		else if (help.Param.contains(Search, Qt::CaseInsensitive) || help.Name.contains(Search, Qt::CaseInsensitive))
			ResultParam.append(help);
		else if (help.Text.contains(Search, Qt::CaseInsensitive))
			ResultText.append(help);
	}
	if (!ResultText.isEmpty()) ResultParam.append(ResultText);
	return ResultParam;
}

ParamsSettings::params_t ParamsSettings::GetVar(QString Param) {
	return VarList.value(Param);
}

void ParamsSettings::SetVar(QString Param, params_t &value) {
	VarList[Param] = value;
}

ParamsSettings::help_t ParamsSettings::GetHelp(QString Param) {
	for (int i = 0; i < HelpList.count(); ++i) {
		help_t help = HelpList.at(i);
		if (help.Param == Param)
			return help;
	}
	return help_t();
}

QString ParamsSettings::GetPrefixName(QString Prefix) {
	return HelpPrefixName.value(Prefix, "");
}

void ParamsSettings::DeleteConfig(QString Config) {
	QString File = QDir::toNativeSeparators(ConfigPath + QDir::separator() + Config + "." + ConfigExt);
	if (QFile::exists(File)) QFile::remove(File);
}

QStringList ParamsSettings::GetOutputFormat(QString &OUTPUT_FORMAT, QStringList &Audio, bool &INDEPENDENT_COMMAND,
		QString &OUTPUT_QUALITY) {
	if (NotEmpty("custom_format_ext")) {
		OUTPUT_AVAILABLE_FORMATS.insert("custom", {
			Value("custom_format_ext"),
			Value("custom_format_video"),
			Value("custom_format_default_quality"),
			false,
			Value("custom_format_audio")
		});
	}
	QStringList FORMAT_OPTIONS;
	if (OUTPUT_FORMAT == "") OUTPUT_FORMAT = Value("format");
	if (OUTPUT_FORMAT == "mkv") OUTPUT_FORMAT = "x264";

	format_definition_t CurrentFormatDefinition = OUTPUT_AVAILABLE_FORMATS.value(OUTPUT_FORMAT, {"", "", "", false, ""});
	if (CurrentFormatDefinition.Ext != "") {
		OUTPUT_FORMAT = CurrentFormatDefinition.Ext;
		INDEPENDENT_COMMAND = CurrentFormatDefinition.IndependentCommand;
		if (CurrentFormatDefinition.Options != "") {
			FORMAT_OPTIONS = CurrentFormatDefinition.Options.split(" ");
			if (Bool("enable_audio") && CurrentFormatDefinition.AudioOptions != "")
				Audio = CurrentFormatDefinition.AudioOptions.split(" ");
			if (CurrentFormatDefinition.DefaultQuality != "") {
				int index = FORMAT_OPTIONS.indexOf("{quality}");

				if (Int("output_quality") != -1)
					OUTPUT_QUALITY = Value("output_quality");
				else
					OUTPUT_QUALITY = CurrentFormatDefinition.DefaultQuality;

				if (index > -1)
					FORMAT_OPTIONS.replace(index, OUTPUT_QUALITY);
			}
		} else {
			FORMAT_OPTIONS.clear();
			Audio.clear();
		}
	} else {
		PrintError("Invalid output format: " + OUTPUT_FORMAT);
		return QStringList();
	}

	if (NotEmpty("pixel_format")) {
		FORMAT_OPTIONS.append("-pix_fmt");
		FORMAT_OPTIONS.append(Value("pixel_format"));
	}
	return FORMAT_OPTIONS;
}

void ParamsSettings::ReadBGAndGridBorderColor(QString &BG, QString &GridBorder) {
	BG = Value("tb_bg_color");
	GridBorder = Value("grid_border_color");
	if (NotEmpty("bg_image") || Value("bg_gradient") != "none") {
		BG = SetTransparentRGB(BG);
		if (GridBorder != "") GridBorder = SetTransparentRGB(GridBorder);
	}

	if (GridBorder == "") GridBorder = BG;
}

QString ParamsSettings::Context2Str(ParamsSettings::context_type Context) {
	switch (Context) {
		case context_global:
			return "";
		case context_stream:
			return "S_";
		case context_video:
			return "SV_";
		case context_audio:
			return "SA_";
		case context_subtitle:
			return "SS_";
		case context_attachment:
			return "SF_";
	}
	return "";
}

ParamsSettings::context_type ParamsSettings::Str2Context(QString Context) {
	if (Context == "") return context_global;
	if (Context == "S_") return context_stream;
	if (Context == "SV_") return context_video;
	if (Context == "SA_") return context_audio;
	if (Context == "SS_") return context_subtitle;
	if (Context == "SF_") return context_attachment;
	PrintError("Incorrect context \"" + Context + "\"");
	return context_global;
}

QString ParamsSettings::help_t::GetTip() {
	return "<b>--" + this->Param + "</b> " + this->Text;
}
