#ifndef IMAGES_H
#define IMAGES_H
#include <QString>
#include <QStringList>
#include <QMap>
#include "contactsheettextblock.h"
#include "textsustitution.h"
#include "paramssettings.h"
#include "ffmpeginstances.h"
#include "filters.h"

class Images {
	public:
		Images(QString imgdir, ParamsSettings *ps, TextSustitution *ts, FFmpegInstances *inst);
		void GenerateImageList();
		void AddFilter(QString Filter);
		void SetFilters(QStringList filters);
		bool SelectImages();
		bool StatsEnabled;
		bool ShowTB;
		bool NeedBG;
		bool NeedWatermark;
		QString OutputQuality;

		struct count_t {
			uint Count;
			uint Size;
		};

		struct statisticts_t {
			uint Count;
			uint Size;
			uint MaxFileSize;
			uint MinFileSize;
			QMap<QString, count_t> ResolutionsList;
			QMap<QString, count_t> FormatsList;
		};

		struct selected_images_t {
			QString File;
			QString Name;
			uint Width;
			uint Height;
			double AspectRatio;
			TextSustitution *ITS;
		};

		QMap<int, selected_images_t> SelectedImages;
		QList<int> SelectedOrder;
		QStringList FfmpegResizeImage(selected_images_t Image, QString Output);
		void CalculateOutputResolution();
		bool GenarateThumbnails();
		bool GenerateGrid();
		bool GenerateAnimation();
		bool GenetateStatsBlock();
		bool ApplyBG();
		bool ApplyWatermark();

		void SetOutput(QString File);

	protected:
		ParamsSettings *PS;
		TextSustitution *TS;
		FFmpegInstances *Inst;
		ContactSheetTextBlock *CTB;
		Filters *F;
		QString ImgDir;
		QString DirName;
		QString DirPath;
		QStringList ImgList;
		QStringList FilterList;
		int OutputW;
		int OutputH;
		QStringList OutputFormat;
		QString OutputFile;
		statisticts_t Stats;
		bool Recursive;
		QString NextStepInput;

		TextSustitution *ReadImageMetadata(QString Image, QString &Name, uint &Width, uint &Height, double &AspectRatio);
		QStringList GenerateImageListReculsive(QString Dir, bool Recursive);
};

#endif // IMAGES_H
