#include "contactsheettextblock.h"
#include "filters.h"

ContactSheetTextBlock::ContactSheetTextBlock(ParamsSettings *ps, TextSustitution *ts):
	CoverTopCalculatedHeight(-2),
	PS(ps),
	TS(ts) {

	TextBlockPosition = String2TextBlockPotion(PS->Value("tb"));
	TextBlockBackgroundHAling = String2HAling(PS->Value("tb_bg_image_halign"));
	TextBlockBackgroundVAling = String2VAling(PS->Value("tb_bg_image_valign"));
	BackgroundHAling = String2HAling(PS->Value("bg_image_halign"));
	BackgroundVAling = String2VAling(PS->Value("bg_image_valign"));

	if (PS->Value("tb_bg_gradient") != "none")
		TB.SetGradient(PS->Value("tb_bg_gradient"));

	Border = PS->Int("tb_border");
	TB.SetBorder(Border);

	if (PS->NotEmpty("tb_bg_image")) {
		TB.SetBGImage(PS->Value("tb_bg_image"),
					  TextBlockBackgroundHAling,
					  TextBlockBackgroundVAling,
					  PS->Bool("tb_bg_image_scale"),
					  PS->Bool("tb_bg_image_tile"),
					  PS->Int("tb_bg_image_blur"),
					  PS->Int("tb_bg_image_exposure"),
					  PS->Int("tb_bg_image_saturation"),
					  PS->Int("tb_bg_image_opacity"),
					  PS->Value("tb_bg_image_mask")
					 );
	}
	if (PS->NotEmpty("tb_fonts_dir")) {
		TB.SetFontDir(PS->Value("tb_fonts_dir"));
	}

	DefaultStyle = new text_sub_style_t("Default", nullptr, ps, "font");
	DefaultStyle->BorderStyle = border_outline_shadow;
	DefaultStyle->MarginL = Border;
	DefaultStyle->MarginR = Border;
	DefaultStyle->MarginV = Border;
	TB.AddStyle(*DefaultStyle);
	TitleStyle = new text_sub_style_t("Title", DefaultStyle, ps, "title");
	TB.AddStyle(*TitleStyle);
	CommentsStyle = new text_sub_style_t("Comments", DefaultStyle, PS, "comments");
	TB.AddStyle(*CommentsStyle);

	PS->ReadBGAndGridBorderColor(BGColor, GridBorderColor);
	TB.SetBGColor(BGColor);
}

ContactSheetTextBlock::~ContactSheetTextBlock() {}

ContactSheetTextBlock::text_block_positions ContactSheetTextBlock::String2TextBlockPotion(QString pos) {
	pos = pos.toLower();
	if (pos == "top") return text_top;
	if (pos == "bottom") return text_bottom;
	if (pos == "hidden") return text_hidden;
	return text_top;
}

bool ContactSheetTextBlock::IsHidden() {
	return TextBlockPosition == text_hidden;
}

void ContactSheetTextBlock::SetTitle(QString title) {
	Title = title;
}

bool ContactSheetTextBlock::GenerateVideoText(bool ShowSubText, bool VR) {
	CalculateDimensions();
	ApplyTitle();

	if (ShowSubText && !PS->Bool("tb_hide_sub_info")) {
		QString SubText = TS->ProcessText("", PS->Value("tb_custom_subtitle"), DefaultStyle);
		TB.AddLine(SubText, "Default",
				   PS->Int("tb_font_left_margin"),
				   PS->Int("tb_font_right_margin"),
				   PS->Int("tb_font_vertical_margin"));
	}

	if ((!ShowSubText || PS->Bool("tb_video_info")) && !PS->Bool("tb_hide_video_info")) {
		QString FinalText = TS->ProcessText("", PS->Value("tb_custom_header"), DefaultStyle);
		int line = TB.AddLine(FinalText, "Default",
							  PS->Int("tb_font_left_margin"),
							  PS->Int("tb_font_right_margin"),
							  PS->Int("tb_font_vertical_margin"));

		if (!PS->Bool("tb_hide_vr_text")) {
			QString VRText = TS->ProcessText("", PS->Value("tb_custom_vr"), DefaultStyle);
			TB.AppendLine(VRText, line);
		}

		for (int i = 0; i < TS->Value("streams.count").toInt(); i++) {
			QString Text;
			QString STREAM_TYPE = TS->Value("streams." + QString::number(i) + ".codec_type");

			if (STREAM_TYPE == "video")
				Text = TS->ProcessText("streams." + QString::number(i) + ".", PS->Value("tb_custom_video_stream"), DefaultStyle);
			else if (STREAM_TYPE == "audio")
				Text = TS->ProcessText("streams." + QString::number(i) + ".", PS->Value("tb_custom_audio_stream"), DefaultStyle);
			else if (STREAM_TYPE == "subtitle")
				Text = TS->ProcessText("streams." + QString::number(i) + ".", PS->Value("tb_custom_subtitle_stream"), DefaultStyle);
			else if (STREAM_TYPE == "attachment")
				Text = TS->ProcessText("streams." + QString::number(i) + ".", PS->Value("tb_custom_attachment_stream"), DefaultStyle);

			TB.AppendLine(Text, line);
		}

		if (PS->Bool("tb_chapters_list")) {
			for (int i = 0; i < TS->Value("chapters.count", "0").toInt(); i++) {
				QString Chapter = TS->ProcessText("chapters." + QString::number(i) + ".", PS->Value("tb_custom_chapters_list"),
												  DefaultStyle);
				TB.AppendLine(Chapter, line);
			}
		} else {
			QString Chapter = TS->ProcessText("", PS->Value("tb_custom_chapters"), DefaultStyle);
			TB.AppendLine(Chapter, line);
		}
	}

	int coments_line = ApplyComments();

	if (VR && !PS->Bool("tb_hide_vr_warning")) {
		if (coments_line >= 0)
			TB.AppendLine("VR Video: {\\b1}These screenshots do not represent the exact content of the video file{\\b0}",
						  coments_line);
		else
			TB.AddLine("VR Video: {\\b1}These screenshots do not represent the exact content of the video file{\\b0}",
					   "Comments",
					   PS->Int("tb_comments_left_margin"),
					   PS->Int("tb_comments_right_margin"),
					   PS->Int("tb_comments_vertical_margin"));
	}

	TextBlockHeight += TB.TotalTextHeight();

	ApplyLogo();

	if (!GenerateBG()) return false;

	if (!TB.Generate(TextBlockWidth, TextBlockHeight, TMP_TEXT_IMG)) return false;

	return true;
}

bool ContactSheetTextBlock::GenerateImagesText() {
	CalculateDimensions();
	ApplyTitle();

	QString FinalText = TS->ProcessText("", PS->Value("tb_custom_images_header"), DefaultStyle);

	int line = TB.AddLine(FinalText, "Default",
						  PS->Int("tb_font_left_margin"),
						  PS->Int("tb_font_right_margin"),
						  PS->Int("tb_font_vertical_margin"));

	if (PS->Bool("images_format_stats")) {
		for (int i = 0; i < TS->Value("stats.formats.count").toInt(); i++) {
			QString Text = TS->ProcessText("stats.formats." + QString::number(i) + ".",
										   PS->Value("tb_custom_images_formats"),
										   DefaultStyle);
			TB.AppendLine(Text, line);
		}
	}

	if (PS->Bool("images_resolution_stats")) {
		for (int i = 0; i < TS->Value("stats.resolutions.count").toInt(); i++) {
			QString Text = TS->ProcessText("stats.resolutions." + QString::number(i) + ".",
										   PS->Value("tb_custom_images_resolutions"),
										   DefaultStyle);
			TB.AppendLine(Text, line);
		}
	}

	ApplyComments();

	TextBlockHeight += TB.TotalTextHeight();

	ApplyLogo();

	if (!GenerateBG()) return false;

	if (!TB.Generate(TextBlockWidth, TextBlockHeight, TMP_TEXT_IMG)) return false;

	return true;
}

bool ContactSheetTextBlock::JoinAll(QString OutputFile, QStringList FormatOptions, QString &Output) {
	QStringList Output_params = {"-hide_banner",
								 "-loglevel",
								 "error",
								 "-y",
								};

	QStringList IndividualFilters;
	QString vstackInputs;
	int vstacki = 0;

	if (PS->NotEmpty("tb_cover_top_image")) {
		Output_params << "-i" << PS->Value("tb_cover_top_image");
		vstackInputs = "[fil" + QString::number(vstacki) + "]";
		IndividualFilters.append("[" + QString::number(vstacki) + "] format=yuva444p,scale=" +
								 QString::number(TextBlockWidth) + ":" + QString::number(CoverTopCalculatedHeight) +
								 ":flags=lanczos+accurate_rnd+full_chroma_int+full_chroma_inp " + vstackInputs);
		vstacki++;
	}

	if (TextBlockPosition == ContactSheetTextBlock::text_top) {
		Output_params << "-i" << TMP_TEXT_IMG << "-i" << TMP_IMAGES + "." + TMP_FRAME_FORMAT;
	} else if (TextBlockPosition == ContactSheetTextBlock::text_bottom) {
		Output_params << "-i" << TMP_IMAGES + "." + TMP_FRAME_FORMAT << "-i" << TMP_TEXT_IMG;
	}

	IndividualFilters.append("[" + QString::number(vstacki) + "] format=yuva444p [fil" + QString::number(vstacki) + "]");
	vstackInputs += "[fil" + QString::number(vstacki) + "]";
	vstacki++;
	IndividualFilters.append("[" + QString::number(vstacki) + "] format=yuva444p [fil" + QString::number(vstacki) + "]");
	vstackInputs += "[fil" + QString::number(vstacki) + "]";
	vstacki++;

	Output_params << "-filter_complex" << IndividualFilters.join(";") + "; " + vstackInputs + " vstack=inputs=" +
				  QString::number(vstacki);

	if (PS->NotEmpty("bg_image") || PS->Value("bg_gradient") != "none") {
		Output = TMP_TRANSPARENT_SHEET;
		Output_params << TMP_FRAME_FORMAT_OPTIONS << TMP_TRANSPARENT_SHEET;
		PrintInfo("Generating transparent sheet:");
	} else if (PS->NotEmpty("watermark")) {
		Output = TMP_WITHOUT_WATERMARK;
		Output_params << TMP_FRAME_FORMAT_OPTIONS << TMP_WITHOUT_WATERMARK;
		PrintInfo("Generating without watermark:");
	} else {
		Output = "";
		Output_params << FormatOptions << OutputFile;
		PrintInfo("Generating output: " + OutputFile);
	}

	if (!ffmpeg(Output_params)) return false;
	PrintInfo("\tOk\n");

	return true;
}

bool ContactSheetTextBlock::ApplyBG(QString In, QString Out, QStringList FormatOptions, QString &Output) {
	QStringList bg_param;
	bg_param << "-hide_banner" << "-loglevel" << "error" << "-y";
	bg_param << "-i" << TMP_BACKGROUND;
	bg_param << "-i" << In <<
			 "-filter_complex" << "[0][1]overlay=format=rgb" << "-vframes" << "1";

	if (PS->NotEmpty("watermark")) {
		Output = TMP_WITHOUT_WATERMARK;
		bg_param << TMP_FRAME_FORMAT_OPTIONS << TMP_WITHOUT_WATERMARK;
		PrintInfo("Generating without watermark:");
	} else {
		Output = "";
		bg_param << FormatOptions << Out;
		PrintInfo("Generating output: " + Out);
	}

	if (!ffmpeg(bg_param)) return false;
	PrintInfo("\tOk\n");
	return true;
}

bool ContactSheetTextBlock::ApplyWatermark(QString In, QString Out, QStringList FormatOptions) {
	PrintInfo("Generating watermark:");

	uint64_t FinalWidth = 0;
	uint64_t FinalHeight = 0;
	GetImageSize(In, FinalWidth, FinalHeight);

	if (!GenerateWatermark(FinalWidth, FinalHeight))
		return false;

	PrintInfo("\tOk\n");

	QStringList wm_param;
	wm_param << "-hide_banner" << "-loglevel" << "error" << "-y";
	wm_param << "-i" << In;
	wm_param << "-i" << TMP_WATERMARK <<
			 "-filter_complex" << "[0][1]overlay=format=rgb" << "-vframes" << "1";
	wm_param << FormatOptions << Out;

	PrintInfo("Applying watermark: " + Out);
	if (!ffmpeg(wm_param)) return false;
	PrintInfo("\tOk\n");
	return true;
}

void ContactSheetTextBlock::CalculateDimensions() {
	uint64_t height;
	GetImageSize(TMP_IMAGES + "." + TMP_FRAME_FORMAT, TextBlockWidth, height);
	if (LayoutX < 1) {
		LAYOUT = PS->Value("layout").toLower();
		NumberOfFrames = LayoutSplit(LAYOUT, LayoutX, LayoutY);
	}

	TextBlockHeight = Border * 2 + PS->Int("tb_add_height");
}

void ContactSheetTextBlock::ApplyTitle() {
	Title = TS->ProcessText("", Title, TitleStyle);
	if (Title != "") {
		TB.AddLine(Title, "Title",
				   PS->Int("tb_title_left_margin"),
				   PS->Int("tb_title_right_margin"),
				   PS->Int("tb_title_vertical_margin"));
	}
}

int ContactSheetTextBlock::ApplyComments() {
	if (PS->NotEmpty("tb_comments")) {
		QString COMMENTS = TS->ProcessText("", PS->Value("tb_comments"), CommentsStyle);
		return TB.AddLine(COMMENTS, "Comments",
						  PS->Int("tb_comments_left_margin"),
						  PS->Int("tb_comments_right_margin"),
						  PS->Int("tb_comments_vertical_margin"));
	}
	return -1;
}

void ContactSheetTextBlock::ApplyLogo() {
	if (PS->NotEmpty("tb_logo")) {
		TB.SetLogo(PS->Value("tb_logo"), PS->Value("tb_logo_pos") == "left", PS->Bool("tb_logo_text_overlay"),
				   PS->Value("tb_logo_transparent_color"), PS->Int("tb_logo_transparent_similarity"),
				   PS->Int("tb_logo_transparent_blend"));
		if (PS->Int("tb_logo_height") > (int)TextBlockHeight) TextBlockHeight = PS->Int("tb_logo_height");
	}
}

bool ContactSheetTextBlock::GenerateBG(uint64_t W, uint64_t H) {
	if (PS->NotEmpty("bg_image") || PS->Value("bg_gradient") != "none") {
		uint64_t FinalWidth, FinalHeight;
		if (W == 0 || H == 0) {
			GetImageSize(TMP_IMAGES + "." + TMP_FRAME_FORMAT, FinalWidth, FinalHeight);
		} else {
			FinalWidth = W;
			FinalHeight = H;
		}

		if (TextBlockPosition != text_hidden) {
			FinalHeight += TextBlockHeight;
			if (PS->NotEmpty("tb_cover_top_image")) {
				uint64_t CoverTopWidth, CoverTopHeight;
				GetImageSize(PS->Value("tb_cover_top_image"), CoverTopWidth, CoverTopHeight);
				double CoverTopAS = (double)CoverTopHeight / (double)CoverTopWidth;
				CoverTopCalculatedHeight = qRound((CoverTopWidth / 2) * CoverTopAS) * 2;
				FinalHeight += CoverTopCalculatedHeight;
			}
		}

		if (PS->NotEmpty("bg_image")) {

			if (!TextBlock::GenerateBackgroundImage(PS->Value("bg_image"),
													TMP_BACKGROUND,
													FinalWidth,
													FinalHeight,
													BackgroundHAling,
													BackgroundVAling,
													PS->Bool("bg_image_scale"),
													PS->Bool("bg_image_tile"),
													PS->Int("bg_image_blur"),
													PS->Int("bg_image_exposure"),
													PS->Int("bg_image_saturation"),
													100,
													GridBorderColor)) {
				PrintError("Error procesing background image");
				return false;
			}
		} else {
			if (!Filters::GenerateGradient(
							TMP_BACKGROUND,
							FinalWidth,
							FinalHeight,
							PS->Value("bg_gradient"))) {
				PrintError("Error generating background gradient image");
				return false;
			}
		}
		if (PS->NotEmpty("tb_cover_top_image") && TextBlockPosition == text_top)
			TB.SetBGInputImage(TMP_BACKGROUND, CoverTopCalculatedHeight);
		else if (TextBlockPosition == text_bottom)
			TB.SetBGInputImage(TMP_BACKGROUND, FinalHeight - TextBlockHeight);
		else
			TB.SetBGInputImage(TMP_BACKGROUND, 0);
	}
	return true;
}

bool ContactSheetTextBlock::GenerateWatermark(uint64_t W, uint64_t H) {
	QString PreFilter = "";
	if (PS->NotEmpty("watermark_transparent_color"))
		PreFilter = Filters::RawColorKey(PS->Value("watermark_transparent_color"),
										 PS->Int("watermark_transparent_similarity"),
										 PS->Int("watermark_transparent_blend"));
	if (!TextBlock::GenerateBackgroundImage(PS->Value("watermark"),
											TMP_WATERMARK,
											W,
											H,
											String2HAling(PS->Value("watermark_halign")),
											String2VAling(PS->Value("watermark_valign")),
											PS->Bool("watermark_scale"),
											PS->Bool("watermark_tile"),
											PS->Int("watermark_blur"),
											PS->Int("watermark_exposure"),
											PS->Int("watermark_saturation"),
											PS->Int("watermark_opacity"),
											"FFFFFF00",
											"",
											"",
											"",
											0,
											PreFilter,
											"")) {
		PrintError("Error procesing watermark image");
		return false;
	}
	return true;
}
