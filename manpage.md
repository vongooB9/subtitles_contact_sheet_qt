%
% Written by vongooB9 <vongooB9@protonmail.com>
%

# NAME

subtitles_contact_sheet - Generates video, subtitles and image folders previews, small animations (gif, webp,...), screenshots, thumbnails, etc.

# DESCRIPTION

This program can create video previews with and without subtitles. These can be contact sheets where file information is displayed along with screenshots taken at regular intervals, individual screenshots and short video clips in gif or webp format. It works with subtitles in **srt**, **ass**, **vtt** format and with all video files supported by **ffmpeg**. It has many options for customization of appearance, performance and control over the output format. It also has batch processing options, can save multiple configurations to make it much easier to use and can rename files using the internal video metadata.

You can also create contact sheets and animations of folders with images.

# SYNOPSYS

**subtitles_contact_sheet** [*subtitle_or_video_file*] [*options*]
: This command processes a video or subtitle file according to the indicated options.

**subtitles_contact_sheet** \-\-batch [*File/Dir*]... [*\-r*] [*\-f \<file\_types\>*]... [*\-p \<1\-16\>*] [*\-c \<program\>*] \-\- [*options*] [\-\-] ...
: Using the batch mode, which has a more complex syntax, several files can be processed in several different formats with a single command.

# OPTIONS
There are several types of options in the program, the most common are **configuration options**, these options can be used both in the parameters when running the program and in the configuration file. Other options are only available as **parameters**, because they are relative to the current job that is being done at the moment, they are for example *\-\-video* or *\-\-config*, which only affect the current job. Finally there are **shortcuts** which are groupings of configuration options for simplicity. I will indicate the type of each by separating them into different sections. I will call them **options**, **parameters** and **shortcuts** respectively.

**Boolean configuration options** when used as a parameter can be forced by adding **1** or **0**. The parameter without specifying the value changes the defaults, if the default value is *1* it sets it to *0* and vice versa. In the documentation they appear with **\[0\|1\]**. For example: *\-\-no\_overwrite* is the same as *\-\-no\_overwrite 1*. When *no\_overwrite=1* is configured in the file, it can be deactivated with *\-\-no\_overwrite 0*.

**The configuration options when used from the parameters** in addition to the literal number, can also have a relative value by adding a **+** before the number. Example: *\-\-tb\_font\_size +10* means increase the font size by *10* and *\-\-tb\_font\_size +\-10* means decrease it by *10*. This allows to change the value quickly in the parameters without having to consult their value in the configuration files. But this option cannot be used in the configuration files.

Since version 0.8 most of the options can use functions to adjust its value depending on the information in the file and other variables. In operation options, such as --ffmpeg or --tmp, cannot be used, they are applied before collecting the data needed to use the functions. More details in the [functions section](#functions).

## General parameters

**\[subtitle\_or\_video\_file\_or\_image\_folder\]**
: When you specify an input file without specifying what it is, the program detects whether it is a video or a subtitle using the extension, see *\-\-list\_extensions* to see the complete list. If it is a subtitle, it automatically searches for a video with the same name, if it does not exist it gives an error, to set it manually use the *\-\-video* and *\-\-sub* options. If a video file has a not listed extension, use *\-\-video* intead. If it is a folder, it will work in the same way as using *\-\-images*.

**\-c**, **\-\-config \<name|file\>**
: Name of the configuration file used or the file itself. For more information see the [configuration section](#configuration). This parameter can be repeated several times to make combinations of options, they are loaded in the same order as the parameters, overwriting the previous options.

**\-\-list\_config**
: Displays the list of available configuration files with their name and description.

**\-\-save**
: Save the actual parameters in the configuration file. If more than one configuration file is specified, it is always saved in the last one. It can be used to convert the syntax of the parameters to that of the configuration file, in some cases there are differences in the allowed characters and their escaping format.

**\-\-save\_all**
: Same as *\-\-save* but creates the complete configuration file, all the unset options with the default value.

**-b**, **\-\-batch**
: Enables batch mode, allows to process multiple input files or multiple options for one file, has its own special parameters and syntax, you can see the complete listing in [Batch mode options section](#batch-mode-options).

**\-\-sub \<file\>**
: Subtitle input file, disabling auto-detection based on the file extension.

**\-\-sub_stream \<stream_number\>**
: When a subtitle stream number is specified, the program automatically extracts it and displays the subtitles. It differs from using a subtitle file directly (using *\-\-sub*) in that this option does not display the subtitle data in the text.

**\-\-video \<file\>**
: Video input file, disabling auto-detection based on the file extension.

**\-\-images \<dir\>**
: Image folder input. Create contact sheets and animations of folders with images. For the moment this is incompatible with batch processing. [Images Options section](#images-options).

**\-\-dvd \<dir\>**
: Uses DVD VIDEO_TS directory as input.

**\-\-dvd\_title \<number\>**
: Select this DVD Title, *0* means automatically select the longest title, Default *0*.

**\-\-out \<file\>**
: Full path to the output file, it also automatically detects the output format using the file extension.

**\-\-mf \[number,\]time**
: Allows manual selection of screenshots. Replaces the automatically selected time with the indicated in time. You can add as many *\-\-mf* as you want, if no **number** is indicated the order of *\-\-mf* in the parameters is used, the first parameter would be 1 and successively. **time** can be in seconds or in *MM:SS* or *HH:MM:SS* format.

**\-\-gui\_progress**
: Displays messages that the GUI interprets to show the progress bar.

**\-\-list\_vars**
: Lists the available variables and their value for use in [text substitution](#text-substitution-format). Terminates without doing anything.

**\-\-list\_extensions**
: Displays the list of all extensions that the program recognizes and exits. They are only used for automatic detection of whether a file corresponds to a video or a subtitle, also to search for the video files that correspond to the subtitles and in the batch mode to find those files in the folders.

**\-v**, **\-\-version**
: Displays the current version of this program.

**-h**, **\-\-help**
: Displays help and command-line options.

**\-\-help-sub**
: Displays subtitles options.

**\-\-help-text**
: Displays text block options.

**\-\-help-time**
: Displays frame times options.

**\-\-help-vr**
: Displays virtual reality options.

**\-\-help-batch**
: Displays batch mode options.

**\-\-help-all**
: Displays all the options.

## General options

**\-\-config\_desc \<text\>**
: Small text to explain the purpose of this configuration file. It is only explanatory and has no real function other than to appear in the configuration list.

**\-\-add\_config \<name|file\>**
: Loads this configuration file, similar to config, the difference is that this one is loaded much later in the program execution, can only be used once and the configuration file can only contain simple options, but has the advantage that you can use functions and apply different configurations depending on the internal metadata of the video. For more information see the [configuration section](#configuration).

**\-\-vertical\_config \<name|file\>**
: Load this configuration when a vertical video input was detected, this config has the same restrictions as *\-\-add_\config*.

**\-\-out\_dir \<dir\>**
: Output directory for the files generated by the program.

**\-\-out\_relative\_dir \<name\>**
: Output directory relative to input file. For example, this allows you to create a *contactsheet* folder inside the folder containing the input file and save the generated files in it.

**\-\-out\_name \<name\>**
: Modify output file name using [text substitution](#text-substitution-format), Default *"%in.filename%"*. This is only the name part, to get the final file name the program adds the suffix and the corresponding extension from the output format.

	Example: remove the extension of input file from output name.

	`--out_name "%in.basename%"`

	The complete output file name is calculated as follows:

	`prefix + prefix_number + out_name + suffix + suffix_number + . + format_extension`

**\-\-suffix \<text>**
: Text appended to the **\-\-out\_name** before the format extension. Default: *.contact_sheet*.

**\-\-suffix\_number \<number|time\_format>**
: Configuration of how the output files are numbered when they are individual. If the value is a *number, zeros are added until the length indicated by the number is reached. The value can also be a time format* and the time of capture will be displayed in that format. If the value is set to **auto**, it will be automatically selected according to the number of screenshots. If it's **time** the same as *hh:mm:ss.zzz*. If it is **short** and the video lasts less than an hour it is *mm:ss*, otherwise it is *hh:mm:ss*. The *\-\-only\_keyframes* and *\-\-rep\_frames* options may cause the time shown in the filename not to be the exact frame represented by the image. Default *1*. The empty value disables it, but if *\-\-prefix\_number* is also disabled, the empty value is considered as *1*. Because it is mandatory to activate at least one.

	Examples:

	- **0, 1**: ...suffix1.jpg,  ...suffix2.jpg, ...suffix3.jpg...
	- **4**: ...suffix0001.jpg, ...suffix0002.jpg, ...suffix0003.jpg...
	- **auto and -l 3x10**: ...suffix01.jpg, ...suffix02.jpg, ...suffix03.jpg...
	- **auto and -l 10x20**: ...suffix001.jpg, ...suffix002.jpg, ...suffix003.jpg...
	- **time**: ...suffix00:00:20.000.jpg, ...suffix00:05:45.560.jpg...
	- **short and lengt<=1h**: ...suffix00:20.jpg, ...suffix05:45.jpg...
	- **short and lengt>1h**: ...suffix00:00:20.jpg, ...suffix00:05:45.jpg...

**\-\-prefix \<text>**
: Text preceding the **\-\-out\_name**. Default empty.

**\-\-prefix\_number \<number|time\_format>**
: It is the same as **\-\-suffix\_number** but added to the prefix instead of the suffix. Default empty.

**\-\-no\_overwrite \[0\|1\]**
: Does not overwrite the output file, if it already exists it simply does nothing. Useful to use in batch mode, to process the same folder several times and to actually process only the new files. Default *0*.

**-p**, **\-\-process \<number\>**
: Nunber of parallel process (0-32). Generates screenshots in parallel, not always a speed improvement, but in some cases it can speed up the process a lot. *0* means automatic detection, uses the number of CPU cores. Default *1*.

**\-\-idle \[0\|1\]**
: When it is 1 launch the ffmpeg processes with idle priority. Default *0*.

**-l**, **\-\-layout \<CxR\>**
: Number of captured frames in the format COLUMSxROWS. Default *3x7*.

**-s**, **\-\-size \<px\>**
: Size in pixels of the largest dimension of the video, the width in horizontal videos, the height in vertical videos, 0 means do not change the size of the input. Default *640*.

**\-\-min\_size \<px\>**
: The smallest dimension of a video will never be smaller than this. Increases the size of the larger dimension while keeping the aspect ratio. 0 to disable. Default *300*.

**\-\-aspect\_ratio \<ar\>**
: Set the aspect ratio of the output, It can be set to an aspect ratio in the form **W:H** such as *16:9* or **display** to use the aspect ratio indicated in the input video metadata or **resolution** to use the aspect ratio of the input resolution. Default *display*. If *\-\-vr* is enabled **display** do the same as **resolution** because in that case the image is manipulated and the aspect ratio metadata isn't relevant, and **resolution** uses the transformed image resolution instead of input resolution.

**\-\-format \<name\>**
: Output format, It can be: *jpg*, *jxl*, *png*, *apng*, *gif*, *giflow*, *giflossy*, *webp*, *webploop*, *xvid*, *x264*, *x265*, *av1*, *webmvp8*, *webmvp9*, *webmav1*, *avif*. For a detailed explanation of each one of them go to section [Output formarts](#output-formarts). There are shortcuts for the directly supported formats, full list in [shortcut options](#shortcut-options). If the *custom\_format\_...* options below are defined, to use them set **custom** here.

**\-\-output\_quality \<number\>**
: For formats that support quality settings (*jpg, jxl, webp, webploop, losslesswebp, xvid, x264, x265, av1, all webm, avif*), each format has different supported values and default, puting this option to *-1* choses that default. Although some formats share the range of possible values, the same value in different formats does not mean equivalent qualities, each codec has its own scale. See [Output formarts](#output-formarts) for more information.

**\-\-mfs \<time list\>**
: List with the time of all selected frames, separated by *\-\-mfs\_separator* (*","* by default). It is used to overwrite the entire selection at once or use the functions to load or generate the list.

**\-\-mfs\_separator \<separator\>**
: Separator between the values of **\-\-mfs**. It can be one or several characters including a space, **\\n** means line break and **\\t** means tabulator. Default *","*.

**\-\-show\_mfs \[0\|1\]**
: When activated it displays the mfs parameter with the current list of selected frames. It is useful to use it as a basis for manual modification. Deafult *0*.

**\-\-enable\_audio \[0\|1\]**
: Enables output audio if the output format allows it, Only works with video formats and when using individual or concatenated output files. Default *0*.

**\-\-custom\_format\_ext \<ext\>**
: Extension for custom output format. Must be one supported by ffmpeg.

**\-\-custom\_format\_video \<string\>**
: Ffmpeg parameters defining an output video format. Can contain ***{quality}*** to use the quality set with *\-\-output\_quality*. x264 example: *\-c:v libx264 \-crf {quality} \-preset medium*.

**\-\-custom\_format\_audio \<string\>**
: Ffmpeg parameters defining an output audio format. Only used when *\-\-enable\_audio* is set to *1*. AAC example: *\-c:a aac \-b:a 96k*.

**\-\-custom\_format\_default\_quality \<string\>**
: Default quality for this format when not defined.

**\-\-pixel\_format \<pix_fmt\>**
: Output pixel format, some formats can use several pixel formats, this option allows you to specify a specific one. Normally it is used when a format supports transparency but we don't want to use it.

	For example, for the PNG format, the most common pixel formats are:

	- *rgb24*: 8bits per color channel, without alpha channel, 24 bits total.
	- *rgba*: 8bits per color channel, with alpha channel, 32 bits total.
	- *rgb48be*: 16bits per color channel, without alpha channel, 48 bits total.
	- *rgba64be*: 16bits per color channel, with alpha channel, 64 bits total.

	For webp:

	- *bgra*: 8bits per color channel, with alpha channel, 32 bits total.
	- *yuv420p*: 8bits per color channel, without alpha channel, 12 bits total.
	- *yuva420p*: 8bits per color channel, with alpha channel, 20 bits total.

**\-\-scale\_algorithm \<alg\>**
: Algorithm used to resize to the desired image size. It's safe use *bilinear*, *bicubic* and *lanczos* because they are supported for both, go to [scale](https://ffmpeg.org/ffmpeg-scaler.html#sws_005fflags) and [zscale](https://ffmpeg.org/ffmpeg-filters.html#zscale-1) to see all algorithms. Default *lanczos*.

**\-\-scale\_flags \<flags\>**
: Additional [flags for the scale filter](https://ffmpeg.org/ffmpeg-scaler.html#sws_005fflags). Default *+accurate_rnd+full_chroma_int*.

**\-\-scale\_custom\_options \<opt\>**
: Additional options passed to the corresponding filter. See the official cara uno manual for more information. [scale options](https://ffmpeg.org/ffmpeg-filters.html#scale-1), [zscale options](https://ffmpeg.org/ffmpeg-filters.html#zscale-1). Default empty.

**\-\-scale\_zscale \[0\|1\]**
: Uses the [zscale](https://ffmpeg.org/ffmpeg-filters.html#zscale-1) filter instead of scale. zscale uses different algorithms and options, it is necessary to set *\-\-scale\_algorithm* and *\-\-scale\_custom\_options* accordingly. Default disabled.

**\-\-custom\_capture\_filters \<filters\>**
: Custom additional [FFmpeg filers](https://ffmpeg.org/ffmpeg-filters) for captured screenshots and clips.

**\-\-start \<time\>**
: Exclude the first part in seconds or formated MM:SS.

**\-\-end \<time\>**
: Is the time in second or formated MM:SS to end, If it is negative, it is subtracted from the duration.

**\-\-mode \<time\|line\|video_time\|video_end|random|fixed\>**
:   Mode of operation, searches for frames to capture using different methods, *time* and *line* are exclusive to subtitles, they are explained in the corresponding section. **video_time** divides the time between *\-\-start* and *\-\-end* into the same number of portions as screenshots, and starts looking for valid frames at the beginning of each interval, **video_end** does the same with the exception of the last screenshot which is fixed starting at *\-\-end*, but means that it splits the time into one less screenshot, so changing the whole distribution of frames. **random** Splits the video into portions just like video_time but selects a random frame within them. **fixed** selects frames at a fixed time interval, activated automatically by setting *\-\-fixed\_interval*. The default value is *time* but it's automatically changed to *video_time* for video files.

	**video_end** in combination with *\-\-rep\_frames*, may cause the last screenshot not to be displayed or not within the desired time range. in combination with *\-\-only\_keyframes*, searches for the next keyframe at the corresponding time, except for the last screenshot which searches for the previous one. Because it may be the case that there is no keyframe after *\-\-end*.

	Graphical explanation of the 2 video modes

	- **-**: The line is the graphical representation of the time duration of the video.
	- **|**: Is the representation of \-\-start and \-\-end. When is attached to the number it means the same time.
	- **1,2,...**: Are the times selected for taking screenshots.

	`video_time:  ---|1----2----3----4----5----|---`

	`video_end:   ---|1-----2-----3-----4-----5|---`

**\-\-fixed\_interval \<time\>**
: When a value is specified, the *\-\-mode* is automatically changed to *fixed*, and screenshots will be taken using this time interval, *\-\-start* and *\-\-end*. As many as correspond to the duration of the video, the rows of the layout will be changed automatically. Default empty. For example, using *60* or *1:00* with *\-\-start 30* and *\-\-end 0* in a 10-minute video, will take 10 screenshots at 0:30, 1:30, 2:30, 3:30...9:30.

**\-\-only\_keyframes \[0\|1\]**
: Try to campure only keyframes, sometimes this is faster. It is only useful when the exact time of the screenshot does not matter. Searches for the next keyframe at the corresponding time.

**\-\-rep\_frames \<number\>**
: Selects the most representative frame considering the specified number of frames beginning at the corresponding time. It uses the ffmpeg thumbnail filter. The higher the number the slower it is. Does not work with subtitles. Default 0 (disabled), normal value is 100. More details in [thumbnail filter documentation](https://ffmpeg.org/ffmpeg-filters.html#thumbnail).

**\-\-no\_grid \[0\|1\]**
: Don't generate the screenshot grid, generates an individual file for each screenshot.

**\-\-grid\_border \<px\>**
: Set all the borders of the grid in pixels. Acts on the specified borders that have a value of *-1*. Deafult *2*.

**\-\-grid\_border\_up \<px\>**
: Set up border of the grid in pixels. *-1* means use **\-\-grid_border** value. Default *-1*.

**\-\-grid\_border\_down \<px\>**
: Set down border of the grid in pixels. *-1* means use **\-\-grid_border** value. Default *-1*.

**\-\-grid\_border\_left \<px\>**
: Set left border of the grid in pixels. *-1* means use **\-\-grid_border** value. Default *-1*.

**\-\-grid\_border\_right \<px\>**
: Set right border of the grid in pixels. *-1* means use **\-\-grid_border** value. Default *-1*.

**\-\-grid\_border\_vertical \<px\>**
: Set vertical internal borders of the grid in pixels. *-1* means use **\-\-grid_border** value. Default *-1*.

**\-\-grid\_border\_horizontal \<px\>**
: Set horizontal internal borders of the grid in pixels. *-1* means use **\-\-grid_border** value. Default *-1*.

**\-\-grid\_border\_color \<color\>**
: Set the background for the borders of the grid in RRGGBBAA format. By default uses the same value as *\-\-tb_bg_color*

**\-\-bg\_image \<file\>**
: Set the background image for the entire contact sheet. Implies that grid and text block backgrounds become transparent

**\-\-bg\_image\_halign \<haling\>**
: Horizontal alignment of background image, can be right, center, left or a percentage.

**\-\-bg\_image\_valign \<valing\>**
: Vertical alignment of background image, can be up, middle, down or a percentage.

**\-\-bg\_image\_scale \[0\|1\]**
: Scale the background image before crop it.

**\-\-bg\_image\_tile \[0\|1\]**
: When the background image is smaller than the result it repeats it in tiles to fill the necessary space, 1 by default.

**\-\-bg\_image\_blur \<number\>**
: Applies a blur filter to the background image. the higher the number the more blurred. Default 0 (disabled).

**\-\-bg\_image\_exposure \<number\>**
: Applies an exposure filter to the background image. allows a number between -300 and 300, negative numbers decrease the exposure making the image darker, positive numbers make the image lighter. Default 0 (disabled).

**\-\-bg\_image\_saturation \<number\>**
: Applies a saturation filter to the background image. Accepts a number between 0 and 300, 0 converts the image to black and white. Default 100 (disabled).

**\-\-bg\_gradient \<gradient\>**
:Generates a gradient as a background image with the indicated parameters. The explanation of how to define a gradient is in the section [Gradients](#gradients).

**\-\-logo \<file\>**
: Add this logo to each screenshot.

**\-\-logo\_pos \<position\>**
: Position of the logo, can be *up_left, up_right, down_left, down_right*. Default *down_left*.

**\-\-logo\_margin \<px\>**
: Pixel separation of logo from borders. Default *10*.

**\-\-logo\_scale \<%\>**
: Size of the logo in percentage with respect to the size of the screenshot. *0* means no resizing, *100* same size as the screenshot. Default *0*.

**\-\-logo\_blur \<number\>**
: Applies a blur filter to the logo. the higher the number the more blurred. Default *0* (disabled).

**\-\-logo\_exposure \<number\>**
: Applies an exposure filter to the logo. allows a number between *-300* and *300*, negative numbers decrease the exposure making the image darker, positive numbers make the image lighter. Default *0* (disabled).

**\-\-logo\_saturation \<number\>**
: Applies a saturation filter to the logo. Accepts a number between *0* and *300*, *0* converts the image to black and white. Default *100* (disabled).

**\-\-logo\_opacity \<number\>**
: Percentage of transparency of the logo. *100* is opaque, *0* totally transparent. Default *100*.

**\-\-logo\_transparent\_color \<color\>**
: Makes this color transparent in the logo, RRGGBB format. Empty by default.

**\-\-logo\_transparent\_similarity \<number\>**
: Percentage of similarity with *\-\-logo\_transparent\_color* to consider a color transparent. from *1* to *100*. Default *1*.

**\-\-logo\_transparent\_blend \<number\>**
: Percentage of transparency for pixels that fall outside the similarity. from *0* to *100*. Default *0*.

**\-\-mask \<color|gradient|file\>**
: Mask applied to each screenshot to define the transparency of each part of the image. It can be a solid color (the whole image has the same opacity), a gradient as explained in [Gradients](gradients). Or an image file. In this option, white means totally opaque and black means totally transparent. It can be used for example to make screenshots with rounded edges.

**\-\-rounded\_corners \<px\>**
: Set the radius of rounded corners in pixels. Default *0*. This option uses the same internal infrastructure as **\-\-mask**, so you cannot use both at the same time.

**\-\-video\_preview \<milliseconds\>**
: Instead of capturing images, it captures videos of the specified duration.

**\-\-video\_fps \<fps\>**
: When capturing videos or generating videos from images, these are the frames per second of the result. Use it to slow down the frames per second of a video or to define the speed at which images change when they are joined together.

**\-\-concat \[0\|1\]**
: Instead of putting the screenshots in a grid, puts them one after the other, requires an output format that supports video.

**\-\-tmp \<dir\>**
: Temporal direcctory. If not configured, it uses the temporary directory of the operating system.

**\-\-ffmpeg \<file\>**
: ffmpeg executable.

**\-\-ffprobe \<file\>**
: ffprobe executable.

**\-\-gifsicle \<file\>**
: gifsicle executable. Necessary to use the giflossy output format.

**\-\-mediainfo \<file\>**
: Optional mediainfo cli executable. Empty by default.

**\-\-enable\_mediainfo**
: Imports the information provided by mediainfo into variables. When activated and configured the executable loads all the information provided by mediainfo into the corresponding variables. It appears in **format.mediainfo.** and **streams.\*.mediainfo.** in each stream. Use *\-\-list_vars* to see all

**\-\-exec\_before \<command\>**
: Execute this command before start. Supports [text substitution](#text-substitution-format) to customize the command.

**\-\-exec\_after \<command\>**
: Execute this command at the end. Supports [text substitution](#text-substitution-format) to customize the command. This command is executed even if no_overwrite is enabled.

**\-\-exec \<command\>**
: Execute this command instead of the main action of the program. Supports [text substitution](#text-substitution-format) to customize the command.

**\-\-var \<var=value\>**
: Allows to create variables to be used within [text substitution](#text-substitution-format), these variables can be saved in the configuration file prefixed with *"VAR_"* and a number to execute them in order. You can use the same [functions](#functions) available for text substitution to calculate the value. They are executed in the order in which they are defined. If the same variable is defined several times, all of them are executed. More information in [Variable System](#variable-system).

**\-\-var\_stream \<var=value\>**
:	Same as *\-\-var* but it is executed in each stream, these variables can be saved in the configuration file prefixed with *"VAR_{number}_S_"*. The value of the variable can start with "context." to define it within that context, if it is not a normal variable. More information in [Variable System](#variable-system).

	Example: Generates a variable with the list of all codecs used in a video.

	`--var_stream "codec_list=concat( ,codec_list,codec_name)"`

**\-\-var\_stream\_video \<var=value\>**
: Same as *-\-\var_stream* but only for the video streams, these variables can be saved in the configuration file prefixed with *"VAR_{number}_SV_"*.. More information in [Variable System](#variable-system).

**\-\-var\_stream\_audio \<var=value\>**
: 	Same as *-\-\var_stream* but only for the audio streams, these variables can be saved in the configuration file prefixed with *"VAR_{number}_SA_"*.. More information in [Variable System](#variable-system).

	Example: Generates a variable with the list of audio languages.

	First, generate a variable inside the stream with the name of the language from the 3-digit code.

	`--var_stream_audio "context.language_name=language_shortname(tags.language)"`

	Now generate a general variable containing the list.

	`--var_stream_audio "audio_languages=concat( ,audio_languages,language_name)"`

**\-\-var\_stream\_subtitle \<var=value\>**
: Same as *-\-\var_stream* but only for the subtitle streams, these variables can be saved in the configuration file prefixed with *"VAR_{number}_SS_"*.. More information in [Variable System](#variable-system).

**\-\-var\_stream\_attachment \<var=value\>**
: 	Same as *-\-\var_stream* but only for the attachment streams, these variables can be saved in the configuration file prefixed with *"VAR_{number}_ST_"*.. More information in [Variable System](#variable-system).

	Example: Generates a variable containing the size of all attachments. First we have to initialize the variable to 0 so that the calculation functions do not fail.

	`--var total_attach_size=0`

	Now we can add up in each attachment.

	`--var_stream_attachment "total_attach_size=sum(total_attach_size,extradata_size)"`

**\-\-var\_file \<file|context=file\>**
: Loads the variables contained in the file. If **context** is specified, they are loaded in *context.name*, otherwise they use only the name defined in the file. It uses the same format as the **load_vars_file** function, go to [functions](#functions) to se a detailed explanation of the format.

	`This option is a simplification of --var "context=load_vars_file(file,,context,1)"`

**\-\-template \<file\>**
: Generate a text using this file as a template using [text substitution](#text-substitution-format), Creates a file with the same name as the input video plus the extension set in the next parameter. It can be used to create templates in html, bbcode, markdown or any other text format in which we want to enter data from the input video. When this parameter is used the program does nothing else. more information un [Templates](#templates).

**\-\-template\_ext \<extension\>**
: Extension added to the video name to generate the file with the substituted values.

**\-\-quiet \[0\|1\]**
: Disable messages, for batch processing.

**\-\-debug \[0\|1\]**
: Enable debug mode.

**\-\-cmd \[0\|1\]**
: Show ffmpeg and ffprobe commands.

## General shortcuts

**\-\-jpeg**:, **\-\-jpg**
: Output format jpg (default). See [jpg format](#jpg-jpeg).

**\-\-jxl**
: Output format jpeg xl. See [jxl format](#jxl).

**\-\-png**
: Output format png. See [png format](#png).

**\-\-apng**
: Output format animated png (Only video). See [apng format](#apng).

**\-\-webp**
: Output format webp. See [webp format](#webp).

**\-\-losslesswebp**
: Output format lossless webp. See [losslesswebp format](#losslesswebp).

**\-\-webploop**
: Output format webp loop video. See [webploop format](#webploop).

**\-\-gif**
: Output format gif. Each frame has its own color palette. See [gif format](#gif).

**\-\-giflow**
: Output format gif. Same color palette for the whole file, has less quality. See [giflow format](giflow).

**\-\-giflossy**
: Output format gif. Same as gif but uses gifsicle to reduce the size and the quality. See [giflossy format](giflossy).

**\-\-xvid**
: Output format mkv with Xvid codec and MP3 audio. See [xvid format](xvid).

**\-\-x264**
: Output format mkv with x264 codec and AAC audio. See [x264 format](x264).

**\-\-x265**
: Output format mkv with x265 codec and AAC audio. See [x265 format](x265).

**\-\-av1**
: Output format mkv with AV1 codec and AAC audio. See [av1 format](av1).

**\-\-webmvp8**
: Output format webm with VP8 codec and Opus audio. See [webmvp8 format](webmvp8).

**\-\-webmvp9**
: Output format webm with VP9 codec and Opus audio. See [webmvp9 format](webmvp9).

**\-\-webmav1**
: Output format webm with AV1 codec and Opus audio. See [webmav1 format](webmav1).

**\-\-avif**
: Output format avif with AV1 codec. See [avif format](avif).

**\-\-gif\_images \<images\>**
: Short gif video with images. Same as *\-\-format gif \-\-layout 1x\<images\> \-\-ft\_hide \-\-concat \-\-no_grid \-\-video\_fps 1 \-\-suffix ".images"*.

**\-\-webp\_images \<images\>**
: Short webp video with images. Same as *\-\-format webploop \-\-l 1x\<images\> \-\-ft\_hide \-\-concat \-\-no_grid \-\-video\_fps 1 \-\-suffix ".images"*.

**\-\-gif\_clip \<clips\>**
: Gif video with short one-second videos. Same as *\-\-format gif \-\-layout 1x\<clips\> \-\-ft_hide \-\-concat \-\-no_grid \-\-video\_preview 1000 \-\-video\_fps 12 \-\-size 320 \-\-min_size 0 \-\-suffix ".clip"*.

**\-\-webp\_clip \<clips\>**
: Gif video with short one-second videos. Same as *\-\-format webploop \-\-layout 1x\<clips\> \-\-ft\_hide \-\-concat \-\-no_grid \-\-video\_preview 1000 \-\-video\_fps 12 \-\-suffix ".clip"*.

**\-\-gif\_singles \<clips\>**
: Same as *\-\-gif\_clip* but does not join the fragments together, it generates as many independent gifs as indicated. Same as *\-\-format gif \-\-layout 1x\<clips\> \-\-ft\_hide \-\-no_grid \-\-video\_preview 1000 \-\-video\_fps 12 \-\-size 320 \-\-min_size 0 \-\-suffix ".single"*.

**\-\-full**
: Screenshots at input resolution, same as *\-\-size ""*.

**\-\-screenshots**
: Full size unaltered screentshots, same as *\--layout 1x\<number\> \--no\_grid \--size "" \--format png \--ft\_hide \--sufix ".screenshot\_"*.

## Subtitles options

**\-\-sub\_show\_embedded \[0\|1\]**
: When enabled the program automatically searches for embedded subtitles to display following the criteria of the options below. By default it is enabled. When a subtitle meets the criteria it is displayed as using the *\--sub\_stream* option.

**\-\-sub\_emb\_select \<0\-5\>**
: Selection method for embedded subtitles is a number from 0 to 5. By default *0*.

	0. Select the first forced subtitle.
	1. Select the first forced subtitle of the selected language.
	2. Select the first non-forced subtitle.
	3. Select the first non-forced subtitle of the selected language.
	4. Select the first subtitle but the forced has priotity.
	5. Select the first subtitle but the forced has priotity of the selected language.

**\-\-sub\_emb\_lang \<lang\>**
: Language in two- or three-letter ISO 639 code. It works in conjunction with options 1, 3 and 5 of *\-\-sub\_emb\_select*.

**\-\-mode \<time\|line\|video_time\|video_end\>**
: Mode of operation, searches for frames to capture using different methods. For subtitles only *time* and *line* are relevant. **time** uses the same method as *video_time* to distribute the time, but after doing so it searches for the next subtitle line within the range that meets the requirements. If there are no subtitles in the range it captures the first frame even if it does not have it. **line** searches only the subtitle lines and selects them according to their number. This mode only selects frames with subtitles omitting the parts of the video that don't have them, as well as the parts of the video with more lines also receive more screenshots.

**\-\-min \<number\>**
: Minimum subtitle size in characters. In time mode, search for subtitles of equal or greater length, but if there are none in the time range, the longest of the possible subtitles is used.In line mode, the subtitles are filtered and only those with a length greater than or equal to the indicated length are selected.

**\-\-start\_line \<number\>**
: Subtitle line in which to start taking screenshots, only used in **line mode**.

**\-\-end\_line \<number\>**
: Subtitle line in which to finish taking screenshots, if it is negative it is deducted from the total number of lines, it is only used in **line mode**.

**\-\-sub\_delay \<ms\>**
: Time in milliseconds added to the start of each subtitle line, to adjust for discrepancies between subtitle start time and frame time, that could cause ffmpeg to capture the previous frame and not display the subtitles. The default, *-1* means add the time corresponding to one frame.

**\-\-sub\_font \<font\>**
: Set the subtitles font name.

**\-\-sub\_fonts_dir \<dir\>**
: Set the aditional directory to search for fonts.

**\-\-sub\_color \<color\>**
: Set the subtitles font color in RRGGBBAA format. Default: FFFFFF.

**\-\-sub\_size \<px\>**
: Set the subtitles font size. Default: 16.

**\-\-sub\_border \<type\>**
: Set the subtitles border: **0=none**, **1=Outline** (Default) *\-\-sub\_oc* for outline color, **3=Each line in one box** *\-\-sub\_oc* for background color, **4=All lines in a box** *\-\-sub\_bc* for background color.

**\-\-sub\_bc \<color\>**
: Set the background color on border=4 in RRGGBBAA format. Example: *\-\-sub\_oc 00000055* for tranparent black background. Default: *00000000*.

**\-\-sub\_oc \<color\>**
: Set the outline and background color on border=3 in RRGGBBAA format. Default: *00000000*.

**\-\-sub\_force\_ass \[0\|1\]**
: Ignores the style inside the ass and force the configured style.

**\-\-sub\_custom \<string\>**
: Custom options for [subtitles ffmpeg filter](https://ffmpeg.org/ffmpeg-filters.html#subtitles-1).

## Subtitle parameters

**\-\-lang \<code\>**
: Force subtitle language in two- or three-letter ISO 639 code. This option is used when the subtitle file does not have the language in the name, so that it is displayed in the text.s

## Subtitle shortcuts

**\-\-time**
: Set time mode of operation.

**\-\-line**
: Set line mode of operation.

**\-\-srt**
: Force srt subtitle file format.

**\-\-ass**
: Force ass subtitle file format.

**\-\-vtt**
: Force vtt subtitle file format.

**\-\-green**
: Simple style, green subtitles text. Same as *\-\-sub\_color 00FF00*.

**\-\-yellow**
: Simple style, yellow subtitles text. Same as *\-\-sub\_color FFEA00*.

**\-\-small**
: Simple style, small subtitle text. Same as *\-\-sub\_size 12*.

**\-\-big**
: Simple style, big subtitle text. Same as *\-\-sub\_size 24*.

**\-\-verybig**
: Simple style, very big subtitle text. Same as *\-\-sub\_size 32*.

**\-\-box**
: Simple style, subtitle text inside a black box. Same as *\-\-sub\_border 4*.

**\-\-tbox**
: Simple style, subtitle text inside a black transparent box. Same as *\-\-sub\_border 4 \-\-sub\_bc 00000050*.

## Text block options

**\-\-tb \<pos\>**
: Location of text block, top, bottom or hidden

**\-\-tb\_title \<text\>**
: Set the title, default use the subtitle file name, can use [text substitution](#text-substitution-format).

**\-\-tb\_border \<px\>**
: Border size in pixels around the text and the logo.

**\-\-tb\_add\_height \<px\>**
: Adds this number of pixels to the height automatically calculated based on the text. It can be used in combination with text alignments to separate texts.

**\-\-tb\_comments \<text\>**
:Set the comment text, can use [text substitution](#text-substitution-format). It is a text that appears at the end of the text block and in which you can put anything. By default it contains the text of "Made with...".

**\-\-tb\_font \<font\>**
: Set the font for text header.

**\-\-tb\_fonts\_dir \<dir\>**
: Set the additional directory to search for fonts.

**\-\-tb\_font\_size \<px\>**
: Set the font height for text header in pixels.

**\-\-tb\_font\_color \<color\>**
: Set the color of the font for text header in RRGGBBAA format.

**\-\-tb\_font\_border \<px\>**
: Set the width in pixels of the border around the font.

**\-\-tb\_font\_border\_color \<color\>**
: Set the color of the border around the font.

**\-\-tb\_font\_border\_blur \<amount\>**
: When it is greater than 0 it blurs the border of text. Default 0. The higher the amount the more out of focus the border will be.

**\-\-tb\_font\_shadow \<px\>**
: Shadow offset in pixels with respect to the text, Default 0 (disabled).

**\-\-tb\_font\_shadow\_color \<color\>**
: Color of shadow, Default **black**.

**\-\-tb\_font\_italic \[0\|1\]**
: Italicizes the text. Default 1.

**\-\-tb\_font\_bold \[0\|1\]**
: Sets the text to bold. Default *0*.

**\-\-tb\_font\_pos \<position\>**
: Alignment of the text within the block, it can be: *up\_left, up\_right, down\_left, down\_right, up\_center, down\_center, middle\_left, middle\_right, middle\_center*. Default *up\_left*.

**\-\-tb\_font\_left\_margin \<px\>**
: Margin to the left in pixels of the text. Default *0*.

**\-\-tb\_font\_right\_margin \<px\>**
: Margin to the right in pixels of the text. Default *0*.

**\-\-tb\_font\_vertical\_margin \<px\>**
: Margin in pixels applied to the text, when the text is aligned up it is the top margin, when aligned down it is the bottom margin. Default *0*.

**\-\-tb\_title\_font \<font\>**
: Set the font for the title. The default value is **""** (empty), When empty use the value of *\-\-tb\_font*.

**\-\-tb\_title\_font\_size \<px\>**
: Set the font height for title in px. The default value is **0**, When **0** use the value of *\-\-tb\_font\_size*.

**\-\-tb\_title\_color \<color\>**
: Set the color of the font for the title in RRGGBBAA format.

**\-\-tb\_title\_border \<px\>**
: Set the width in pixels of the border around the title.

**\-\-tb\_title\_border\_color \<color\>**
: Set the color of the border around the title.

**\-\-tb\_title\_border\_blur \<amount\>**
: When it is greater than 0 it blurs the border of title. Default 0. The higher the amount the more out of focus the border will be.

**\-\-tb\_title\_shadow \<px\>**
: Shadow offset in pixels for the title with respect to the text, Default *0* (disabled).

**\-\-tb\_title\_shadow\_color \<color\>**
: Color of shadow for the title, Default **black**.

**\-\-tb\_title\_italic \[0\|1\]**
: Italicizes the title. Default 0.

**\-\-tb\_title\_bold \[0\|1\]**
: Sets the title to bold. Default 1.

**\-\-tb\_title\_pos \<position\>**
: Alignment of the title within the block, it can be: *up\_left, up\_right, down\_left, down\_right, up\_center, down\_center, middle\_left, middle\_right, middle\_center*. Default *up\_left*.

**\-\-tb\_title\_left\_margin \<px\>**
: Margin to the left in pixels of the title text. Default *0*.

**\-\-tb\_title\_right\_margin \<px\>**
: Margin to the right in pixels of the title text. Default *0*.

**\-\-tb\_title\_vertical\_margin \<px\>**
: Margin in pixels applied to the title, when the title is aligned up it is the top margin, when aligned down it is the bottom margin. Default *0*.

**\-\-tb\_comments\_font \<font\>**
: Set the font for the comments. The default value is **""** (empty), When empty use the value of *\-\-tb\_font*.

**\-\-tb\_comments\_font\_size \<px\>**
: Set the font height for comments in px. The default value is **0**, When **0** use the value of *\-\-tb\_font\_size*.

**\-\-tb\_comments\_color \<color\>**
: Set the color of the font for the comments.

**\-\-tb\_comments\_border \<px\>**
: Set the color of the border around the comments.

**\-\-tb\_comments\_border\_color \<color\>**
: Set the color of the border around the comments.

**\-\-tb\_comments\_border\_blur \<amount\>**
: When it is greater than 0 it blurs the border of comments. Default 0. The higher the amount the more out of focus the border will be.

**\-\-tb\_comments\_shadow \<px\>**
: Shadow offset in pixels for the comments with respect to the text, Default *0* (disabled).

**\-\-tb\_comments\_shadow\_color \<color\>**
: Color of shadow for the comments, Default **black**.

**\-\-tb\_comments\_italic \[0\|1\]**
: Italicizes the comments. Default *1*.

**\-\-tb\_comments\_bold \[0\|1\]**
: Sets the comments to bold. Default *0*.

**\-\-tb\_comments\_pos \<position\>**
: Alignment of the comments within the block, it can be: *up\_left, up\_right, down\_left, down\_right, up\_center, down\_center, middle\_left, middle\_right, middle\_center*. Default *up\_left*.

**\-\-tb\_comments\_left\_margin \<px\>**
: Margin to the left in pixels of the comments text. Default *0*.

**\-\-tb\_comments\_right\_margin \<px\>**
: Margin to the right in pixels of the comments text. Default *0*.

**\-\-tb\_comments\_vertical\_margin \<px\>**
: Margin in pixels applied to the comments, when the comments is aligned up it is the top margin, when aligned down it is the bottom margin. Default *0*.

**\-\-tb\_bg\_color \<color\>**
: Set the background color for text header in RRGGBBAA format.

**\-\-tb\_bg\_gradient \<gradient\>**
: Generates a gradient as a background image for the text block with the indicated parameters. The explanation of how to define a gradient is in the section [Gradients](gradients).

**\-\-tb\_bg\_image \<file\>**
: Set the background image for the text header, if the image is smaller than the space to be occupied, it is repeated, if it is larger, it is cropped.

**\-\-tb\_bg\_image\_halign \<haling\>**
: Horizontal alignment of background image, can be right, center, left or a percentage. Default *center*.

**\-\-tb\_bg\_image\_valign \<valing\>**
: Vertical alignment of background image, can be up, middle, down or a percentage. Default *middle*.

**\-\-tb\_bg\_image\_scale \[0\|1\]**
: Scale the background image before crop it. Default *0*.

**\-\-tb\_bg\_image\_tile \[0\|1\]**
: When the background image is smaller than the result it repeats it in tiles to fill the necessary space. Default *1*.

**\-\-tb\_bg\_image\_blur \<number\>**
: Applies a blur filter to the background image. the higher the number the more blurred. Default 0 (disabled).

**\-\-tb\_bg\_image\_exposure \<number\>**
: Applies an exposure filter to the background image. allows a number between *-300* and *300*, negative numbers decrease the exposure making the image darker, positive numbers make the image lighter. Default *0* (disabled).

**\-\-tb\_bg\_image\_saturation \<number\>**
: Applies a saturation filter to the background image. Accepts a number between *0* and *300*, *0* converts the image to black and white. Default *100* (disabled).

**\-\-tb\_bg\_image\_opacity \<number\>**
: Percentage of transparency of the background image with respect to the other backgrounds (solid color, gradient...). *100* is opaque, *0* totally transparent. Default *100*.

**\-\-tb\_bg\_image\_mask \<color|gradient|file\>**
: Mask applied to the background image to define the transparency of each part of the image. It can be a solid color (the whole image has the same opacity), a gradient as explained in [Gradients](gradients). Or an image file. In this option, white means totally opaque and black means totally transparent.

**\-\-tb\_logo \<file\>**
: Put this logo in the header aligned to the right and rescaled to the header height.

**\-\-tb\_logo\_height \<px\>**
: Force the logo to be this height in pixels, enlarge the entire text block if necessary.

**\-\-tb\_logo\_pos \<left\|right\>**
: Position of the logo in the text block, it can be right or left. Default right.

**\-\-tb\_logo\_text\_overlay \[0\|1\]**
: Allows the text to overlay the logo. Default 0.

**\-\-tb\_logo\_transparent\_color \<color\>**
: Makes this color transparent in the text block logo, RRGGBB format. Empty by default.

**\-\-tb\_logo\_transparent\_similarity \<number\>**
: Percentage of similarity with *\-\-tb\_logo\_transparent\_color* to consider a color transparent. from *1* to *100*. Default *1*.

**\-\-tb\_logo\_transparent\_blend \<number\>**
: Percentage of transparency for pixels that fall outside the similarity. from *0* to *100*. Default *0*.

**\-\-tb\_cover\_top\_image \<file\>**
: Displays this cover on top of the text block, automatically adjusts it to the width respecting the aspect ratio.

**\-\-tb\_video\_info \[0\|1\]**
: Force video info in header, when show subtitles info, video info is hidden. This option force to show it. Default *0*.

**\-\-tb\_chapters\_list \[0\|1\]**
: Enables the display of the list of chapters in the video file, disabled only shows its number. Default *0*.

**\-\-tb\_hide\_video\_info \[0\|1\]**
: Don't show video information in text header. Default *0*.

**\-\-tb\_hide\_sub\_info \[0\|1\]**
: Don't show subtitle information in text header. Default *0*.

**\-\-tb\_hide\_vr\_warning \[0\|1\]**
: Don't show the warning when enable VR options. Default *0*.

**\-\-tb\_hide\_vr\_text \[0\|1\]**
: Don't show the VR format in text header. Default *0*.

**\-\-tb\_custom\_subtitle \<text\>**
: Personalize the text displayed when the input is a subtitle. Uses [text substitution format](#text-substitution-format).

**\-\-tb\_custom\_header \<text\>**
: Personalize the text displayed when the input is a video, only the basic part. Uses [text substitution format](#text-substitution-format).

**\-\-tb\_custom\_vr \<text\>**
: Personalize the text displayed when the input is a VR video. Uses [text substitution format](#text-substitution-format) and the autodetection of [VR options](#vr-options).

**\-\-tb\_custom\_video\_stream \<text\>**
: Personalize video stream text lines. Uses [text substitution format](#text-substitution-format).

**\-\-tb\_custom\_audio\_stream \<text\>**
: Personalize audio stream text lines. Uses [text substitution format](#text-substitution-format).

**\-\-tb\_custom\_subtitle\_stream \<text\>**
: Personalize subtitles stream text lines. Uses [text substitution format](#text-substitution-format).

**\-\-tb\_custom\_attachment\_stream \<text\>**
: Personalize attachments text lines. Uses [text substitution format](#text-substitution-format).

**\-\-tb\_custom\_chapters \<text\>**
: Personalize the chapters count text. Uses [text substitution format](#text-substitution-format).

**\-\-tb\_custom\_chapters\_list \<text\>**
: Personalize the list of chapters text lines. Uses [text substitution format](#text-substitution-format).

## Text block parameters

**\-\-tb\_comment\_append \<text\>**
: Adds the specified text after the current comment text, can use [text substitution](#text-substitution-format). Useful to add comments specific to a particular file without affecting the general comments for all.

**\-\-tb\_comment\_prepend \<text\>**
: Adds the specified text before the current comment text, can use [text substitution](#text-substitution-format).

**\-\-tb\_list\_vars**
: Lists the available variables and their value for use in [text substitution](#text-substitution-format). Terminates without doing anything.

**\-\-tb\_use\_mediainfo**
: Activate mediainfo and set the *--tb_custom_\** options to use mediainfo information.

## Text block Shortcuts

**\-\-tb\_transparent**
: Set the background transparent. Same as *\-\-tb\_bg\_color 00000000 \-\-grid\_border\_color 00000000 \-\-format png*

**\-\-tb\_left**
: Aligns text to the left. Same as *\-\-tb\_title\_pos up\_left \-\-tb\_font\_pos middle\_left \-\-tb\_comments\_pos down\_left \-\-tb\_logo\_pos right*

**\-\-tb\_right**
: Aligns text to the right. Same as *\-\-tb\_title\_pos up\_right \-\-tb\_font\_pos middle\_right \-\-tb\_comments\_pos down\_right \-\-tb\_logo\_pos left*

## Frame times options
 This options controls the position and aspect of the time inside each frame

**\-\-ft\_hide \[0\|1\]**
: Hide the time of frames.

**\-\-ft\_overlay \[0\|1\]**
: Prints the time of frames on top of the image. By default it is enabled, when not enabled it enlarges the image and fills the space with the background color. It can only be deactivated when the text follows the same direction as the edge in which it is aligned.

**\-\-ft\_font \<font/file\>**
: Font for the time of frames.

**\-\-ft\_color \<color\>**
:Color of text for the time of frames, RRGGBBAA format or color name. Default *white*.

**\-\-ft\_font\_border \<px\>**
: Set the width in pixels of the border around font. Default: *0*.

**\-\-ft\_font\_border\_color \<color\>**
: Set the color of the border around the font. Default: *black*.

**\-\-ft\_shadow\_color \<color\>**
: Set the color of the shadow of the font. Default: *black*.

**\-\-ft\_shadow\_x \<px\>**
: Set the offset on the horizontal axis of the shadow with respect to the font. Default: *0*.

**\-\-ft\_shadow\_y \<px\>**
: Set the offset on the vertical axis of the shadow with respect to the font. Default: *0*.

**\-\-ft\_bg \<color\>**
: Background color for the time of frames, RRGGBBAA format or color name. Default: *00000090*.

**\-\-ft\_size \<px\>**
: Text size for the time of frames. Default *12*.

**\-\-ft\_border \<px\>**
: Border in pixels for the time of frames. Default *2*.

**\-\-ft\_margin \<px\>**
: Pixel spacing of the edges of the screen. Default *0*.

**\-\-ft\_pos \<pos\>**
: Time of frames position up_left, up_right, down_left, down_right, up_center, down_center, middle_left, middle_right.

**\-\-ft\_vertical \<direction\>**
: Print the time of frames in vertical position, it can be: *disabled*, *upward*, *downward*. Default: *disabled*.

**\-\-ft\_mask\_layer\_pos \<pos\>**
: Controls where to print frame time with respect to applying the image mask. Only has effect when the **\-\-mask** is configured. Default: *before*.

	- **before**: Before applying the mask. Mask also affects frame time.
	- **after**: After applying the mask. Mask does not affect frame time.
	- **inside**: Prints it on the mask. Instead of showing directly, it modifies the opacity of the image.

**\-\-ft\_format \<format\>**
: Configures how the time appears in each frame, uses the same format characters as the C++ [strftime function](https://cplusplus.com/reference/ctime/strftime/) or the following fixed values:

	- **HH:MM:SS.mmm**: Full time with milliseconds, Default, same as empty.
	- **HH:MM:SS**: Full time. Same as %T or %H:%M:%S.
	- **MM:SS**: Displays only minutes and seconds. Same as %M:%S.
	- **short**: For durations of less than a hour MM:SS, if not HH:MM:SS.
	- **seconds**: Number of seconds from start.
	- **miliseconds**: Number of miliseconds from start.

**\-\-ft\_custom \<text\>**
: Time of frames custom filter [drawtext options](https://ffmpeg.org/ffmpeg-filters.html#drawtext-1)

## VR options
This options allow you to correct the image of a VR video so that its content can be better appreciated. It can correct the distortion of the different VR video formats, but by default it simply discards the content of one eye so that the screenshots do not show the double image.

When active, a warning message is added to the text block. to explain that the screenshots were altered and do not represent the exact content of the video, you can delete it with the option *\-\-tb\_hide\_vr\_warning*.

It mainly uses the [ffmpeg v360 filter](https://ffmpeg.org/ffmpeg-filters.html#v360).

The program is able to detect whether a video is VR or not by the file name and metadata. This detection is always enabled, but it only covers **vr.file**, **vr.format**, **vr.fov** and **vr.mode** variables, it does not apply to options. This detection it is not always accurate, because the file names and metadata do not always contain the necessary information. There is also no standard, for file names only text strings that are commonly used, like for example mkx200, fisheye190, 180x180, etc. And metadata not contains fov information. When there is insufficient data, but it appears to be a VR video, the detection makes assumptions based on the most common case.

**\-\-vr \[0\|1\]**
: Activate the processing of the screenshots to better appreciate the content, you can correct the projection distrosion used in VR video or cut the image to show only one eye view. When disabled, none of these options have any effect on screenshots. By default it is disabled.

**\-\-vr\_auto\_enable \[0\|1\]**
: Automatically activates the VR and fill input options when it detects a VR video. Default *0*.

**\-\-vr\_disable\_autodetection \[0\|1\]**
: Disables VR video detection completely. Default *0*.

**\-\-vr\_detect\_ar \[0\|1\]**
: When enabled, it uses the aspect ratio of the video to detect whether it is a VR video or not, when the aspect ratio is 2, 1 or 0.5 it is considered VR video, 180SBS, 360TB and 180TB respectively. It does not recognize many formats, but should be sufficient for use with *\-\-vr\_only\_crop*. Default *0*.

**\-\-vr\_config \<name|file\>**
: Load this configuration when a VR video input was detected, this config has the same restrictions as *\-\-add\_config*.

**\-\-vr\_only\_crop \[0\|1\]**
: If set to 1 it cuts only one of the eyes of the video, and deactivates the distortion correction. Default 1. This option is intended to make unmodified screenshots but only one eye. Uses the [crop filter](https://ffmpeg.org/ffmpeg-filters.html#crop). It is only affected by the options *\-\-vr\_in\_stereo* to know if the cut is vertical or horizontal, and *\-\-vr\_right\_eye* to chose the eye.

**\-\-vr\_in \<format\>**
: Input image format. Default *hequirect*.

**\-\-vr\_out \<format\>**
: Input format. Default *flat*.

**\-\-vr\_in\_stereo \<stereo\>**
: Input stereo mode, can be: *sbs*(default), *tb*, *2d*.

**\-\-vr\_out\_stereo \<stereo\>**
: Output stereo mode, Same options than input. Default *2d*.

**\-\-vr\_ih\_fov \<degrees\>**
: Set the input horizontal field of view. Default *180*.

**\-\-vr\_iv\_fov \<degrees\>**
: Set the input vertical field of view. Default *180*.

**\-\-vr\_d\_fov \<degrees\>**
: Set the output diagonal field of view. Default *130*.

**\-\-vr\_h\_fov \<degrees\>**
: Set the output horizontal field of view. Default *0*.

**\-\-vr\_v\_fov \<degrees\>**
: Set the output vertical field of view. Default *0*.

**\-\-vr\_yaw \<degrees\>**
: Set rotation for the output video. Default *0*.

**\-\-vr\_pitch \<degrees\>**
: Set rotation for the output video. Default *0*.

**\-\-vr\_roll \<degrees\>**
: Set rotation for the output video. Default *0*.

**\-\-vr\_w \<degrees\>**
: Set the output resolution width. Default the same as size.

**\-\-vr\_h \<degrees\>**
: Set the output resolution height. Default the same as size.

**\-\-vr\_interp \<int\_method\>**
: Interpolation method. Default *lanczos*.

**\-\-vr\_h\_flip \[0\|1\]**
: Flip the output video horizontally.

**\-\-vr\_right\_eye \[0\|1\]**
: Use the right eye, by default use the left. This disable the in_stereo and uses crop to select the correct eye.

**\-\-vr\_aditional \<options\>**
: Pass aditional options to v360 ffmpeg filter.

## VR shortcuts
These parameters force the values of the VR detection and control the text over the formatting. But unless VR mode is enabled *(\-\-vr or \-\-vr\_auto\_enable)* they do not change the image in the screenshots.

**\-\-vr.format \<format\>**
: Sets the variable to configure the format. It can be **equirect** or **fisheye**. This variable is used to detect a VR video, if it is empty the video is not considered VR independently of the value of the other variables. Similar to *\-\-var "vr.format=\<format\>"* but this shortcut avoids the auto detection and the *\-\-var* overwrites it.

**\-\-vr.mode \<mode\>**
: Sets the variable to configure the 3D mode. I can be **tb**, **sbs** or **2d**. Similar to *\-\-var "vr.mode=\<mode\>"* but this shortcut avoids the auto detection and the *\-\-var* overwrites it.

**\-\-vr.fov \<FOV\>**
: Sets the variable to configure the FOV. I can be **tb**, **sbs** or **2d**. Similar to *\-\-var "vr.fov=\<FOV\>"* but this shortcut avoids the auto detection and the *\-\-var* overwrites it.

**\-\-fisheye200**
: For fisheye 200FOV video files. Same as *\-\-vr.format fisheye \-\-vr.mode sbs \-\-vr.fov 200*.

**\-\-fisheye190**
:  For fisheye 190FOV video files. *\-\-vr.format fisheye \-\-vr.mode sbs \-\-vr.fov 190*

**\-\-vr180**
: For equirectangular 180º SBS video files. Same as *\-\-vr.format equirect \-\-vr.mode sbs \-\-vr.fov 180*.

**\-\-vr360**
: For equirectangular 360º TB video files. Same as *\-\-vr.format equirect \-\-vr.mode tb \-\-vr.fov 360*.

## Batch mode parameters
In this mode the specified program is executed for each of the input files, if a program is not specified it is itself, if -r and a folder are specified it executes for each file found in folders and sub-folders. It can work as a daemon or service searching for new files in the specified folders.

You can add the parameter %file% to the parameters to indicate the position of
the path to the file within the program parameters, otherwise it will be the first parameter. Other parameters are also available, they can be consulted at the end of this section of the manual.

You can run several programs in parallel by specifying **\-p \<n\>**, n can be a number between 1 and 16. Not to be confused with the normal **\-p** or **\-\-process** parameter, they are independent and complementary options.

Example:

    subtitles_contact_sheet --batch /path/to/folder -r --video -- --config normal

Runs the command *subtitles\_contact\_sheet \-\-config normal* on each video file (mkv, mp4, avi, mov, wmv or m4v) found in folders and subfolders starting from the end of the path. Note the use of the **\-\-** parameter to separate batch mode parameters from parameters passed to the program being executed by batch mode.

If you use the **\-\-** parameter more than once, the program will run as many times as there are **\-\-** parameters with the following parameters passed.

Example:

    subtitles_contact_sheet --batch file.mkv file2.mkv -- --config normal -- --screenshots

Is the same as execute:

    subtitles_contact_sheet file.mkv --config normal
    subtitles_contact_sheet file.mkv --screenshots
    subtitles_contact_sheet file2.mkv --config normal
    subtitles_contact_sheet file2.mkv --screenshots

Options:

**\-r**
: Recursive through the specified directories.

**\-f**, **\-\-file \<tile_types\>**
: Select this files only, example "*.mkv". It is possible to put as many -f as required.

**\-t**, **\-\-time \<seconds\>**
: Searches for new files every seconds. This means that the program never ends by itself.

**\-w**, **\-\-watch**
: Uses file system events to detect when there are changes inside the folders.

**-m**, **\-\-modified \<seconds\>**
: Only select files with modification time older than this. This option avoids processing files that are currently being written. Default for windows: 10, on linux 1.

**\-\-recheck \<seconds\>**
: Recheck incomplete files after this time. 0 to disable. Default for windows: 10, on linux 0.

**\-d**, **\-\-delete \<seconds\>**
: Time in seconds when it performs a check of the deleted files to remove them from its internal list. Default 3600.

**\-q**, **\-\-quiet**
: Hide all messages, except for errors.

**\-\-videofiles**
: Same as *-f '\*.mkv' -f '\*.mp4' -f '\*.avi' -f '\*.mov' -f '\*.wmv' -f '\*.m4v'*... etc. See *\-\-list\_extensions* for see the complete list.

**\-\-subfiles**
: Same as *-f '\*.srt' -f '\*.vtt' -f '\*.ass'*.

**\-p**, **\-\-parallel \<1-16\>**
: Launch this number of processes in parallel.

**\-\-idle**
: Launch processes with idle priority. Using this is not necesary to use the *\-\-idle* normal option.

**\-\-no_fail**
: Continue even if a process fail.

**\-\-cmd**
: Show commands without executing them.

**\-\-show\_output**
: Show the output of te executed commands, only works using *--parallel 1*.

**\-\-command \<program\>**
: Program to launch. When not specified the program that launches is itself. If it is used before the first *\-\-* it applies to all commands, if it is used after it, only to those commands where it appears.

**\-\-**
: Separator, the following parameters are passed to the program to be executed

**%file%**
: Defines te position of file in the params.

**%filename%**
: Input file name, without path and extension.

**%path%**
: The path to the input file.

**%ext%**
: The extension of the input file.

## Rename options

These options allow you to rename, move, copy, create symbolic and hard links from input video and available variables read by ffprobe and mediainfo. When enabled, the program only performs this action, leaving all other unrelated options without effect. It is especially useful in conjunction with batch processing.

It uses the input video and processes **\-\-rename\_dest** using [text substitution](#text-substitution-format) to generate the destination path. The destination path may contain folders also processed by text substitution. This way it can be used to organize into folders and not just rename. Once the destination has been calculated, execute the action indicated in **\-\-rename\_type**. It is highly recommended to start using it first using *print*, to get familiar with the program and then test it by creating links of any kind so as not to affect the original files.

**\-\-rename \[0\|1\]**
: Enables video renaming.

**\-\-rename\_type \<type\>**
:   Operation on the file system that will be executed, Can be:

    * **print**: Shows renaming information without really doing it, Default.
    * **move**: Move the original file to destination.
    * **copy**: Copy the original file to destination.
    * **symlink**: Creates a symbolic link. (Windows requires administrator privileges or developer mode enabled).
    * **hardlink**: Creates a hard link.
    * **shortcut**: Works only in Windows, creates *.lnk* file.
    * **link**: Try to create a hardlink, if it doesn't work try to create a symlink and in windows it doesn't work create a shortcut.
    * **fastcopy**: Try to create a hardlink, if it doesn't work copy the file.

**\-\-rename\_dest \<text\>**
: Path to the output file for renaming. Use [text substitution](#text-substitution-format).

**\-\-rename\_overwrite \[0\|1\]**
: When activated it overwrites the output if it already exists. By default it fails.

**\-\-rename\_mkdir \[0\|1\]**
: When activated it creates all the intermediate folders to reach the output file. By default it fails if a folder that does not exist is used.

## Images options

These options allow you to configure the behavior and appearance of contact sheets and animations generated from image folders. All options here are specific and only apply when using a folder with images as input. Apart from these options there are many other generic options that apply here, such as those for the text block *(\-\-tb...)* or for animations *(\-\-concat, \-\-video_fps,...)*.

**\-\-images\_search\_filter \<extensions\>**
: List of filters separated by | to search for image files in the specified folder. Normally it is a list of file extensions in this form *\*.jpg*, but you can also include part of the name to search for more specific files. By default it includes a list of the most common image extensions.

**\-\-images\_recursive \[0\|1\]**
: When it is 1 go into the subfolders to search for image files. If not, just look for the files that are directly inside the specified folder. Default *0*.

**\-\-images\_select \<select\>**
: Method for selecting the images to show. Can be:

	* **range**: Divide the list of images into ranges and select the first of each. Default.
	* **random_range**: Divide the list of images into ranges and select one using a random number.
	* **random_ordered**: Selects completely random images and displays them in order.
	* **random**: Selects completely random images and displays them in random order as well.

**\-\-images\_format\_stats \[0\|1\]**
: When it is 1, the program shows statistics of all image formats found. This does not increase the processing time. Default *1*.

**\-\-images\_resolution\_stats \[0\|1\]**
: When it is 1, the program collects statistics of all images resolution found. As it has to read all the images, this option greatly increases the time it takes for the program to generate the contact sheet. Default *0*.

**\-\-images\_title \<text\>**
: Set the title, default use the forder name, can use text substitution.

**\-\-images\_text \[0\|1\]**
: Displays the individual text information of the selected images. Normally the file name. Default *1*.

**\-\-images\_custom\_text \<text\>**
: Set the text of the selected images. By default the file name, can use text substitution but uses a diferent variables especific to images:

	* **file.filename**: Image file name.
	* **file.size**: Size in bytes.
	* **file.basename**: Image file name without extension.
	* **file.extension**: Extemsion of the image file.
	* **file.dir**: Relative directory where the image file is located.
	* **file.absolute**: Absolute directory where the image file is located.
	* **image.width**: Image width in pixels.
	* **image.height**: Image height in pixels.
	* **image.codec_name**: Codec provided by ffprobe.
	* **image.codec_long_name**: Description of the codec provided by ffprobe.
	* **image.ar_resolution**: Aspect ratio, the same as width divided by height.

**\-\-images\_max\_text \<number\>**
: Maximum number of characters of the text of each selected image. When a text exceeds this number of characters, the first **39** characters and the last **38** characters are selected, separated by **...**.  Default *80*.

**\-\-images\_text\_overlay \[0\|1\]**
: Prints the text on top of the image. Default *0*.

**\-\-images\_text\_padding \[0\|1\]**
: When enabled the text appears aligned to the margin added to the image to adjust the aspect ratio, instead of being aligned to the image itself. Default *0*.

**\-\-images\_ar\_selection \<%\>**
: When the percentage of images with a different orientation exceeds this number, all images are enlarged to fill the 1:1 aspect ratio. Default *25*. Examples:

	* *0:* always uses the most commonly used aspect ratio,
	* *50 to 100:* always uses 1:1.
	* 25:* apply 1:1 when there are 25% or more images with different orientation (vertical/horizontal).

**\-\-images\_font \<font\>**
: Font for the text of selected images.

**\-\-images\_color \<color\>**
: Color of text of selected images, RRGGBBAA format or color name. Default *white*.

**\-\-images\_font\_border \<px\>**
: Set the width in pixels of the border around font. Default *0*.

**\-\-images\_font\_border_color \<color\>**
: Set the color of the border around the font. Default *black*.

**\-\-images\_shadow\_color \<color\>**
: Set the color of the shadow of the font. Default *black*.

**\-\-images\_shadow\_x \<px\>**
: Set the offset on the horizontal axis of the shadow with respect to the font. Default *0*.

**\-\-images\_shadow\_y \<px\>**
: Set the offset on the vertical axis of the shadow with respect to the font. Default *0*.

**\-\-images\_bg \<color\>**
: Background color of selected images. Default *00000090*.

**\-\-images\_size \<px\>**
: Text size of selected images. Default *12*.

**\-\-images\_border \<px\>**
: Border in pixels of selected images. Default *2*.

**\-\-images\_margin \<px\>**
: Pixel spacing of the edges of the screen. Default *0*.

**\-\-images\_pos \<pos\>**
: Text of selected images position up_left, up_right, down_left, down_right, up_center, down_center, middle_left, middle_right. Default *down_center*.

**\-\-images\_vertical \<direction\>**
: Print the text of selected images in vertical position, it can be: disabled, upward, downward. Default *disabled*.

**\-\-images\_fill\_color \<color\>**
: Color to fill the enlarged images. Can be:

	* *Color:* Use this specific [color](#color-format).
	* *grid_border:* to use the same color as the grid (default).
	* *image_fill:* to use a blured and altered version of the image.

**\-\-images\_fill\_custom \<filter\>**
: Custom ffmpeg filter to the fill area.

## Watermark

These options permit the addition of an image on top of the final output result.

**\-\-watermark \<file\>**
: Set the watermark image for the entire contact sheet, if the image is smaller than the space to be occupied, it is repeated, if it is larger, it is cropped.

**\-\-watermark\_halign \<haling\>**
: Horizontal alignment of watermark image, can be right, center, left or a percentage. Default *center*.

**\-\-watermark\_valign \<valing\>**
: Vertical alignment of watermark image, can be up, middle, down or a percentage. Default *middle*.

**\-\-watermark\_scale \[0\|1\]**
: Scale the watermark image before crop it. Default *0*.

**\-\-watermark\_tile \[0\|1\]**
: When the watermark image is smaller than the result it repeats it in tiles to fill the necessary space. Default *1*.

**\-\-watermark\_blur \<number\>**
: Applies a blur filter to the watermark image. the higher the number the more blurred. Default *0* (disabled).

**\-\-watermark\_exposure \<number\>**
: Applies an exposure filter to the watermark image. allows a number between *-300* and *300*, negative numbers decrease the exposure making the image darker, positive numbers make the image lighter. Default *0* (disabled).

**\-\-watermark\_saturation \<number\>**
: Applies a saturation filter to the watermark image. Accepts a number between *0* and *300*, *0* converts the image to black and white. Default *100* (disabled).

**\-\-watermark\_opacity \<number\>**
: Percentage of transparency of the watermark image. *100* is opaque, *0* totally transparent. Default *100*.

**\-\-watermark\_transparent\_color \<color\>**
: Makes this color transparent in the watermark, RRGGBB format. Empty by default.

**\-\-watermark\_transparent\_similarity \<number\>**
: Percentage of similarity with *\-\-watermark\_transparent\_color* to consider a color transparent. from *1* to *100*. Default *1*.

**\-\-watermark\_transparent\_blend \<number\>**
: Percentage of transparency for pixels that fall outside the similarity. from *0* to *100*. Default *0*.

# OUTPUT FORMATS

Because different formats support different options, here are case-by-case explanations of each format and what they can and cannot do, specifying which options are supported.

## jpg, jpeg

It is the default format, used with **\-\-format jpg** or with the shortcut **\-\-jpg**. Can use the **\-\-output\_quality \<number\>** option to adjust the quality and size of the output file, the supported values range **from 1 to 31**, lower is better quality, and with the default value *-1* is the same as *3*.

Only images are supported, so can't use video nor audio options, such as **\-\-video_preview**, **\-\-concat**, **\-\-enable_audio**, among others. Does not support transparency in the final result, which means that if after processing all layers there are still transparent areas, these are lost. Suported **\-\-pixel\_format**: *yuvj420p yuvj422p yuvj444p yuv420p yuv422p yuv444p*.

[https://en.wikipedia.org/wiki/JPEG](https://en.wikipedia.org/wiki/JPEG)

## png

Used with **\-\-format png** or with the shortcut **\-\-png** and the default format used in **\-\-screenshots** and **\-\-tb_transparent**. It is a lossless format, so **\-\-output\_quality** has no effect.

Only images are supported, but there is a version which supports animations, [see the next format apng](#apng). Supports transparency and the **\-\-pixel\_format**: *rgb24 rgba rgb48be rgba64be pal8 gray ya8 gray16be ya16be monob*.

[https://en.wikipedia.org/wiki/PNG](https://en.wikipedia.org/wiki/PNG)

## apng

Used with **\-\-format apng** or with the shortcut **\-\-apng**. It is the same as [png](#png) but supports animations (**\-\-video_preview**, **\-\-concat**), but not audio (**\-\-enable_audio**).

[https://en.wikipedia.org/wiki/APNG](https://en.wikipedia.org/wiki/APNG)

## gif

Used with **\-\-format gif** or with the shortcuts **\-\-gif**, **\-\-gif\_images**, **\-\-gif\_clip** and **\-\-gif\_singles**. Although it is a lossless format, since it only supports color palettes of 256 colors, it cannot be considered as such for practical purposes. Does not support **\-\-output\_quality** but [gitlossy](#gitlossy) does. This version uses a palette for each frame of 265 colors.

Although it supports images and animations, it is not usually used for images because jpg and png are much better. It only supports 1-bit transparency, it means that a pixel can only be totally transparent or totally opaque. No audio support.

[https://en.wikipedia.org/wiki/GIF](https://en.wikipedia.org/wiki/GIF)

## giflow

Used with **\-\-format giflow** or with the shortcut **\-\-giflow**, is the same as [gif](#gif) but uses the same 256 color palette for the entire file.

## giflossy

Used with **\-\-format giflossy** or with the shortcut **\-\-giflossy**. is the same as [gif](#gif) and produced the same output file but also generate another file, the name ends in **.lossy.gif**, which is a lower quality and smaller version. It is necessary to install and configure the [gifsicle program](https://www.lcdf.org/gifsicle/) (**\-\-gifsicle** option) for this format to work. Can use the **\-\-output\_quality \<number\>** option to adjust the quality and size of the output file, the supported values range **from 1 to 1000**, lower is better quality, and with the default value *-1* is the same as *120*.

As this format generates 2 output files, the **\-\-no\_overwrite** option only works for the lossless version, so if run a second time with **\-\-no\_overwrite** it will only regenerate the lossy file. This way it is possible to regenerate the lossy version and adjust the **\-\-output\_quality** quickly.

## webp

Used with **\-\-format webp** or with the shortcut **\-\-webp**. Can use the **\-\-output\_quality \<number\>** option to adjust the quality and size of the output file, the supported values range **from 1 to 100**, like a % of quality, higher is better quality, and with the default value *-1* is the same as *80*. For a lossless output use [losslesswebp](#losslesswebp).

Only supports images, although technically it also supports animations, these will only be played once, with the last frame remaining fixed, use [webploop](#webploop) for animations. Supports transparency and the **\-\-pixel\_format**: *bgra yuv420p yuva420p*. To deactivate transparency use *\-\-pixel\_format yuv420p*.

[https://en.wikipedia.org/wiki/WebP](https://en.wikipedia.org/wiki/WebP)

## losslesswebp

Used with **\-\-format losslesswebp** or with the shortcut **\-\-losslesswebp**, is the same as [webp](#webp) but lossless, so the **\-\-output\_quality** option has no effect.

## webploop

Used with **\-\-format webploop** or with the shortcuts **\-\-webploop**, **\-\-webp\_images** and **\-\-webp\_clip** is the same as [webp](#webp) but to generate animations.

## avif

Used with **\-\-format avif** or with the shortcut **\-\-avif**. Can use the **\-\-output\_quality \<number\>** option to adjust the quality and size of the output file, the supported values range **from 1 to 63**, lower is better quality, and with the default value *-1* is the same as *24*. Uses the [SVT-AV1 encoder](https://gitlab.com/AOMediaCodec/SVT-AV1/).

Supports images and animations (**\-\-video_preview** and **\-\-concat** options) but no audio (**\-\-enable_audio**). Does not support transparency yet and the **\-\-pixel\_format** can be *yuv420p yuv420p10le*.

[https://en.wikipedia.org/wiki/AVIF](https://en.wikipedia.org/wiki/AVIF)

## avifaom

Used with **\-\-format avifaom**, is the same as [avif](#avif) but uses a diferent encoder **AOM AV1**.

## xvid

Used with **\-\-format xvid** or with the shortcut **\-\-xvid**. Can use the **\-\-output\_quality \<number\>** option to adjust the quality and size of the output file, the supported values range **from 1 to 31**, lower is better quality, and with the default value *-1* is the same as *3*.

It is a video format (used with **\-\-video_preview** and **\-\-concat** options), supports audio (**\-\-enable_audio**) and although it can do it is recommended to use it for images. Uses [mkv](https://en.wikipedia.org/wiki/Matroska) as container [xvid](https://en.wikipedia.org/wiki/Xvid) as video format and [mp3](https://en.wikipedia.org/wiki/MP3) as audio format. Does not support transparency and only supports the **\-\-pixel\_format** *yuv420p*.

## x264

Used with **\-\-format x264** or with the shortcut **\-\-x264**. Can use the **\-\-output\_quality \<number\>** option to adjust the quality and size of the output file, the supported values range **from 1 to 51**, lower is better quality, and with the default value *-1* is the same as *24*.

It is a video format (used with **\-\-video_preview** and **\-\-concat** options), supports audio (**\-\-enable_audio**) and although it can do it is recommended to use it for images. Uses [mkv](https://en.wikipedia.org/wiki/Matroska) as container [h264](https://en.wikipedia.org/wiki/Advanced_Video_Coding) as video format and [aac](https://en.wikipedia.org/wiki/Advanced_Audio_Coding) as audio format. Does not support transparency and supports the **\-\-pixel\_format** *yuv420p yuvj420p yuv422p yuvj422p yuv444p yuvj444p nv12 nv16 nv21 yuv420p10le yuv422p10le yuv444p10le nv20le gray gray10le*.

## x265

Used with **\-\-format x265** or with the shortcut **\-\-x265**. Can use the **\-\-output\_quality \<number\>** option to adjust the quality and size of the output file, the supported values range **from 1 to 51**, lower is better quality, and with the default value *-1* is the same as *24*.

It is a video format (used with **\-\-video_preview** and **\-\-concat** options), supports audio (**\-\-enable_audio**) and although it can do it is recommended to use it for images. Uses [mkv](https://en.wikipedia.org/wiki/Matroska) as container [h265](https://en.wikipedia.org/wiki/High_Efficiency_Video_Coding) as video format and [aac](https://en.wikipedia.org/wiki/Advanced_Audio_Coding) as audio format. Does not support transparency and supports the **\-\-pixel\_format** *yuv420p yuvj420p yuv422p yuvj422p yuv444p yuvj444p gbrp yuv420p10le yuv422p10le yuv444p10le gbrp10le yuv420p12le yuv422p12le yuv444p12le gbrp12le gray gray10le gray12le*.

## av1

Used with **\-\-format av1** or with the shortcut **\-\-av1**. Can use the **\-\-output\_quality \<number\>** option to adjust the quality and size of the output file, the supported values range **from 0 to 63**, lower is better quality, and with the default value *-1* is the same as *24*.

It is a video format (used with **\-\-video_preview** and **\-\-concat** options), supports audio (**\-\-enable_audio**) and although it can do it is recommended to use it for images. Uses [mkv](https://en.wikipedia.org/wiki/Matroska) as container [av1](https://en.wikipedia.org/wiki/AV1) as video format and [aac](https://en.wikipedia.org/wiki/Advanced_Audio_Coding) as audio format. Does not support transparency and supports the **\-\-pixel\_format** *yuv420p yuv420p10le*.

## webmvp8

Used with **\-\-format webmvp8** or with the shortcut **\-\-webmvp8**. Can use the **\-\-output\_quality \<number\>** option to adjust the quality and size of the output file, the supported values range **from 1 to 63**, lower is better quality, and with the default value *-1* is the same as *24*.

It is a video format (used with **\-\-video_preview** and **\-\-concat** options), supports audio (**\-\-enable_audio**) and although it can do it is recommended to use it for images. Uses [webm](https://en.wikipedia.org/wiki/WebM) as container [vp8](https://en.wikipedia.org/wiki/VP8) as video format and [opus](https://en.wikipedia.org/wiki/Opus_\(audio_format\)) as audio format. Support transparency and the **\-\-pixel\_format** *yuv420p yuva420p*.

## webmvp9

Used with **\-\-format webmvp9** or with the shortcut **\-\-webmvp9**.Can use the **\-\-output\_quality \<number\>** option to adjust the quality and size of the output file, the supported values range **from 1 to 63**, lower is better quality, and with the default value *-1* is the same as *24*.

It is a video format (used with **\-\-video_preview** and **\-\-concat** options), supports audio (**\-\-enable_audio**) and although it can do it is recommended to use it for images. Uses [webm](https://en.wikipedia.org/wiki/WebM) as container [vp9](https://en.wikipedia.org/wiki/VP9) as video format and [opus](https://en.wikipedia.org/wiki/Opus_\(audio_format\)) as audio format. Support transparency and the **\-\-pixel\_format** *yuv420p yuva420p yuv422p yuv440p yuv444p yuv420p10le yuv422p10le yuv440p10le yuv444p10le yuv420p12le yuv422p12le yuv440p12le yuv444p12le gbrp gbrp10le gbrp12le*.

## webmav1

Used with **\-\-format webmav1** or with the shortcut **\-\-webmav1**. Can use the **\-\-output\_quality \<number\>** option to adjust the quality and size of the output file, the supported values range **from 1 to 63**, lower is better quality, and with the default value *-1* is the same as *24*.

It is a video format (used with **\-\-video_preview** and **\-\-concat** options), supports audio (**\-\-enable_audio**) and although it can do it is recommended to use it for images. Uses [webm](https://en.wikipedia.org/wiki/WebM) as container [av1](https://en.wikipedia.org/wiki/AV1) as video format and [opus](https://en.wikipedia.org/wiki/Opus_\(audio_format\)) as audio format. Does not support transparency and supports the **\-\-pixel\_format** *yuv420p yuv420p10le*.

## webmaomav1

Used with **\-\-format webmaomav1**, is the same as [webmav1](#webmav1) but uses a diferent encoder **AOM AV1**.

## jxl

Used with **\-\-format jxl** or with the shortcut **\-\-jxl**. Can use the **\-\-output\_quality \<number\>** option to adjust the quality and size of the output file, the supported values range **from 1 to 100**, like a % of quality, higher is better quality, and with the default value *-1* is the same as *90*.

Only images are supported, so can't use video nor audio options, such as **\-\-video_preview**, **\-\-concat**, **\-\-enable_audio**, among others. Support transparency. Suported **\-\-pixel\_format**: *rgb24 rgba rgb48le rgba64le rgbf32le rgbaf32le gray ya8 gray16le ya16le grayf32le*.

[https://en.wikipedia.org/wiki/JPEG_XL](https://en.wikipedia.org/wiki/JPEG_XL)

## custom

It is not a specific format, when used with **\-\-format custom**, it uses the **\-\-custom\_format\_ext**, **\-\-custom\_format\_video**, **\-\-custom\_format\_audio** and **\-\-custom\_format\_default\_quality** options as output format. If these options are not defined correctly the program will fail.


# COLOR FORMAT
All options that have a color as input use the **RRGGBB\[AA\]** format. When a color does not specify the alpha channel, *FF*, which is fully opaque, is applied. Any color with alpha *00* is totally transparent and will not be seen. You can also use the color names from [ffmpeg](https://ffmpeg.org/ffmpeg-utils.html#Color).

Options that are sent directly to ffmpeg as **\-\-ft_custom**, do not follow this rule and you have to adjust the color format to what ffmpeg and the corresponding filter supports.

# GRADIENTS

Options that have a gradient input, such as **\-\-bg\_gradient** or **\-\-tb\_bg\_gradient**, expect a text string that defines the type, colors and direction in the following format.

`<Type>:<Direction>:<Colors>`

  * **Type:** The type of gradient can be **linear**, **radial**, **circular** or **spiral**.
  * **Direction:** In the linear gradients define the direction, in the others the center and size. It can be **vertical** (**v**), **horizontal** (**h**), **diagonalup** (**du**), **diagonaldown** (**dd**) or can be the coordinates of two points of the image in percentages, as follows *0x0-100x100*. This example uses point *0x0* (upper left corner) as the start and point *100x100* (lower right corner) as the end of the gradient.
  * **Colors:** It is a list of the colors, 2 minimum and 8 maximum, separated by commas that will compose the gradient. The colors have the same format as the one explained in [Color Format](#color-format).

Examples:

Horizontal linear gradient from white to black.
: `linear:h:white,black`

Vertical linear gradient red, blue and green.
: `linear:v:red,blue,green`

Linear gradient using only 50% of the entire image, centered.
: `linear:25x0-75x0:c67adc,cbb0cf`

Radial gradient centered and occupying the entire height of the image.
: `radial:50x50-50x100:black,white`

# TEXT SUBSTITUTION FORMAT
This format is used in the text block options, in *exec_after*, *rename_dest*,... to customize its content. There are 2 ways to specify text substitution.

The first and the simplest is to put the variable name between **%**. The program will read the value and insert it in the indicated position. Example **%format.format_name%**.

The second and more complicated way is used for variables that may or may not have a value, in case they are undefined, it allows to delete part of the text. Have the following format. **\$text before\$%variable%\$text after\$**. Text before and after Can also be variables in simple format.

Stream-related parameters (*\-\-tb\_custom\_video\_stream, \-\-tb\_custom\_audio\_stream, \-\-tb\_custom\_subtitle\_stream and \-\-tb\_custom\_attachment\_stream*) look for variables in the context of the particular stream in which they are running. The related variables of the streams in the list (*\-\-list\_vars*) look like this *"streams.1.codec_name"*, But to use them in these texts you must refer to them simply with the final part, after the stream number, because the program by context already knows the first part *"%codec_name%"*. From these options it is not possible to read general variables, only those available in the current stream.

## Text format variables
There are special variables to control text formatting, italics, bold, colors. Only available for the text block options.

**%N%**
: Line break.

**%h%**, **%h2%**, **%h4%**, **%h8%**
: Adds 1, 2, 4, 8 spaces respectively.

**%I1%**
: Italic font start.

**%I0%**
: Italic font end.

**%B1%**
: Bold font start.

**%B0%**
: Bold font end.

**%O0%**, **%O1%**, **%O2%**, **%O3%**, **%O4%**, **%O5%**
: Set the font border width to 0, 1, 2, 3, 4, 5 respectively.

**%S0%**, **%S1%**, **%S2%**, **%S3%**, **%S4%**, **%S5%**
: Set the font shadow offset to 0, 1, 2, 3, 4, 5 respectively.

**%#%**
: Set the font color to the default for that line.

**%#XXXXXX%**
: Set the font color to RRGGBB color specified.

**%#\<color\>%**
: Set the font color to the name of color specified.

**%#\<var\>%**
: Set the font color to the value of the variable specified.

**%O#%**
: Set the font border color to the default for that line.

**%O#XXXXXX%**
: Set the font border color to RRGGBB color specified.

**%O#\<color\>%**
: Set the font border color to the name of color specified.

**%O#\<var\>%**
: Set the font shadow color to the value of the variable specified.

**%S#%**
: Set the font shadow color to the default for that line.

**%S#XXXXXX%**
: Set the font shadow color to RRGGBB color specified.

**%S#\<color\>%**
: Set the font shadow color to the name of color specified.

**%S#\<var\>%**
: Set the font shadow color to the value of the variable specified.

# FUNCTIONS
Functions can be used within [text substitution](#text-substitution-format), in user-defined variables and also in options in both the parameters and in the configuration file. Functions can be applied to manipulate or format the value of variables. To use them you must put the function before the variable and surround it with parenthesis. For example: *%upper(file.filename)%*.

If used inside texts, it is possible to specify the name of a variable to store the output of the function in it, in which case the declaration of the resulting text is always removed. To do this, add **>** followed by the variable name at the end of the function. For example: *%upper(file.filename)>file.filename_upper%*.

This behavior is useful for generating variables within the context of the streams. To do so, the output variable has to include a **\*.** at the beginning, which will be replaced by the current context.

When functions are used in user-defined variables, using the *\-\-var* parameter or *VAR_* in the configuration, it is not necessary to use the % at the beginning and at the end. The difference is that when they are used the variable contains a string with the call to the function, which is executed when it is used in the text, but without them the function is executed before and its value is the one returned by the function. In this way you can chain functions to calculate more complex values. The variables are executed in alphabetical order, so make sure that when some variables depend on others they are executed in the correct order, for example by adding an order number to the name.

When functions are used in the options, they work similar to user-defined variables. Normally you use user-defined variables to calculate values and then apply them to the options in the last step.

These are the available functions.

**upper**
: Transforms to uppercase the value of the variable

**capitalize**
: Transforms to uppercase the first character of the variable value.

**lower**
: Transforms to lowercase the value of the variable.

**time**
: Transforms the time in seconds into HH:MM:SS, used to format the duration.

**human\_size**
: Transforms sizes in bytes to KiB, MiB, GiB... Selecting the most appropriate in each case.

**numsep**
: Formats a number by adding separators. For example: *1000000* in *1,000,000*.

**human\_framerate**
: Transforms the frame rate used by ffprobe to something easily understandable by humans. For example: *"19001/317"* into *"59.94 fps"*.

**human\_bitdepth**
: Transforms the *"pix\_fmt"* value of ffprobe to the respective color depth. For example: *"yuv420p"* into *"8 bits"*.

**human\_colormodel**
: Same as above but shows color model. For example: *"yuv420p"* into *"YUV"*.

**human\_chromasub**
: Same as above but shows chroma subsampling. For example: *"yuv420p"* into *"4:2:0"*.

**human\_pixformat**
: Same as above but all at once. For example: *"yuv420p"* into *"YUV 4:2:0 8 bits"*.

**human\_bitrate**
: Transforms the bitrates in bytes to bit/s, kbit/s, Mbit/s...

**human\_sample_rate**
: Transforms the sample rate in Hz to Hz, kHz, MHz...

**language\_name**
: Transforms the language code *Iso639* to language name.

**language\_shortname**
: Transforms the language code *Iso639* to the first language name.

**prefix**
:   Customizable version of **human\_bitrate**, **human\_sample\_rate** and **human\_size**. This version accepts more parameters to configure its behavior:

    1. Variable.
    2. Boolean, 0 means divide by 1000, 1 divide by 1024.
    3. Number of decimals.
    4. Boolean, 1 add a space between the value and the prefix.
    5. Boolean, 1 format the nunber by adding separators.
    6. Text, first prefix.
    7. Text, second prefix.
    8. ...

    Example: *"%prefix(format.size,0,2,1,0,Bytes,kBytes,MBytes,GBytes)%"*

**if**
:   conditional statement. uses the following parameters:

    1. Variable to compare.
    2. Comparison, it can be:
        * **=**: True if the two values are equal.
        * **>**: True if the value of 1 is greater than 3.
        * **<**: True if the value of 1 is less than 3.
        * **<=**: True if the value of 1 is greater than or equal to 3.
        * **<=**: True if the value of 1 is less than or equal to 3.
        * **!=**: True if the two values are different.
        * **contains**: True if 1 contains the value of 3.
        * **lenght>**: True if the length of the value of 1 is greater than the value of 3.
        * **lenght<**: True if the length of the value of 1 is less than the value of 3.
        * **lenght>=**: True if the length of the value of 1 is greater than or equal the value of 3.
        * **lenght<=**: True if the length of the value of 1 is less than or equal the value of 3.
    3. Value or variable with which to compare the first.
    4. Value or variable to be used as a result if the comparison is true.
    5. Value or variable to be used as a result if the comparison is false.

**ifempty**
:	Returns the value of the variable of the first parameter that isn't empty. All parameters must be variables except the last one which must be a literal text.

**replace**
:   Replaces characters or text sequences with other ones. uses the following parameters:

    1. Variable containing the text.
    2. Substitution list separated by `|`, as follows `search_text=new_text|other_substitution=new_substitution|text_to_remove=`. The replacement text can be empty to delete. It can be the name of a variable that contains this list.
	3. When it is 1, stops at the first occurrence.

	Example: Change Codec name *"%replace(codec_name,h264=AVC|hevc=HEVC)%"*

**replace\_regexp**
:   Replaces parts of the input string using a regular expression. uses the following parameters:

    1. Variable containing the text.
    2. Regular expression or variable containing it.
    3. Substitution expression or variable containing it. It can contain the wildcards \1, \2, \3.... to use the capture groups of the regular expression.

	Regular expressions use a lot of special characters in their format and have their own way of escaping them, this may conflict with the command terminal escapes or the format of the configuration ini file.

**replace\_file**
:   It is the same as replace, but instead of writing the list of substitutions in the second parameter, you have to put the name of a file saved in the same folder where the configuration is saved, containing the list of substitutions. This time one substitution per line of the file.

	Example: *short_resolutions.txt*, Content:

	```
	1920x1080=1080p
	1280x720=720p
	960x540=540p
	```

	`%concat(x,width,height)>full_resolution%%replace(full_resolution,short_resolutions.txt)%` in tb_custom_video_streams.

	**%concat(x,width,height)>full_resolution%** creates the variable full_resolution that combines width and height, The contact function is explained below, Then **%replace(full_resolution,short_resolutions.txt)%** reads that variable and replaces the resolutions listed in the file with the equivalent shorter versions.

**concat**
:   Join texts and variable values to calculate other variables. The first parameter is the separator or the variable that contains it, which will be used to separate each of the elements. It supports as many parameters as required. They can be literal texts or variables.

    In the previous example, concatenates the value of the variable width with the value of height using x as separator, in the context of a video stream.

    `%concat(x,width,height)%`

    It can also be used without this context using the first video stream, for example to create a new variable to use in [rename](#rename-options).

    `--var "resolution=concat(x,streams.video.first.width,streams.video.first.height)"`

**div**, **mul**, **sum**, **sub**, **percent**
:   All these functions share the same parameters, but perform different operations with them.

	1. Variable.
	2. Variable or number.
	3. Number of decimals of the result.
	4. Boolean, 1 means format resulting nunber by adding separators.

	Operation performed by each function:

	* **div**: Divide the value of parameter 1 by the number or value of parameter 2.
	* **mul**: Multiplies the value of parameter 1 by the number or value of parameter 2.
	* **sum**: Sum the value of parameter 1 and the number or value of parameter 2.
	* **sub**: Subtracts from the value of parameter 1 the number or value of parameter 2.
	* **percent**: Calculates the percentage that the value of parameter 1 represents compared to the number or value of parameter 2.

	Example: Calculates the percentage that represents each streams's bit_rate in the total. Used in *tb_custom_video_stream* and *tb_custom_audio_stream*.

	`%percent(bit_rate,format.bit_rate,2,0)%`

**file\_exists**
:	Checks if the file indicated in the parameters exists, if exists it returns its complete path, if not it returns an empty string.

	1. Filename.
	2. Path to file. If not specified, it uses the contents of the file.dir variable.

	Example: Uses a png as logo that has the same name as the input file and ends in .logo.png.

	`--var "logo_file_name=concat(,file.basename,.logo.png)" --tb_logo "file_exists(logo_file_name)"`

**read\_text\_file**
:	Returns the contents of a text file, if it does not exist it returns an empty string.

	1. Filename.
	2. Path to file. If not specified, it uses the contents of the file.dir variable.
	3. Trimm. 1 means deleting leading and trailing whitespaces.
	4. Hide Error. 1 means no error if the file does not exist.

	Example: Load the content of a file in the comments

	`--var "text_filename=concat(,file.basename,.txt)" --tb_comments "read_text_file(text_filename)"`

**load\_vars\_file**
:	Loads the variables contained in a file with a specific format. Parameters:

	1. Filename.
	2. Path to file. If not specified, it uses the contents of the file.dir variable.
	3. Context. Variables will be named using the context and the variable name in the file separated by ".".
	3. Trimm. 1 means deleting leading and trailing whitespaces in each var.

	In the file the single line vars will be defined with the name, equal and value, one in each line. Vars names can only contain alphanumeric characters "_" and ".".

	```
	name=value
	other_name=other value
	```

	Vars of more than one line can be defined by enclosing the name in square brackets, their value starts on the next line and continues to the next var or the end.

	```
	[var_name]
	more
	lines
	value
	other_var_name=single line value
	```

**command**
:	Executes the program specified in the first parameter, passing it the following parameters. If the program executes correctly it returns the program output, otherwise an empty string.

**line**
:	Retunrs the selected lines of a variable. Parameters:

	1. The variable where to look for the lines.
	2. Start line, Default 0.
	3. End line, -1 means the last line, Default start line.

**section**
:   Returns the corresponding sections of a text by dividing it by a separator. Similar to *line* but can use any separator not only the line separator.

	1. The variable where to look for the section.
	2. Separator of sections.
	3. Start section, Default 0.
	4. End section, -1 means the last section, Default Start.

**trim**
:	Retunrs the same as input ut with the whitespaces at the beginning and end removed. Parameters:

	1. The variable to trim.

**mediainfo**
:	Execute the configured mediainfo command by passing the text of the first parameter as the parameters to mediainfo. Unlike using command this function always is executed in the folder where the video file is located.

**cut**
:	Returns the chopped content of the variable specified in the first parameter to the number of characters in the second parameter.

	1. The variable to cut.
	2. Variable or number for the maximum characters for output.
	3. Indicates where to make the cut, Indicates where to make the cut, it can set **r** for *right*, **l** for *left* or **c** for *center*. Default *c*.

**grep**
:	Filters the input lines using a regular expression, similar to the grep command.

	1. The variable to search.
	2. Regular expression or variable containing it.
	3. Invert, *1* means that the function returns the unmatched lines, by default *0*.

**sort**
:	Sorts the input lines.

	1. The variable to sort.
	2. Numerical, when it is *1* uses the natural order of the numbers to sort (1,2,10,11) instead of the alphabetic (1,10,11,2), by default *0*.
	3. Inverse order, when *1* is valid, it sorts from highest to lowest (C,B,A) instead of (A,B,C), by default *0*.

**random**
:	Generates random integers between the parameter values. They can be negative, if the two values are equal the returned value is always that one and if the minimum value is less than the maximum they are interchanged.

	1. Minimum expected value (inclusive), default 1.
	2. Maximum expected value (exclusive), default 10.

**text**
:	It is used to define a text string containing line breaks and/or tabs. It uses **"\n"** for line breaks and **"\t"** for tabs. This way they can be used without any problem from the command line. Only has one parameter to put the text in. Example:

	`--var "more_lines_text=text(line1\nline2\nline3)"`

**find_program**
:	Look for a program in the PATH and in the specified folder. When found it returns the full path to the program executable or an empty string if not. Parameters:

	1. Name of the program to find.
	2. Folder where to look for the program in addition to the PATH.

# VARIABLE SYSTEM

This system is used to define and manipulate the variables that are read from **ffprobe** and **mediainfo**, and used as a source of information for text substitution, allowing you to do much more than you can do by simply changing the text. It can be used from parameters or from the configuration files using **\-\-var**, **\-\-var\_stream**, **\-\-var\_stream\_video**, **\-\-var\_stream\_audio**, **\-\-var\_stream\_subtitles**, **\-\-var\_stream\_attachment**. When a variable is defined it is available for use in variables defined later and in text substitution. The variables may contain values directly, chunks of the text substitution format or function calls that are executed before the [text substitution](#text-substitution-format).

## Context

When using the **\-\-var** parameter, all variables use the general context, the definition as well as the possible variables used in function calls. This means that they always use the full name as you can see when using **\-\-list\_vars**.

Variables defined in a context (all *\-\-var\_stream*), the definition uses the general context, but the variables used in the functions first look for a variable with that name in the current context, and if not found, they look for it in the general context. To define variables within the context can use the prefix on the name **context.**.

## Order of definition

The order of definition is important when some variables depend on others, when parameters are used the order is of the parameters themselves, when they are in the configuration file, the order is defined by the number after the **VAR_**, it does not matter how it is ordered in the file. When several configuration files are used, the order in which the files are loaded is respected. When all is used, the order to be applied is as follows.

1. Default configuration file (config.ini or config.conf).
2. Other configuration files in order of parameters.
3. Parameters in order.

The order of the parameters with variables with respect to the configuration parameters does not matter. The configurations are applied first.

## Variables containing values

It is the simplest form, they are variables with a literal value. Examples of use from parameters, from the configuration and the final variable.

```
Parameters									Configuration							Name and value in text substitution

--var constant=5							VAR_{Number}_constant=5					constant=5
--var_stream context.constant=10			VAR_{Number}_S_context.constant=10		streams.0.constant=10, streams.1.constant=10,...
--var_stream_audio context.constant=20		VAR_{Number}_SA_context.constant=20		streams.1.constant=20

{Number} is automatically calculated to respect the order in which they were defined.
```
As these variables are defined in order, when a variable is defined in a configuration file, if its value is changed from the parameters, this does not affect the other variables that depend on it defined in the same configuration file, because they are calculated before applying the change in the value indicated in the parameters. To force the change of the variable definition from the parameters you can use **==**. Example:

configuration file:
```
	VAR_001_example_value=10
	VAR_002_example_calc=mul(example_value,10)
```

Change example_value in params:
```
	subtitles_contact_sheet videofile.mkv --var example_value=5
```
Resulting variables:
```
	example_value=5
	example_calc=100
```

Change example_value definition in params:
```
	subtitles_contact_sheet videofile.mkv --var example_value==5
```

Resulting variables:
```
	example_value=5
	example_calc=50
```

## Variables containing text substitution format

They are the same as variables with a value, but you have to take into account where they are going to be used. As they will be applied long after all the variables are defined, it does not matter the order of the variables that can be used inside. This type of variable is useful to simplify the formatting in the text block. Example:

Variables containing the format of the descriptions and values of a video. Configuration file:
```
VAR_001_ds="%B1%%#gray%"
VAR_002_de="%B0%%#%"
VAR_003_vs="%I1%%#white%"
VAR_004_ve="%I0%%#%"

tb_custom_header=%ds%Duration:%de% %vs%%time(format.duration)%%ve% %ds%Size:%de% %vs%%human_size(format.size)% (%numsep(format.size)% Bytes)%ve%$ %ds%Bitrate:%de% %vs%$%human_bitrate(format.bit_rate)%$%ve%$ %ds%Container:%de% %vs%%format.format_long_name%%ve%
```

This way it is easy to change the appearance of the text by simply changing the variables and it will be applied in all parts of the text where it is used.

## Variables containing function calls

These variables are what make this system really useful for many things. Uses the same [functions explained in Functions](#functions). They allow using the information of the video file and other variables to generate new variables to use in the text substitution of the text block or the commands to be executed before and after. They are used after the **=** directly, without the **%**. If they are put between **%** we would be in the previous case of a [variable containing text substitution format](#variables-containing-text-substitution-format). The difference is that they are executed at different times.

# TEMPLATES

The *\-\-template* and *\-\-template\_ext* options allow to use subtitles_contact_sheet as a text generator using templates, using the [text substitution system](#text-substitution-format). It can be used to generate simple or formatted texts such as Html, BBCode, Markdown, etc. It can use all data from input files and all user-defined variables. When a template file is set up the program does nothing else, it just generates the corresponding text and terminates.

Example of use: Generate a simple html table with the technical data of the video format.

Template file:

```
<html>
<body>
  <table>
    <tr><th>Format<th><td>%format.format_long_name%<td></tr>
    <tr><th>Size<th><td>%human_size(format.size)% (%numsep(format.size)%) Bytes<td></tr>
    <tr><th>Duration<th><td>%time(format.duration)%<td></tr>
    <tr><th>Resolution<th><td>%streams.video.first.width%x%streams.video.first.height%<td></tr>
  </table>
</body>
</html>
```
Command usage:
```
subtitles_contact_sheet videofile.mp4 --template templatefile.txt --template_ext html
```
Result: videofile.html
```
<html>
<body>
  <table>
    <tr><th>Format<th><td>QuickTime / MOV<td></tr>
    <tr><th>Size<th><td>1.75 GiB (1,883,596,467) Bytes<td></tr>
    <tr><th>Duration<th><td>00:40:56<td></tr>
    <tr><th>Resolution<th><td>1920x1080<td></tr>
  </table>
</body>
</html>
```

# CONFIGURATION
All **options** can be saved in a configuration file, but not the **parameters** and **shortcuts**. The configuration files on windows it will be in the same folder as the application exe file, on linux in *$HOME/.config/subtitles\_contact\_sheet/*, on macOS in *"~/Library/Preferences/subtitles\_contact\_sheet/*. In windows and macOS the configuration files use the extension *.ini* and *.conf* in linux.

If no *\-\-config* is specified, It used always **"config"** *(config.ini or config.conf)*. The options that can be configured are the same as the **options** explained above without the dashes at the beginning. To generate this file you can use *\-\-save* and *\-\-save\_all*. With *\-\-save\_all* it is also generated with all the options set to their default values, so that it is easy to consult them and change them if necessary. Configuration file example:

```
[General]
layout=3x10
size=720
format=png
```

Boolean options use the values **0** or **1**. It may be necessary for strings containing certain characters such as "[", "]", "=", ",", ... to be enclosed in double quotation marks. And also certain characters escaped with "\". More information on [wikipedia INI File](https://en.wikipedia.org/wiki/INI_file)

The default configuration file is always loaded first, even if another one is specified. This allows you to place important options common to the installation in one place, such as the ffmpeg path or the temporary folder.

When loading more than one configuration file the order is very important. It always loads *"config"* first and then the rest in the same order as indicated in the parameters. each configuration file that is loaded applies its options by overwriting the previous value. For this reason it is important to keep order and only put the relevant options in each configuration file. *\-\-save* and *\-\-save\_all* always save to the last loaded configuration file.

Almost all options in the configuration file can contain functions to automatically calculate their value. Examples:

Use a different layout depending if the video is vertical (5x3) or horizontal (3x5):

	layout="if(format.vertical,=,1,5x3,3x5)"

Captures at half the size of the original:

	size="div(streams.video.first.height,2)"

The **\-\-config\_desc** option is a special case, because it allows you to use functions to decide which configuration file to load. This is done much later in the execution of the program. The configuration files you can load can only have simple values, no function calls, some options are too late to be changed *(ffmpeg,...)*, unlike using --config, the options configured in these files overwrite the parameters passed from the command line. Example:

```
[General]
add_config="if(format.vertical,=,1,verticalconfing,)"
```

# EXAMPLES

## Contact sheet

Generates the default contact sheet for the indicated video.
: `subtitles_contact_sheet videofile.mkv`

Change the layout and the size of screenshots.
: `subtitles_contact_sheet videofile.mkv --layout 2x5 --size 800`

Change the aspect to white background and black text.
: `subtitles_contact_sheet videofile.mkv --tb_bg_color white --tb_title_color black --tb_font_color gray --ft_bg white --ft_color black`

Change all of the above together, and also save the options in the default configuration file, the next time you run the first example, you will get the same result as now.
: `subtitles_contact_sheet videofile.mkv --layout 2x5 --size 800 --tb_bg_color white --tb_title_color black --tb_font_color gray --ft_bg white --ft_color black --save`

Remove the "Made with subtitles contact sheet" text.
: `subtitles_contact_sheet videofile.mkv --tb_comments ""`

Disable complety the text and no space between screenshots.
: `subtitles_contact_sheet videofile.mkv --tb hidden --grid_border 0`

Disable the frame time.
: `subtitles_contact_sheet videofile.mkv --ft_hide`

Text and grid with background image and more screenshot separation to apreciate the it.
: `subtitles_contact_sheet videofile.mkv --bg_image bg.png --grid_border 20`

Transparent background and webp format to edit it on other program or integrate it in a website with its own background.
: `subtitles_contact_sheet videofile.mkv --tb_transparent --webp`

Only select keyframe for make screenshots and 4 process in parallel. This can speed up the process.
: `subtitles_contact_sheet videofile.mkv --only_keyframes --process 4`

Select the most representative frames within the next 100 frames, this avoids totally black screenshots. This option may slow down the process.
: `subtitles_contact_sheet videofile.mkv --rep_frames 100`

Adjusts the size of screenshots to half of the original size.
: `subtitles_contact_sheet videofile.mkv --size "div(streams.video.first.width,2)"`

## Screenshots

10 Unaltered screenshots, useful to check the quality of the video.
: `subtitles_contact_sheet videofile.mkv --screenshots 4`

3 Screenshots at specified time (01:30, 05:00 and 10:00), jpg format and 800 width.
: `subtitles_contact_sheet videofile.mkv --screenshots 3 --jpg --size 800 --mf 01:30 --mf 05:00 --mf 10:00`

Webp Screenshot with overlay logo image, up left corner at 02:50.
: `subtitles_contact_sheet videofile.mkv --screenshots 1 --webp  --mf 02:50 --logo logo.png --logo_pos up_left`

## Images

Generates the default contact sheet for the indicated images folder.
: `subtitles_contact_sheet --images images_folder`

Hides the text block and the name of image file
: `subtitles_contact_sheet --images images_folder`--tb hidden --images_text 0

Generates the contact sheet by searching for images in all subdirectories.
: `subtitles_contact_sheet --images images_folder`--images_recursive

## Animations

Create a gif animation with 5 clips of 1 second duration each.
: `subtitles_contact_sheet videofile.mkv --gif_clip 5`

Create 5 gif animations of 1 seconsd duration each.
: `subtitles_contact_sheet videofile.mkv --gif_singles 5`

Create a 5 second webp at 03:00, with a overlay logo.
: `subtitles_contact_sheet videofile.mkv --webp_clip 1 --video_preview 5000 --mf 03:00 --logo logo.png`

Creates a webp animation of 20 images changing once per second.
: `subtitles_contact_sheet videofile.mkv --webp_images 20`

Creates a gif animation of 10 images at 640 width changing every two seconds.
: `subtitles_contact_sheet videofile.mkv --gif_images 10 --video_fps 0.5 --size 640`

Process with gifsicle the resulting gif to create a smaller version.
: `subtitles_contact_sheet videofile.mkv --gif_clip 5 --exec_after "gifsicle \"%out.file%\" -o \"%out.dir%/%out.basename%.lossy.gif\" --lossy=%lossy%" --var lossy=60`

## Subtitles

Contact sheet of subtitles, video file with same name.
: `subtitles_contact_sheet videofile.en.srt`

Contact sheet of subtitles. video file with diferent name.
: `subtitles_contact_sheet --sub subfile.en.srt --video videofile.mkv`

Contact sheet changing the aspect of subtitles, big green font on transparent black box.
: `subtitles_contact_sheet videofile.en.srt --green --big --tbox`

Gif animation with subtitles.
: `subtitles_contact_sheet videofile.en.srt --gif_clip 5`

## Batch

Create the default contact sheet of all video files inside a folder.
: `subtitles_contact_sheet --batch dir/ --video`

Create the default contact sheet of all video files inside a folder and sub folders, put the result contact sheet in a different folder
: `subtitles_contact_sheet --batch dir/ --video -r -- --out_dir "../Contact Sheets Folders"`

Create the contact sheet, gif animation and screenshots of a file
: `subtitles_contact_sheet --batch videofile.mkv -- -- --gif_clip 5 -- --screenshots --jpg --layout 1x10`

Create the contact sheet and webp clip of all mp4 files of 2 folders recursively and save them on a sub folder at the same level of file, no overwrite when the contact sheet already exists.
: `subtitles_contact_sheet --batch dir/ dir2/ -f "*.mp4" -r -- --out_relative_dir "ContactSheet" --no_overwrite -- --webp_clip 10 --no_overwrite --out_relative_dir "Animations"`

Create the contact sheet of all video files inside a folder, use file system events to discover new files and reescan all every 5 minutes.
: `subtitles_contact_sheet --batch dir/ --video --watch --time 300 -- --no_overwrite`

## Rename

Add the resolution to the end of the file name in all files within a folder and subfolders. *(simulation, change "print" to "move" to really doing it)*
: `subtitles_contact_sheet --batch -r dir/ --video --show_output -- --rename_dest "%file.dir%/%file.basename% [%streams.video.first.width%x%streams.video.first.height%].%file.extension%" --rename --rename_type print`

Add the list of audio languajes at the end of the file name in all files within a folder. *(simulation, change "print" to "move" to really doing it)*
: `subtitles_contact_sheet --batch dir/ --video --show_output -- --var_stream_audio "context.language_name=language_shortname(tags.language)" --var_stream_audio "audio_languages=concat( ,audio_languages,language_name)" --rename_dest "%file.dir%/%file.basename% [Audio: %lower(audio_languages%)].%file.extension%" --rename --rename_type print`

Classify all files using symbolic liks from one folder to another by resolution.
: `subtitles_contact_sheet --batch origin/* --video -- --rename_dest "by_resolution/%streams.video.first.width%x%streams.video.first.height%/%file.filename%" --rename --rename_type symlink --rename_mkdir`

Classify all files using symbolic liks from one folder to another by duration (Very short < 20min <= Short < 35min <= Normal < 55min <= Long < 70min <= Very long).
: `subtitles_contact_sheet --batch origin/ --video -- --var "rel_duration=Very short" --var "rel_duration=if(format.duration,>,1200,Short,rel_duration)" --var "rel_duration=if(format.duration,>,2100,Normal,rel_duration)" --var "rel_duration=if(format.duration,>,3000,Long,rel_duration)" --var "rel_duration=if(format.duration,>,4200,Very long,rel_duration)" --rename --rename_dest "by_duration/%rel_duration%/%file.filename%" --rename_type symlink --rename_mkdir`

## Configuration Files

Example of how to use user-defined variables for text coloring

```
[General]
VAR_001_format_color=9bc8a4
VAR_002_other_color=c8c69b
VAR_003_size_color=c89b9b
tb_font_color=cacaca
tb_bg_color=00000000
tb_comments_color=808080

tb_custom_attachment_stream="Stream %index% Attachment: %B1%%#other_color%%tags.filename% %#size_color%%human_size(extradata_size)% (%numsep(extradata_size)% Bytes) %#format_color%%tags.mimetype%%#%%B0%"

tb_custom_audio_stream="Stream %index% Audio: %B1%%#other_color%$$%language_name(tags.language)%$$$ $%capitalize(channel_layout)%$$$ $%human_sample_rate(sample_rate)%$$$ %#size_color%$%human_bitrate(bit_rate)%$$ %#format_color%%codec_long_name%%#%%B0%"

tb_custom_chapters="$Chapters: %B1%%#other_color%$%chapters.count%$%#%%B0%$"

tb_custom_chapters_list="Chapter %B0%%#other_color%%time(start_time)%: %B1%%#format_color%%tags.title%%#%%B0%"

tb_custom_header="Container: %B1%%#format_color%%format.format_long_name%%#%%B0% Duration: %B1%%#other_color%%time(format.duration)%%#%%B0% Size: %B1%%#size_color%%human_size(format.size)% (%numsep(format.size)% Bytes)%#%%B0%$ Bitrate: %B1%%#size_color%$%human_bitrate(format.bit_rate)%$%#%%B0%$"

tb_custom_subtitle="Format: %B1%%#format_color%%subtitle.format%%#%%B0% Size: %B1%%#size_color%%human_size(subtitle.size)%%#%%B0%$ Language: %B1%%#other_color%$%subtitle.language%$%#%%B0%$%N%Lines: %B1%%#other_color%%subtitle.lines%%#%%B0% Length, Total: %B1%%#other_color%%subtitle.total_length%%#%%B0% Avegare: %B1%%#other_color%%subtitle.avg_lenght%%#%%B0%%N%Duration, Video: %B1%%#other_color%%time(format.duration)%%#%%B0% Subtitles: %B1%%#other_color%%time(subtitle.total_duration)%%#%%B0% Average Line: %B1%%#other_color%%subtitle.avg_duration_ms%ms%#%%B0%"

tb_custom_subtitle_stream="Stream %index% Subtitle: %B1%$%#other_color%$%capitalize(tags.title)%$ - $$%#other_color%$%language_name(tags.language)%$ $%#format_color%%codec_long_name%%#%%B0%

tb_custom_video_stream="Stream %index% Video: %B1%%#other_color%%width%x%height%$ $%human_pixformat(pix_fmt)%$ bits$ %human_framerate(r_frame_rate)% fps$ %#size_color%$%human_bitrate(bit_rate)%$$ %#format_color%%codec_long_name%%#%%B0%"
```

# EXIT VALUES
The program returns 0 when it terminates correctly, and a positive number when there is an error. For now the meaning of these numbers is undocumented.

# CHANGELOG
This section describes the changes that affect the options, parameters and all those changes that affect the interaction with the user. But it does not include bug fixes or other internal program changes, those changes can be found in [gitlab in the releases page](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/releases) or in the [commits page](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/commits/main?ref_type=heads).

All new options and parameters listed here have their corresponding explanations in the previous sections. Deprecated list shows options that still work but will be removed in future versions, but they have been removed from the documentation so that nobody starts using them now.

## Changes between version 1.0 and 1.1

### New

Parameter **\-\-list\_config**
:	Not really new but undocumented in version 1.0.

Option **\-\-config\_desc \<text\>**
:	Same case as the previous parameter

Parameter **\-\-list\_extensions**
:	Now that there are more extensions supported, the complete list can be consulted with this parameter.

Parameter **\-\-sub_stream \<stream_number\>**
:	To use subtitles enbeed in video files.

Option **\-\-sub\_show\_embedded \[0\|1\]**
:	This option is enabled by default, it automatically selects one of the subtitles embedded in the video and displays it. This changes the default behavior until now which did not show embedded subtitles. It can be a bit slow to extract the subtitles, to return to the previous state set to 0.

Options **\-\-sub\_emb\_select \<0\-5\>** and **\-\-sub\_emb\_lang \[0\|1\]**
:	How the program decides which subtitle should be displayed when none is indicated.

Option **\-\-pixel\_format \<pix_fmt\>**
:	Forces a pixel format in the output format, by default empty.

Batch parameter **\-\-videofiles**
:	Renamed *\-\-video* to not overlap with te same parameter of normal operation. Now also adds more extensions than before, see *\-\-list\_extensions* for the complete list.

Batch parameter **\-\-subfiles**
:	Renamed *\-\-subs* for consistency with the previous option.

Batch parameter **\-\-show\_output**
:	Not really new but undocumented in version 1.0.

Function **trim**
:	Removes spaces before and after a variable

### Deprecated

Batch parameter **\-\-video** and **\-\-subs**
:	Renamed to *\-\-videofiles* and *\-\-subfiles*

### Changed

Function **read\_text\_file**
:	Adds a parameter to remove spaces at the beginning and end of the file.

Options **\-\-size** and **\-\-min\_size**
:	Allows the value 0 to over "" to disable or not to change the input size. This change is motivated by consistency with the GUI.

Parameter **\-\-lang \<code\>**
:	Now suppots two- or three-letter ISO 639 codes.

Option **\-\-format \<ext\>**
:	New formats: *webmvp8*, *webmvp9*, *webmav1*, *avif* and the new shortcuts to them. But not fully tested yet. Removed the possibility to set any ffmpeg format, because it hasn't worked for a long time.

### Deleted

Parameter **\-\-overwrite**
:	Obsolete since *\-\-no\_overwrite* can be activated and deactivated.

Option **\-\-format\_options \<options\>**
:	It has not been working for some time, I am removing it while waiting to fix it with other custom formatting options.

## Changes between version 1.1 and 1.2

### New

Option **\-\-suffix_number \<number|time_format>**
:	When the output are individual files, configure how they are numbered.

Options **\-\-scale\_algorithm**, **\-\-scale\_flags** and **\-\-scale\_custom\_options**
:	Configure the image resizing.

Option **\-\-scale\_zscale \[0\|1\]**
:	Uses a different filter for resizing.

Option **\-\-show\_mfs \[0\|1\]**
:	Displays the current mfs list. Previously it was always displayed, but now it must be activated.

Option **\-\-sub\_delay \<ms\>**
:	Allows to add a delay to the time of the screen captures, the default value *-1* fixes a problem with ffmpeg that sometimes captured the previous frame and not the subtitles. Set to *0* to continue with the previous behavior.

Function **mediainfo**
:	Run the configured mediainfo and return its output.

Option **\-\-vr\_disable\_autodetection \[0\|1\]**
:	Disable VR Autodetection. Enabled by default, put this option to 1 to maintain the old behavior.

Option **\-\-vr\_detect\_ar \[0\|1\]**
:	Detects the vr videos based on aspect ratio.

Option **\-\-add\_config \<name|file\>**
:	Unlike *\-\-config* it allows the use of functions, but has some limitations.

Options **\-\-vertical\_config** and **\-\-vr\_config**
:	The same as *\-\-add\_config* but for vertical and VR only.

Option **\-\-tb\_hide\_vr\_text \[0\|1\]**
:	Disables the new VR format text in header. This text is new and displayed by default, put this option to 1 to maintain the old behavior.

Option **\-\-tb\_custom\_vr \<text\>**
:	Customize the text of the VR format.

Option **\-\-aspect\_ratio \<ar\>**
:	Set the aspect ratio of the output. The default value changes the behavior so far, set to **reslolution** to keep it.

Option **\-\-enable\_audio \[0\|1\]**
:	Enables audio output.

Options **\-\-custom\_format\_ext**, **\-\-custom\_format\_video**, **\-\-custom\_format\_audio** and **\-\-custom\_format\_default\_quality**
:	To define a custom output format.

Option **\-\-sub\_custom \<string\>**
:	Custom options for subtitles ffmpeg filter.

Option **\-\-custom\_capture\_filters \<filters\>**
:	Additional filters for captured screenshots and clips.

Shortcuts **\-\-vr.format**, **\-\-vr.mode**, **\-\-vr.fov** and , **\-\-fisheye190** and **\-\-vr360**
:	Force VR autodetection to these values.

Shortcuts **\-\-av1** and **\-\-jxl**
:	New shortcuts for the new formats.

### Changed

Option **\-\-ft\_format \<format\>**
:	New possible values: *seconds*, *miliseconds*, *HH:MM:SS*, *MM:SS* and *short* now uses MM:SS for durations less an hour.

Function **read\_text\_file**
:	Adds a parameter to hide error if the file does not exists.

Functions **replace** and **replace\_file**
:	Adds a parameter to stop replacing at the firts occurrence.

**Integer Options used as parameter**
:	Now can be set to a relative value, adding **+** before the number. More details in [options section](#options).

Shortcuts **\-\-fisheye200** and **\-\-vr180**
:	Before they activated the VR mode and set the format, now they only force the autodetection to these values, but do not activate the VR mode by themselves.

Option **\-\-format**
:	Add av1 and jxl formats.

## Changes between version 1.2 and 1.3

### New

Parameter **\-\-images \<dir\>**
:	Create contact sheets and animations of folders with images.

Section [Images Options](#images-options)
:	Many new options for configuring contact sheets and image animations.

function **cut**
:	Cuts a variable to the specified number of characters.

Option **\-\-ft\_overlay \[0\|1\]**
:	Set if the time in the frames is printed inside or outside the screenshot.

### Changed

Option **\-\-format**
:	Add losslesswebp format.

## Changes between version 1.3 and 1.4

### New

functions **sort** and **grep**
:	To sort and filter the lines of a variable.

### Changed

**functions now can include other functions**
:	Functions can now be used as parameters of other functions. Example:

	upper(read_text_file(filename))

## Changes between version 1.4 and 1.5

### New

function **random**
:	Generates random integers.

Option **\-\-fixed\_interval \<time\>**
:	Takes screenshots at fixed time intervals.

Option **\-\-mask \<color|gradient|file\>**
:	Applies a mask to the screenshots.

Option **\-\-ft\_mask\_layer\_pos \<pos\>**
:	Controls where the frame time is printed with respect to the mask.

Option **\-\-rounded\_corners \<px\>**
:	Round the corners of the screenshots.

**\-\-logo\_scale \<%\>**
:	Logo size in percentage.

**\-\-logo\_blur \<number\>**
:	Logo related options.

**\-\-logo\_exposure \<number\>**
:	Logo related options.

**\-\-logo\_saturation \<number\>**
:	Logo related options.

**\-\-logo\_opacity \<number\>**
:	Logo related options.

Option **\-\-watermark \<file\>**
:	Adds a watermark to the entire contact sheet

Option **\-\-watermark\_halign \<haling\>**
:	Watermark related options.

Option **\-\-watermark\_valign \<valing\>**
:	Watermark related options.

Option **\-\-watermark\_scale \[0\|1\]**
:	Watermark related options.

Option **\-\-watermark\_tile \[0\|1\]**
:	Watermark related options.

Option **\-\-watermark\_blur \<number\>**
:	Watermark related options.

Option **\-\-watermark\_exposure \<number\>**
:	Watermark related options.

Option **\-\-watermark\_saturation \<number\>**
:	Watermark related options.

Option **\-\-watermark\_opacity \<number\>**
:	Watermark related options.

Option **\-\-idle \[0\|1\]**
:	Launch ffmpeg with idle priority.

Batch parameter **\-\-idle**
:	Launch processes with idle priority.

### Changed

Option **\-\-mode** adds **random** and **fixed**

Option **\-\-process** adds *0* to values range, means automatic detection, uses the number of CPU cores.

## Changes between version 1.5 and 1.6

### New

Option **\-\-grid\_border\_up \<px\>**

Option **\-\-grid\_border\_down \<px\>**

Option **\-\-grid\_border\_left \<px\>**

Option **\-\-grid\_border\_right \<px\>**

Option **\-\-grid\_border\_vertical \<px\>**

Option **\-\-grid\_border\_horizontal \<px\>**
:	Set the grid borders independently.

Option **\-\-logo\_transparent\_color \<color\>**

Option **\-\-logo\_transparent\_similarity \<number\>**

Option **\-\-logo\_transparent\_blend \<number\>**
:	Makes in the frame logo this color transparent.

Option **\-\-tb\_logo\_transparent\_color \<color\>**

Option **\-\-tb\_logo\_transparent\_similarity \<number\>**

Option **\-\-tb\_logo\_transparent\_blend \<number\>**
:	Makes in the text block logo this color transparent.

Option **\-\-watermark\_transparent\_color \<color\>**

Option **\-\-watermark\_transparent\_similarity \<number\>**

Option **\-\-watermark\_transparent\_blend \<number\>**
:	Makes in the watermark this color transparent.

Parameter **\-\-dvd \<dir\>**

Parameter **\-\-dvd\_title \<number\>**
: Add suport to read DVD VIDEO_TS folder.

## Changes between version 1.6 and 1.7

### New

Option **\-\-gifsicle \<file\>**
: gifsicle executable. Necessary to use the giflossy output format.

format **giflossy**
: Same as gif but uses gifsicle to reduce the size and the quality.

shortcut **\-\-losslesswebp**
: Missing shortcut for a previously supported format.

function **text**
: To use line breaks and tabs from the command line.

Option **\-\-mfs\_separator \<separator\>**
: Separator for **\-\-mfs**.

Option **\-\-var\_file \<file|context=file\>**

function **load\_vars\_file**
: To load a file with vars.

Option **\-\-exec \<command\>**
: To exexute a command instead of main action of the program.

function **find_program**
: Look for a program in the PATH and in the specified folder.

Option **\-\-prefix \<text>** and **\-\-prefix\_number \<number|time\_format>**
: To set the beginning of the output name.

### Changed

Parameter **\-\-mfs** changed to Option.
: To save its value in the configuration file.

Incorrect documented parameter **\-\-show\_mfs** changed to Option.

# BUGS
You can report bugs or problems on the [gitlab issues page](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/issues).

# COPYRIGHT
Copyright © vongooB9.

License GPLv3: GNU GPL version 3 [https://gnu.org/licenses/gpl.html](https://gnu.org/licenses/gpl.html). This is free software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law.


---
header-includes: |
  <style>
  body {max-width: 90%;}
  a, a:visited {color: #1a1aaa;}
  ol, ul {margin-bottom: 1em;}
  dd {margin-top: 0.5em; margin-bottom: 0.5em;}
  dd p {margin: 0.5em 0;}
  @media (prefers-color-scheme: dark) {
    html {background: black; color: white;}
    a, a:visited {color: #AAF;}
  }
  </style>
---
