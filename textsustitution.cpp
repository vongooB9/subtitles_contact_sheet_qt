#include "textsustitution.h"
#include <QRegularExpression>
#include <QFile>
#include <QJsonArray>
#include <QLocale>
#include <QProcess>
#include "iso639.h"
#include "common.h"
#include "colors.h"
#include <QCollator>
#include <algorithm>
#include <QRandomGenerator>
#include <QStandardPaths>

QRegularExpression
TextSustitution::SearchTextEmpty("(?<!\\\\)\\$([^\\$]*)(?<!\\\\)\\$(?<!\\\\)\\%([^\\$\\%]+)(?<!\\\\)\\%(?<!\\\\)\\$([^\\$]*)(?<!\\\\)\\$");
QRegularExpression TextSustitution::SearchSimpleText("(?<!\\\\)\\%([^\\$\\%]+)\\%");
QRegularExpression TextSustitution::SearchFormatText("\\%([a-zA-Z0-9\\-_.,#]+)\\%");
QRegularExpression TextSustitution::FunctionSearch("^([a-z_]+)\\((.*)\\)>?([^\\$\\%]*)$");
QRegularExpression TextSustitution::ParameterSearch("^[a-z_]+\\(");
QRegularExpression TextSustitution::VarSearch("^([a-zA-Z0-9_.]+)=(.*)$");
QRegularExpression TextSustitution::MultineVarSearch("^\\[([a-zA-Z0-9_.]+)\\]$");

const QMap<QString, pix_fmt_data> TextSustitution::PixelFormatDatabase = {
	{"yuv420p", {8, "4:2:0", "YUV"}},
	{"yuvj420p", {8, "4:2:0", "YUV"}},
	{"yuv422p", {8, "4:2:2", "YUV"}},
	{"yuvj422p", {8, "4:2:2", "YUV"}},
	{"yuv444p", {8, "4:4:4", "YUV"}},
	{"yuvj444p", {8, "4:4:4", "YUV"}},
	{"gbrp", {8, "4:4:4", "GBR"}},
	{"yuv420p10le", {10, "4:2:0", "YUV"}},
	{"yuv422p10le", {10, "4:2:2", "YUV"}},
	{"yuv444p10le", {10, "4:4:4", "YUV"}},
	{"gbrp10le", {10, "4:4:4", "GBR"}},
	{"yuv420p12le", {12, "4:2:0", "YUV"}},
	{"yuv422p12le", {12, "4:2:2", "YUV"}},
	{"yuv444p12le", {12, "4:4:4", "YUV"}},
	{"gbrp12le", {12, "4:4:4", "GBR"}},
	{"gray", {8, "", "Y"}},
	{"gray10le", {10, "", "Y"}},
	{"gray12le", {12, "", "Y"}},
	{"nv12", {8, "4:2:0", "YUV"}},
	{"nv16", {8, "4:2:2", "YUV"}},
	{"nv21", {8, "4:2:0", "YUV"}},
	{"nv20le", {8, "4:2:2", "YUV"}},
	{"yuva420p", {8, "4:2:0", "YUV"}}
};

TextSustitution::TextSustitution() {
	MediaInfo = "";
	Local = new QLocale("EN");
}

QString TextSustitution::ScapeCaracters(QString in) {
	in.replace("%", "\\%");
	in.replace("$", "\\$");
	return in;
}

void TextSustitution::SetReplaceDir(QString dir) {
	ReplaceDir = dir;
}

void TextSustitution::SetMediaInfo(QString mediainfo) {
	MediaInfo = mediainfo;
}

void TextSustitution::LoadVars(QJsonObject Object, QString Name) {

	for (QJsonObject::const_iterator i = Object.begin(); i != Object.end(); ++i) {
		if (i.value().isUndefined()) continue;
		if (i.value().isString()) Vars.insert(Name + "." + i.key(), ScapeCaracters(i.value().toString()));
		if (i.value().isDouble()) Vars.insert(Name + "." + i.key(), QString::number(i.value().toDouble()));
		if (i.value().isBool()) Vars.insert(Name + "." + i.key(), QString::number(i.value().toBool()));

		if (i.value().isObject()) {
			QString levelname;
			if (Name != "")
				levelname = Name + "." + i.key();
			else
				levelname = i.key();

			LoadVars(i.value().toObject(), levelname);
		}
		if (i.value().isArray()) {
			QJsonArray array = i.value().toArray();
			QString arrayname;
			if (Name != "")
				arrayname = Name + "." + i.key();
			else
				arrayname = i.key();

			Vars.insert(arrayname + "." + "count", QString::number(array.count()));
			for (int i2 = 0; i2 < array.count(); i2++) {
				QString levelname = arrayname + "." + QString::number(i2);
				LoadVars(array.at(i2).toObject(), levelname);
			}
		}
	}
}

void TextSustitution::CopyVars(QString OriginContext, QString DestinationContext) {
	if (OriginContext == "" || DestinationContext == "") return;
	QStringList Keys = Vars.keys();
	foreach (QString Key, Keys) {
		if (!Key.startsWith(OriginContext)) continue;
		QString NewKey = Key;
		NewKey.replace(OriginContext, DestinationContext);
		QString Value = Vars.value(Key);
		Vars.insert(NewKey, Value);
	}
}

QString TextSustitution::PrefixConverter(double Value, bool Binary, uint8_t Decimals, bool Separated, bool Formated,
		QStringList Units, QList<uint8_t> DecimalsList) {
	uint8_t uinitindex = 0;
	int divisor = 1000;
	if (Binary) divisor = 1024;
	while (Value >= divisor) {
		uinitindex++;
		Value = Value / divisor;
		if (Units.size() - 1 == uinitindex) break;
	}

	QString unit = Units.value(uinitindex);
	if (Separated) unit = " " + unit;

	if (Value - (int)Value == 0)
		Decimals = 0;
	else if (!DecimalsList.isEmpty() && DecimalsList.count() > uinitindex)
		Decimals = DecimalsList.value(uinitindex);

	if (Formated)
		return Local->toString(Value, 'f', Decimals) + unit;
	else
		return QString::number(Value, 'f', Decimals) + unit;

}

QString TextSustitution::Operation(QString Operation, QString Value, QStringList Params, QString Context) {
	QString Operator;
	if (!Params.isEmpty()) Operator = Params.takeFirst();
	Operator = ValueCheckContext(Operator, Context);
	QString Decimals = 0;
	if (!Params.isEmpty()) Decimals = Params.takeFirst();
	Decimals = ValueCheckContext(Decimals);
	bool Formated = false;
	if (!Params.isEmpty()) Formated = Params.takeFirst().toInt() != 0;
	bool ok;
	double op1 = Value.toDouble(&ok);
	if (!ok) return "";
	double op2 = Operator.toDouble(&ok);
	if (!ok) return "";
	double Result = 0;
	if (Operation == "div") Result = op1 / op2;
	if (Operation == "mul") Result = op1 * op2;
	if (Operation == "sum") Result = op1 + op2;
	if (Operation == "res") Result = op1 - op2;
	if (Operation == "percent") Result = op1 / op2 * 100;
	if (qIsNaN(Result)) return "";
	if (Formated)
		return Local->toString(Result, 'f', Decimals.toInt());
	else
		return QString::number(Result, 'f', Decimals.toInt());
}

QString TextSustitution::ProcessFunctions(QString Function, QStringList Params, QString Context, bool TextFormat,
		int level) {

	QList<bool> ParamsFunction;
	for (int i = 0; i < Params.count(); ++i) {
		QString CurParam = Params.at(i);
		ParamsFunction.insert(i, false);
		if (!CurParam.contains("(")) continue;

		QRegularExpressionMatch FunctionMatch = FunctionSearch.match(CurParam);
		if (FunctionMatch.hasMatch()) {
			QString CurFunction = FunctionMatch.captured(1);
			QStringList CurParams = SplitFunctionParameters(FunctionMatch.captured(2));
			Params[i] = ProcessFunctions(CurFunction, CurParams, Context, TextFormat, level + 1);
			ParamsFunction[i] = true;
			PrintDebug("\t\t\tResult: " + Params.at(i));
		}
	}

	if (level > 0) PrintDebug("\t\tRecursive Function: " + Function + "(" + Params.join(",") + ")");

	if (Function == "ifempty") {
		for (int i = 0; i < Params.count(); ++i) {
			QString Value = Params.at(i);
			if (ParamsFunction[i]) {
				Value = ValueCheckContext(Value, Context, false);
			} else {
				Value = ValueCheckContext(Value, Context, (i < Params.count() - 1));
			}
			if (Value != "") return Value;
		}
		return "";
	}

	if (Function == "random") {
		int Start = 1;
		if (!Params.isEmpty()) Start = Params.takeFirst().toInt();
		int End = 10;
		if (!Params.isEmpty()) End = Params.takeFirst().toInt();
		if (Start == End) End++;
		if (Start > End) {
			int tmp = Start;
			Start = End;
			End = tmp;
		}
		int R = QRandomGenerator::global()->bounded(Start, End);
		return QString::number(R);
	}

	if (Function == "text") {
		return Text(Params.join(","));
	}

	QString Param = Params.takeFirst();
	QString Result = ValueCheckContext(Param, Context, !ParamsFunction[0]);

	if (Function == "upper") return Result.toUpper();

	if (Function == "lower") return Result.toLower();

	if (Function == "trim") return Result.trimmed();

	if (Function == "capitalize") {
		if (Result == "") return "";
		Result[0] = Result[0].toUpper();
		return Result;
	}

	if (Function == "time") {
		if (Result == "") return "";
		return QTime::fromMSecsSinceStartOfDay(Result.toDouble() * 1000).toString("hh:mm:ss");
	}

	if (Function == "human_size") {
		if (Result == "") return "";
		return Local->formattedDataSize(Result.toLongLong(), 2, QLocale::DataSizeIecFormat);
	}

	if (Function == "numsep") {
		if (Result == "") return "";
		return Local->toString(Result.toLongLong());
	}

	if (Function == "human_framerate") {
		if (Result == "") return "";
		if (!Result.contains('/')) return Result;
		double FrameRate = HumanFrameRate(Result);
		if (FrameRate == 0) return "";
		uint8_t decimals = 2;
		if (FrameRate - (int)FrameRate == 0)
			decimals = 0;
		return QString::number(FrameRate, 'f', decimals);
	}

	if (Function == "human_bitdepth") {
		if (PixelFormatDatabase.contains(Result)) {
			return QString::number(PixelFormatDatabase.value(Result).bits);
		}
		return "";
	}

	if (Function == "human_colormodel") {
		if (PixelFormatDatabase.contains(Result)) {
			return PixelFormatDatabase.value(Result).Mode;
		}
		return "";
	}

	if (Function == "human_chromasub") {
		if (PixelFormatDatabase.contains(Result)) {
			return PixelFormatDatabase.value(Result).Chroma;
		}
		return "";
	}

	if (Function == "human_pixformat") {
		if (PixelFormatDatabase.contains(Result)) {
			pix_fmt_data Data = PixelFormatDatabase.value(Result);
			Result = Data.Mode;
			if (Data.Chroma != "") Result += " " + Data.Chroma;
			Result += " " + QString::number(Data.bits);
			return Result;
		}
		return "";
	}

	if (Function == "prefix") {
		if (Result == "") return "";
		//Example "%prefix(format.size,0,2,1,0,Bytes,kBytes,MBytes,GBytes,0,0,2,3)%"
		double value = Result.toDouble();
		if (value == 0) return "";
		bool Binary = false;
		if (!Params.isEmpty()) Binary = Params.takeFirst().toInt() != 0;
		uint8_t Decimals = 3;
		if (!Params.isEmpty()) Decimals = Params.takeFirst().toInt();
		bool Separated = false;
		if (!Params.isEmpty()) Separated = Params.takeFirst().toInt() != 0;
		bool Formated = false;
		if (!Params.isEmpty()) Formated = Params.takeFirst().toInt() != 0;

		QStringList Texts;
		QList<uint8_t> DecimalsList;

		while (!Params.isEmpty()) {
			QString param = Params.takeFirst();
			bool ok = false;
			uint8_t Dec = param.toUInt(&ok);
			if (ok) DecimalsList.append(Dec);
			else Texts.append(param);
		}

		return PrefixConverter(value, Binary, Decimals, Separated, Formated, Texts, DecimalsList);
	}

	if (Function == "human_bitrate") {
		if (Result == "") return "";
		double bitrate = Result.toDouble();
		if (bitrate == 0) return "";
		return PrefixConverter(bitrate, false, 3, true, true, {"bit/s", "kbit/s", "Mbit/s", "Gbit/s"}, {0, 0, 2, 3});
	}

	if (Function == "human_sample_rate") {
		if (Result == "") return "";
		double sample = Result.toDouble();
		if (sample == 0) return "";
		return PrefixConverter(sample, false, 2, true, true, {"Hz", "kHz", "MHz", "GHz"}, {0, 1, 2, 2});
	}

	if (Function == "language_name") {
		if (Result == "") return "";
		if (Result == "und") return "";
		if (Result.length() == 2) {
			QLocale locale(Result);
			return QLocale::languageToString(locale.language());
		} else {
			return Iso639ToLangName(Result);
		}
	}

	if (Function == "language_shortname") {
		if (Result == "") return "";
		if (Result == "und") return "";
		if (Result.length() == 2) {
			QLocale locale(Result);
			return QLocale::languageToString(locale.language());
		} else {
			return Iso639ToShortName(Result);
		}
	}

	if (Function == "if") {
		//Example "%if(index,=,0,Stream:\\N,\\h\\h)%"

		QString Operation;
		if (!Params.isEmpty()) Operation = Params.takeFirst();
		QString CompareValue;
		if (!Params.isEmpty()) CompareValue = Params.takeFirst();
		QString iftrue = "";
		if (!Params.isEmpty()) iftrue = Params.takeFirst();
		QString ifelse = "";
		if (!Params.isEmpty()) ifelse = Params.takeFirst();

		CompareValue = ValueCheckContext(CompareValue, Context);

		iftrue = ValueCheckContext(iftrue, Context);
		ifelse = ValueCheckContext(ifelse, Context);

		if (Operation == "=") return (Result == CompareValue) ? iftrue : ifelse;
		if (Operation == "<") return (Result.toDouble() < CompareValue.toDouble()) ? iftrue : ifelse;
		if (Operation == ">") return (Result.toDouble() > CompareValue.toDouble()) ? iftrue : ifelse;
		if (Operation == "<=") return (Result.toDouble() <= CompareValue.toDouble()) ? iftrue : ifelse;
		if (Operation == ">=") return (Result.toDouble() >= CompareValue.toDouble()) ? iftrue : ifelse;
		if (Operation == "!=") return (Result != CompareValue) ? iftrue : ifelse;
		if (Operation == "contains") return (Result.contains(CompareValue)) ? iftrue : ifelse;
		if (Operation == "lenght<") return (Result.length() < CompareValue.toInt()) ? iftrue : ifelse;
		if (Operation == "length>") return (Result.length() > CompareValue.toInt()) ? iftrue : ifelse;
		if (Operation == "lenght<=") return (Result.length() <= CompareValue.toInt()) ? iftrue : ifelse;
		if (Operation == "lenght>=") return (Result.length() >= CompareValue.toInt()) ? iftrue : ifelse;
	}

	if (Function == "replace") {
		if (Result == "") return "";
		QString ReplaceList = "";
		bool Stop = false;
		if (!Params.isEmpty()) ReplaceList = Params.takeFirst();
		if (!Params.isEmpty()) Stop = Params.takeFirst().toInt() != 0;
		ReplaceList = ValueCheckContext(ReplaceList);
		QStringList List = ReplaceList.split("|");
		for (int i = 0; i < List.count(); ++i) {
			QString Ori = List.at(i).section("=", 0, 0);
			QString Dest = List.at(i).section("=", 1);
			if (Ori.contains("\\n")) Ori.replace("\\n", "\n");
			if (Dest.contains("\\n")) Dest.replace("\\n", "\n");
			if (Result.contains(Ori)) {
				Result.replace(Ori, Dest);
				if (Stop) break;
			}
		}
		return Result;
	}

	if (Function == "replace_regexp") {
		if (Result == "") return "";
		if (Params.isEmpty()) return "";
		QString Regexp = Params.takeFirst();
		Regexp = ValueCheckContext(Regexp);
		QString To = "";
		if (!Params.isEmpty()) {
			To = Params.takeFirst();
			To = ValueCheckContext(To);
		}
		QRegularExpression RE(Regexp);
		return Result.replace(RE, To);
	}

	if (Function == "replace_file") {
		if (Result == "") return "";
		QString ReplaceFile;
		bool Stop = false;
		if (!Params.isEmpty()) ReplaceFile = Params.takeFirst();
		if (!Params.isEmpty()) Stop = Params.takeFirst().toInt() != 0;
		ReplaceFile = ValueCheckContext(ReplaceFile);
		if (!QFile::exists(ReplaceFile))
			ReplaceFile = QDir::toNativeSeparators(ReplaceDir + QDir::separator() + ReplaceFile);
		QStringList ReplaceList;
		if (Replace.contains(ReplaceFile)) {
			ReplaceList = Replace[ReplaceFile];
		} else {
			PrintDebug("\t\tReplace_text load file: " + ReplaceFile);

			QFile RF(ReplaceFile);

			if (!RF.open(QIODevice::ReadOnly | QIODevice::Text)) {
				PrintError("reading replace file: " + ReplaceFile);
				return "";
			}
			while (!RF.atEnd()) {
				QByteArray line = RF.readLine();
				ReplaceList.append(line.trimmed());
			}
			RF.close();
			Replace.insert(ReplaceFile, ReplaceList);
		}

		for (int i = 0; i < ReplaceList.count(); ++i) {
			QString Ori = ReplaceList.at(i).section("=", 0, 0);
			QString Dest = ReplaceList.at(i).section("=", 1);
			if (Ori.contains("\\n")) Ori.replace("\\n", "\n");
			if (Dest.contains("\\n")) Dest.replace("\\n", "\n");
			if (Result.contains(Ori)) {
				Result.replace(Ori, Dest);
				if (Stop) break;
			}
		}

		return Result;
	}

	//Example: %mul(size,8,0,1)%
	//Example: %porcent(tags.NUMBER_OF_BYTES,format.size,2,1)%
	if (Function == "div" ||
		Function == "mul" ||
		Function == "sum" ||
		Function == "sub" ||
		Function == "percent") return Operation(Function, Result, Params, Context);

	if (Function == "concat") {
		QString Separation = Param;
		Result = "";
		while (!Params.isEmpty()) {
			QString Value = Params.takeFirst();
			Value = ValueCheckContext(Value, Context);
			if (Value != "") {
				if (Result != "") Result += Separation;
				Result += Value;
			}
		}
		return Result;
	}

	if (Function == "file_exists") {
		QString Name = Result;
		QString Dir = "";
		if (!Params.isEmpty()) Dir = Params.takeFirst();
		if (Dir == "" && Vars.contains("file.dir"))
			Dir = Vars.value("file.dir");
		QString File = QDir::toNativeSeparators(Dir + QDir::separator() + Name);
		if (QFile::exists(File))
			return File;
		else
			return "";
	}

	if (Function == "read_text_file") {
		if (Result == "") Result = Param;
		QString Name = Result;
		QString Dir = "";
		QString File = Name;
		bool Trim = false;
		bool HideError = false;
		if (!Params.isEmpty()) Dir = Params.takeFirst();
		if (!Params.isEmpty()) Trim = Params.takeFirst().toInt() != 0;
		if (!Params.isEmpty()) HideError = Params.takeFirst().toInt() != 0;
		if (!QFile::exists(File)) {
			if (Dir == "" && Vars.contains("file.dir"))
				Dir = Vars.value("file.dir");
			File = QDir::toNativeSeparators(Dir + QDir::separator() + Name);
		}

		PrintDebug("\t\tloading file: " + File);

		QFile F(File);
		if (!F.open(QIODevice::ReadOnly | QIODevice::Text)) {
			if (!HideError) PrintError("reading file: " + File);
			return "";
		}
		QByteArray Content = F.readAll();
		F.close();
		Result = Content;
		if (Trim) Result = Result.trimmed();
		if (TextFormat) Result.replace("\n", "%N%");
		return Result;
	}

	if (Function == "load_vars_file") {
		if (Result == "") Result = Param;
		QString Name = Result;
		QString Dir = "";
		QString File = Name;
		QString ThisContext = "";
		bool Trim = false;
		if (!Params.isEmpty()) Dir = Params.takeFirst();
		if (!Params.isEmpty()) ThisContext = Params.takeFirst();
		if (!Params.isEmpty()) Trim = Params.takeFirst().toInt() != 0;
		if (!QFile::exists(File)) {
			if (Dir == "" && Vars.contains("file.dir"))
				Dir = Vars.value("file.dir");
			File = QDir::toNativeSeparators(Dir + QDir::separator() + Name);
		}

		PrintDebug("\t\tloading vars file: " + File);

		return LoadVarsFromFile(File, ThisContext, Trim);
	}

	if (Function == "command") {
		if (Param == "") return "";
		QStringList CommandParams;
		while (!Params.isEmpty()) {
			QString CurParam = Params.takeFirst();
			CurParam = ValueCheckContext(CurParam, Context);
			if (CurParam != "") CommandParams.append(CurParam);
		}
		Result = ReadProgram(Param, CommandParams);
		Result.replace("%", "\\%");
		if (TextFormat) Result.replace("\n", "%N%");
		return Result;
	}

	if (Function == "line") {
		if (Result == "") return "";
		int StartLine = 0;
		if (!Params.isEmpty()) StartLine = Params.takeFirst().toInt();
		int EndLine = StartLine;
		if (!Params.isEmpty()) EndLine = Params.takeFirst().toInt();
		return Result.section("\n", StartLine, EndLine);
	}

	if (Function == "section") {
		if (Param == "") return "";
		QString Separator = Params.takeFirst();
		if (Separator == "") return "";
		int Start = 0;
		if (!Params.isEmpty()) Start = Params.takeFirst().toInt();
		int End = Start;
		if (!Params.isEmpty()) End = Params.takeFirst().toInt();
		return Result.section(Separator, Start, End);
	}

	if (Function == "mediainfo") {
		if (MediaInfo == "") {
			PrintError("Mediainfo function disabled");
			return "";
		}
		QStringList CommandParams;
		if (Param != "")
			CommandParams = QProcess::splitCommand(Param);
		CommandParams.append(Vars.value("file.filename"));
		Result = ReadProgram(MediaInfo, CommandParams, Vars.value("file.dir"));
		Result.replace("%", "\\%");
		if (TextFormat) Result.replace("\n", "%N%");
		return Result;
	}

	if (Function == "cut") {
		int Number = 0;
		if (!Params.isEmpty()) {
			QString NumberStr = Params.takeFirst();
			NumberStr = ValueCheckContext(NumberStr);
			Number = NumberStr.toInt();
		}
		QString Place = "";
		if (!Params.isEmpty()) {
			Place = Params.takeFirst();
			Place = ValueCheckContext(Place);
		}
		if (Place == "") Place = "c";
		if (Number == 0 || Result.length() <= Number)
			return Result;
		if (Place == "r")
			return Result.right(Number);
		if (Place == "l")
			return Result.left(Number);
		if (Place == "c") {
			return CutCenterText(Result, Number);
		}
	}

	if (Function == "grep") {
		if (Result == "") return "";
		if (Params.isEmpty()) return "";
		QString Regexp = Params.takeFirst();
		Regexp = ValueCheckContext(Regexp);
		bool Invert = false;
		if (!Params.isEmpty()) Invert = Params.takeFirst().toInt() != 0;
		QRegularExpression RE(Regexp);
		QStringList Output;
		QStringList Lines = Result.split("\n");
		for (int i = 0; i < Lines.count(); ++i) {
			QString Line = Lines.at(i);
			QRegularExpressionMatch Match = RE.match(Line);
			if ((Invert && !Match.hasMatch()) || (!Invert && Match.hasMatch()))
				Output.append(Line);
		}
		return Output.join("\n");
	}

	if (Function == "sort") {
		if (Result == "") return "";
		bool Numeric = false;
		if (!Params.isEmpty()) Numeric = Params.takeFirst().toInt() != 0;
		bool Desc = false;
		if (!Params.isEmpty()) Desc = Params.takeFirst().toInt() != 0;
		QStringList Lines = Result.split("\n");
		QCollator Collator;
		Collator.setNumericMode(Numeric);
		std::sort(Lines.begin(), Lines.end(), Collator);
		if (Desc) std::reverse(Lines.begin(), Lines.end());
		return Lines.join("\n");
	}

	if (Function == "find_program") {
		if (Param == "") return "";
		QString Dir = "";
		if (!Params.isEmpty()) Dir = Params.takeFirst();
		QString Found = "";
		if (!Dir.isEmpty()) {
			QStringList ExeSearchPaths = ListAllDirs(Dir);
			Found = QDir::toNativeSeparators(QStandardPaths::findExecutable(Param, ExeSearchPaths));
		}
		if (Found.isEmpty())
			Found = QDir::toNativeSeparators(QStandardPaths::findExecutable(Param));
		return Found;
	}

	PrintError("unrecognized function " + Function);
	return "";
}

bool TextSustitution::DetectFunction(QString Text) {
	if (Text == "") return false;
	QRegularExpressionMatch FunctionMatch = FunctionSearch.match(Text);
	return FunctionMatch.hasMatch();
}

QString TextSustitution::DetectAndEjecuteFunction(QString Text, QString Context, bool *Executed) {
	if (Executed != nullptr) *Executed = false;
	if (Text == "") return Text;

	QRegularExpressionMatch FunctionMatch = FunctionSearch.match(Text);

	if (!FunctionMatch.hasMatch()) return Text;

	QString Function = FunctionMatch.captured(1);
	QString ParamsStr = FunctionMatch.captured(2);
	QStringList Params = SplitFunctionParameters(ParamsStr);

	if (Function != "") {
		if (Executed != nullptr) *Executed = true;
		PrintDebug("Executing function: " + Text);
		QString Result = ProcessFunctions(Function, Params, Context, false);
		PrintDebug("\tResult: " + Result);
		return Result;
	}
	return Text;
}

void TextSustitution::EmptyMatch(QString Context, QString &Result, bool TextFormat) {
	int Offset = 0;
	int GlobalCount = 0;
	int PosCount = 0;
	int AntPos = 0;
	QRegularExpressionMatch Match;
	while (Match = SearchTextEmpty.match(Result, Offset), Match.hasMatch()) {
		QString TextBefore = Match.captured(1);
		QString TextAfter = Match.captured(3);
		QString Function = "";
		QString Var = Match.captured(2);
		QString Value = "";
		QStringList Params;
		QString Output = "";

		GlobalCount++;

		if (AntPos != Match.capturedStart()) {
			AntPos = Match.capturedStart();
			PosCount = 0;
		} else {
			PosCount++;
		}

		if (PosCount > 10 || GlobalCount > 100) {
			PrintError("Error reached text substitution limit, posible infinite loop");
			break;
		}

		// PrintDebug("\tMatch: " + Match.captured(0) + " - " + TextBefore + " - " + Var + " - " + TextAfter);
		PrintDebug("\n\tMatch: " + Var);

		QRegularExpressionMatch FunctionMatch = FunctionSearch.match(Var);

		if (FunctionMatch.hasMatch()) {
			Function = FunctionMatch.captured(1);
			Var = FunctionMatch.captured(2);
			Output = FunctionMatch.captured(3);
			Params = SplitFunctionParameters(Var);
			Value = ProcessFunctions(Function, Params, Context, TextFormat);
			if (Output != "") {
				if (Output.startsWith("*")) Output.replace("*", Context);
				Vars.insert(Output, Value);
				PrintDebug("\t\tResult to var: " + Output + "=" + Value);
				Value = "";
			}
		} else {
			if (Vars.contains(Context + Var)) {
				Value = Vars.value(Context + Var);
			} else if (Vars.contains(Var)) {
				Value = Vars.value(Var);
			} else {
				Result.replace(Match.captured(0), "");
				PrintDebug("\t\tResult: Empty");
				continue;
			}
		}

		if (Value == "") {
			Result.replace(Match.captured(0), "");
			PrintDebug("\t\tResult: Empty");
		} else {
			Result.replace(Match.captured(0), TextBefore + Value + TextAfter);
			PrintDebug("\t\tResult: " + TextBefore + Value + TextAfter);
		}
	}
}

void TextSustitution::SimpleMatch(QString Context, QString &Result, bool TextFormat) {
	int Offset = 0;
	int GlobalCount = 0;
	int PosCount = 0;
	int AntPos = 0;
	QRegularExpressionMatch Match;
	while (Match = SearchSimpleText.match(Result, Offset), Match.hasMatch()) {
		QString Function = "";
		QString Var = Match.captured(1);
		QString Value = "";
		QStringList Params;
		QString Output = "";

		if (Var.startsWith("#") || Var.length() < 3) {
			Offset = Match.capturedEnd();
			continue;
		}

		GlobalCount++;

		if (AntPos != Match.capturedStart()) {
			AntPos = Match.capturedStart();
			PosCount = 0;
		} else {
			PosCount++;
		}

		if (PosCount > 10 || GlobalCount > 100) {
			PrintError("Error reached text substitution limit, posible infinite loop");
			return;
		}

		// PrintDebug("\n\tMatch Simple: " + Match.captured(0) + " - " + Var);
		PrintDebug("\n\tMatch Simple: " + Var);

		QRegularExpressionMatch FunctionMatch = FunctionSearch.match(Var);

		if (FunctionMatch.hasMatch()) {
			Function = FunctionMatch.captured(1);
			Params = SplitFunctionParameters(FunctionMatch.captured(2));
			Output = FunctionMatch.captured(3);
		}

		if (Function != "") {
			Value = ProcessFunctions(Function, Params, Context, TextFormat);
			if (Output != "") {
				if (Output.startsWith("*")) Output.replace("*", Context);
				Vars.insert(Output, Value);
				PrintDebug("\t\tResult to var: " + Output + " = " + Value);
				Value = "";
			}
		} else {
			if (Vars.contains(Context + Var)) {
				Value = Vars.value(Context + Var);
			} else if (Vars.contains(Var)) {
				Value = Vars.value(Var);
			}
		}

		Result.replace(Match.captured(0), Value);
		if (Value == "")
			PrintDebug("\t\tResult: Empty");
		else
			PrintDebug("\t\tResult: " + Value);
	}
}

QString TextSustitution::ValueCheckContext(QString Var, QString Context, bool Empty) {
	if (Var == "") return Var;
	if (Context != "" && Vars.contains(Context + Var))
		return Vars.value(Context + Var);
	else if (Vars.contains(Var))
		return Vars.value(Var);
	if (Empty)
		return "";
	else
		return Var;
}

QString TextSustitution::ProcessText(QString Context, QString Text, text_sub_style_t *Style) {
	if (Text == "") return "";
	QString Result = Text;

	PrintDebug("Text substitution for:" + Text);

	EmptyMatch(Context, Result);

	PrintDebug("\tComplex Intermediate Result: " + Result);

	SimpleMatch(Context, Result);

	PrintDebug("\tSimple Intermediate Result: " + Result);

	QRegularExpressionMatchIterator Matches = SearchFormatText.globalMatch(Result);
	while (Matches.hasNext()) {
		QRegularExpressionMatch Match = Matches.next();
		QString Var = Match.captured(1);
		QString Value = "";
		PrintDebug("\tMatch Format: " + Match.captured(0) + " - " + Var);

		if (Var == "N") Value = "\\N";
		else if (Var == "n") Value = "\\n";
		else if (Var == "h") Value = "\\h";
		else if (Var == "h2") Value = "\\h\\h";
		else if (Var == "h4") Value = "\\h\\h\\h\\h";
		else if (Var == "h8") Value = "\\h\\h\\h\\h\\h\\h\\h\\h";
		else if (Var == "I1") Value = "{\\i1}";
		else if (Var == "I0") Value = "{\\i0}";
		else if (Var == "B1") Value = "{\\b1}";
		else if (Var == "B0") Value = "{\\b0}";
		else if (Var == "O0") Value = "{\\bord0}";
		else if (Var == "O1") Value = "{\\bord1}";
		else if (Var == "O2") Value = "{\\bord2}";
		else if (Var == "O3") Value = "{\\bord3}";
		else if (Var == "O4") Value = "{\\bord4}";
		else if (Var == "O5") Value = "{\\bord5}";
		else if (Var == "S0") Value = "{\\shad0}";
		else if (Var == "S1") Value = "{\\shad1}";
		else if (Var == "S2") Value = "{\\shad2}";
		else if (Var == "S3") Value = "{\\shad3}";
		else if (Var == "S4") Value = "{\\shad4}";
		else if (Var == "S5") Value = "{\\shad5}";
		else if (Var.startsWith("O#")) {
			Var.remove(0, 2);
			if ( Var == "")  {
				Var = Style->OutlineColour;
			} else {
				if (Vars.contains(Var)) Var = Vars.value(Var, Style->OutlineColour);
			}
			Value = "{\\3c&H" + StringColor2BGR(Var) + "&}";
		} else if (Var.startsWith("S#")) {
			Var.remove(0, 2);
			if ( Var == "")  {
				Var = Style->BackColour;
			} else {
				if (Vars.contains(Var)) Var = Vars.value(Var, Style->BackColour);
			}
			Value = "{\\3c&H" + StringColor2BGR(Var) + "&}";
		} else if (Var.startsWith("#")) {
			Var.remove(0, 1);
			if ( Var == "")  {
				Var = Style->PrimaryColour;
			} else {
				if (Vars.contains(Var)) Var = Vars.value(Var, Style->PrimaryColour);
			}
			Value = "{\\1c&H" + StringColor2BGR(Var) + "&}";
		}
		Result.replace(Match.captured(0), Value);
		PrintDebug("\t\tResult: " + Value);
	}

	if (Result == "") return "";

	Result.replace("\\%", "%");
	Result.replace("\\$", "$");

	PrintDebug("Total Result: " + Result);
	PrintDebug("");
	return Result;
}

QString TextSustitution::ProcessCMD(QString Context, QString CMD) {
	QString Result = CMD;
	PrintDebug("CMD substitution for:" + CMD);
	EmptyMatch(Context, Result, false);
	PrintDebug("\tComplex Intermediate Result: " + Result);
	SimpleMatch(Context, Result, false);
	Result.replace("\\%", "%");
	Result.replace("\\$", "$");
	PrintDebug("Total Result: " + Result);
	PrintDebug("");
	return Result;
}

QString TextSustitution::ProcessTemplate(QString Template) {
	QString Result = ReadFile(Template);
	PrintDebug("Template substitution for file:" + Template);
	EmptyMatch("", Result, false);
	SimpleMatch("", Result, false);
	Result.replace("\\%", "%");
	Result.replace("\\$", "$");
	return Result;
}

QStringList TextSustitution::SplitFunctionParameters(QString Params) {
	if (!Params.contains("(")) return Params.split(",");

	QStringList Output;

	while (!Params.isEmpty()) {
		QRegularExpressionMatch FunctionMatch = ParameterSearch.match(Params);
		if (FunctionMatch.hasMatch()) {
			QString Function = "";
			QString Rest = "";
			int np = 0;
			bool end = false;
			for (int i = 0; i < Params.size(); ++i) {
				QChar P = Params.at(i);
				if (end) {
					Rest.append(P);
					continue;
				}
				if (P == '(') np++;
				if (P == ')') np--;
				if (P == ',' && np == 0) {
					end = true;
					continue;
				}
				Function.append(P);
			}
			Output.append(Function);
			Params = Rest;
		} else {
			Output.append(Params.section(',', 0, 0));
			Params = Params.section(',', 1);
		}
	}
	return Output;
}

void TextSustitution::Insert(QString Var, QString Value, bool scape) {
	if (scape)
		Vars.insert(Var, ScapeCaracters(Value));
	else
		Vars.insert(Var, Value);
}

QString TextSustitution::Value(QString Var, QString Default) {
	return Vars.value(Var, Default);
}

bool TextSustitution::Contains(QString Var) {
	return Vars.contains(Var);
}

void TextSustitution::Remove(QString Var) {
	Vars.remove(Var);
}

void TextSustitution::Update(QString Var, QString Value, bool scape) {
	if (scape)
		Vars[Var] = ScapeCaracters(Value);
	else
		Vars[Var] = Value;
}

void TextSustitution::PrintAll() {
	PrintInfo("Variables available in text substitution:");
	for (QMap<QString, QString>::const_iterator i = Vars.constBegin();
		 i != Vars.constEnd();
		 ++i) {
		PrintInfo("\t" + i.key() + " = " + i.value());
	}
	PrintInfo("\tOK\n");
}

double TextSustitution::HumanFrameRate(QString Var) {
	if (Var == "") return 0;
	if (!Var.contains('/')) return Var.toDouble();
	QStringList SplitFrameRate = Var.split('/');
	double div1 = SplitFrameRate.first().toInt();
	double div2 = SplitFrameRate.last().toInt();
	double FrameRate = div1 / div2;
	return FrameRate;
}

QString TextSustitution::LoadFileVars(QString File) {
	QFileInfo FI(File);
	Insert("file.filename", FI.fileName());
	Insert("file.size", QString::number(FI.size()));
	Insert("file.basename", FI.completeBaseName());
	Insert("file.extension", FI.suffix());
	Insert("file.dir", QDir::toNativeSeparators(FI.absolutePath()));
	Insert("file.absolute", QDir::toNativeSeparators(FI.absoluteFilePath()));
	return FI.fileName();
}

QString TextSustitution::CutCenterText(QString text, int number) {
	if (text.length() <= number) return text;
	return text.left(number / 2 - 1) + "..." + text.right(number / 2 - 2);
}

QString TextSustitution::Text(QString text, bool invert) {
	if (invert) {
		text.replace("\n", "\\n");
		text.replace("\t", "\\t");
	} else {
		text.replace("\\n", "\n");
		text.replace("\\t", "\t");
	}
	return text;
}

QString TextSustitution::TextFunction(QString text, bool invert) {
	if (invert) {
		return "text(" + Text(text, true) + ")";
	} else {
		text.remove(0, 5);
		text.chop(1);
		return Text(text);
	}
}

bool TextSustitution::TextTest(QString text) {
	return (text.startsWith("text(") && text.endsWith(")"));
}

QString TextSustitution::LoadVarsFromFile(QString File, QString Context, bool Trim) {
	QString Content = ReadFile(File);
	QStringList Lines = Content.split("\n");
	if (Context != "") Context += ".";
	QString CurrentVar = "";
	QString CurrentVal = "";
	for (int i = 0; i < Lines.count(); ++i) {
		QString Line = Lines.at(i);
		QRegularExpressionMatch VarMatch = VarSearch.match(Line);
		QRegularExpressionMatch MultilineMatch = MultineVarSearch.match(Line);
		if ((VarMatch.hasMatch() || MultilineMatch.hasMatch()) && CurrentVar != "") {
			if (Trim) CurrentVal = CurrentVal.trimmed();
			Insert(Context + CurrentVar, CurrentVal);
			CurrentVar = "";
			CurrentVal = "";
		}

		if (VarMatch.hasMatch()) {
			Insert(Context + VarMatch.captured(1), VarMatch.captured(2));
			continue;
		}

		if (MultilineMatch.hasMatch()) {
			CurrentVar = MultilineMatch.captured(1);
			continue;
		}

		if (CurrentVar == "") continue;
		if (CurrentVal != "") CurrentVal += "\n";
		CurrentVal += Line;
	}
	if (CurrentVar != "") {
		if (Trim) CurrentVal = CurrentVal.trimmed();
		Insert(Context + CurrentVar, CurrentVal);
	}
	return Content;
}
