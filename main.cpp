#include "colors.h"
#include <QCoreApplication>
#include <QCommandLineParser>
#include <QTextStream>
#include <QSettings>
#include <QVariant>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QTemporaryDir>
#include <QLocale>
#include <QProcess>
#include <QTime>
#include <QRegularExpression>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QStandardPaths>
#include <batch.h>
#include <subtitles.h>
#include <modes.h>

const char *help_batch =
#include "help_batch.txt"
			;

#include "paramssettings.h"
#include "textsustitution.h"
#include "common.h"
#include "iso639.h"
#include "ffmpeginstances.h"
#include "filters.h"
#include "contactsheettextblock.h"
#include "images.h"
#include "configurationfunctions.h"
#include "layeredfilter.h"

//Subtitle format variable
sub_format SubFormat = sub_none;

QString check_video_file(QString dir, QString name, QString ext) {
	QString video = dir + QDir::separator() + name + "." + ext;
	if (QFile::exists(video))
		return video;
	else
		return "";
}

#define TERMINATE(error) Inst.Free();\
if (Save) PS.UpdateConfigFile(true, SaveAll);\
a.exit(error); \
return error

bool ExecuteCustomCommand(QString Command, TextSustitution *TS, bool ShowOutput = false) {
	if (Command == "") return false;
	QString CMD = Command;
	CMD = TS->ProcessCMD("", CMD);
	QStringList Params = QProcess::splitCommand(CMD);
	if (Params.isEmpty()) return true;
	QString Program = Params.takeFirst();

	QProcess process;
	if (Cmd) PrintInfo(CMD);
	process.start(Program, Params);
	process.waitForStarted(-1);
	if (ShowOutput) {
		while (process.state() == QProcess::Running) {
			process.waitForFinished(20);
			QByteArray read = process.readAllStandardError();
			if (!read.isEmpty()) {
				fprintf(stderr, "%s", read.data());
				fflush(stderr);
			}

			read = process.readAllStandardOutput();
			if (!read.isEmpty()) {
				fprintf(stdout, "%s", read.data());
				fflush(stdout);
			}
		}
	} else {
		process.waitForFinished(-1);
	}

	if (process.exitCode() != 0 ) {
		QString error = process.readAllStandardError();
		PrintError(CMD);
		PrintError(error);
	}
	return true;
}

bool ExecuteExecOption(QString Var, ParamsSettings *PS, TextSustitution *TS) {
	QString CMD = PS->Value(Var);
	if (CMD == "") return false;
	PrintInfo("Executing " + Var + " option:");
	ExecuteCustomCommand(CMD, TS, true);
	PrintInfo("\tOK\n");
	return true;
}

bool ExecuteGifsicle(ParamsSettings *PS, TextSustitution *TS, QString OUTPUT_QUALITY) {
	if (PS->Value("format") != "giflossy") return false;
	if (PS->Value("gifsicle") == "") {
		PrintError("giflossy format needs gifsicle option configured");
		return false;
	}
	QString command = "\"" + PS->Value("gifsicle") + "\" \"%out.file%\" -o \"%out.dir%/%out.basename%.lossy.gif\" --lossy="
					  + OUTPUT_QUALITY;
	PrintInfo("Executing gifsicle:");
	bool result = ExecuteCustomCommand(command, TS);
	PrintInfo("\tOk\n");
	return result;
}

int main(int argc, char *argv[]) {

	QCoreApplication a(argc, argv);

	#if defined (Q_OS_WIN)
	if (argc == 1) {
		QString GUI = QCoreApplication::applicationDirPath() + "SCSGUI.exe";
		if (QFile::exists(GUI)) {
			QProcess::startDetached(GUI, QStringList());
			return 0;
		}
	}
	#endif

	ParamsSettings PS(QCoreApplication::applicationDirPath());

	QStringList CONFIG;
	CONFIG.append("config");
	for (int i = 1; i < argc; i++) {
		QString param = READPARAMETER(i);
		if (param == "--config" || param == "-c") {
			QString CurConfig = QString::fromUtf8(argv[i + 1]);
			if (!CONFIG.contains(CurConfig))
				CONFIG.append(CurConfig);
		} else if (param == "--list_config") {
			QStringList ConfigList = PS.ConfigList();
			PrintInfo("");
			PrintInfo("Available configuration files:");
			if (ConfigList.count() > 0) {
				int maxlenght = 0;
				for (int i = 0; i < ConfigList.count(); ++i)
					if (ConfigList.at(i).length() > maxlenght) maxlenght = ConfigList.at(i).length();
				maxlenght += 1;
				for (int i = 0; i < ConfigList.count(); ++i) {
					QString Name = ConfigList.at(i).section(":", 0, 0);
					QString Desc = ConfigList.at(i).section(":", 1, -1);
					PrintInfo("  " + Name.leftJustified(maxlenght, ' ') + " | " + Desc.trimmed());
				}
			} else {
				PrintInfo("");
				PrintInfo("No configuration files available.");
			}
			return 0;
		} else if (param == "--list_config_gui") {
			QStringList ConfigList = PS.ConfigList();
			for (int i = 0; i < ConfigList.count(); ++i) {
				PrintInfo(ConfigList.at(i).section(":", 0, 0));
			}
			return 0;
		} else if (param == "--quiet") {
			Quiet = true;
		} else if (param == "-b" || param == "--batch") {
			Batch BatchMode;
			return BatchMode.Execute(argc, argv);
		} else if (param == "--debug") {
			Debug = true;
		} else if (param == "--version" || param  == "-v") {
			fprintf(stdout, "Subtitles Contact Sheet version %s\n", SCS_VERSION);
			return 0;
		} else if (param == "--help" || param == "-h") {
			PS.printhelp(QCoreApplication::applicationName(), "");
			PS.printhelp(QCoreApplication::applicationName(), "out");
			PS.printhelp(QCoreApplication::applicationName(), "as");
			PS.printhelp(QCoreApplication::applicationName(), "ad");
			return 0;
		} else if (param == "--help-sub") {
			PS.printhelp(QCoreApplication::applicationName(), "sub");
			return 0;
		} else if (param == "--help-text") {
			PS.printhelp(QCoreApplication::applicationName(), "tb");
			return 0;
		} else if (param == "--help-time") {
			PS.printhelp(QCoreApplication::applicationName(), "ft");
			return 0;
		} else if (param == "--help-vr") {
			PS.printhelp(QCoreApplication::applicationName(), "vr");
			return 0;
		} else if (param == "--help-rename") {
			PS.printhelp(QCoreApplication::applicationName(), "rename");
			return 0;
		} else if (param == "--help-batch") {
			fprintf(stdout, help_batch, QCoreApplication::applicationName().toUtf8().constData());
			return 0;
		} else if (param == "--help-images") {
			PS.printhelp(QCoreApplication::applicationName(), "images");
			return 0;
		} else if (param == "--help-wm") {
			PS.printhelp(QCoreApplication::applicationName(), "wm");
			return 0;
		} else if (param == "--help-all") {
			PS.printhelp(QCoreApplication::applicationName(), "");
			PS.printhelp(QCoreApplication::applicationName(), "out");
			PS.printhelp(QCoreApplication::applicationName(), "as");
			PS.printhelp(QCoreApplication::applicationName(), "ad");
			PS.printhelp(QCoreApplication::applicationName(), "sub");
			PS.printhelp(QCoreApplication::applicationName(), "tb");
			PS.printhelp(QCoreApplication::applicationName(), "ft");
			PS.printhelp(QCoreApplication::applicationName(), "vr");
			PS.printhelp(QCoreApplication::applicationName(), "rename");
			PS.printhelp(QCoreApplication::applicationName(), "images");
			fprintf(stdout, help_batch, QCoreApplication::applicationName().toUtf8().constData());
			return 0;

		} else if (param == "--list_extensions") {
			PrintInfo("Video Extensions: " + VideoExtensions.join(", "));
			PrintInfo("");
			PrintInfo("Subtitles Extensions: " + SubtitleExtensions.join(", "));
			return 0;

		} else {
			if (!param.startsWith("--")) continue;
			QString ParamCut = param.remove(0, 2);
			if (PS.Exists(ParamCut)) continue;
			if (PS.ConfigExists(ParamCut)) {
				if (!CONFIG.contains(ParamCut))
					CONFIG.append(ParamCut);
			}
		}
	}

	for (int i = 0; i < CONFIG.size(); ++i) {
		PrintDebug("");
		PrintDebug("Loading config (" + CONFIG.at(i) + ") file variables");
		PS.LoadConfig(CONFIG.at(i));
	}

	bool Save = false;
	bool SaveAll = false;

	//Comand line params only vars
	QString SUBFILE = "";
	QString VIDEOFILE = "";
	QString IMAGES = "";
	QString DVD = "";
	QString DVD_TITLE = "";
	QString OUTPUTFILE = "";
	QString SUBLANGCODE = "";
	QStringList MANUALFRAMES;
	QString MFS = "";

	//Subtitle vars
	bool SubtitleEnable = false;
	QString SUBDIR = "";
	QString SUBFILEMAME = "";
	QString SUBNAME = "";
	int SubStream = -1;
	QString SUBEXT = "";
	uint64_t SubtitleFileSize = 0;
	QString SUBTITLE_FORMAT = "";
	QString SUBLANG = "";
	QString SUBNAME_LANG = "";
	bool ShowSubText = false;

	//Ffmpeg params vars
	QStringList FORMAT_OPTIONS;
	QStringList FORMAT_AUDIO_OPTIONS;
	bool INDEPENDENT_COMMAND = false;

	//Video file vars
	QString VIDEOFILENAME;
	int VideoWidth = -1;
	int VideoHeight = -1;
	int NewVideoWidth = -1;
	int NewVideoHeight = -1;
	bool VerticalVideo = false;
	double ResolutionAspectRatio = 0;
	double DisplayAspectRatio = 0;
	double OutputAspectRatio = 0;
	bool VRChangeAR = false;
	QString VIDEO_JSON = "";
	QJsonDocument VideoMetadata;
	QJsonParseError JsonError;
	QJsonObject JsonRoot;
	QJsonArray VideoStreams;
	bool VR;
	QString VR_IN = "";
	QString VR_IN_STEREO = "";
	int VR_IH_FOV = 0;
	int VR_IV_FOV = 0;

	//Output vars
	QString OUTPUT_FORMAT;
	QString OUTPUT_QUALITY;

	//Processing vars
	QList<sub_line_t> Timestamps;
	QList<sub_line_t> Screenshots;
	subtitle_totals_t Totals;
	QString TOTALLENFORMATED = "";
	QStringList SCREENSHOT_FILTERS;
	QString SCREENSHOT_FILTER;

	//Text block vars
	bool PrintVars = false;

	QString CAPTURE_NAME;
	QStringList VarFiles;

	PrintDebug("");
	PrintDebug("Loading parameter variables");

	//Moves the loop to the next element and displays an error if it does not exist
#define next_param(error) i++; \
	if (i == argc) {\
		qFatal(error);\
		return 1;\
	}

#define debug_par_str(var) PrintDebug("\t"+param + ": "+ QString(#var)+" = " +var)
#define debug_par_int(var) PrintDebug("\t"+param + ": "+ QString(#var)+" = " +QString::number(var))
#define debug_par_bool(var) PrintDebug("\t"+param + ": "+ QString(#var)+" = " + QString((var) ? "1" : "0"))

	for (int i = 1; i < argc; i++) {
		QString param = READPARAMETER(i);
		if (param == "--sub") {
			next_param("Incorrect use of --sub");
			SUBFILE = READPARAMETER(i);
			debug_par_str(SUBFILE);
			SubtitleEnable = true;
			debug_par_bool(SubtitleEnable);

		} else if (param == "--sub_stream") {
			next_param("Incorrect use of --sub_stream");
			SubStream = READPARAMETER(i).toInt();
			debug_par_int(SubStream);

		} else if (param == "--video") {
			next_param("Incorrect use of --video");
			VIDEOFILE = READPARAMETER(i);
			debug_par_str(VIDEOFILE);

		} else if (param == "--images") {
			next_param("Incorrect use of --images");
			IMAGES = READPARAMETER(i);
			debug_par_str(IMAGES);

		} else if (param == "--dvd") {
			next_param("Incorrect use of --dvd");
			DVD = READPARAMETER(i);
			debug_par_str(DVD);

		} else if (param == "--dvd_title") {
			next_param("Incorrect use of --dvd_title");
			DVD_TITLE = READPARAMETER(i);
			debug_par_str(DVD_TITLE);

		} else if (param == "--out") {
			next_param("Incorrect use of --out");
			OUTPUTFILE = READPARAMETER(i);
			debug_par_str(OUTPUTFILE);
			OUTPUT_FORMAT = QFileInfo(OUTPUTFILE).suffix().toLower();
			debug_par_str(OUTPUT_FORMAT);

		} else if (param == "--config" || param == "-c") {
			i++;
			continue;

		} else if (param == "--time") {
			PS.setValue("mode", "time");
		} else if (param == "--line") {
			PS.setValue("mode", "line");
		} else if (param == "--full") {
			PS.setValue("size", "");
		} else if (param == "--screenshots") {
			next_param("Incorrect use of --screenshots");
			PS.setValue("layout", "1x" + READPARAMETER(i));
			PS.setValue("format", "png");
			PS.setValueBool("no_grid", true);
			PS.setValueBool("ft_hide", true);
			PS.setValueBool("concat", false);
			PS.setValue("size", "");
			PS.setValue("suffix", ".screenshot_");

		} else if (param == "--srt") {
			SubFormat = sub_srt;
			PrintDebug(param + ": SubFormat = srt");

		} else if (param == "--ass") {
			SubFormat = sub_ass;
			PrintDebug(param + ": SubFormat = ass");

		} else if (param == "--vtt") {
			SubFormat = sub_vtt;
			PrintDebug(param + ": SubFormat = vtt");

		} else if (param == "--lang") {
			next_param("Incorrect use of --lang");
			SUBLANGCODE = READPARAMETER(i);
			debug_par_str(SUBLANGCODE);

		} else if (param == "--jpg") {
			PS.setValue("format", "jpg");

		} else if (param == "--jpeg") {
			PS.setValue("format", "jpg");

		} else if (param == "--jxl") {
			PS.setValue("format", "jxl");

		} else if (param == "--png") {
			PS.setValue("format", "png");

		} else if (param == "--apng") {
			PS.setValue("format", "apng");

		} else if (param == "--webp") {
			PS.setValue("format", "webp");

		} else if (param == "--losslesswebp") {
			PS.setValue("format", "losslesswebp");

		} else if (param == "--webploop") {
			PS.setValue("format", "webploop");

		} else if (param == "--gif") {
			PS.setValue("format", "gif");

		} else if (param == "--giflow") {
			PS.setValue("format", "giflow");

		} else if (param == "--giflossy") {
			PS.setValue("format", "giflossy");

		} else if (param == "--xvid") {
			PS.setValue("format", "xvid");

		} else if (param == "--x264") {
			PS.setValue("format", "x264");

		} else if (param == "--x265") {
			PS.setValue("format", "x265");

		} else if (param == "--av1") {
			PS.setValue("format", "av1");

		} else if (param == "--webmvp8") {
			PS.setValue("format", "webmvp8");

		} else if (param == "--webmvp9") {
			PS.setValue("format", "webmvp9");

		} else if (param == "--webmav1") {
			PS.setValue("format", "webmav1");

		} else if (param == "--avif") {
			PS.setValue("format", "avif");

		} else if (param == "--tb_comment_append") {
			next_param("Incorrect use of --tb_add_comment");
			QString Comments = PS.Value("tb_comments");
			Comments += "%N%%#%" + READPARAMETER(i);
			PS.setValue("tb_comments", Comments);

		} else if (param == "--tb_comment_prepend") {
			next_param("Incorrect use of --tb_add_comment");
			QString Comments = PS.Value("tb_comments");
			Comments = "%#%" + READPARAMETER(i) + "%N%" + Comments;
			PS.setValue("tb_comments", Comments);

		} else if (param == "--tb_list_vars" || param == "--list_vars") {
			PrintVars = true;
			debug_par_bool(PrintVars);

		} else if (param == "--tb_transparent") {
			PS.setValue("tb_bg_color", "00000000");
			PS.setValue("grid_border_color", "00000000");
			PS.setValue("format", "png");

		} else if (param == "--tb_left") {
			PS.setValue("tb_title_pos", "up_left");
			PS.setValue("tb_font_pos", "middle_left");
			PS.setValue("tb_comments_pos", "down_left");
			PS.setValue("tb_logo_pos", "right");

		} else if (param == "--tb_right") {
			PS.setValue("tb_title_pos", "up_right");
			PS.setValue("tb_font_pos", "middle_right");
			PS.setValue("tb_comments_pos", "down_right");
			PS.setValue("tb_logo_pos", "left");

		} else if (param == "--green") {
			PS.setValue("sub_color", "00FF00");
		} else if (param == "--yellow") {
			PS.setValue("sub_color", "FFEA00");
		} else if (param == "--small") {
			PS.setValue("sub_size", "12");
		} else if (param == "--big") {
			PS.setValue("sub_size", "24");
		} else if (param == "--verybig") {
			PS.setValue("sub_size", "32");
		} else if (param == "--box") {
			PS.setValue("sub_border", "4");
		} else if (param == "--tbox") {
			PS.setValue("sub_border", "4");
			PS.setValue("sub_bc", "00000050");

		} else if (param == "--fisheye200") {
			AutoDetectVRFormat = "fisheye";
			AutoDetectVRMode = "sbs";
			AutoDetectVRFov = "200";

		} else if (param == "--fisheye190") {
			AutoDetectVRFormat = "fisheye";
			AutoDetectVRMode = "sbs";
			AutoDetectVRFov = "190";

		} else if (param == "--vr180") {
			AutoDetectVRFormat = "equirect";
			AutoDetectVRMode = "sbs";
			AutoDetectVRFov = "180";

		} else if (param == "--vr360") {
			AutoDetectVRFormat = "equirect";
			AutoDetectVRMode = "tb";
			AutoDetectVRFov = "360";

		} else if (param == "--vr.format") {
			next_param("Incorrect use of --vr.format");
			AutoDetectVRFormat = READPARAMETER(i);

		} else if (param == "--vr.mode") {
			next_param("Incorrect use of --vr.mode");
			AutoDetectVRMode = READPARAMETER(i);

		} else if (param == "--vr.fov") {
			next_param("Incorrect use of --vr.fov");
			AutoDetectVRFov = READPARAMETER(i);

		} else if (param == "--gif_images") {
			next_param("Incorrect use of --gif_images");
			PS.setValueBool("concat", true);
			PS.setValueBool("no_grid", true);
			PS.setValueBool("ft_hide", true);
			PS.setValue("format", "gif");
			PS.setValue("layout", "1x" + READPARAMETER(i));
			PS.setValueInt("video_fps", 1);
			PS.setValue("suffix", ".images");

		} else if (param == "--webp_images") {
			next_param("Incorrect use of --webp_images");
			PS.setValueBool("concat", true);
			PS.setValueBool("no_grid", true);
			PS.setValueBool("ft_hide", true);
			PS.setValue("format", "webploop");
			PS.setValue("layout", "1x" + READPARAMETER(i));
			PS.setValueInt("video_fps", 1);
			PS.setValue("suffix", ".images");

		} else if (param == "--gif_clip") {
			next_param("Incorrect use of --gif_clip");
			PS.setValueBool("concat", true);
			PS.setValueBool("no_grid", true);
			PS.setValueBool("ft_hide", true);
			PS.setValue("format", "gif");
			PS.setValue("layout", "1x" + READPARAMETER(i));
			PS.setValueInt("video_preview", 1000);
			PS.setValueInt("size", 320);
			PS.setValueInt("min_size", 0);
			PS.setValueInt("video_fps", 12);
			PS.setValue("suffix", ".clip");

		} else if (param == "--gif_singles") {
			next_param("Incorrect use of --gif_clip");
			PS.setValueBool("concat", false);
			PS.setValueBool("no_grid", true);
			PS.setValueBool("ft_hide", true);
			PS.setValue("format", "gif");
			PS.setValue("layout", "1x" + READPARAMETER(i));
			PS.setValueInt("video_preview", 1000);
			PS.setValueInt("size", 320);
			PS.setValueInt("min_size", 0);
			PS.setValueInt("video_fps", 12);
			PS.setValue("suffix", ".clip");

		} else if (param == "--webp_clip") {
			next_param("Incorrect use of --webp_clip");
			PS.setValueBool("concat", true);
			PS.setValueBool("no_grid", true);
			PS.setValueBool("ft_hide", true);
			PS.setValue("format", "webploop");
			PS.setValue("layout", "1x" + READPARAMETER(i));
			PS.setValueInt("video_preview", 1000);
			PS.setValueInt("video_fps", 12);
			PS.setValueInt("min_size", 0);
			PS.setValue("suffix", ".clip");

		} else if (param == "--mf") {
			next_param("Incorrect use of --mf");
			MANUALFRAMES.append(READPARAMETER(i));
			PrintDebug("\t" + param + " = " + READPARAMETER(i));

		} else if (param == "--gui_progress") {
			GUIProgress = true;

		} else if (param == "--save") {
			Save = true;

		} else if (param == "--save_all") {
			Save = true;
			SaveAll = true;
		} else if (param == "--var") {
			next_param("Incorrect use of --var");
			PrintDebug("\t--var " + READPARAMETER(i));
			PS.UserDefinedVar(READPARAMETER(i));
		} else if (param == "--var_stream") {
			next_param("Incorrect use of --var_stream");
			PrintDebug("\t--var_stream " + READPARAMETER(i));
			PS.UserDefinedVar(READPARAMETER(i), ParamsSettings::context_stream);
		} else if (param == "--var_stream_video") {
			next_param("Incorrect use of --var_stream_video");
			PrintDebug("\t--var_stream_video " + READPARAMETER(i));
			PS.UserDefinedVar(READPARAMETER(i), ParamsSettings::context_video);
		} else if (param == "--var_stream_audio") {
			next_param("Incorrect use of --var_stream_audio");
			PrintDebug("\t--var_stream_audio " + READPARAMETER(i));
			PS.UserDefinedVar(READPARAMETER(i), ParamsSettings::context_audio);
		} else if (param == "--var_stream_subtitle") {
			next_param("Incorrect use of --var_stream_subtitle");
			PrintDebug("\t--var_stream_subtitle " + READPARAMETER(i));
			PS.UserDefinedVar(READPARAMETER(i), ParamsSettings::context_subtitle);
		} else if (param == "--var_stream_attachment") {
			next_param("Incorrect use of --var_stream_attachment");
			PrintDebug("\t--var_stream_attachment " + READPARAMETER(i));
			PS.UserDefinedVar(READPARAMETER(i), ParamsSettings::context_attachment);

		} else if (param == "--var_file") {
			next_param("Incorrect use of --var_file");
			PrintDebug("\t--var_file " + READPARAMETER(i));
			VarFiles.append(READPARAMETER(i));

		} else if (param == "--tb_use_mediainfo") {
			PS.setValueBool("enable_mediainfo", true);
			PS.setValue("tb_custom_header",
						"Container: %B1%%format.mediainfo.Format%%B0% Duration: %B1%%format.mediainfo.Duration_String%%B0% Size: %B1%%format.mediainfo.FileSize_String% (%numsep(format.mediainfo.FileSize)% Bytes)%B0%$ Bitrate: %B1%$%format.mediainfo.OverallBitRate_String%$%B0%$");

			PS.setValue("tb_custom_video_stream",
						"Stream %index% Video: %B1%%mediainfo.Width%x%mediainfo.Height% %mediainfo.ColorSpace% %mediainfo.ChromaSubsampling% %mediainfo.BitDepth_String% %mediainfo.FrameRate% fps$ $%mediainfo.BitRate_String%$$ %mediainfo.Format%%B0%");
			PS.setValue("tb_custom_audio_stream",
						"Stream %index% Audio: %B1%$$%mediainfo.Language_String%$$$ $%mediainfo.Channels_String%$$$ $%mediainfo.SamplingRate_String%$$$ $%mediainfo.BitRate_String%$$ %mediainfo.Format%%B0%");
			PS.setValue("tb_custom_subtitle_stream",
						"Stream %index% Subtitle: %B1%$$%mediainfo.Title%$ - $$$%mediainfo.Language_String%$ $%mediainfo.Format%%B0%");
		} else if (PS.ProcessParameter(i, argc, argv)) {
			continue;
		} else {
			if (CONFIG.contains(param.remove(0, 2))) continue;

			QFileInfo FileInfo(READPARAMETER(i));
			QString Ext = FileInfo.suffix().toLower();
			if (FileInfo.isFile() && SubtitleExtensions.contains(Ext)) {
				SUBFILE = READPARAMETER(i);
				PrintDebug("\tSUBFILE = " + SUBFILE);
				SubtitleEnable = true;
				debug_par_bool(SubtitleEnable);
			} else if (FileInfo.isFile() && VideoExtensions.contains(Ext)) {
				VIDEOFILE = READPARAMETER(i);
				PrintDebug("\tVIDEOFILE = " + VIDEOFILE);
			} else if (FileInfo.isDir()) {
				IMAGES = READPARAMETER(i);
				PrintDebug("\tIMAGES = " + IMAGES);
			} else {
				PrintError("Incorrect option: " + READPARAMETER(i));
				return 10;
			}
		}
	}

	FFMPEG = PS.Value("ffmpeg");
	FFPROBE = PS.Value("ffprobe");
	TMP = PS.Value("tmp");

	FFmpegInstances Inst(PS.Int("process"));
	Idle = PS.Bool("idle");

	if (SubtitleEnable && !QFile::exists(SUBFILE)) {
		PrintError("Input subtitle file missing");
		TERMINATE(1);
	}

	if (PS.Bool("quiet")) Quiet = true;
	if (PS.Bool("debug")) Debug = true;
	if (PS.Bool("cmd")) Cmd = true;

	PrintProgress(0, 1);

	if (!DetectFontconfig()) {
		TERMINATE(9);
	}

	//Generate tmpdir from options
	if (TMP == "")
		TMP = QDir::tempPath() + QDir::separator() + a.applicationName() + "_XXXXXX";
	else
		TMP = TMP + QDir::separator() + a.applicationName() + "_XXXXXX";

	QTemporaryDir TmpDir(TMP);
	if (TmpDir.isValid()) {
		TMP = QDir::toNativeSeparators(TmpDir.path());
		if (Debug) TmpDir.setAutoRemove(false);
	} else {
		PrintError("Error creting tmp directory");
		return 7;
	}

	TMP_TEXT = TMP + QDir::separator() + "head_subtitle.ass";
	TMP_TEXT_IMG = TMP + QDir::separator() + "text.png";
	TMP_IMAGES = TMP + QDir::separator() + "images";
	TMP_BACKGROUND = TMP + QDir::separator() + "background.png";
	TMP_WATERMARK = TMP + QDir::separator() + "watermark.png";
	TMP_TRANSPARENT_SHEET = TMP + QDir::separator() + "transparent.png";
	TMP_WITHOUT_WATERMARK = TMP + QDir::separator() + "without_watermark.png";

	DEBUGVAR(TMP);
	DEBUGVAR(TMP_TEXT);
	DEBUGVAR(TMP_TEXT_IMG);
	DEBUGVAR(TMP_IMAGES);

	DEBUGVAR(SUBFILE);

	//ffv1
	TMP_VIDEO_FORMAT_OPTIONS << "-c:v" << "ffv1";
	//x264 lossless
	//TMP_VIDEO_FORMAT_OPTIONS << "-c:v" << "libx264" << "-preset" << "ultrafast" << "-qp" << "0";
	//VP9 lossless
	// TMP_VIDEO_FORMAT_OPTIONS << "-c:v" << "libvpx-vp9" << "-lossless" << "1";

	TMP_AUDIO_FORMAT_OPTIONS << "-c:a" << "flac";

	int VideoPreview = PS.Int("video_preview");
	if (VideoPreview != 0) {
		CAPTURE_NAME = "Video fragment";
		TMP_FRAME_FORMAT = "mkv";
		TMP_FRAME_FORMAT_OPTIONS = TMP_VIDEO_FORMAT_OPTIONS;
	} else {
		CAPTURE_NAME = "Screenshot";
		TMP_FRAME_FORMAT = "png";
	}

	TextSustitution TS;
	#ifndef PORTABLE_CONFIG
	TS.SetReplaceDir(QStandardPaths::locate(QStandardPaths::ConfigLocation, "subtitles_contact_sheet",
											QStandardPaths::LocateDirectory));
	#else
	TS.SetReplaceDir(QCoreApplication::applicationDirPath());
	#endif

	TS.Insert("self", QCoreApplication::applicationFilePath());
	TS.Insert("self.dir", QCoreApplication::applicationDirPath());

	for (int i = 0; i < VarFiles.count(); ++i) {
		QString File = VarFiles.at(i);
		QString Context = "";
		if (File.contains("=")) {
			Context = File.section("=", 0, 0);
			File = File.section("=", 1);
		}
		if (QFile::exists(File))
			TS.LoadVarsFromFile(File, Context, true);
	}

	if (IMAGES != "") {
		Images *I = new Images(IMAGES, &PS, &TS, &Inst);
		Steps = NumberOfFrames * 2 + 3;
		if (!PS.Bool("concat") && I->ShowTB) Steps++;
		if (PS.Value("exec_after") != "") Steps++;
		if (PS.Value("format") == "giflossy") Steps++;

		I->SetOutput(OUTPUTFILE);

		I->GenerateImageList();
		AddProgress();

		if (!I->SelectImages()) {
			TERMINATE(5);
		}

		ProcessFunctions(&PS, &TS);

		I->CalculateOutputResolution();
		AddProgress();

		if (PrintVars || Debug) {
			TS.PrintAll();
			if (PrintVars) {
				TERMINATE(0);
			}
		}

		if (PS.Bool("no_overwrite") && (!PS.Bool("no_grid") || PS.Bool("concat"))) {
			if (QFile::exists(OUTPUTFILE)) {
				PrintInfo("No overwriting file: " + OUTPUTFILE);
				ExecuteGifsicle(&PS, &TS, I->OutputQuality);
				ExecuteExecOption("exec_after", &PS, &TS);
				PrintProgress(1, 1);
				TERMINATE(0);
			}
		}

		I->GenarateThumbnails();

		if (PS.Bool("concat")) {
			I->GenerateAnimation();
			AddProgress();
		} else {
			I->GenerateGrid();
			AddProgress();
			if (I->ShowTB) {
				I->GenetateStatsBlock();
				AddProgress();
			}
			I->ApplyBG();
			I->ApplyWatermark();
		}

		if (ExecuteGifsicle(&PS, &TS, I->OutputQuality)) AddProgress();
		if (ExecuteExecOption("exec_after", &PS, &TS)) AddProgress();

		TERMINATE(0);
	}

	if (SubtitleEnable) {
		//Generate all varibles from subtitle filename
		QFileInfo SubFileInfo(SUBFILE);
		SUBDIR = QDir::toNativeSeparators(SubFileInfo.absolutePath());
		SUBFILEMAME = SubFileInfo.fileName();
		SUBNAME = SubFileInfo.completeBaseName();
		SUBEXT = SubFileInfo.suffix();
		if (SubFormat == sub_none) SubFormat = String2SubFormat(SUBEXT);
		SubtitleFileSize = SubFileInfo.size();
		ShowSubText = true;

		DEBUGVAR(SUBDIR);
		DEBUGVAR(SUBFILEMAME);
		DEBUGVAR(SUBNAME);
		DEBUGVAR(SUBEXT);
		DEBUGVAR(SubtitleFileSize);

		//Generate text input format for header
		switch (SubFormat) {
			case sub_srt:
				SUBTITLE_FORMAT = "SubRip";
				DebugVarFunt("SUBFORMAT", "srt");
				break;
			case sub_ass:
				SUBTITLE_FORMAT = "Advanced SubStation Alpha";
				DebugVarFunt("SUBFORMAT", "ass");
				break;
			case sub_vtt:
				SUBTITLE_FORMAT = "WebVTT";
				DebugVarFunt("SUBFORMAT", "vtt");
				break;
			case sub_none:
				PrintError("Imposible determine subtitle format, use --srt or --ass or --vtt");
				return 2;
		}

		SUBLANG = "";
		SUBNAME_LANG = "";
		if (SUBLANGCODE == "") {
			SUBLANGCODE = QFileInfo(SUBNAME).suffix();
			if (SUBLANGCODE == SUBNAME) { SUBLANGCODE = ""; }
			if (SUBLANGCODE != "") {
				QLocale locale(SUBLANGCODE);
				SUBLANG = QLocale::languageToString(locale.language());
				if (SUBLANG == "" || SUBLANG == "C") {
					SUBLANGCODE = "";
					SUBLANG = "";
				} else {
					SUBNAME_LANG = QFileInfo(SUBNAME).completeBaseName();
				}
			}
		}
		DEBUGVAR(SUBLANGCODE);
		DEBUGVAR(SUBNAME_LANG);

		if (SUBLANGCODE != "") {
			QLocale locale(SUBLANGCODE);
			SUBLANG = QLocale::languageToString(locale.language());
			DEBUGVAR(SUBLANG);
		}

		PrintInfo("Using subtitle file: " + SUBFILE);
		PrintInfo("\tSubtitle format: " + SUBTITLE_FORMAT);
		if (SUBLANG != "") PrintInfo("\tSubtitle language: " + SUBLANG);
		PrintInfo("");
	}

	if (DVD != "") {
		PrintInfo("Using DVD VIDEO_TS folder: " + DVD);
		if (DVD_TITLE.toInt() < 1) {
			PrintInfo("\tDetecting longest DVD Title:");
			bool TitleError = false;
			int TitleCount = 1;
			double MaxDuration = 0;
			int MaxTitle = 0;
			while (!TitleError) {
				QString Dur;
				if (!ffprobe(DVD, -1, "format=duration", Dur, true, TitleCount, true)) {
					TitleCount = true;
					break;
				}
				PrintInfo("\t\tTitle " + QString::number(TitleCount) + ": " + Dur);
				if (Dur.toDouble() > MaxDuration) {
					MaxDuration = Dur.toDouble();
					MaxTitle = TitleCount;
				}
				TitleCount++;
			}
			DVD_TITLE = QString::number(MaxTitle);
		}

		PrintInfo("\tTitle: " + DVD_TITLE);

		//TODO: Error on invalid VIDEO_TS

		QDir D(DVD);
		QString Absolute = D.absolutePath();
		if (Absolute.toUpper().endsWith("VIDEO_TS"))
			D.cdUp();

		TS.Insert("file.filename", D.dirName());
		TS.Insert("file.size", QString::number(DirSize(DVD)));
		TS.Insert("file.basename", D.dirName());
		TS.Insert("file.extension", "");
		TS.Insert("file.dir", QDir::toNativeSeparators(D.absolutePath()));
		TS.Insert("file.absolute", QDir::toNativeSeparators(D.absolutePath()));

		TS.Insert("title", "DVD: " + D.dirName() + " Title: " + DVD_TITLE);
		TS.Insert("in.filename", D.dirName() + "_title" + DVD_TITLE);
		TS.Insert("in.basename", TS.Value("file.basename"));
		TS.Insert("in.extension", TS.Value("file.extension"));
		TS.Insert("in.dir", QDir::toNativeSeparators(D.absolutePath()));

		if (!ffprobe(DVD, VIDEO_JSON, true, DVD_TITLE.toInt())) {
			PrintError("Error reading video file metadata");
			TERMINATE(7);
		}
		//TODO: Try to enable subtitles

	} else {

		if (VIDEOFILE == "") {
			for (int i = 0; i < VideoExtensions.count(); i++) {
				VIDEOFILE = check_video_file(SUBDIR, SUBNAME, VideoExtensions.at(i));
				if (VIDEOFILE != "") break;
				if (SUBNAME_LANG != "") {
					VIDEOFILE = check_video_file(SUBDIR, SUBNAME_LANG, VideoExtensions.at(i));
					if (VIDEOFILE != "") break;
				}
			}
		}

		VIDEOFILE = QDir::toNativeSeparators(VIDEOFILE);

		PrintInfo("Using video file: " + VIDEOFILE);

		if (!QFile::exists(VIDEOFILE)) {
			PrintError("Input video file missing");
			TERMINATE(3);
		}

		VIDEOFILENAME = TS.LoadFileVars(VIDEOFILE);

		if (SubtitleEnable) {
			TS.Insert("title", SUBFILEMAME);
			TS.Insert("in.filename", SUBFILEMAME);
			TS.Insert("in.basename", SUBNAME);
			TS.Insert("in.extension", SUBEXT);
			TS.Insert("in.dir", SUBDIR);
		} else {
			TS.Insert("title", VIDEOFILENAME);
			TS.Insert("in.filename", VIDEOFILENAME);
			TS.Insert("in.basename", TS.Value("file.basename"));
			TS.Insert("in.extension", TS.Value("file.extension"));
			TS.Insert("in.dir", TS.Value("file.dir"));
		}

		DEBUGVAR(VIDEOFILE);

		if (!ffprobe(VIDEOFILE, VIDEO_JSON)) {
			PrintError("Error reading video file metadata");
			TERMINATE(7);
		}

	}

	VideoMetadata = QJsonDocument::fromJson(VIDEO_JSON.toUtf8(), &JsonError);
	if (!VideoMetadata.isObject()) {
		PrintError("Error reading video file metadata");
		TERMINATE(7);
	}

	JsonRoot = VideoMetadata.object();
	VideoStreams = JsonRoot.value("streams").toArray();

	TS.LoadVars(JsonRoot);

	if (TS.Value("format.size" "") == "") TS.Insert("format.size", TS.Value("file.size"));

	int FirstVideo = -1;
	int FirstAudio = -1;
	int VideoCount = 0;
	int AudioCount = 0;
	int SubCount = 0 ;
	int AttachCount = 0;

	for (int i = 0; i < VideoStreams.count(); i++) {
		QString Bitrate = "";
		QString Context = "streams." + QString::number(i) + ".";
		QString BitrateVar = Context + "bit_rate";
		//		QString FrameRateVar = Context + "r_frame_rate";
		Bitrate = TS.Value(BitrateVar, "");
		if (Bitrate == "N/A" || Bitrate == "")
			Bitrate = TS.Value(Context + "tags.BPS-eng", "");
		if (Bitrate == "N/A" || Bitrate == "")
			Bitrate = TS.Value(Context + "tags.BPS", "");

		if (Bitrate == "" && TS.Contains(BitrateVar))
			TS.Remove(BitrateVar);

		if (Bitrate != "") TS.Update(BitrateVar, Bitrate);

		QString StreamType = TS.Value(Context + "codec_type");

		if (TS.Value(Context + "tags.filename") != "") {
			TS.Update(Context + "codec_type", "attachment");
			StreamType = "attachment";
		}

		if (StreamType == "audio" &&
			TS.Value(Context + "channel_layout") == "unknown") {
			TS.Update(Context + "channel_layout",
					  TS.Value(Context + "channels") + "Channels");
		}

		if (StreamType == "video") {
			if (FirstVideo < 0) FirstVideo = i;
			VideoHeight = TS.Value(Context + "height").toInt();
			VideoWidth = TS.Value(Context + "width").toInt();
			if (VideoWidth > 0 && VideoHeight > 0) {
				ResolutionAspectRatio = (double)VideoWidth / (double)VideoHeight;
				TS.Insert(Context + "ar_resolution", QString::number(ResolutionAspectRatio));
			}
			if (TS.Contains(Context + "display_aspect_ratio")) {
				QString ar1 = TS.Value(Context + "display_aspect_ratio").section(':', 0, 0);
				QString ar2 = TS.Value(Context + "display_aspect_ratio").section(':', 1, 1);
				DisplayAspectRatio = ar1.toDouble() / ar2.toDouble();
			} else {
				DisplayAspectRatio = ResolutionAspectRatio;
			}
			TS.Insert(Context + "ar_display", QString::number(DisplayAspectRatio));
			if (DisplayAspectRatio != ResolutionAspectRatio)
				PrintInfo("\tDiferent display (" + QString::number(DisplayAspectRatio) +
						  ") and resolution (" + QString::number(ResolutionAspectRatio) + ") aspect ratio");
			VerticalVideo = (VideoHeight > VideoWidth);
			if (VerticalVideo) PrintInfo("\tVertical video detected");
			TS.Insert(Context + "vertical", QString::number(VerticalVideo));
			if (FirstVideo == i) TS.Insert("format.vertical", QString::number(VerticalVideo));

			QString ColorTransfer = TS.Value(Context + "color_transfer", "");
			if (ColorTransfer == "arib-std-b67" || ColorTransfer == "smpte2084") {
				TS.Insert(Context + "HDR", "1");
				if (FirstVideo == i) TS.Insert("format.HDR", "1");
			} else {
				TS.Insert(Context + "SDR", "1");
				if (FirstVideo == i) TS.Insert("format.SDR", "1");
			}
			VideoCount++;
		} else if (StreamType == "audio") {
			if (FirstAudio < 0) FirstAudio = i;
			AudioCount++;
		} else if (StreamType == "subtitle") {
			SubCount++;
		} else if (StreamType == "attachment") {
			AttachCount++;
		}
	}

	if (VideoCount > 0) TS.Insert("streams.video.count", QString::number(VideoCount));
	if (AudioCount > 0) TS.Insert("streams.audio.count", QString::number(AudioCount));
	if (SubCount > 0) TS.Insert("streams.subtitles.count", QString::number(SubCount));
	if (AttachCount > 0) TS.Insert("streams.attachment.count", QString::number(AttachCount));
	if (VideoCount > 1 || AudioCount > 1 || SubCount > 1 ||
		AttachCount > 1) TS.Insert("streams.multiple_same_type", "1");

	if (TS.Value("chapters.count", "0") == "0") TS.Remove("chapters.count");

	if (PS.NotEmpty("mediainfo") && PS.Bool("enable_mediainfo")) {
		QString mediainfo_json = ReadProgram(PS.Value("mediainfo"), {"-f", "--Output=JSON", VIDEOFILE});
		if (mediainfo_json != "") {
			QJsonDocument MediainfoMetadata = QJsonDocument::fromJson(mediainfo_json.toUtf8(), &JsonError);
			if (MediainfoMetadata.isObject()) {
				QJsonObject rootobj = MediainfoMetadata.object();
				QJsonObject mediaobj = rootobj.value("media").toObject();
				QJsonArray tracksobj = mediaobj.value("track").toArray();
				for (int i = 0; i < tracksobj.count(); ++i) {
					QJsonObject track = tracksobj.at(i).toObject();
					if (track.value("@type").toString() == "General") {
						TS.LoadVars(track, "format.mediainfo");
						continue;
					}
					if (!track.value("StreamOrder").isUndefined())
						TS.LoadVars(track, "streams." + track.value("StreamOrder").toString() + ".mediainfo");
				}
			}
		}
	}

	if (FirstVideo > -1) TS.CopyVars("streams." + QString::number(FirstVideo), "streams.video.first");
	if (FirstAudio > -1) TS.CopyVars("streams." + QString::number(FirstAudio), "streams.audio.first");

	VRAutodetection(&PS, &TS, VIDEOFILENAME, ResolutionAspectRatio, VideoWidth, VideoHeight);

	QString DURATION = TS.Value("format.duration");
	DEBUGVAR(DURATION);
	int64_t Duration = DURATION.toFloat() * 1000;
	DEBUGVAR(Duration);
	//TODO: Try remove this only usefun to info message
	PrintInfo("\tVideo file duration: " + QTime::fromMSecsSinceStartOfDay(Duration).toString("hh:mm:ss"));

	if (PS.Bool("sub_show_embedded")) {
		int Match = -1;
		int ForcedMatch = -1;
		for (int i = 0; i < TS.Value("streams.count").toInt(); i++) {

			QString CONTEXT = "streams." + QString::number(i) + ".";
			if (TS.Value(CONTEXT + "codec_type") != "subtitle") continue;
			if (PS.Int("sub_emb_select") == 0) {
				if (TS.Value(CONTEXT + "disposition.forced") == "1") {
					Match = i;
					break;
				}
			} else if (PS.Int("sub_emb_select") == 1) {
				if (TS.Value(CONTEXT + "disposition.forced") == "1") {
					if (!EqualLang(TS.Value(CONTEXT + "tags.language"), PS.Value("sub_emb_lang"))) continue;
					Match = i;
					break;
				}
			} else if (PS.Int("sub_emb_select") == 2) {
				if (TS.Value(CONTEXT + "disposition.forced") == "0") {
					Match = i;
					break;
				}
			} else if (PS.Int("sub_emb_select") == 3) {
				if (TS.Value(CONTEXT + "disposition.forced") == "0") {
					if (!EqualLang(TS.Value(CONTEXT + "tags.language"), PS.Value("sub_emb_lang"))) continue;
					Match = i;
					break;
				}
			} else if (PS.Int("sub_emb_select") == 4) {
				if (TS.Value(CONTEXT + "disposition.forced") == "1") {
					ForcedMatch = i;
					Match = i;
				} else {
					if (ForcedMatch > -1) Match = ForcedMatch;
					else if (Match == -1) Match = i;
				}
			} else if (PS.Int("sub_emb_select") == 5) {
				if (!EqualLang(TS.Value(CONTEXT + "tags.language"), PS.Value("sub_emb_lang"))) continue;
				if (TS.Value(CONTEXT + "disposition.forced") == "1") {
					ForcedMatch = i;
					Match = i;
				} else {
					if (ForcedMatch > -1) Match = ForcedMatch;
					else if (Match == -1) Match = i;
				}
			} else  {
				break;
			}
		}
		if (Match > -1) {
			SubStream = Match;
			PrintInfo("\tUsing embebed subtitle track " + QString::number(SubStream));
		}
	}

	if (SubStream > -1 && TS.Value("streams." + QString::number(SubStream) + ".codec_type") == "subtitle") {
		QString SubCodec = TS.Value("streams." + QString::number(SubStream) + ".codec_name");
		QString SubOutExt = "srt";
		SubFormat = sub_srt;
		bool SubCopy = false;
		if (SubCodec == "subrip") SubCopy = true;
		if (SubCodec == "ass") {
			SubOutExt = "ass";
			SubCopy = true;
			SubFormat = sub_ass;
		}
		TMP_SUBSTREAM = TMP + QDir::separator() + "substream." + SubOutExt;

		QStringList SubStreamParams;
		SubStreamParams << "-y" << "-i" << VIDEOFILE << "-map" << "0:" + QString::number(SubStream);
		if (SubCopy) SubStreamParams << "-c" << "copy";
		SubStreamParams << TMP_SUBSTREAM;
		PrintInfo("\tExtracting subtitle stream (can be slow): " + TMP_SUBSTREAM + ": ");
		if (ffmpeg(SubStreamParams)) {
			PrintInfo("\tOK");
			TS.Insert("streams." + QString::number(SubStream) + ".subtitle_visible", "1");
			SubtitleEnable = true;
			ShowSubText = false;
			SUBFILE = TMP_SUBSTREAM;
		} else {
			PrintInfo("\tERROR");
			TMP_SUBSTREAM = "";
		}
	}

	PrintInfo("");

	if (SubtitleEnable) {
		QStringList SubtitleContents;
		QFile SubtitleFile(SUBFILE);

		if (!SubtitleFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
			PrintError("Error reading subtitle file");
			TERMINATE(4);
		}

		while (!SubtitleFile.atEnd()) {
			QByteArray line = SubtitleFile.readLine();
			SubtitleContents.append(line);
		}

		PrintInfo("Processing subtitle file: " + SUBFILEMAME + "\n");

		int FrameDuration = (1000 / TS.HumanFrameRate(TS.Value("streams.video.first.r_frame_rate"))) + 1;
		int SubDelay = PS.Value("sub_delay").toInt();
		if (SubDelay < 0) SubDelay = FrameDuration;

		Timestamps = ProcessSubtitles(SubFormat, SubtitleContents, Totals, SubDelay);

		TS.Insert("subtitle.filename", SUBFILEMAME);
		TS.Insert("subtitle.extension", SUBEXT);
		TS.Insert("subtitle.format", SUBTITLE_FORMAT);
		TS.Insert("subtitle.size", QString::number(SubtitleFileSize));
		TS.Insert("subtitle.language", SUBLANG);
		TS.Insert("subtitle.langcode", SUBLANGCODE);
		TS.Insert("subtitle.lines", QString::number(Totals.Sub));
		TS.Insert("subtitle.words", QString::number(Totals.Words));
		TS.Insert("subtitle.empty_lines", QString::number(Totals.EmptyLines));
		TS.Insert("subtitle.total_length", QString::number(Totals.TextLen));
		TS.Insert("subtitle.avg_lenght", QString::number(Totals.TextLen / Totals.Sub));
		TS.Insert("subtitle.total_duration", QString::number((double)Totals.Len / 1000));
		TS.Insert("subtitle.avg_duration_ms", QString::number(Totals.Len / Totals.Sub));

		TOTALLENFORMATED = QTime::fromMSecsSinceStartOfDay(Totals.Len).toString("hh:mm:ss");

		PrintInfo("Subtitle file statics: ");
		PrintInfo("\tTotals");
		PrintInfo("\t\tDuration: " + TOTALLENFORMATED);
		PrintInfo("\t\tTextLengt: " + QString::number(Totals.TextLen) + " Characters");
		PrintInfo("\t\tWords: " + QString::number(Totals.Words));
		PrintInfo("\t\tLines: " + QString::number(Totals.Sub));
		PrintInfo("\t\tEmpty Lines: " + QString::number(Totals.EmptyLines));
		PrintInfo("\tAverages");
		PrintInfo("\t\tDuration: " + QString::number(Totals.Len / Totals.Sub) + "ms");
		PrintInfo("\t\tTextLengt: " + QString::number(Totals.TextLen / Totals.Sub));
		PrintInfo("\t\tWords: " + QString::number(Totals.Words / Totals.Sub));
		PrintInfo("");
	}

	ProcessFunctions(&PS, &TS);

	if (PS.NotEmpty("vr_config") && TS.Value("vr.file", "0").toInt() != 0) {
		PrintInfo("Loading VR config (" + PS.Value("vr_config") + ")");
		PS.LoadConfig(PS.Value("vr_config"));
		if (Save || SaveAll) {
			PrintInfo("\tDisabling --save and --save_all because are incompatible with --vr_config");
			Save = false;
			SaveAll = false;
		}
	} else if (PS.NotEmpty("vertical_config") && TS.Value("format.vertical", "0").toInt() != 0) {
		PrintInfo("Loading Vertical config (" + PS.Value("vertical_config") + ")");
		PS.LoadConfig(PS.Value("vertical_config"));
		if (Save || SaveAll) {
			PrintInfo("\tDisabling --save and --save_all because are incompatible with --vertical_config");
			Save = false;
			SaveAll = false;
		}
	}

	if (PS.NotEmpty("add_config")) {
		PrintInfo("Loading additional config (" + PS.Value("add_config") + ")");
		PS.LoadConfig(PS.Value("add_config"));
		if (Save || SaveAll) {
			PrintInfo("\tDisabling --save and --save_all because are incompatible with --add_config");
			Save = false;
			SaveAll = false;
		}
	}

	VR = PS.Bool("vr");
	VR_IN = PS.Value("vr_in");
	VR_IN_STEREO = PS.Value("vr_in_stereo");
	VR_IH_FOV = PS.Value("vr_ih_fov").toInt();
	VR_IV_FOV = PS.Value("vr_iv_fov").toInt();

	if (PS.Bool("vr_auto_enable")) {
		if (TS.Value("vr.file") == "1") {
			PrintInfo("\nVR Video detected, Format: " + TS.Value("vr.format") + ", Mode: " + TS.Value("vr.mode") + ", FOV:" +
					  TS.Value("vr.fov") + "\n");
			VR = true;
			VR_IN = TS.Value("vr.format");
			VR_IN_STEREO = TS.Value("vr.mode");
			VR_IH_FOV = TS.Value("vr.fov").toInt();
			VR_IV_FOV = TS.Value("vr.fov").toInt();
			if (VR_IH_FOV == 360) VR_IV_FOV = 180;
			if (VR_IN == "equirect" && VR_IH_FOV == 180)
				VR_IN = "hequirect";
		}
	}

	QString OUTPUT_DIR = QDir::toNativeSeparators(PS.Value("out_dir"));
	if (OUTPUT_DIR == "" && PS.Value("out_relative_dir") != "") {
		QString Relative = PS.Value("out_relative_dir");
		QString Parent;
		if (SubtitleEnable && SubStream == -1) {
			Parent = SUBDIR;
		} else {
			Parent = TS.Value("file.dir");
		}
		OUTPUT_DIR = QDir::toNativeSeparators(Parent + QDir::separator() + Relative);
		QDir ParentDir(Parent);
		if (!ParentDir.exists(Relative)) ParentDir.mkdir(Relative);
	}

	FORMAT_OPTIONS = PS.GetOutputFormat(OUTPUT_FORMAT, FORMAT_AUDIO_OPTIONS, INDEPENDENT_COMMAND, OUTPUT_QUALITY);
	DEBUGVAR(OUTPUT_FORMAT);

	QString OUTPUTNAME = "";
	if (OUTPUTFILE == "") {
		OUTPUTNAME = TS.ProcessCMD("", PS.Value("out_name"));
		if (OUTPUT_DIR == "") {
			if (SubtitleEnable && SubStream == -1) {
				OUTPUT_DIR = SUBDIR;
			} else {
				OUTPUT_DIR = TS.Value("file.dir");
			}
		}
		OUTPUTFILE = OUTPUT_DIR + QDir::separator() + PS.Value("prefix") + OUTPUTNAME + PS.Value("suffix") + "." +
					 OUTPUT_FORMAT;
	}

	OUTPUTFILE = QDir::toNativeSeparators(OUTPUTFILE);
	DEBUGVAR(OUTPUTFILE);
	TS.Insert("out.file", OUTPUTFILE);
	TS.Insert("out.basename", PS.Value("prefix") + OUTPUTNAME + PS.Value("suffix"));
	TS.Insert("out.extension", OUTPUT_FORMAT);
	TS.Insert("out.dir", OUTPUT_DIR);
	TS.Insert("out.absolute", QDir::toNativeSeparators(OUTPUT_DIR + QDir::separator() + PS.Value("prefix") + OUTPUTNAME +
			  PS.Value("suffix") + "." + OUTPUT_FORMAT));

	if (PrintVars || Debug) {
		TS.PrintAll();
		if (PrintVars) {
			TERMINATE(0);
		}
	}

	TS.SetMediaInfo(PS.Value("mediainfo"));

	if (PS.NotEmpty("aspect_ratio")) {
		if (PS.Value("aspect_ratio") == "display") {
			OutputAspectRatio = DisplayAspectRatio;
		} else if (PS.Value("aspect_ratio") == "resolution") {
			OutputAspectRatio = ResolutionAspectRatio;
		} else {
			int ar1 = PS.Value("aspect_ratio").section(':', 0, 0).toInt();
			int ar2 = PS.Value("aspect_ratio").section(':', 1, 1).toInt();
			OutputAspectRatio = (double)ar1 / (double)ar2;
		}
	} else {
		OutputAspectRatio = DisplayAspectRatio;
	}

	SizeString = PS.Value("size");
	if (SizeString == "0") SizeString = "";
	if (SizeString != "") {
		Size = PS.Int("size");
		int MinSize	= PS.Int("min_size");

		PrintDebug("VideoWidth: " + QString::number(VideoWidth));
		PrintDebug("VideoHeight: " + QString::number(VideoHeight));

		if (VideoWidth > 0 && VideoHeight > 0) {
			if (VerticalVideo) {
				NewVideoHeight = Size;
				NewVideoWidth = qRound((Size / 2) * OutputAspectRatio) * 2;
				if (NewVideoWidth < MinSize) {
					NewVideoWidth = MinSize;
					NewVideoHeight = qRound((MinSize / 2) / OutputAspectRatio) * 2;
					SizeString = QString::number(NewVideoHeight);
					DEBUGVAR(SizeString);
				}
			} else {
				NewVideoWidth = Size;
				NewVideoHeight = qRound((Size / 2) / OutputAspectRatio) * 2;
				if (NewVideoHeight < MinSize) {
					NewVideoHeight = MinSize;
					NewVideoWidth = qRound((MinSize / 2) * OutputAspectRatio) * 2;
					SizeString = QString::number(NewVideoWidth);
					DEBUGVAR(SizeString);
				}
			}

			PrintDebug("NewVideoWidth: " + QString::number(NewVideoWidth));
			PrintDebug("NewVideoHeight: " + QString::number(NewVideoHeight));
		}
	} else {
		NewVideoWidth = VideoWidth;
		NewVideoHeight = VideoHeight;
	}

	LAYOUT = PS.Value("layout").toLower();
	NumberOfFrames = LayoutSplit(LAYOUT, LayoutX, LayoutY);

	modes Mode(PS.Value("mode"), SubtitleEnable, &PS, &TS);

	bool NoGrid = PS.Bool("no_grid");
	bool Concat = PS.Bool("concat");
	if (Concat) NoGrid = true;

	ContactSheetTextBlock CTB(&PS, &TS);
	if (NoGrid || VideoPreview > 0) CTB.TextBlockPosition = ContactSheetTextBlock::text_hidden;

	if (GUIProgress) {
		CurrentStep = 0;
		Steps = NumberOfFrames + 1;
		if (!(NoGrid && !Concat)) Steps++;
		if (!CTB.IsHidden()) Steps += 2;
		if (PS.NotEmpty("bg_image") || PS.Value("bg_gradient") != "none") Steps++;
		if (PS.Value("format") == "giflossy") Steps++;
		if (PS.Value("exec") != "") Steps = 1;
		if (PS.Value("exec_before") != "") Steps++;
		if (PS.Value("exec_after") != "") Steps++;
		PrintProgress(1, Steps);
	}

	QString TB_TITLE = PS.Value("tb_title");

	if (PS.Bool("rename") && PS.NotEmpty("rename_dest")) {
		QString Dest = QDir::toNativeSeparators(TS.ProcessCMD("", PS.Value("rename_dest")));
		QFileInfo DestInfo(Dest);
		QDir Dir(DestInfo.path());
		if (DestInfo.path() != "" && !Dir.exists()) {
			if (PS.Bool("rename_mkdir")) {
				PrintInfo("Rename creating path: \"" + DestInfo.path() + "\"");
				if (QDir().mkpath(DestInfo.path())) {
					PrintInfo("Done");
				} else {
					PrintError("");
					TERMINATE(20);
				}
			} else {
				PrintError("Parent output directory not exist");
				TERMINATE(21);
			}
		}
		if (!PS.Bool("rename_overwrite") && QFile::exists(Dest)) {
			PrintError("The destination file \"" + Dest + "\" anready exists");
			TERMINATE(22);
		}

		bool Result = false;
		if (PS.Value("rename_type") == "move") {
			PrintInfo("Moving file \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
			Result = QFile::rename(VIDEOFILE, Dest);
		} else if (PS.Value("rename_type") == "copy") {
			PrintInfo("Copying file \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
			Result = QFile::copy(VIDEOFILE, Dest);
		} else if (PS.Value("rename_type") == "symlink") {
			PrintInfo("Creating a symlink from \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
			Result = CreateSymlink(VIDEOFILE, Dest);
		} else if (PS.Value("rename_type") == "hardlink") {
			PrintInfo("Creating a hardlink from \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
			Result = CreateHardlink(VIDEOFILE, Dest);
		} else if (PS.Value("rename_type") == "shortcut") {
			PrintInfo("Creating a shortcut from \"" + VIDEOFILE + "\" to \"" + Dest + ".lnk\"");
			Result = CreateShortcutU(VIDEOFILE, Dest, PS.Bool("rename_overwrite"));
		} else if (PS.Value("rename_type") == "link") {
			PrintInfo("Attempt to create a hardlink from \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
			Result = CreateHardlink(VIDEOFILE, Dest);
			if (!Result) {
				#ifdef Q_OS_WIN
				PrintInfo("Attempt to create a symlink from \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
				#else
				PrintInfo("Creating a symlink from \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
				#endif
				Result = CreateSymlink(VIDEOFILE, Dest);
			}
			#ifdef Q_OS_WIN
			if (!Result) {
				PrintInfo("Creating a shortcut from \"" + VIDEOFILE + "\" to \"" + Dest + ".lnk\"");
				Result = CreateShortcutU(VIDEOFILE, Dest, PS.Bool("rename_overwrite"));
			}
			#endif
		} else if (PS.Value("rename_type") == "link") {
			PrintInfo("Attempt to create a hardlink from \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
			Result = CreateHardlink(VIDEOFILE, Dest);
			if (!Result) {
				PrintInfo("Copying file \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
				Result = QFile::copy(VIDEOFILE, Dest);
			}
		} else {
			PrintInfo("Rename simulation: \"" + VIDEOFILE + "\" to \"" + Dest + "\"");
			Result = true;
		}

		PrintProgress(1, 1);

		if (Result) {
			PrintInfo("Done");
			TERMINATE(0);
		} else {
			PrintError("");
			TERMINATE(23);
		}
	}

	if (PS.NotEmpty("template") && QFile::exists(PS.Value("template"))) {
		QString Outfile = QDir::toNativeSeparators(OUTPUT_DIR + QDir::separator() + OUTPUTNAME + "." +
						  PS.Value("template_ext"));
		PrintInfo("Generating template from " + PS.Value("template") + " to " + Outfile);
		QString Content = TS.ProcessTemplate(PS.Value("template"));
		WriteFile(Outfile, Content);
		PrintProgress(1, 1);
		TERMINATE(0);
	}

	if (ExecuteExecOption("exec_before", &PS, &TS))
		AddProgress();

	if (ExecuteExecOption("exec", &PS, &TS)) {
		AddProgress();
		ExecuteExecOption("exec_after", &PS, &TS);
		PrintProgress(1, 1);
		TERMINATE(0);
	}

	if (!NoGrid || Concat) {
		PrintInfo("Using output file: " + OUTPUTFILE);
		PrintInfo("\tOutput format: " + OUTPUT_FORMAT);
		PrintInfo("");
	}

	if (PS.Bool("no_overwrite") && (!NoGrid || Concat)) {
		if (QFile::exists(OUTPUTFILE)) {
			PrintInfo("No overwriting file: " + OUTPUTFILE);
			ExecuteGifsicle(&PS, &TS, OUTPUT_QUALITY);
			ExecuteExecOption("exec_after", &PS, &TS);
			PrintProgress(1, 1);
			TERMINATE(0);
		}
	}

	Mode.SetTimestamps(&Timestamps);
	Screenshots = Mode.GenerateScreenshots();

	MFS = TS.DetectAndEjecuteFunction(PS.Value("mfs"), "");
	if (MFS != "") {
		QString Separator = TextSustitution::Text(PS.Value("mfs_separator"));
		QStringList SplitMFS = MFS.split(Separator);
		for (int i = 0; i < SplitMFS.count(); ++i) {
			QString MF = SplitMFS.at(i);
			if (MF == "") continue;
			MANUALFRAMES.append(MF);
		}
	}

	if (!MANUALFRAMES.isEmpty()) {
		int FrameIndex = -1;
		int64_t FrameTime = -1;
		static QRegularExpression FrameNumber("^([0-9]+),([0-9:\\.ms]+)$");
		for (int i = 0; i < MANUALFRAMES.count(); i++) {
			QString ManualFrame = MANUALFRAMES.at(i);
			QRegularExpressionMatch Match = FrameNumber.match(ManualFrame);
			if (Match.hasMatch()) {
				FrameIndex = Match.captured(1).toInt() - 1;
				FrameTime = StringTime2ms(Match.captured(2));
			} else {
				FrameIndex = i;
				FrameTime = StringTime2ms(ManualFrame);
			}
			if (FrameIndex >= Screenshots.count() || FrameTime < 0) {
				PrintError("Invalid option --mf " + ManualFrame);
				continue;
			}
			PrintInfo("\tReplacing selected frame " +
					  QTime::fromMSecsSinceStartOfDay(Screenshots.at(FrameIndex).start).toString("hh:mm:ss.zzz") +
					  " For " + QTime::fromMSecsSinceStartOfDay(FrameTime).toString("hh:mm:ss.zzz"));
			Screenshots.replace(FrameIndex, {(uint64_t)FrameTime, 0, 0, 0, false, false});
			continue;
		}
	}

	PrintInfo("\tOK\n");

	if (PS.Bool("show_mfs")) {
		QStringList SelectFramesList;
		for (int i = 0; i < Screenshots.count(); i++) {
			SelectFramesList.append(QTime::fromMSecsSinceStartOfDay(Screenshots.at(i).start).toString("hh:mm:ss.zzz"));
		}
		PrintInfo("mfs params to select current frames:");
		PrintInfo("\t--mfs " + SelectFramesList.join(","));
		PrintInfo("\tOK\n");
	}

	Filters F(&PS, &TS);

	if (PS.Int("video_fps") > 0 && VideoPreview > 0) {
		SCREENSHOT_FILTERS.append(F.FPS());
	}

	if (Mode.RepFramesEnabled()) {
		SCREENSHOT_FILTERS.append(F.Thunbnail());
	}

	if (VR) {
		QString Eye = F.EyeCrop(VR_IN_STEREO);
		if (Eye != "") {
			SCREENSHOT_FILTERS.append(Eye);
			if (VR_IN_STEREO == "sbs") {
				VideoWidth = VideoWidth / 2;
				VRChangeAR = true;
				VR_IN_STEREO = "2d";
			} else if (VR_IN_STEREO == "tb") {
				VR_IN_STEREO = "2d";
				VideoHeight = VideoHeight / 2;
				VRChangeAR = true;
			}
		}

		if (!PS.Bool("vr_only_crop")) {
			QString VR_H = PS.Value("vr_h");
			QString VR_W = PS.Value("vr_w");
			if (VR_H == "") VR_H = SizeString;
			if (VR_H == "") VR_H = "800";
			if (VR_W == "") VR_W = SizeString;
			if (VR_W == "") VR_W = "800";

			VideoWidth = VR_W.toInt();
			VideoHeight = VR_H.toInt();
			VRChangeAR = true;

			SCREENSHOT_FILTERS.append(F.VR(VR_IN_STEREO, VR_W, VR_H, VR_IH_FOV, VR_IV_FOV));
		}
	}

	SCREENSHOT_FILTERS.append("format=yuva444p");

	if (VRChangeAR) {
		ResolutionAspectRatio = (double)VideoWidth / (double)VideoHeight;
		//Once processed VR display aspect ratio is incorrect
		if (PS.Value("aspect_ratio") == "resolution" || PS.Value("aspect_ratio") == "display")
			OutputAspectRatio = ResolutionAspectRatio;
		NewVideoHeight = SizeString.toInt() * ResolutionAspectRatio;
	}

	if (OutputAspectRatio != ResolutionAspectRatio) {
		int ARCorrectH = VideoWidth / OutputAspectRatio;
		SCREENSHOT_FILTERS.append(F.Scale(VideoWidth, ARCorrectH));
	}

	if (SubtitleEnable) {
		QString TMPSUBTITLE = SUBFILE;
		if (SUBFILE.contains("'")) {
			TMPSUBTITLE = TMP + QDir::separator() + "subtitle." + SUBEXT;
			QFile CopySub(SUBFILE);
			CopySub.copy(TMPSUBTITLE);
		}
		SCREENSHOT_FILTERS.append(F.Subtitles(TMPSUBTITLE, (SubFormat != sub_ass || PS.Bool("sub_force_ass"))));
	}

	if (SizeString != "") {
		int w = SizeString.toInt();
		int h = -2;
		if (VerticalVideo) {
			w = -2;
			h = SizeString.toInt();
		}
		SCREENSHOT_FILTERS.append(F.Scale(w, h));
	}

	int AddedW = 0;
	int AddedH = 0;
	QString Drawtext = "";
	if (!PS.Bool("ft_hide")) {
		Drawtext = F.Drawtext(AddedW, AddedH);
		if (!(PS.NotEmpty("mask") || PS.Int("rounded_corners") > 0) || PS.Value("ft_mask_layer_pos") == "before")
			SCREENSHOT_FILTERS.append(Drawtext);
	}

	if (PS.NotEmpty("custom_capture_filters")) {
		SCREENSHOT_FILTERS.append(PS.Value("custom_capture_filters"));
	}

	QString LOGO_OVERLAY_OPTIONS = "";
	QString LOGO_FILTERS = "";
	if (PS.NotEmpty("logo")) {
		QStringList LogoFiltersList;
		if (PS.NotEmpty("logo_transparent_color")) {
			LogoFiltersList.append(F.RawColorKey(PS.Value("logo_transparent_color"),
												 PS.Int("logo_transparent_similarity"),
												 PS.Int("logo_transparent_blend")));
		}
		if (PS.Int("logo_scale") != 0) {
			int NewLogoWidth = (NewVideoWidth * PS.Int("logo_scale")) / 100;
			LogoFiltersList.append(F.Scale(NewLogoWidth, -1));
		}
		if (PS.Int("logo_blur") != 0)
			LogoFiltersList.append(F.RawBlur(QString::number((double)PS.Int("logo_blur") / 100)));

		if (PS.Int("logo_exposure") != 0)
			LogoFiltersList.append(F.RawExposure(QString::number((double)PS.Int("logo_exposure") / 100)));

		if (PS.Int("logo_saturation") != 100)
			LogoFiltersList.append(F.RawSaturation(QString::number((double)PS.Int("logo_saturation") / 100)));

		if (PS.Int("logo_opacity") != 100)
			LogoFiltersList.append(F.RawOpacity(QString::number((double)PS.Int("logo_opacity") / 100)));

		if (!LogoFiltersList.isEmpty())LOGO_FILTERS = LogoFiltersList.join(",");
		LOGO_OVERLAY_OPTIONS = F.ScreenshostLogoOverlayOptions();
	}

	SCREENSHOT_FILTER = SCREENSHOT_FILTERS.join(',');

	bool Mask = false;
	int MaskW = NewVideoWidth + AddedW;
	int MaskH = NewVideoHeight + AddedH;
	QString MASK_ADITIONAL_FILTER = "";
	if (PS.NotEmpty("mask") || PS.Int("rounded_corners") > 0) {
		Mask = true;
		if (Drawtext != "") {
			if (PS.Value("ft_mask_layer_pos") == "inside") {
				MASK_ADITIONAL_FILTER = Drawtext;
			} else if (PS.Value("ft_mask_layer_pos") == "after") {
				MaskW = NewVideoWidth;
				MaskH = NewVideoHeight;
				MASK_ADITIONAL_FILTER = Drawtext;
			}
		}
		if (MASK_ADITIONAL_FILTER != "" ) MASK_ADITIONAL_FILTER += ",";
		MASK_ADITIONAL_FILTER += "setpts=PTS-STARTPTS";

		if (NoGrid && !Concat && (PS.NotEmpty("bg_image") || PS.Value("bg_gradient") != "none")) {
			CTB.GenerateBG(NewVideoWidth + AddedW, NewVideoHeight + AddedH);
		}
	} else {
		if (SCREENSHOT_FILTER != "") SCREENSHOT_FILTER += ",";
		SCREENSHOT_FILTER += "setpts=PTS-STARTPTS";
	}

	if (PS.NotEmpty("watermark")) {
		CTB.GenerateWatermark(NewVideoWidth + AddedW, NewVideoHeight + AddedH);
	}

	PrintInfo("Generating " + CAPTURE_NAME + "s:");

	for (int i = 0; i < Screenshots.count(); i++) {
		const sub_line_t *ActualScreen = &Screenshots.at(i);

		QStringList sc_params;
		QString Message;
		QStringList dependent_params;
		QString dependent_message;
		QString OutFile;

		sc_params << "-hide_banner" << "-loglevel" << "error" << "-y";

		QString NormalInputParameters = "-ss " + QString::number(ActualScreen->start) + "ms";
		if (ActualScreen->end == 0 && PS.Bool("only_keyframes")) {
			if (Mode.IsVideoEnd() && i == Screenshots.count() - 1)
				NormalInputParameters += " -noaccurate_seek";
			else
				NormalInputParameters += " -skip_frame nokey";
		}
		NormalInputParameters += " -copyts";

		LayeredFilter LF;

		if (NoGrid && !Concat && (PS.NotEmpty("mask") || PS.Int("rounded_corners") > 0) &&
			(PS.NotEmpty("bg_image") || PS.Value("bg_gradient") != "none")) {
			LF.AddInputFile(TMP_BACKGROUND, "bg", "");
		}

		QString InputString;
		if (DVD != "") {
			InputString = NormalInputParameters + " -f dvdvideo -title " + DVD_TITLE + " -i \"" + DVD + "\"";
		} else {
			InputString = NormalInputParameters + " -i \"" + VIDEOFILE + "\"";
		}

		if (Mask) {
			LF.AddInput(InputString, "video", SCREENSHOT_FILTER, LF.merge_alphamerge);
			F.LayeredMask(LF, MaskW, MaskH, "", "," + MASK_ADITIONAL_FILTER);
		} else {
			LF.AddInput(InputString, "video", SCREENSHOT_FILTER);
		}

		if (PS.NotEmpty("logo"))
			LF.AddInputFile(PS.Value("logo"), "logo", LOGO_FILTERS, LF.merge_overlay, LOGO_OVERLAY_OPTIONS);

		if (NoGrid && !Concat && PS.NotEmpty("watermark")) {
			LF.AddInputFile(TMP_WATERMARK, "watermark", "");
		}

		sc_params << LF.GetInputParams();

		if (VideoPreview > 0) {
			sc_params << "-to" << QString::number(VideoPreview) + "ms";
		}

		sc_params << LF.GetComplexFilter() << LF.GetOutputName();

		if (!FORMAT_AUDIO_OPTIONS.isEmpty()) {
			sc_params << "-map" << "0:a:0";
			sc_params << "-af" << "asetpts=PTS-STARTPTS";
		}

		if (VideoPreview == 0) sc_params << "-vframes" << "1";

		QString SuffixCurrentNumber = "";
		QString PrefixCurrentNumber = "";
		if (NoGrid && !Concat) {
			SuffixCurrentNumber = GetIndividalNumber(&TS, i + 1, ActualScreen, PS.Value("suffix_number"));
			PrefixCurrentNumber = GetIndividalNumber(&TS, i + 1, ActualScreen, PS.Value("prefix_number"));
			if (SuffixCurrentNumber == "" && PrefixCurrentNumber == "") SuffixCurrentNumber = QString::number(i + 1);
		}

		OutFile = OUTPUT_DIR + QDir::separator() + PS.Value("prefix") + PrefixCurrentNumber + OUTPUTNAME +
				  PS.Value("suffix") + SuffixCurrentNumber + "." + OUTPUT_FORMAT;

		if (PS.Bool("no_overwrite") && QFile::exists(OutFile)) {
			PrintInfo("\tNo overwriting file: " + OutFile);
			PrintProgress(++CurrentStep, Steps);
			continue;
		};

		if (NoGrid && !Concat && !INDEPENDENT_COMMAND) {
			sc_params << FORMAT_OPTIONS;
			if (!FORMAT_AUDIO_OPTIONS.isEmpty()) sc_params << FORMAT_AUDIO_OPTIONS;
			sc_params << OutFile;
			Message = "\t" + CAPTURE_NAME + " at " + QTime::fromMSecsSinceStartOfDay(ActualScreen->start).toString("hh:mm:ss.zzz") +
					  " File: "
					  + OutFile;
		} else {
			sc_params << TMP_FRAME_FORMAT_OPTIONS;
			if (!FORMAT_AUDIO_OPTIONS.isEmpty()) sc_params << TMP_AUDIO_FORMAT_OPTIONS;
			sc_params << TMP + QDir::separator() + "screenshot" + QString::number(i + 1) + "." + TMP_FRAME_FORMAT;
			Message = "\t" + CAPTURE_NAME + " at " + QTime::fromMSecsSinceStartOfDay(ActualScreen->start).toString("hh:mm:ss.zzz");

			if (NoGrid && !Concat && INDEPENDENT_COMMAND) {
				dependent_params << "-hide_banner" << "-loglevel" << "error" << "-y" <<
								 "-i" << TMP + QDir::separator() + "screenshot" + QString::number(i + 1) + "." + TMP_FRAME_FORMAT;
				dependent_params << FORMAT_OPTIONS;
				if (!FORMAT_AUDIO_OPTIONS.isEmpty()) dependent_params << FORMAT_AUDIO_OPTIONS;
				dependent_params << OutFile;
				dependent_message = "\tFile: " + OutFile;
			}
		}

		Inst.AddJob(sc_params, Message, dependent_params, dependent_message);
	}

	if (!Inst.Run()) {
		TERMINATE(5);
	}

	PrintInfo("\tOK\n");

	if (NoGrid && !Concat) {
		if (ExecuteGifsicle(&PS, &TS, OUTPUT_QUALITY)) AddProgress();
		if (ExecuteExecOption("exec_after", &PS, &TS)) AddProgress();
		TERMINATE(0);
	}

	if (!Concat)
		PrintInfo("Generating grid:");
	else
		PrintInfo("Generating video:");

	uint64_t height;
	GetImageSize(TMP + QDir::separator() + "screenshot1." + TMP_FRAME_FORMAT, Size, height);

	QStringList grid_params;

	bool NeedBG = false;
	bool NeedWatermark = false;

	if (VideoPreview > 0) {
		grid_params << "-hide_banner" << "-loglevel" << "error" << "-y";

		int W, H;
		if (Concat) {
			W = Size;
			H = height;
		} else {
			F.XstackSizeCalc(W, H, height);
		}

		LayeredFilter LF;

		if (PS.NotEmpty("bg_image") || PS.Value("bg_gradient") != "none") {
			CTB.GenerateBG(W, H);
			LF.AddInputFile(TMP_BACKGROUND, "bg", "");
		}

		if (Concat) {
			QStringList ConcatContents;
			ConcatContents.append("ffconcat version 1.0");
			for (uint i = 0; i < NumberOfFrames; ++i) {
				QString File = TMP + QDir::separator() + "screenshot" + QString::number(i + 1) + "." + TMP_FRAME_FORMAT;
				QString FileScaped = QDir::toNativeSeparators(File);
				if (FileScaped.contains("'")) FileScaped.replace("'", "'\\''");
				ConcatContents.append("file '" + FileScaped + "'");
				ConcatContents.append("duration " + QString::number((double)VideoPreview / 1000, 'f', 6) + "");
			}

			QString ConcatFile = QDir::toNativeSeparators(TMP + QDir::separator() + "Concat.txt");
			WriteFile(ConcatFile, ConcatContents.join("\n"));

			LF.AddInput("-f concat -safe 0 -i \"" + ConcatFile + "\"", "concat", "");
		} else {
			F.Xstack(LF, height, CTB.GridBorderColor);
		}

		if (PS.NotEmpty("watermark")) {
			CTB.GenerateWatermark(W, H);
			LF.AddInputFile(TMP_WATERMARK, "watermark", "");
		}

		grid_params << LF.GetAll();

		if (CTB.IsHidden() && OUTPUT_FORMAT != "gif") {
			grid_params << FORMAT_OPTIONS;
			if (!FORMAT_AUDIO_OPTIONS.isEmpty() && Concat) grid_params << FORMAT_AUDIO_OPTIONS;
			grid_params << OUTPUTFILE;
		} else {
			grid_params << TMP_FRAME_FORMAT_OPTIONS;
			if (!FORMAT_AUDIO_OPTIONS.isEmpty() && Concat) grid_params << TMP_AUDIO_FORMAT_OPTIONS;
			grid_params << TMP_IMAGES + "." + TMP_FRAME_FORMAT;
			if (INDEPENDENT_COMMAND) {
				if (!ffmpeg(grid_params)) return 6;
				grid_params.clear();
				grid_params << "-hide_banner" << "-loglevel" << "error" << "-y" << "-i" << TMP_IMAGES + "." + TMP_FRAME_FORMAT <<
							FORMAT_OPTIONS << OUTPUTFILE;
			}
		}

	} else if (Concat) {
		grid_params << "-hide_banner" << "-loglevel" << "error" << "-y";

		LayeredFilter LF;

		if (PS.NotEmpty("bg_image") || PS.Value("bg_gradient") != "none") {
			CTB.GenerateBG(Size, height);
			LF.AddInput(" -r " + PS.Value("video_fps") + " -i \"" + TMP_BACKGROUND + "\"", "bg", "");
		}

		LF.AddInput(" -r " + PS.Value("video_fps") + " -i \"" + TMP + QDir::separator() + "screenshot%d." + TMP_FRAME_FORMAT +
					"\"", "main", "");

		if (PS.NotEmpty("watermark")) {
			CTB.GenerateWatermark(Size, height);
			LF.AddInput(" -r " + PS.Value("video_fps") + " -i \"" + TMP_WATERMARK + "\"", "watermark", "");
		}

		grid_params << LF.GetAll();

		if (INDEPENDENT_COMMAND) {
			grid_params << TMP_VIDEO_FORMAT_OPTIONS << TMP_IMAGES + ".mkv";
			if (!ffmpeg(grid_params)) return 6;

			grid_params.clear();
			grid_params << "-hide_banner" << "-loglevel" << "error" << "-y" << "-i" << TMP_IMAGES + ".mkv";
		}

		grid_params << FORMAT_OPTIONS;
		if (!FORMAT_AUDIO_OPTIONS.isEmpty()) grid_params << FORMAT_AUDIO_OPTIONS;
		grid_params << OUTPUTFILE;

	} else {
		grid_params << "-hide_banner" << "-loglevel" << "error" << "-y";
		QString GRID_FILTER = F.Xstack(grid_params, height, CTB.GridBorderColor);
		grid_params << "-filter_complex" << GRID_FILTER << "-vframes" << "1";

		if (CTB.IsHidden() && !(PS.NotEmpty("bg_image") || PS.Value("bg_gradient") != "none")) {
			if (PS.NotEmpty("watermark")) {
				grid_params << TMP_FRAME_FORMAT_OPTIONS;
				grid_params << TMP_WITHOUT_WATERMARK;
				NeedWatermark = true;
			} else {
				grid_params << FORMAT_OPTIONS;
				grid_params << OUTPUTFILE;
			}
		} else {
			grid_params << TMP_FRAME_FORMAT_OPTIONS;
			grid_params << TMP_IMAGES + "." + TMP_FRAME_FORMAT;
			if (PS.NotEmpty("bg_image") || PS.Value("bg_gradient") != "none") NeedBG = true;
			if (PS.NotEmpty("watermark")) NeedWatermark = true;
		}
	}

	if (!ffmpeg(grid_params)) return 6;
	PrintInfo("\tOk\n");
	AddProgress();

	QString NextStepInput = "";

	if (!CTB.IsHidden()) {
		PrintInfo("Generating text block:");
		CTB.SetTitle(TB_TITLE);
		if (!CTB.GenerateVideoText(ShowSubText, VR)) {
			TERMINATE(5);
		}
		PrintInfo("\tOk\n");
		AddProgress();

		if (!CTB.JoinAll(OUTPUTFILE, FORMAT_OPTIONS, NextStepInput)) {
			TERMINATE(5);
		}
		AddProgress();
	}

	if (NeedBG) {
		AddProgress();
		CTB.GenerateBG();
		if (NeedWatermark) {
			if (!CTB.ApplyBG(NextStepInput, TMP_WITHOUT_WATERMARK, TMP_FRAME_FORMAT_OPTIONS, NextStepInput)) {
				TERMINATE(5);
			}
		} else {
			if (!CTB.ApplyBG(NextStepInput, OUTPUTFILE, FORMAT_OPTIONS, NextStepInput)) {
				TERMINATE(5);
			}
		}
	}

	if (NeedWatermark) {
		if (!CTB.ApplyWatermark(NextStepInput, OUTPUTFILE, FORMAT_OPTIONS)) {
			TERMINATE(5);
		}
	}

	if (ExecuteGifsicle(&PS, &TS, OUTPUT_QUALITY)) AddProgress();
	if (ExecuteExecOption("exec_after", &PS, &TS)) AddProgress();

	TERMINATE(0);
}
